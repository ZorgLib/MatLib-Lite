# MatLib
MatLib (Materials Library) is a library of constitutive models for non-linear, 
dissipative, coupled behaviours. Although it has been designed in the framework 
of the ZorgLib library, it provides a general, universal interface designed to
be used from arbitrary simulation software.

## Pre-requisites
C++ compiler, Python2/3, swig, CMake