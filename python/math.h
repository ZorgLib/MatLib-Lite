/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
%{
#include <math/MathUtils.h>
#include <math/TensorAlgebra.h>
  
#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif
%}


/*****************************
   Tensor algebra operations
 *****************************/

/**
 * Class encapsulating 1D vector objects.
 * It inherits methods and internal structure from ShortArray.
 */
class Vector1D : virtual public ShortArray {
  
 public:
  
  static const unsigned int MEMSIZE=1;
  
  // default constructor
  Vector1D();
  
  // constructor (also serves as copy constructor)
  Vector1D(const ShortArray&,unsigned int = 0);
  Vector1D(const ShortArrayBase&,unsigned int = 0);
  
  // assignment operator
  %ignore operator=;
  Vector1D& operator=(const Vector1D&);
  Vector1D& operator=(const ShortArrayBase&);
  Vector1D& operator=(z_real_t);
  
  // inner product operator
  z_real_t operator*(const Vector1D&) const;
};

/**
 * Class encapsulating 2D vector objects.
 * It inherits methods and internal structure from ShortArray.
 */
class Vector2D : virtual public ShortArray {
  
 public:
  
  static const unsigned int MEMSIZE=2;
  
  // default constructor
  Vector2D();
  
  // constructor (also serves as copy constructor)
  Vector2D(const ShortArray&,unsigned int = 0);
  Vector2D(const ShortArrayBase&,unsigned int = 0);
  
  // assignment operator
  %ignore operator=;
  Vector2D& operator=(const Vector2D&);
  Vector2D& operator=(const ShortArrayBase&);
  Vector2D& operator=(z_real_t);
  
  // inner product operator
  z_real_t operator*(const Vector2D&) const;
};

/**
 * Class encapsulating 3D vector objects.
 * It inherits methods and internal structure from ShortArray.
 */
class Vector3D : virtual public ShortArray {
  
 public:
  
  static const unsigned int MEMSIZE=3;
  
  // default constructor
  Vector3D();
  
  // constructor (also serves as copy constructor)
  Vector3D(const ShortArray&,unsigned int = 0);
  Vector3D(const ShortArrayBase&,unsigned int = 0);
  
  // assignment operator
  %ignore operator=;
  Vector3D& operator=(const Vector3D&);
  Vector3D& operator=(const ShortArrayBase&);
  Vector3D& operator=(z_real_t);
  
  // inner product operator
  z_real_t operator*(const Vector3D&) const;
};

Vector3D crossProd(const Vector3D&,const Vector3D&);

/****************************
   Basic algebra operations
 ****************************/

// default precision for matrix inversion
static const z_real_t DEFAULT_MATH_PRECISION = 1.0e-16;

// vector functions
void addvec(const z_real_t[],const z_real_t[],z_real_t[],unsigned int);
void mulvec(z_real_t,const z_real_t[],z_real_t[],unsigned int);
z_real_t nrmvec0(const z_real_t[],unsigned int);
z_real_t nrmvec1(const z_real_t[],unsigned int);
z_real_t nrmvec2(const z_real_t[],unsigned int);

// 2x2 matrix functions
void mulmat2(const z_real_t *,const z_real_t *,z_real_t *);
z_real_t detmat2(const z_real_t *);
z_real_t invmat2(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t tnvmat2(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t detsym2(const z_real_t *);
z_real_t invsym2(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);

// 3x3 matrix functions
void mulmat3(const z_real_t *,const z_real_t *,z_real_t *);
z_real_t detmat3(const z_real_t *);
z_real_t invmat3(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t tnvmat3(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t detsym3(const z_real_t *);
z_real_t invsym3(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
