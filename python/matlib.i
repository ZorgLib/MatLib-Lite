/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
%module matlib

%include "std_pair.i"
%include "std_string.i"
%include "std_vector.i"

// config
%include <matlib_macros.h>

// instantiate templates
namespace std {
  %template(RealPair) pair<z_real_t,z_real_t>;
}

%include "data.h"
%include "math.h" /* API partially exported */
%include "matl.h"
//%include "opti.h"

%pythoncode %{
  def MatLibArray(*args): return ShortArray(*args)
  def MatLibMatrix(*args): return ShortSqrMatrix(*args)
%}

