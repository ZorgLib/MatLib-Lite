/*
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2023, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
%{
#include <data/Chronometer.h>
#include <data/Copiable.h>
#include <data/Property.h>
#include <data/Rotation2D.h>
#include <data/Rotation3D.h>
#include <data/ShortSqrMatrix.h>
#include <data/ConstantFunction.h>
#include <data/RampFunction.h>
#include <data/TabulatedFunction2.h>
  
#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif
%}


/***************
   Chronometer
 ***************/

/**
 * Class for CPU chronometer.
 */
class Chronometer {

 public:
  
  // time type
  typedef unsigned long long time_t;

  // constructor
  Chronometer();

  // copy constructor
  Chronometer(const Chronometer&);

  // destructor
  ~Chronometer() {}

  // start chrono
  void start();

  // stop chrono
  void stop();

  // reset chrono
  void reset();

  // elapsed time
  time_t elapsed();

  // format elapsed time
  static std::string toString(time_t);
};

#ifdef _OPENMP
/**
 * Class for OpenMP-compatible chronometer.
 */
class OMPChronometer {
  
 public:
  
  // time type
  typedef double time_t;

  // constructor
  OMPChronometer();
  
  // copy constructor
  OMPChronometer(const OMPChronometer&);
  
  // destructor
  ~OMPChronometer() {}
  
  // start chrono
  void start();
  
  // stop chrono
  void stop();
  
  // reset chrono
  void reset();
  
  // elapsed time
  time_t elapsed();
  
  // format elapsed time
  static std::string toString(time_t);
};
#endif


/***********************
   Cloneable interface
 ***********************

/**
 * Interface for cloneable objects.
 */
class Cloneable {
  
 public:
  
  // clone operation
  Cloneable* clone() const = 0;
};


/**********************
   Copiable interface
 **********************

/**
 * Interface for copiable objects.
 */
class Copiable {
  
 public:
  
  // copy operation
  void copy(const Copiable&) = 0;
};


/*************
   Functions
 *************/

/**
 * Base class for functions.
 */
class Function : virtual public Cloneable {

 public:
  
  // duplicate object
  Function* clone() const = 0;
  
  // get the function's argument name
  std::string getArgName() const;

  // get the function's name
  std::string getName() const;
  
  // get value
  z_real_t value(z_real_t) = 0;
  
  // get derivative
  z_real_t slope(z_real_t) = 0;
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&) = 0;
  
  // print-out
  std::string toString() const = 0;
  %extend {
    char* __str__() {
      static char tmp[1024];
      std::strcpy(tmp,self->toString().c_str());
      return tmp;
    }
  }
};

/**
 * Base class for functions for which a curvature can be computed.
 * It normally implies C1 continuity.
 */
class Function2 : virtual public Function {
  
 public:
  
  // duplicate object
  Function2* clone() const = 0;
  
  // get value
  z_real_t value(z_real_t) = 0;
  
  // get derivative
  z_real_t slope(z_real_t) = 0;
  
  // get curvature
  z_real_t curvature(z_real_t) = 0;
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&) = 0;
  
  // get value and derivatives
  z_real_t value(z_real_t,z_real_t&,z_real_t&) = 0;
};

/**
 * Constant functions.
 */
class ConstantFunction : virtual public Function2 {
  
 public:
  
  // default constructor
  ConstantFunction(const std::string& = "no name",z_real_t = 0.0);
  
  // copy constructor
  ConstantFunction(const ConstantFunction&);
  
  // duplicate object
  ConstantFunction* clone() const;
  
  // get value
  z_real_t value(z_real_t);
  
  // get derivative
  z_real_t slope(z_real_t);
  
  // get curvature
  z_real_t curvature(z_real_t);
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&);
  
  // get value and derivatives
  z_real_t value(z_real_t,z_real_t&,z_real_t&);
  
  // print-out
  std::string toString() const;
};

/**
 * Ramp functions.
 */
class RampFunction : virtual public Function2 {
 
 public:
  
  // default constructor
  RampFunction(const std::string&,const std::string& = "no name",z_real_t = 1.0);
  
  // copy constructor
  RampFunction(const RampFunction&);
  
  // duplicate object
  RampFunction* clone() const;
  
  // get value
  z_real_t value(z_real_t);
  
  // get derivative
  z_real_t slope(z_real_t);
  
  // get curvature
  z_real_t curvature(z_real_t);
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&);
  
  // get value and derivatives
  z_real_t value(z_real_t,z_real_t&,z_real_t&);
  
  // print-out
  std::string toString() const;
};

/**
 * Tabulated functions.
 */
class TabulatedFunction : virtual public Function {
  
 public:
    
  // default constructor
  TabulatedFunction(const std::string&,const std::string& = "no name",unsigned int = 0);
  
  // constructor
  TabulatedFunction(const std::string&,const std::string&,unsigned int,z_real_t*,z_real_t*);
  
  // copy constructor
  TabulatedFunction(const TabulatedFunction&);
  
  // duplicate object
  TabulatedFunction* clone() const;

  // resize
  void resize(unsigned int);
  
  // get number of points
  unsigned int nPoints() const;
  
  // get point of given index
  std::pair<z_real_t,z_real_t> getPoint(unsigned int) const;

  // set value
  void setPoint(unsigned int,z_real_t,z_real_t);
  
  // get value
  z_real_t value(z_real_t);
  
  // get derivative
  z_real_t slope(z_real_t);
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&);
  
  // print-out
  std::string toString() const;
};

/**
 * Tabulated functions (with C1-continuity).
 */
class TabulatedFunction2 : virtual public TabulatedFunction,
                           virtual public Function2 {
  
 public:
    
  // default constructor
  TabulatedFunction2(const std::string&,const std::string& = "no name",unsigned int = 0);
  
  // constructor
  TabulatedFunction2(const std::string&,const std::string&,unsigned int,z_real_t*,z_real_t*);
  
  // copy constructor
  TabulatedFunction2(const TabulatedFunction2&);
  
  // duplicate object
  TabulatedFunction2* clone() const;
  
  // resize
  void resize(unsigned int);
  
  // set value
  void setPoint(unsigned int,z_real_t,z_real_t);
  
  // get value
  z_real_t value(z_real_t);
  
  // get derivative
  z_real_t slope(z_real_t);
  
  // get curvature
  z_real_t curvature(z_real_t);
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&);
  
  // get value and derivatives
  z_real_t value(z_real_t,z_real_t&,z_real_t&);

  // print-out
  std::string toString() const;
};


// allow exception handling in python
%catches(std::runtime_error) PythonFunction::PythonFunction();
%catches(std::runtime_error) PythonFunction::value(z_real_t);
%catches(std::runtime_error) PythonFunction::slope(z_real_t);

%exception {
  try {
    $action
  }
  catch (std::runtime_error e) {
    PyErr_SetString(PyExc_TypeError,e.what());
    return 0;
  }
}

%inline %{
  
/**
 * Base class for functions defined in python.
 */
class PythonFunction : virtual public Function2 {
  
 private:
  
  PyObject* pyfct;
  
 public:
  
  // constructor
  PythonFunction(PyObject* f,const std::string& arg_name,
                 const std::string& fct_name = "undefined")
  : Function(arg_name,fct_name) {
    if (!PyCallable_Check(f))
      throw std::runtime_error("first argument must be a callable python object.");
    pyfct = f; Py_INCREF(f);
  }
  
  // copy constructor
  PythonFunction(const PythonFunction& src)
  : Function(src) {
    pyfct = src.pyfct; Py_INCREF(src.pyfct);
  }
  
  // destructor
  virtual ~PythonFunction() {Py_DECREF(pyfct);}
  
  // duplicate object
  PythonFunction* clone() const {
    return new PythonFunction(*this);
  }
  
  // get value
  z_real_t value(z_real_t x) {
    PyObject *arglist;
    arglist = Py_BuildValue("(d)",x);
    PyObject *result;
    result = PyObject_CallObject(pyfct,arglist);
    Py_DECREF(arglist);
    if (!result) throw std::runtime_error("invalid python function call");
    double val = 0.0;
    if (PyFloat_Check(result))
      val = PyFloat_AsDouble(result);
    else if (PyTuple_Check(result) && PyTuple_GET_SIZE(result) > 0)
      val = PyFloat_AsDouble(PyTuple_GET_ITEM(result,0));
    else
      throw std::runtime_error("python function must return a float or float tuple");
    Py_DECREF(result);
    return static_cast< z_real_t >(val);
  }
  
  // get derivative
  z_real_t slope(z_real_t x) {
    PyObject *arglist;
    arglist = Py_BuildValue("(d)",x);
    PyObject *result;
    result = PyObject_CallObject(pyfct,arglist);
    Py_DECREF(arglist);
    if (!result) throw std::runtime_error("invalid python function call");
    double val = 0.0;
    if (PyTuple_Check(result) && PyTuple_GET_SIZE(result) > 1)
      val = PyFloat_AsDouble(PyTuple_GET_ITEM(result,1));
    else
      throw std::runtime_error("python function must return a float tuple of size 2");
    Py_DECREF(result);
    return static_cast< z_real_t >(val);
  }
  
  // get curvature
  z_real_t curvature(z_real_t x) {
    PyObject *arglist;
    arglist = Py_BuildValue("(d)",x);
    PyObject *result;
    result = PyObject_CallObject(pyfct,arglist);
    Py_DECREF(arglist);
    if (!result) throw std::runtime_error("invalid python function call");
    double val = 0.0;
    if (PyTuple_Check(result) && PyTuple_GET_SIZE(result) > 2)
      val = PyFloat_AsDouble(PyTuple_GET_ITEM(result,2));
    else
      throw std::runtime_error("python function must return a float tuple of size 3");
    Py_DECREF(result);
    return static_cast< z_real_t >(val);
  }
  
  // get value and derivative
#ifdef SWIG
  %ignore PythonFunction::value(z_real_t,z_real_t&);
#endif
  z_real_t value(z_real_t x,z_real_t& df) {
    PyObject *arglist;
    arglist = Py_BuildValue("(d)",x);
    PyObject *result;
    result = PyObject_CallObject(pyfct,arglist);
    Py_DECREF(arglist);
    if (!result) throw std::runtime_error("invalid python function call");
    double val = 0.0;
    df = 0.0;
    if (PyFloat_Check(result)) {
      val = PyFloat_AsDouble(result);
      df  = 0.0;
    }
    else if (PyTuple_Check(result) && PyTuple_GET_SIZE(result) > 1) {
      val = PyFloat_AsDouble(PyTuple_GET_ITEM(result,0));
      df  = PyFloat_AsDouble(PyTuple_GET_ITEM(result,1));
    }
    else
      throw std::runtime_error("python function must return a float or float tuple of size 2");
    Py_DECREF(result);
    return static_cast< z_real_t >(val);
  }
  
  // get value and derivatives
#ifdef SWIG
  %ignore PythonFunction::value(z_real_t,z_real_t&,z_real_t&);
#endif
  z_real_t value(z_real_t x,z_real_t& df,z_real_t& d2f) {
    PyObject *arglist;
    arglist = Py_BuildValue("(d)",x);
    PyObject *result;
    result = PyObject_CallObject(pyfct,arglist);
    Py_DECREF(arglist);
    if (!result) throw std::runtime_error("invalid python function call");
    double val = 0.0;
    df = 0.0;
    if (PyFloat_Check(result)) {
      val = PyFloat_AsDouble(result);
      df  = 0.0;
      d2f  = 0.0;
    }
    else if (PyTuple_Check(result) && PyTuple_GET_SIZE(result) > 1) {
      val  = PyFloat_AsDouble(PyTuple_GetItem(result,0));
      df   = PyFloat_AsDouble(PyTuple_GetItem(result,1));
      if (PyTuple_GET_SIZE(result) > 2)
        d2f  = PyFloat_AsDouble(PyTuple_GetItem(result,2));
      else
        d2f = 0.0;
    }
    else
      throw std::runtime_error("python function must return a float or float tuple");
    Py_DECREF(result);
    return static_cast< z_real_t >(val);
  }
  
  // print-out
  std::string toString() const {return getName();}
};

%}

%exception;


/**************
   Properties
 **************/

/**
 * Base class for multi-type properties.
 */
class Property : virtual public Cloneable {
  
 public:
  
  // duplicate object
  Property* clone() const = 0;
  
  // output as a string
  std::string toString() const = 0;
  %extend {
    char* __str__() {
      static char tmp[1024];
      std::strcpy(tmp,self->toString().c_str());
      return tmp;
    }
  }
};

/**
 * Boolean-valued property.
 */
class BooleanProperty : public Property {
  
 public:
  
  // default constructor
  BooleanProperty(bool v = false);
  
  // copy constructor
  BooleanProperty(const BooleanProperty&);
  
  // get value
  bool value() const;
  
  // set value
  void setValue(bool);
  
  // duplicate object
  BooleanProperty* clone() const;
  
  // output as a string
  std::string toString() const;
};

/**
 * Integer-valued property.
 */
class IntegerProperty : public Property {
  
 public:
  
  // default constructor
  IntegerProperty(z_int_t = 0);
  
  // copy constructor
  IntegerProperty(const IntegerProperty&);
  
  // get value
  z_int_t value() const;
  
  // set value
  void setValue(z_int_t);
  
  // duplicate object
  IntegerProperty* clone() const;
  
  // output as a string
  std::string toString() const;
};

/**
 * Real-valued property.
 */
class RealProperty : public Property {
  
 public:
  
  // default constructor
  RealProperty(z_real_t = 0.0);
  
  // copy constructor
  RealProperty(const RealProperty&);
  
  // get value
  z_real_t value() const;
  
  // set value
  void setValue(z_real_t);
  
  // duplicate object
  RealProperty* clone() const;
  
  // output as a string
  std::string toString() const;
};

/**
 * String-valued (character string) property.
 */
class StringProperty : public Property {
  
 public:
  
  // default constructor
  StringProperty(const char*);
  
  // copy constructor
  StringProperty(const StringProperty&);
  
  // get value
  std::string value() const;
  
  // set value
  void setValue(const char*);
  
  // duplicate object
  StringProperty* clone() const;
  
  // output as a string
  std::string toString() const;
};

/**
 * Function-valued property.
 */
class FunctionProperty : public Property {
  
 public:
  
  // default constructor
  FunctionProperty(Function&);
  
  // copy constructor
  FunctionProperty(const FunctionProperty&);
  
  // get value
  Function& function() const;
  
  // set value
  void setFunction(Function&);
  
  // duplicate object
  FunctionProperty* clone() const;
  
  // output as a string
  std::string toString() const;
};


/**
 * Define new type PropertyTable
 */
typedef Dictionary<Property*>::Type PropertyTable;


/***********************
   Arrays and Matrices
 ***********************/

/**
 * Base class for (short) arrays.
 */
class ShortArrayBase {
  
 public:
  
  // constructor
  ShortArrayBase();
  
  // copy constructor
  ShortArrayBase(const ShortArrayBase&);

  // size
  unsigned int size() const = 0;
    
  // expression operator
  %ignore operator[];
  z_real_t operator[](unsigned int) const = 0;
  %extend {
    z_real_t operator()(unsigned int i) const {return self->operator[](i);}
  }

  // print to string object
  std::string toString() const;
  %extend {
    char* __str__() {
      static char tmp[1024];
      std::strcpy(tmp,self->toString().c_str());
      return tmp;
    }
  }
};

/**
 * Class for short arrays.
 */
class ShortArray : virtual public ShortArrayBase {
  
 public:
    
  // constructor
  ShortArray(unsigned int = 0);
  
  // subarray constructor
  ShortArray(ShortArray&,unsigned int,unsigned int = 0);
  ShortArray(ShortArrayBase&,unsigned int,unsigned int = 0);
  /* add interface to python arrays? */
  
  // copy constructor
  ShortArray(const ShortArray&);
  ShortArray(const ShortArrayBase&);
  /* add interface to python arrays? */

  // assignment operator
  %ignore operator=;
  ShortArray& operator=(const ShortArray&);
  %extend {
    void copy(const ShortArray& src) {self->operator=(src);}
  }
  ShortArray& operator=(const ShortArrayBase&);
  %extend {
    void copy(const ShortArrayBase& src) {self->operator=(src);}
  }
  ShortArray& operator=(z_real_t);
  %extend {
    void set(z_real_t val) {self->operator=(val);}
  }
  /* add interface to python arrays? */

  // unary operators
  ShortArray& operator+=(const ShortArray&);
  ShortArray& operator+=(const ShortArrayBase&);
  ShortArray& operator-=(const ShortArray&);
  ShortArray& operator-=(const ShortArrayBase&);
  ShortArray& operator*=(z_real_t);
  ShortArray& operator/=(z_real_t);
  
  // size
  unsigned int size() const;
  
  // resize
  void resize(unsigned int);
  
  // wrap another array
  void wrap(ShortArray&,unsigned int,unsigned int = 0);

  // access operators
  %ignore operator[];
  z_real_t operator[](unsigned int) const;
  z_real_t operator()(unsigned int);
  %extend {
    void set(unsigned int i,z_real_t val) {self->operator()(i) = val;}
  }
};

/**
 * ShortArray sums and differences.
 */
class ShortArraySum : public ShortArrayBase {
  
 public:
  
  // constructor
  ShortArraySum(const ShortArrayBase&,const ShortArrayBase&);
  
  // copy constructor
  ShortArraySum(const ShortArraySum&);
  
  // size
  unsigned int size() const;
  
  // expression operator
  %ignore operator[];
  z_real_t operator[](unsigned int) const;
  %extend {
    z_real_t operator()(unsigned int i) const {return self->operator[](i);}
  }
};

%extend ShortArrayBase {
  ShortArraySum __add__(const ShortArrayBase& other) const {
    return (*self)+other;
  }
};


class ShortArrayDifference : public ShortArrayBase {
  
 public:
    
  // constructor
  ShortArrayDifference(const ShortArrayBase&,const ShortArrayBase&);
  
  // copy constructor
  ShortArrayDifference(const ShortArrayDifference&);
  
  // size
  unsigned int size() const;
  
  // expression operator
  %ignore operator[];
  z_real_t operator[](unsigned int) const;
  %extend {
    z_real_t operator()(unsigned int i) const {return self->operator[](i);}
  }
};

%extend ShortArrayBase {
  ShortArrayDifference __sub__(const ShortArrayBase& other) const {
    return (*self)-other;
  }
};

/**
 * Array products.
 */
class ShortArrayScalarProduct : public ShortArrayBase {
  
 public:
    
  // constructor
  ShortArrayScalarProduct(const ShortArrayBase&,z_real_t);
  
  // copy constructor
  ShortArrayScalarProduct(const ShortArrayScalarProduct&);
  
  // size
  unsigned int size() const;
  
  // expression operator
  %ignore operator[];
  z_real_t operator[](unsigned int) const;
  %extend {
    z_real_t operator()(unsigned int i) const {return self->operator[](i);}
  }
};

%extend ShortArrayBase {
  ShortArrayScalarProduct __div__(z_real_t f) const {
    return (*self)/f;
  }
  ShortArrayScalarProduct __truediv__(z_real_t f) const {
    return (*self)/f;
  }
  ShortArrayScalarProduct __mul__(z_real_t f) const {
    return f*(*self);
  }
  ShortArrayScalarProduct __neg__() const {
    return -1.*(*self);
  }
};
%inline %{
  ShortArrayScalarProduct mul(z_real_t f,const ShortArrayBase& a) {
    return f*a;
  }
%}

// define inner product
z_real_t innerProd(const ShortArrayBase&,const ShortArrayBase&);

// compute norm of an array
z_real_t normL1(const ShortArrayBase&);
z_real_t normL2(const ShortArrayBase&);

// compute max absolute value
z_real_t normLInf(const ShortArrayBase&);


/**
 * Base class for (short) matrices.
 */
class ShortMatrixBase {
  
 public:
  
  // constructor
  ShortMatrixBase();
  
  // copy constructor
  ShortMatrixBase(const ShortMatrixBase&);

  // matrix size
  unsigned int nCols() const = 0;
  unsigned int nRows() const = 0;
    
  // expression operator
  z_real_t operator()(unsigned int,unsigned int) const = 0;

  // print to string object
  std::string toString() const;
  %extend {
    char* __str__() {
      static char tmp[16384];
      std::strcpy(tmp,self->toString().c_str());
      return tmp;
    }
  }
};

/**
 * Class for small, full matrices.
 */
class ShortMatrix : virtual public ShortMatrixBase {

 public:
  
  // constructor
  ShortMatrix(unsigned int = 0,unsigned int = 1);
  
  // submatrix constructor
  ShortMatrix(const ShortMatrix&,unsigned int,unsigned int,unsigned int = 0,unsigned int = 0);
  ShortMatrix(const ShortMatrixBase&,unsigned int,unsigned int,unsigned int = 0,unsigned int = 0);
  
  // copy constructor
  ShortMatrix(const ShortMatrix&);
  ShortMatrix(const ShortMatrixBase&);
  
  // assignment operator
  %ignore operator=;
  ShortMatrix& operator=(const ShortMatrix&);
  %extend {
    void copy(const ShortMatrix& src) {self->operator=(src);}
  }
  ShortMatrix& operator=(const ShortMatrixBase&);
  %extend {
    void copy(const ShortMatrixBase& src) {
      self->operator=(src);
    }
  }
  ShortMatrix& operator=(z_real_t val);
  %extend {
    void set(z_real_t val) {self->operator=(val);}
  }

  // unary operators
  ShortMatrix& operator+=(const ShortMatrix&);
  ShortMatrix& operator+=(const ShortMatrixBase&);
  ShortMatrix& operator-=(const ShortMatrix&);
  ShortMatrix& operator-=(const ShortMatrixBase&);
  ShortMatrix& operator*=(z_real_t);
  ShortMatrix& operator/=(z_real_t);
  
  // size
  unsigned int nRows() const;
  unsigned int nCols() const;
  
  // resize
  void resize(unsigned int,unsigned int);
  
  // wrap another matrix
  void wrap(const ShortMatrix&,unsigned int,unsigned int,unsigned int = 0, unsigned int = 0);

  // access operators
  z_real_t operator()(unsigned int i,unsigned int j) const;
  %extend {
    void set(unsigned int i,unsigned int j,z_real_t val) {
      self->operator()(i,j) = val;
    }
  }
  
  // build matrix by vector outer product
  static void outerProd(const ShortArrayBase&,const ShortArrayBase&,ShortMatrix&);
};

/**
 * ShortMatrix sums and differences.
 */
class ShortMatrixSum : public ShortMatrixBase {
  
 public:
    
  // constructor
  ShortMatrixSum(const ShortMatrixBase&,const ShortMatrixBase&);
  
  // copy constructor
  ShortMatrixSum(const ShortMatrixSum&);
  
  // matrix size
  unsigned int nCols() const;
  unsigned int nRows() const;
  
  // expression operator
  z_real_t operator()(unsigned int,unsigned int) const;
};

%extend ShortMatrixBase {
  ShortMatrixSum __add__(const ShortMatrixBase& other) const {
    return (*self)+other;
  }
};


class ShortMatrixDifference : public ShortMatrixBase {
  
 public:
  
  // constructor
  ShortMatrixDifference(const ShortMatrixBase&,const ShortMatrixBase&);
  
  // copy constructor
  ShortMatrixDifference(const ShortMatrixDifference&);
  
  // matrix size
  unsigned int nCols() const;
  unsigned int nRows() const;
  
  // expression operator
  z_real_t operator()(unsigned int,unsigned int) const;
};

%extend ShortMatrixBase {
  ShortMatrixDifference __sub__(const ShortMatrixBase& other) const {
    return (*self)-other;
  }
};

/**
 * Matrix products.
 */
class ShortMatrixScalarProduct : public ShortMatrixBase {
  
 public:
    
  // constructor
  ShortMatrixScalarProduct(const ShortMatrixBase&,z_real_t) ;
  
  // copy constructor
  ShortMatrixScalarProduct(const ShortMatrixScalarProduct&);
  
  // matrix size
  unsigned int nCols() const;
  unsigned int nRows() const;
  
  // expression operator
  z_real_t operator()(unsigned int,unsigned int) const;
};

%extend ShortMatrixBase {
  ShortMatrixScalarProduct __div__(z_real_t f) const {
    return (*self)/f;
  }
  ShortMatrixScalarProduct __truediv__(z_real_t f) const {
    return (*self)/f;
  }
  ShortMatrixScalarProduct __mul__(z_real_t f) const {
    return f*(*self);
  }
  ShortMatrixScalarProduct __neg__() const {
    return -1.*(*self);
  }
};

%inline %{
  ShortMatrixScalarProduct mul(z_real_t f,const ShortMatrixBase& a) {
    return f*a;
  }
%}

// define outer product
class ShortArrayOuterProduct : public ShortMatrixBase {
  
 public:
  
  // constructor
  ShortArrayOuterProduct(const ShortArrayBase&,const ShortArrayBase&);
  
  // copy constructor
  ShortArrayOuterProduct(const ShortArrayOuterProduct&);
  
  // matrix size
  unsigned int nCols() const;
  unsigned int nRows() const;
  
  // expression operator
  z_real_t operator()(unsigned int,unsigned int) const;
};

ShortArrayOuterProduct outerProd(const ShortArrayBase&,const ShortArrayBase&);

%extend ShortMatrixBase {
  // simple matrix*array product
  ShortArray __mul__(const ShortArrayBase& a) const {
    return (*self)*a;
  }
  // simple matrix*matrix product
  ShortMatrix __mul__(const ShortMatrixBase& M) const {
    return (*self)*M;
  }
};

// define inner product
z_real_t innerProd(const ShortMatrixBase&,const ShortMatrixBase&);

// compute norm of an array
z_real_t normL1(const ShortMatrix&);
z_real_t normL1(const ShortMatrixBase&);
z_real_t normL2(const ShortMatrix&);
z_real_t normL2(const ShortMatrixBase&);

// compute max absolute value
z_real_t normLInf(const ShortMatrix&);
z_real_t normLInf(const ShortMatrixBase&);


/**
 * Exception thrown when attempting to factorize a singular matrix.
 */
class SingularMatrixException : public std::runtime_error {
  
 public:
  
  // default constructor
  SingularMatrixException(const std::string& = "singular matrix");
  
  // copy constructor
  SingularMatrixException(const SingularMatrixException&);
};

// instantiate templates used by ShortSqrMatrix class
namespace std {
  %template(BooleanVector) vector<bool>;
}

// exception handling
%exception {
  try {
    $action
  }
  catch (SingularMatrixException) {
    PyErr_SetString(PyExc_RuntimeError,"Singular matrix");
    SWIG_fail;
  }
}

/**
 * Class for small, full, square matrices.
 */
class ShortSqrMatrix : virtual public ShortMatrix { 

 public:
    
  // constructor
  ShortSqrMatrix(unsigned int = 0);
  
  // submatrix constructor
  ShortSqrMatrix(const ShortSqrMatrix&,unsigned int,unsigned int = 0);
  ShortSqrMatrix(const ShortMatrixBase&,unsigned int,unsigned int = 0);

  // copy constructor
  ShortSqrMatrix(const ShortSqrMatrix&);
  
  // assignment operator
  %ignore operator=;
  ShortSqrMatrix& operator=(const ShortMatrix&);
  ShortSqrMatrix& operator=(const ShortMatrixBase&);
  ShortSqrMatrix& operator=(z_real_t);
  
  // size
  unsigned int size() const;
    
  // resize
  void resize(unsigned int);
  
  // wrap another square matrix
  void wrap(const ShortSqrMatrix&,unsigned int,unsigned int = 0);
  
  // wrap another matrix
  void wrap(const ShortMatrix&,unsigned int,unsigned int,unsigned int);

  // get identity matrix
  static ShortSqrMatrix identity(unsigned int);
  
  // invert (in place)
  void invert();

  // factorize (LU)
  void factorize(bool);
  
  // back-substitute
  void backsubstitute(ShortArray&,const ShortArrayBase&) const;
  
  // solve linear system
  void solve(ShortArray&,const ShortArrayBase&,bool=false);
  
  // solve a linear system known to be symmetric
  // (will be modified if not definite positive)
  bool symSolve(ShortArray&,const ShortArrayBase&,std::vector<bool>&,
                z_real_t = 0.0,z_real_t = 0.0);
};

%exception;

/*************
   Rotations
 *************/

/**
 * Class describing a rotation operation.
 */
class Rotation {
  
 public:
  
  // useful types
  typedef ShortSqrMatrix RotationMatrix;
  typedef ShortArray     RotationTensor;
  
  // export to matrix
  virtual void toMatrix(RotationMatrix&) const = 0;
  
  // export to tensor
  virtual void toTensor(RotationTensor&) const = 0;
};

/**
 * Class for 2D rotations.
 */
class Rotation2D : virtual public Rotation {
  
 public:
  
  // constructor
  Rotation2D(z_real_t = 0.0);
    
  // constructor (from rotation matrix)
  Rotation2D(const RotationMatrix&);

  // copy constructor
  Rotation2D(const Rotation2D&);
  
  // export to matrix
  void toMatrix(RotationMatrix&) const;
  
  // export to tensor
  void toTensor(RotationTensor&) const;
};

/**
 * Class for 3D rotations.
 */
class Rotation3D : virtual public Rotation {
  
 public:
  
  // types of parameterization
  enum Type {BUNGE,KOCKS};
  
 public:
  
  // constructor (from Euler angles)
  Rotation3D(z_real_t,z_real_t,z_real_t,Type = BUNGE);
  
  // constructor (from Euler parameters)
  Rotation3D(const ShortArrayBase&);
  
  // constructor (from rotation matrix)
  Rotation3D(const RotationMatrix&);
  
  // copy constructor
  Rotation3D(const Rotation3D&);
  
  // export to Euler angles
  void toEulerAngles(z_real_t&,z_real_t&,z_real_t&,Type = BUNGE) const;
  
  // export to matrix
  void toMatrix(RotationMatrix&) const;
  
  // export to tensor
  void toTensor(RotationTensor&) const;
  
  // from Euler parameters to CRV representation
  static void eul2crv(const ShortArrayBase&,ShortArray&);
  
  // from matrix representation to Euler parameters
  static void euler(const RotationMatrix&,ShortArray&);
  
  // from vector to matrix representation
  static void spin(const ShortArrayBase&,RotationMatrix&);
};
