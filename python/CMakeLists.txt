################################################################################
#  This file is part of ZorgLib, a computational simulation framework          #
#  for thermomechanics of solids and structures (systems in general).          #
#                                                                              #
#  Copyright (c) 2001-2024, L. Stainier.                                       #
#  See file LICENSE.txt for license information.                               #
#  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.     #
################################################################################

if(POLICY CMP0078)
  # use old SWIG target naming scheme
  cmake_policy(SET CMP0078 NEW)
endif(POLICY CMP0078)

if(POLICY CMP0086)
  # pass module name to SWIG if defined
  cmake_policy(SET CMP0086 NEW)
endif(POLICY CMP0086)

if(POLICY CMP0094)
  # stop at first python found
  cmake_policy(SET CMP0094 NEW)
endif(POLICY CMP0094)

find_package(Python COMPONENTS Interpreter Development REQUIRED)
find_package(SWIG COMPONENTS python REQUIRED)
MESSAGE(STATUS "Using Python version " "${Python_VERSION}")

include(${SWIG_USE_FILE})
include_directories(${Python_INCLUDE_DIRS})

SET_SOURCE_FILES_PROPERTIES(matlib.i PROPERTIES CPLUSPLUS ON )

if(NOT ${PYTHON_VERSION_MAJOR} LESS 3)
  set(CMAKE_SWIG_FLAGS "-py3")
endif(NOT ${PYTHON_VERSION_MAJOR} LESS 3)

set(HEADERS_FOR_PYTHON_INTERFACE "data.h" "math.h" "matl.h")
if(CMAKE_VERSION VERSION_LESS 3.8.2)
  SWIG_ADD_MODULE(matlib2python python "matlib.i" ${HEADERS_FOR_PYTHON_INTERFACE})
else(CMAKE_VERSION VERSION_LESS 3.8.2)
  SWIG_ADD_LIBRARY(matlib2python LANGUAGE python SOURCES "matlib.i" ${HEADERS_FOR_PYTHON_INTERFACE})
endif(CMAKE_VERSION VERSION_LESS 3.8.2)

if(${USING_ANACONDA})
  SWIG_LINK_LIBRARIES(matlib2python matlib_lite)
  set_target_properties(${SWIG_MODULE_matlib2python_REAL_NAME} PROPERTIES
                        CXX_STANDARD 11
                        LINK_FLAGS "-undefined dynamic_lookup")
else(${USING_ANACONDA})
  SWIG_LINK_LIBRARIES(matlib2python matlib_lite ${Python_LIBRARIES})
  set_target_properties(${SWIG_MODULE_matlib2python_REAL_NAME} PROPERTIES
                        CXX_STANDARD 11 )
endif(${USING_ANACONDA})

install( TARGETS ${SWIG_MODULE_matlib2python_REAL_NAME} 
         LIBRARY DESTINATION python
         BUNDLE DESTINATION python )
install( FILES ${CMAKE_CURRENT_BINARY_DIR}/matlib.py DESTINATION python)

# create executable
add_executable(MatLibRun MatLibRun.cpp)
set_property( TARGET MatLibRun PROPERTY CXX_STANDARD 11 )
target_link_libraries( MatLibRun matlib_lite ${Python_LIBRARIES} ${M_LIB})

# installing application
install( TARGETS MatLibRun RUNTIME DESTINATION bin )
