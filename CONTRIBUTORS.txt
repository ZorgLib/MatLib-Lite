MatLib is copyright (C) 2001-2021 Laurent Stainier <laurent.stainier at ec-nantes.fr>

This library has been progressively built upon research work of the main author, started in the late 90s. 
This C++ version is the evolution of a previous one using C (and many pointers!).

It has benefited from many more or less direct contributions, among which the following ones:
- F. Dubois (LMGC) for long-term support, testing and feedback
- R. Mozul (LMGC) for help with CMake and Swig
- D. Ambard (LMGC) for hyperelastic models
- F. Perales (IRSN) for testing and feedback
- B. Piar (IRSN) for the automatic model loading mechanism
- A. Emmel Selke and B. Aguire Tessaro for thermo-visco-hyperelastic models
- ...
