/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_CONSTITUTIVE_MODEL_H
#define ZORGLIB_MATL_CONSTITUTIVE_MODEL_H

// config
#include <matlib_macros.h>

// std C++ library
#include <iostream>
#include <stdexcept>
// local
#ifndef WITH_MATLIB_H
#ifndef MATLIB_USE_EIGEN_ARRAY
#include <data/ShortSqrMatrix.h>
#endif
#include <data/Rotation.h>
#include <matl/MaterialProperties.h>
#include <matl/MaterialState.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Exception thrown when the constitutive update fails.
 */
class UpdateFailedException : public std::runtime_error {
  
 public:
  
  // default constructor
  UpdateFailedException(const std::string& msg = "update failed")
  : std::runtime_error(msg) {}
  
  // copy constructor
  UpdateFailedException(const UpdateFailedException& src)
  : std::runtime_error(src) {}
};

/**
 * Define generic matrix type
*/
#ifndef MATLIB_USE_EIGEN_ARRAY
typedef ShortSqrMatrix MatLibMatrix;
#endif

/**
 * Virtual base class for constitutive models.
 */
class ConstitutiveModel {

 public:
  
  // define type for external parameters
  typedef Dictionary< z_real_t >::Type ParameterSet;

  // define variable types handled by const. models
  enum VariableType {
    TYPE_NONE,
    TYPE_SCALAR,
    TYPE_VECTOR,
    TYPE_SYM_TENSOR,
    TYPE_TENSOR,
    TYPE_STD_SYM_TENSOR,
    TYPE_STD_TENSOR
  };

 protected:
  
  // constructor
  ConstitutiveModel() {}

 public:
  
  // virtual destructor
  virtual ~ConstitutiveModel() {}

  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream*) = 0;
  void checkProperties(MaterialProperties&,const char* = 0);
  
  // apply rotation to material properties
  virtual void rotateProperties(MaterialProperties&,const Rotation&) {}
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ParameterSet&) {}
  
  // how many external variables ?
  virtual unsigned int nExtVar() const = 0;
  
  // self-documenting utilities
  virtual unsigned int nExtVarBundled() const = 0;
  virtual VariableType typeExtVar(unsigned int) const = 0;
  virtual unsigned int indexExtVar(unsigned int) const = 0;
  virtual std::string labelExtVar(unsigned int) const = 0;
  virtual std::string labelExtForce(unsigned int) const = 0;
  
  // how many internal variables ?
  virtual unsigned int nIntVar() const = 0;
  
  // self-documenting utilities
  virtual unsigned int nIntVarBundled() const = 0;
  virtual unsigned int getIntVar(const std::string&) const = 0;
  virtual VariableType typeIntVar(unsigned int) const = 0;
  virtual unsigned int indexIntVar(unsigned int) const = 0;
  virtual std::string labelIntVar(unsigned int) const = 0;
  
  // utility function
  static unsigned int dimension(VariableType,unsigned int);

  // check if the material behaviour is linear ?
  virtual bool isLinear() const {return false;}
  
  // check if the material is "standard" ?
  virtual bool isVariational() const {return false;}
  
  // initialize the state(s) of the material
  virtual void initState(const MaterialProperties&,MaterialState& state) {
    state.grad.resize(nExtVar());
    state.flux.resize(nExtVar());
    state.internal.resize(nIntVar());
  }
  virtual void initStates(const MaterialProperties& mater,
                          MaterialState& state0,MaterialState& state1) {
    initState(mater,state0);
    initState(mater,state1);
  }

  // update the state of the material (with the ability to compute tangents)
  virtual void updateState(const MaterialProperties&,const ParameterSet&,
                           const MaterialState&,MaterialState&,z_real_t,
                           MatLibMatrix&,bool) = 0;
  
  // compute material tangents (without updating)
  virtual void computeTangent(const MaterialProperties& mater,const ParameterSet& extPar,
                              const MaterialState& state0,const MaterialState& state1,
                              z_real_t dTime,MatLibMatrix& tgt) {
    computeNumericalTangent(mater,extPar,state0,state1,dTime,tgt);
  }
  
  // compute material tangents by numerical perturbation
  void computeNumericalTangent(const MaterialProperties&,const ParameterSet&,
                               const MaterialState&,const MaterialState&,z_real_t,
                               MatLibMatrix&);
};


/**
 * Additional interface for standard (i.e. variational) materials.
 */
class StandardMaterial : virtual public ConstitutiveModel {
  
 protected:
  
  // constructor
  StandardMaterial() {}

 public:
  
  // destructor
  virtual ~StandardMaterial() {}
  
  // check if the material is "standard" ?
  bool isVariational() const {return true;}
  
  // compute the incremental potential
  virtual z_real_t incrementalPotential(const MaterialProperties&,
                                        const ConstitutiveModel::ParameterSet&,
                                        const MaterialState&,MaterialState&,
                                        z_real_t,MatLibMatrix&,bool,bool) = 0;
  
  // update the state of the material
  void updateState(const MaterialProperties& mater,
                   const ConstitutiveModel::ParameterSet& extPar,
                   const MaterialState& state0,MaterialState& state1,
                   z_real_t dTime,MatLibMatrix& tgt,bool compTgt) {
    incrementalPotential(mater,extPar,state0,state1,dTime,tgt,true,compTgt);
  }
  
  // compute material tangents (without updating)
  void computeTangent(const MaterialProperties& mater,
                      const ConstitutiveModel::ParameterSet& extPar,
                      const MaterialState& state0,const MaterialState& state1,
                      z_real_t dTime,MatLibMatrix& tgt) {
    incrementalPotential(mater,extPar,state0,const_cast<MaterialState&>(state1),
                         dTime,tgt,false,true);
  }
};


/**
 * Placeholder constitutive model, for convenience.
 */
class EmptyMaterial : virtual public StandardMaterial {

 public:

  // constructor
  EmptyMaterial() {}
  
  // destructor
  virtual ~EmptyMaterial() {}

  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream*) {}
  
  // how many external variables ?
  unsigned int nExtVar() const {return 0;}
  
  // self-documenting utilities
  unsigned int nExtVarBundled() const {return 0;}
  VariableType typeExtVar(unsigned int) const {return ConstitutiveModel::TYPE_NONE;}
  unsigned int indexExtVar(unsigned int) const {return 0;}
  std::string labelExtVar(unsigned int) const {return "";}
  std::string labelExtForce(unsigned int) const {return "";}
  
  // how many internal variables ?
  unsigned int nIntVar() const {return 0;}
  
  // self-documenting utilities
  unsigned int nIntVarBundled() const {return 0;}
  unsigned int getIntVar(const std::string&) const {return 0;}
  VariableType typeIntVar(unsigned int) const {return ConstitutiveModel::TYPE_NONE;}
  unsigned int indexIntVar(unsigned int) const {return 0;}
  std::string labelIntVar(unsigned int) const {return "";}

  // check if the material behaviour is linear ?
  bool isLinear() const {return true;}
  
  // compute the incremental potential
  z_real_t incrementalPotential(const MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&,
                                const MaterialState&,MaterialState& state,
                                z_real_t,MatLibMatrix& M,bool first,bool second) {
    if (first) state.flux = 0.0;
    if (second) M = 0.0;
    return 0.0;
  }
};


#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
