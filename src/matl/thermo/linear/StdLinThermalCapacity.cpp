/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "StdLinThermalCapacity.h"

// std C++ library
#include <stdexcept>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

// check consistency of material properties
void StdLinThermalCapacity::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\n\t***Standard linearized thermal capacity***" << std::endl;

  // capacity
  try {
    z_real_t Cv = material.getRealProperty("VOLUMIC_HEAT_CAPACITY");
    if (os) (*os) << "\n\tvolumic capacity = " << Cv << std::endl;
  }
  catch (NoSuchPropertyException) {
    try{
      z_real_t C = material.getRealProperty("SPECIFIC_HEAT_CAPACITY");
      if (os) (*os) << "\n\tspecific capacity = " << C << std::endl;
      z_real_t rho = material.getRealProperty("MASS_DENSITY");
      z_real_t Cv = rho*C;
      material.setProperty("VOLUMIC_HEAT_CAPACITY",Cv);
      if (os) (*os) << "\tvolumic capacity  = " << Cv << std::endl;
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: volumic capacity cannot be computed." << std::endl;
      throw e;
    }
  }

  // reference temperature
  try {
    z_real_t TRef = material.getRealProperty("REFERENCE_TEMPERATURE");
    if (TRef <= 0.0) {
      if (os) (*os) << "ERROR: reference temperature must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference temperature");
    }
    if (os) (*os) << "\n\treference temperature = " << TRef << std::endl;
  }
  catch (NoSuchPropertyException) {
    // use initial temperature
    try {
      z_real_t T0 = material.getRealProperty("INITIAL_TEMPERATURE");
      if (T0 <= 0.0) {
        if (os) (*os) << "ERROR: initial temperature must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: initial temperature");
      }
      material.setProperty("REFERENCE_TEMPERATURE",T0);
      if (os) (*os) << "\n\treference temperature = " << T0 << std::endl;
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: reference temperature cannot be set." << std::endl;
      throw e;
    }
  }
}

// compute
z_real_t StdLinThermalCapacity::freeEnergy(const MaterialProperties& material,
                                           const ConstitutiveModel::ParameterSet& extPar,
                                           z_real_t Th,z_real_t& N,z_real_t& C,
                                           bool computeFirst,bool computeSecond) {

  // get volumic capacity and initial temperature
  z_real_t Cv = material.getRealProperty("VOLUMIC_HEAT_CAPACITY");
  z_real_t T0 = material.getRealProperty("REFERENCE_TEMPERATURE");

  // compute internal energy
  z_real_t W;
  z_real_t coef = -Cv/T0;
  if (computeFirst) {
    N = coef*Th;
    W = 0.5*Th*N;
  }
  else
    W = 0.5*coef*Th*Th;

  if (computeSecond) C = coef;

  return W;
}
