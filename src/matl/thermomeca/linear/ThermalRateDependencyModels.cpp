/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "ThermalRateDependencyModels.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for class ThermalPowerLawRateDependencyModel.
 */

// check consistency of material properties
void ThermalPowerLawRateDependencyModel::checkProperties(MaterialProperties& material,
                                                         std::ostream* os) {
  if (os) (*os) << "\n\t***Power-law rate dependency model (temperature-dependent)***" << std::endl;

  // reference temperature
  z_real_t T0;
  try {
    T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
    if (T0 <= 0.0) {
      if (os) (*os) << "ERROR: reference temperature must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference temperature");
    }
  }
  catch (NoSuchPropertyException) {
    // use initial temperature
    try {
      T0 = material.getRealProperty("INITIAL_TEMPERATURE");
      if (T0 <= 0.0) {
        if (os) (*os) << "ERROR: initial temperature must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: initial temperature");
      }
      material.setProperty("REFERENCE_TEMPERATURE",T0);
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: reference temperature cannot be set." << std::endl;
      throw e;
    }
  }

  // reference stress
  z_real_t sig0;
  try {
    Function& fct = material.getFunctionProperty("REFERENCE_STRESS_EVOLUTION");
    sig0 = fct.value(T0);
    material.setProperty("REFERENCE_STRESS",sig0);
    if (os) {
      (*os) << "\n\treference stress temperature dependence: ";
      (*os) << fct << std::endl;
    }
  }
  catch (NoSuchPropertyException) {
    try {
      sig0 = material.getRealProperty("REFERENCE_STRESS");
    }
    catch (NoSuchPropertyException) {
      sig0 = 0.0;
      material.setProperty("REFERENCE_STRESS",0.0);
    }
  }
  if (sig0 == 0.0) {
    if (os) (*os) << "\tnot defined (REFERENCE_STRESS = 0.0)" << std::endl;
    return;
  }
  if (sig0 < 0.0) {
    if (os) (*os) << "ERROR: reference stress must be positive or zero." << std::endl;
    throw std::runtime_error("invalid property: reference stress");
  }

  // reference strain-rate
  z_real_t epsDot0;
  try {
    try {
      Function& fct = material.getFunctionProperty("REFERENCE_STRAIN_RATE_EVOLUTION");
      epsDot0 = fct.value(T0);
      material.setProperty("REFERENCE_STRAIN_RATE",epsDot0);
      if (os) {
        (*os) << "\n\treference strain rate temperature dependence: ";
        (*os) << fct << std::endl;
      }
    }
    catch (NoSuchPropertyException) {
      epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    }
    if (epsDot0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain rate must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain rate");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: reference strain rate is not defined." << std::endl;
    throw e;
  }

  // rate-dependency exponent
  z_real_t m;
  try {
    m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
    if (m <= 1.0) {
      if (os) (*os) << "ERROR: rate dependency exponent must be > 1." << std::endl;
      throw std::runtime_error("invalid property: rate dependency exponent");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: rate dependency exponent is not defined." << std::endl;
    throw e;
  }

  // print-out properties
  if (os) {
    (*os) << "\n\tAt reference temperature (T = " << T0 << "):" << std::endl;
    (*os) << "\treference stress               = " << sig0    << std::endl;
    (*os) << "\treference strain rate          = " << epsDot0 << std::endl;
    (*os) << "\trate dependency exponent       = " <<   m     << std::endl;
  }
}

// update properties in function of external parameters
void ThermalPowerLawRateDependencyModel::updateProperties(MaterialProperties& material,
                                                          const ConstitutiveModel::ParameterSet& extPar) {
  if (!extPar.count("TEMPERATURE")) return;
  z_real_t T = extPar.find("TEMPERATURE")->second;

  // reference stress
  z_real_t sig0;
  try {
    Function& fct = material.getFunctionProperty("REFERENCE_STRESS_EVOLUTION");
    sig0 = fct.value(T);
    material.setProperty("REFERENCE_STRESS",sig0);
  }
  catch (NoSuchPropertyException) {
    sig0 = material.getRealProperty("REFERENCE_STRESS");
  }
  if (sig0 == 0.0) return;
  if (sig0 < 0.0) throw std::runtime_error("invalid property: reference stress");

  // reference strain-rate
  z_real_t epsDot0;
  try {
    Function& fct = material.getFunctionProperty("REFERENCE_STRAIN_RATE_EVOLUTION");
    epsDot0 = fct.value(T);
    material.setProperty("REFERENCE_STRAIN_RATE",epsDot0);
  }
  catch (NoSuchPropertyException) {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
  }
  if (epsDot0 <= 0.0) throw std::runtime_error("invalid property: reference strain rate");
}

// dissipated energy
z_real_t ThermalPowerLawRateDependencyModel::dissipatedThMEnergy(const MaterialProperties& material,
                                                                 const ConstitutiveModel::ParameterSet& extPar,
                                                                 const MatLibArray& intPar0,
                                                                 MatLibArray& intPar1,
                                                                 z_real_t eps,z_real_t epsDot,z_real_t Th,
                                                                 z_real_t& sig1,z_real_t& sig2,z_real_t& N,
                                                                 z_real_t& h11,z_real_t& h22,z_real_t& C,
                                                                 z_real_t& h12,z_real_t& h1T,z_real_t& h2T,
                                                                 bool first,bool second) {
  // temperature
  z_real_t T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
  z_real_t T = T0+Th;

  // get material parameters
  z_real_t sig0,dSig0;
  try {
    Function& fct = material.getFunctionProperty("REFERENCE_STRESS_EVOLUTION");
    sig0 = fct.value(T,dSig0);
  }
  catch (NoSuchPropertyException) {
    sig0 = material.getRealProperty("REFERENCE_STRESS");
    dSig0 = 0.0;
  }
  if (sig0 == 0.0 || epsDot <= 0.0) {
    sig1 = sig2 = N = 0.0;
    h11 = h22 = h12 = 0.0;
    C   = h1T = h2T = 0.0;
    return 0.0;
  }
  z_real_t epsDot0,dEpsDot0;
  try {
    Function& fct = material.getFunctionProperty("REFERENCE_STRAIN_RATE_EVOLUTION");
    epsDot0 = fct.value(T,dEpsDot0);
  }
  catch (NoSuchPropertyException) {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    dEpsDot0 = 0.0;
  }
  z_real_t m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");

  // compute dissipation pseudo-potential and derivatives
  z_real_t phi;
  z_real_t expo = 1.0/m;
  z_real_t expo1 = expo+1;
  z_real_t coef = 1.0/expo1;
  z_real_t val = epsDot/epsDot0;
  if (first) {
    sig1 = 0.0;
    sig2 = sig0*std::pow(val,expo);
    N = coef*(dSig0/sig0*epsDot0-expo*dEpsDot0)*sig2*val;
    phi = coef*epsDot*sig2;
  }
  else {
    phi = coef*sig0*epsDot0*std::pow(val,expo1);
  }

  // compute second derivatives
  if (second) {
    h11 = h12 = h1T = 0.0;
    z_real_t val1 = std::pow(val,expo-1);
    h22 = expo*sig0/epsDot0*val1;
    h2T = (dSig0-expo*sig0*dEpsDot0/epsDot0)*val1*val;
    C = -2*coef*dSig0*dEpsDot0*expo*val1*val*val;
  }

  return phi;
}


/*
 * Methods for class ThermalASinhRateDependencyModel.
 */

// check consistency of material properties
void ThermalASinhRateDependencyModel::checkProperties(MaterialProperties& material,
                                                      std::ostream* os) {
  if (os) (*os) << "\n\t***ASinh rate dependency model (thermally-activated)***" << std::endl;

  // reference temperature
  z_real_t T0;
  try {
    T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
    if (T0 <= 0.0) {
      if (os) (*os) << "ERROR: reference temperature must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference temperature");
    }
  }
  catch (NoSuchPropertyException) {
    // use initial temperature
    try {
      T0 = material.getRealProperty("INITIAL_TEMPERATURE");
      if (T0 <= 0.0) {
        if (os) (*os) << "ERROR: initial temperature must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: initial temperature");
      }
      material.setProperty("REFERENCE_TEMPERATURE",T0);
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: reference temperature cannot be set." << std::endl;
      throw e;
    }
  }

  // reference stress
  z_real_t sig0;
  try {
    sig0 = material.getRealProperty("REFERENCE_STRESS");
  }
  catch (NoSuchPropertyException) {
    sig0 = 0.0;
    material.setProperty("REFERENCE_STRESS",0.0);
  }
  if (sig0 < 0.0) {
    if (os) (*os) << "ERROR: reference stress must be positive or zero." << std::endl;
    throw std::runtime_error("invalid property: reference stress");
  }
  else if (sig0 == 0.0) {
    if (os) (*os) << "\tnot defined (REFERENCE_STRESS = 0.0e0)" << std::endl;
    return;
  }

  // reference strain-rate
  z_real_t epsDot0;
  try {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    if (epsDot0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain rate must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain rate");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: reference strain rate is not defined." << std::endl;
    throw e;
  }

  // critical temperature
  z_real_t Tc;
  try {
    Tc = material.getRealProperty("CRITICAL_TEMPERATURE");
    if (Tc <= 0.0) {
      if (os) (*os) << "ERROR: critical temperature must be > 0." << std::endl;
      throw std::runtime_error("invalid property: critical temperature");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: critical temperature is not defined." << std::endl;
    throw e;
  }

  // intrinsic properties
  z_real_t sig1,epsDot1;
  z_real_t coef = Tc/T0;
  sig1 = coef*sig0;
  material.setProperty("INTRINSIC_REFERENCE_STRESS",sig1);
  epsDot1 = epsDot0*std::exp(coef);
  material.setProperty("INTRINSIC_REFERENCE_STRAIN_RATE",epsDot1);

  // print-out properties
  if (os) {
    (*os) << "\n\tAt reference temperature (T = " << T0 << "):" << std::endl;
    (*os) << "\treference stress                = " << sig0    << std::endl;
    (*os) << "\treference strain rate           = " << epsDot0 << std::endl;
    (*os) << "\tcritical temperature            = " <<   Tc    << std::endl;
    (*os) << "\tintrinsic reference stress      = " << sig1    << std::endl;
    (*os) << "\tintrinsic reference strain rate = " << epsDot1 << std::endl;
  }
}

// dissipated energy
z_real_t ThermalASinhRateDependencyModel::dissipatedThMEnergy(const MaterialProperties& material,
                                                              const ConstitutiveModel::ParameterSet& extPar,
                                                              const MatLibArray& intPar0,
                                                              MatLibArray& intPar1,
                                                              z_real_t eps,z_real_t epsDot,z_real_t Th,
                                                              z_real_t& sig1,z_real_t& sig2,z_real_t& N,
                                                              z_real_t& h11,z_real_t& h22,z_real_t& C,
                                                              z_real_t& h12,z_real_t& h1T,z_real_t& h2T,
                                                              bool first,bool second) {
  // temperature
  z_real_t T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
  z_real_t T = T0+Th;

  // get material parameters
  z_real_t sig0 = material.getRealProperty("INTRINSIC_REFERENCE_STRESS");
  z_real_t epsDot0 = material.getRealProperty("INTRINSIC_REFERENCE_STRAIN_RATE");
  z_real_t Tc = material.getRealProperty("CRITICAL_TEMPERATURE");

  // effective properties
  z_real_t val0 = 1.0/Tc;
  z_real_t val1 = 1.0/T;
  z_real_t coef0 = T*val0;
  z_real_t coef1 = std::exp(-Tc*val1);
  z_real_t sigE = coef0*sig0;
  z_real_t dSigE = sig0*val0;
  z_real_t epsDotE = coef1*epsDot0;
  z_real_t dEpsDotE = epsDotE*val1*val1*Tc;
  z_real_t d2EpsDotE = dEpsDotE*val1*(Tc*val1-2.0);

  // compute dissipation pseudo-potential and derivatives
  z_real_t phi;
  z_real_t val2 = epsDot/epsDotE;
  z_real_t val3 = std::sqrt(val2*val2+1.0);
  if (first) {
    sig1 = 0.0;
    sig2 = sigE*asinh(val2);
    phi = epsDotE*(val2*sig2-sigE*val3+sigE);
    N = dSigE/sigE*phi-sigE*dEpsDotE*(val3-1.0);
  }
  else {
    phi = sigE*epsDotE*(val2*asinh(val2)-val3+1.0);
  }

  // compute second derivatives
  if (second) {
    z_real_t val4 = dEpsDotE*val2;
    h11 = h12 = h1T = 0.0;
    h22 = sigE/(epsDotE*val3);
    h2T = dSigE*asinh(val2)-h22*val4;
    C = -(2*dSigE*dEpsDotE+sigE*d2EpsDotE)*(val3-1.0)+h22*val4*val4;
  }

  return phi;
}


/*
 * Methods for class ThermalNortonHoffRateDependencyModel.
 */

// check consistency of material properties
void ThermalNortonHoffRateDependencyModel::checkProperties(MaterialProperties& material,
                                                           std::ostream* os) {
  if (os) (*os) << "\n\t***Norton-Hoff rate dependency model (temperature-dependent)***" << std::endl;

  // reference temperature
  z_real_t T0;
  try {
    T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
    if (T0 <= 0.0) {
      if (os) (*os) << "ERROR: reference temperature must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference temperature");
    }
  }
  catch (NoSuchPropertyException) {
    // use initial temperature
    try {
      T0 = material.getRealProperty("INITIAL_TEMPERATURE");
      if (T0 <= 0.0) {
        if (os) (*os) << "ERROR: initial temperature must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: initial temperature");
      }
      material.setProperty("REFERENCE_TEMPERATURE",T0);
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: reference temperature cannot be set." << std::endl;
      throw e;
    }
  }

  // reference yield stress
  z_real_t sig0;
  try {
    sig0 = material.getRealProperty("REFERENCE_YIELD_STRESS");
  }
  catch (NoSuchPropertyException) {
    sig0 = 0.0;
    material.setProperty("REFERENCE_YIELD_STRESS",0.0);
  }
  if (sig0 < 0.0) {
    if (os) (*os) << "ERROR: reference yield stress must be positive or zero." << std::endl;
    throw std::runtime_error("invalid property: reference stress");
  }
  else if (sig0 == 0.0) {
    if (os) (*os) << "\tnot defined (REFERENCE_YIELD_STRESS = 0.0e0)" << std::endl;
    return;
  }

  // critical temperature
  z_real_t Tc;
  try {
    Tc = material.getRealProperty("CRITICAL_TEMPERATURE");
    if (Tc <= 0.0) {
      if (os) (*os) << "ERROR: critical temperature must be > 0." << std::endl;
      throw std::runtime_error("invalid property: critical temperature");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: critical temperature is not defined." << std::endl;
    throw e;
  }

  // reference strain-rate
  z_real_t epsDot0,m;
  try {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    if (epsDot0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain rate must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain rate");
    }
  }
  catch (NoSuchPropertyException e) {
    epsDot0 = 1.0;
    material.setProperty("REFERENCE_STRAIN_RATE",epsDot0);
  }

  try {
    m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
    if (m <= 1.0) {
      if (os) (*os) << "ERROR: rate dependency exponent must be > 1." << std::endl;
      throw std::runtime_error("invalid property: rate dependency exponent");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: rate dependency exponent is not defined." << std::endl;
    throw e;
  }

  // reference strain
  z_real_t eps0,n;
  try {
    eps0 = material.getRealProperty("REFERENCE_STRAIN_NH");
    if (eps0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: reference strain is not defined." << std::endl;
    throw e;
  }

  try {
    n = material.getRealProperty("HARDENING_EXPONENT_NH");
    if (n <= 1.0) {
      if (os) (*os) << "ERROR: hardening exponent must be > 1." << std::endl;
      throw std::runtime_error("invalid property: hardening exponent");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: hardening exponent is not defined." << std::endl;
    throw e;
  }

   // intrinsic properties
   z_real_t sig1;
   z_real_t coef = Tc/T0;
   sig1 = sig0*std::exp(-coef);
   material.setProperty("INTRINSIC_REFERENCE_STRESS",sig1);

  // print-out properties
  if (os) {
    (*os) << "\n\tAt reference temperature (T = " << T0 << "):" << std::endl;
    (*os) << "\treference yield stress          = " << sig0    << std::endl;
    (*os) << "\tcritical temperature            = " <<   Tc    << std::endl;
    (*os) << "\tintrinsic reference stress      = " << sig1    << std::endl;
    (*os) << "\treference strain rate           = " << epsDot0 << std::endl;
    (*os) << "\trate dependency exponent        = " <<   m     << std::endl;
    (*os) << "\treference strain                = " << eps0    << std::endl;
    (*os) << "\thardening exponent              = " <<   n     << std::endl;
  }
}

// dissipated energy
z_real_t ThermalNortonHoffRateDependencyModel::dissipatedThMEnergy(const MaterialProperties& material,
                                                                   const ConstitutiveModel::ParameterSet& extPar,
                                                                   const MatLibArray& intPar0,
                                                                   MatLibArray& intPar1,
                                                                   z_real_t eps,z_real_t epsDot,z_real_t Th,
                                                                   z_real_t& sig1,z_real_t& sig2,z_real_t& N,
                                                                   z_real_t& h11,z_real_t& h22,z_real_t& C,
                                                                   z_real_t& h12,z_real_t& h1T,z_real_t& h2T,
                                                                   bool first,bool second) {
  // temperature
  z_real_t T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
  z_real_t T = T0+Th;

  // get material parameters
  z_real_t sig0 = material.getRealProperty("INTRINSIC_REFERENCE_STRESS");
  if (sig0 == 0.0) {
    sig1 = 0.0;
    sig2 = 0.0;
    N = 0.0;
    h11 = h22 = h12 = 0.0;
    C = h1T = h2T = 0.0;
    return 0.0;
  }
  z_real_t Tc = material.getRealProperty("CRITICAL_TEMPERATURE");
  z_real_t coef = Tc/T;
  z_real_t sigE = sig0*std::exp(coef);
  z_real_t epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
  z_real_t m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
  z_real_t eps0 = material.getRealProperty("REFERENCE_STRAIN_NH");
  z_real_t n = material.getRealProperty("HARDENING_EXPONENT_NH");

  // compute dissipation pseudo-potential and derivatives
  z_real_t phi;
  z_real_t expo1 = 1.0/n;
  z_real_t expo2 = 1.0/m;
  z_real_t val1,val2,val11,val21;
  if (eps >= eps0) {
    val1 = eps/eps0;
    val11 = std::pow(val1,expo1);
  }
  else {
    val1 = 1.0;
    val11 = 1.0;
  }
  if (epsDot >= epsDot0) {
    val2 = epsDot/epsDot0;
    val21 = std::pow(val2,expo2);
  }
  else {
    val2 = 1.0;
    val21 = 1.0;
  }

  // pseudo-potential
  if (epsDot >= epsDot0)
    phi = sigE*val11*(epsDot*val21+epsDot0*expo2)/(expo2+1);
  else
    phi = sigE*val11*epsDot;
  if (!first && !second) return phi;

  // first derivative
  z_real_t coef1 = -coef/T;
  if (first) {
    if (eps >= eps0)
      sig1 = phi*expo1/eps;
    else
      sig1 = 0.0;
    sig2 = sigE*val11*val21;
    N = phi*coef1;
  }

  // compute second derivatives
  if (second) {
    if (eps >= eps0) {
      h11 = phi*expo1*(expo1-1)/(eps*eps);
      h12 = sigE*val11*val21*expo1/eps;
    }
    else {
      h11 = 0.0;
      h12 = 0.0;
    }
    h1T = sig1*coef1;
    if (epsDot >= epsDot0)
      h22 = sigE*val11*expo2/epsDot0*std::pow(val2,expo2-1.);
    else
      h22 = 0.0;
    h2T = sig2*coef1;
    C = N*coef1;
  }

  return phi;
}
