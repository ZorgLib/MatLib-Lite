/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "J2ThermoPlasticityMixed.h"

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for class LinearMixedJ2ThermoPlasticityBuilder.
 */

// the instance
LinearMixedJ2ThermoPlasticityBuilder const* LinearMixedJ2ThermoPlasticityBuilder::BUILDER
= new LinearMixedJ2ThermoPlasticityBuilder();

// constructor
LinearMixedJ2ThermoPlasticityBuilder::LinearMixedJ2ThermoPlasticityBuilder() {
  ModelDictionary::add("LINEAR_MIXED_J2_THERMO_PLASTICITY",*this);
}

// build model
ConstitutiveModel* LinearMixedJ2ThermoPlasticityBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
      return new LinearMixedJ2ThermoPlasticity3D();
      break;
    case 2:
      return new LinearMixedJ2ThermoPlasticity2D();
      break;
    case 1:
      return new LinearMixedJ2ThermoPlasticity1D();
      break;
    default:
      return 0;
      break;
  }
}

/*
 * Methods for class NonLinearMixedJ2ThermoPlasticityBuilder.
 */

// the instance
NonLinearMixedJ2ThermoPlasticityBuilder const* NonLinearMixedJ2ThermoPlasticityBuilder::BUILDER
= new NonLinearMixedJ2ThermoPlasticityBuilder();

// constructor
NonLinearMixedJ2ThermoPlasticityBuilder::NonLinearMixedJ2ThermoPlasticityBuilder() {
  ModelDictionary::add("NONLINEAR_MIXED_J2_THERMO_PLASTICITY",*this);
}

// build model
ConstitutiveModel* NonLinearMixedJ2ThermoPlasticityBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
      return new NonLinearMixedJ2ThermoPlasticity3D();
      break;
    case 2:
      return new NonLinearMixedJ2ThermoPlasticity2D();
      break;
    case 1:
      return new NonLinearMixedJ2ThermoPlasticity1D();
      break;
    default:
      return 0;
      break;
  }
}
