/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "ThermoViscoPlasticitySimple.h"

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


// constructor
StdThermoViscoPlasticitySimple::StdThermoViscoPlasticitySimple(ThermalIsotropicHardeningModel* h,
                                                               ThermalScalarRateDependencyModel* v) {
  count = new unsigned int(1);
  hardening = h;
  viscous = v;
}

// copy constructor
StdThermoViscoPlasticitySimple::StdThermoViscoPlasticitySimple(const StdThermoViscoPlasticitySimple& src) {
  count = src.count;
  (*count)++;
  hardening = src.hardening;
  viscous = src.viscous;
}

// destructor
StdThermoViscoPlasticitySimple::~StdThermoViscoPlasticitySimple() {
  if (--(*count) > 0) return;
  delete count;
  if (hardening) delete hardening;
  if (viscous) delete viscous;
}

// check consistency of material properties
void StdThermoViscoPlasticitySimple::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\n\t***Standard thermoviscoplasticity model (isotropic hardening)***" << std::endl;

  // look for algorithmic parameter
  z_real_t alpha = 0.5;
  try {
    alpha = material.getRealProperty("TVP_ALGORITHMIC_PARAMETER");
  }
  catch (NoSuchPropertyException) {
    material.setProperty("TVP_ALGORITHMIC_PARAMETER",alpha);
  }
  if (os) (*os) << "\talgorithmic parameter = " << alpha << std::endl;

  // rate-independent part (stored and dissipated)
  if (hardening) hardening->checkProperties(material,os);

  // dissipated viscous (rate-dependent) part
  if (viscous) viscous->checkProperties(material,os);
}

// update properties in function of external parameters
void StdThermoViscoPlasticitySimple::updateProperties(MaterialProperties& material,
                                                      const ConstitutiveModel::ParameterSet& extPar) {
  // rate-independent part (stored and dissipated)
  if (hardening) hardening->updateProperties(material,extPar);

  // dissipated viscous (rate-dependent) part
  if (viscous) viscous->updateProperties(material,extPar);
}

// number of internal parameters
unsigned int StdThermoViscoPlasticitySimple::nIntPar() const {
  unsigned int np = 2; // plastic stored energy + heat fraction

  // rate-independent part (stored and dissipated)
  if (hardening) np += hardening->nIntPar();

  // dissipated viscous (rate-dependent) part
  if (viscous) np += viscous->nIntPar();

  return np;
}

// compute irreversible energy and derivatives
z_real_t StdThermoViscoPlasticitySimple::irreversibleEnergy(const MaterialProperties& material,
                                                            const ConstitutiveModel::ParameterSet& extPar,
                                                            const MatLibArray& intPar0,MatLibArray& intPar,
                                                            z_real_t epsPl0,z_real_t epsPl1,z_real_t Th0,z_real_t Th1,
                                                            z_real_t T0,z_real_t T1,z_real_t& sig,z_real_t& Np,
                                                            z_real_t& dNp,z_real_t& h,z_real_t& dSig,z_real_t& C,
                                                            z_real_t dTime,bool first,bool second) {
  // get initial plastic stored energy
  z_real_t Wp0,Wp;
  Wp0 = intPar0[0];

  // get algorithmic parameter
  z_real_t alpha = material.getRealProperty("TVP_ALGORITHMIC_PARAMETER");
  z_real_t epsPl = (1.0-alpha)*epsPl0+alpha*epsPl1;
  z_real_t Th = (1.0-alpha)*Th0+alpha*Th1;
  z_real_t dT = Th1-Th0;

  // get temperature ratio(s)
  z_real_t ratio1 = dT/T0;
  z_real_t ratio0 = T1/T0;
  z_real_t ratio2 = dT/T1;
  z_real_t ratio3 = T0/T1;

  // compute hardening part (stored and dissipated energy)
  unsigned int nIntParHarden = 0;
  z_real_t sig0=0.0,h0=0.0,dSig0=0.0,C0=0.0,dummy;
  z_real_t Dp=0.0,sig1a=0.0,sig1b=0.0,h1a=0.0,h1b=0.0,dSig1=0.0;
  if (hardening) {
    nIntParHarden = hardening->nIntPar();
    const MatLibArray intP0Harden(intPar0,nIntParHarden,2);
    MatLibArray intP1Harden(intPar,nIntParHarden,2);
    // compute plastic stored energy
    Wp = hardening->storedThMEnergy(material,extPar,intP0Harden,intP1Harden,
                                    Wp0,epsPl0,epsPl1,Th1,sig0,Np,h0,dSig0,C0,
                                    first,second);
    // compute plastic dissipated energy
    sig1a = hardening->yieldThMStress(material,extPar,intP0Harden,intP1Harden,
                                      epsPl,Th0,h1a,dummy,first || second);
    sig1b = hardening->yieldThMStress(material,extPar,intP0Harden,intP1Harden,
                                      epsPl,Th,h1b,dSig1,first || second);
    Dp = (sig1a+ratio1*sig1b)*(epsPl1-epsPl0);
  }
  else {
    Wp = 0.0;
    if (first) Np = 0.0;
  }
  intPar[0] = Wp;

  // compute viscous (rate-dependent) dissipated energy
  unsigned int nIntParDiss = 0;
  z_real_t Dv=0.0,sig2=0.0,N2=0.0,h2=0.0,dSig2=0.0,C2=0.0;
  if (viscous && dTime > 0.0) {
    nIntParDiss = viscous->nIntPar();
    const MatLibArray intP0Diss(intPar0,nIntParDiss,2+nIntParHarden);
    MatLibArray intP1Diss(intPar,nIntParDiss,2+nIntParHarden);
    z_real_t epsPlDot = ratio0*(epsPl1-epsPl0)/dTime;
    z_real_t Dv1,Dv2,sigv1a,sigv2a,sigv1b,sigv2b,sigc;
    z_real_t hv1aa,hv2aa,hv1bb,hv2bb,hcc,hv1ab,hv2ab,hac,hbc;
    Dv1 = dTime*viscous->dissipatedThMEnergy(material,extPar,intP0Diss,intP1Diss,
                                             epsPl,epsPlDot,Th0,sigv1a,sigv1b,sigc,
                                             hv1aa,hv1bb,hcc,hv1ab,hac,hbc,
                                             first || second,second);
    Dv2 = dTime*viscous->dissipatedThMEnergy(material,extPar,intP0Diss,intP1Diss,
                                             epsPl,epsPlDot,Th,sigv2a,sigv2b,sigc,
                                             hv2aa,hv2bb,hcc,hv2ab,hac,hbc,
                                             first || second,second);
    Dv = ratio3*Dv1+ratio2*Dv2;
    z_real_t coef = alpha*dTime;
    if (first) {
      z_real_t val = sigv1b+ratio1*sigv2b;
      sig2 = coef*(ratio3*sigv1a+ratio2*sigv2a) + val;
      N2 = (val*(epsPl1-epsPl0) + coef*dT*sigc + dTime*ratio3*(Dv2-Dv1))/T1;
    }
    if (second) {
      h2 =  alpha*coef*(ratio3*hv1aa+ratio2*hv2aa) + 2*alpha*(hv1ab+ratio1*hv2ab)
          + ratio0*(hv1bb+ratio1*hv2bb)/dTime;
      dSig2 =  alpha*ratio2*(coef*hac + ratio0*hbc)
             + (coef*(hv1ab+ratio1*hv2ab) + ratio0*(hv1bb+ratio1*hv2bb))*epsPlDot*ratio3/T1
             + sigv2b/T0 + coef*ratio3*(sigv2a-sigv1a)/T1;
      C2 =  coef*ratio2*(2*hbc*epsPlDot/T1 + alpha*hcc)
          + 2*dTime*(alpha*sigc - (Dv2-Dv1)/T1)*ratio3/T1
          + ((hv1bb+ratio1*hv2bb)*epsPlDot + 2*(sigv2b-sigv1b))*(epsPl1-epsPl0)/(T1*T1);
    }
  }

  // assemble components
  if (first) {
    sig = sig0 + sig1a+ratio1*sig1b+alpha*(h1a+ratio1*h1b)*(epsPl1-epsPl0) + sig2;
    dNp = Np + (sig1b/T0+alpha*ratio1*dSig1)*(epsPl1-epsPl0) + N2;
  }
  if (second) {
    h = h0 + 2*alpha*(h1a+ratio1*h1b) + h2;
    dSig = dSig0 + alpha*ratio1*dSig1+(sig1b+alpha*h1b*(epsPl1-epsPl0))/T0 + dSig2;
    C = C0 + 2*alpha*dSig1*(epsPl1-epsPl0)/T0 + C2;
  }

  // compute heat fraction
  if (first) {
    z_real_t beta = 1.0;
    if (sig > 1.0e-16) beta = (sig-sig0)/sig;
    /*z_real_t Dt = (sig-sig0)*(epsPl1-epsPl0); // this version includes entropic effects
    z_real_t Wt = Wp-Wp0+Dt;
    if (Wt > 1.e-16) beta = Dt/Wt;*/
    intPar[1] = beta;
  }

  return Wp-Wp0+Dp+Dv;
}
