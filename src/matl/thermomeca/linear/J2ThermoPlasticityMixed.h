/*
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2023, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_THERMO_LINEAR_J2_PLASTICITY_MIXED_H
#define ZORGLIB_MATL_MECA_THERMO_LINEAR_J2_PLASTICITY_MIXED_H

// config
#include <matlib_macros.h>

// std C library
#include <cmath>
// std C++ library
#include <limits>
// local
#include <matl/thermomeca/linear/ThermoElastoPlasticity.h>
#include <matl/thermomeca/linear/ThermalHardeningModels.h>
#include <matl/thermomeca/linear/IsotropicThermoElasticity.h>
#include <matl/thermomeca/linear/ThermalRateDependencyModels.h>
#include <matl/thermomeca/linear/ThermoViscoPlasticitySimple.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * J2 thermo-plasticity with mixed hardening (isotropic + linear kinematic).
 */
template <class ALG>
class J2ThermoPlasticityMixed : virtual public ThermoElastoPlasticity<ALG> {

 public:

  // define new types
  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;

 protected:

  // associated thermo-visco-plasticity model
  ThermoViscoPlasticitySimple *viscoPlasticity;

  // empty constructor
  J2ThermoPlasticityMixed(ThermoViscoPlasticitySimple* vp = 0) {
    viscoPlasticity = vp;
  }

 public:

  // constructor
  J2ThermoPlasticityMixed(ThermoViscoPlasticitySimple& vp)
    : ThermoElasticity<ALG>(new IsotropicThermoElasticPotential<ALG>()) {viscoPlasticity = &vp;}

  // copy constructor
  J2ThermoPlasticityMixed(const J2ThermoPlasticityMixed& src)
    : ThermoElasticity<ALG>(src), ThermoElastoPlasticity<ALG>(src) {viscoPlasticity = src.viscoPlasticity;}

  // destructor
  virtual ~J2ThermoPlasticityMixed() {
    if (*(this->count) > 1) return;
    if (viscoPlasticity) delete viscoPlasticity;
  }

  // check consistency of properties
  void checkProperties(MaterialProperties& material,std::ostream *os = 0) {
     if (os) (*os) << "\nJ2 thermo-plasticity model with mixed hardening (small strains):" << std::endl;

    // density
    try {
      z_real_t rho = material.getRealProperty("MASS_DENSITY");
      if (rho <= 0.0) {
        if (os) (*os) << "ERROR: mass density must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: mass density");
      }
      if (os) (*os) << "\n\tmass density = " << rho << std::endl;
    }
    catch (NoSuchPropertyException) {
      if (os) (*os) << "\n\tmass density is not defined" << std::endl;
    }

    // elastic potential
    this->potential->checkProperties(material,os);

    // check capacity
    this->capacity->checkProperties(material,os);

    // dilatancy model
    if (this->dilatancy) this->dilatancy->checkProperties(material,os);

    // reference temperature
    try {
      z_real_t TRef = material.getRealProperty("REFERENCE_TEMPERATURE");
      if (TRef <= 0.0) {
        if (os) (*os) << "ERROR: reference temperature must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: reference temperature");
      }
      if (os) (*os) << "\treference temperature = " << TRef << std::endl;
    }
    catch (NoSuchPropertyException) {
      // use initial temperature
      try {
        z_real_t T0 = material.getRealProperty("INITIAL_TEMPERATURE");
        if (T0 <= 0.0) {
          if (os) (*os) << "ERROR: initial temperature must be strictly positive." << std::endl;
          throw std::runtime_error("invalid property: initial temperature");
        }
        material.setProperty("REFERENCE_TEMPERATURE",T0);
        if (os) (*os) << "\treference temperature = " << T0 << std::endl;
      }
      catch (NoSuchPropertyException e) {
        if (os) (*os) << "ERROR: reference temperature cannot be set." << std::endl;
        throw e;
      }
    }

    // kinematic hardening modulus
    z_real_t Hk;
    try {
      z_real_t T0 = material.getRealProperty("REFERENCE_TEMPERATURE");
      Function& fct = material.getFunctionProperty("KINEMATIC_HARDENING_MODULUS_EVOLUTION");
      Hk = fct.value(T0);
      material.setProperty("KINEMATIC_HARDENING_MODULUS",Hk);
    }
    catch (NoSuchPropertyException) {
      try {
        Hk = material.getRealProperty("KINEMATIC_HARDENING_MODULUS");
      }
      catch (NoSuchPropertyException e) {
        if (os) (*os) << "ERROR: kinematic hardening modulus is not defined." << std::endl;
        throw e;
      }
    }
    if (Hk < 0.0) {
      if (os) (*os) << "ERROR: kinematic hardening modulus must be positive." << std::endl;
      throw std::runtime_error("invalid property: kinematic hardening modulus");
    }

    // viscoplastic model
    try {
      viscoPlasticity->checkProperties(material,os);
    }
    catch (std::runtime_error& e) {
      if (!std::strcmp(e.what(),"invalid property: hardening modulus")) {
        if (os) (*os) << "       Check that condition is verified including kinematic hardening." << std::endl;
      }
      else
        throw e;
    }

    if (os) {
      (*os) << "\n\t***Linear kinematic hardening model***" << std::endl;
      (*os) << "\tkinematic hardening modulus = " << Hk << std::endl;
    }
  }

  // update properties in function of external parameters
  void updateProperties(MaterialProperties& mater,
                        const ConstitutiveModel::ParameterSet& extPar) {
    ThermoElasticity<ALG>::updateProperties(mater,extPar);
    viscoPlasticity->updateProperties(mater,extPar);
  }

  // number of internal variables
  unsigned int nIntVar() const {
    return SYM_TENSOR::MEMSIZE+4+viscoPlasticity->nIntPar();
  }

  // self-documenting utilities
  unsigned int nIntVarBundled() const {return 6;}
  unsigned int getIntVar(const std::string& str) const {
    if (str == "PSTN")
      return 0;
    else if (str == "EPLS")
      return 1;
    else if (str == "ENTP")
      return 2;
    else if (str == "ENRG")
      return 3;
    else if (str == "TNRG")
      return 4;
    else if (str == "PNRG")
      return 5;
    else
      return 6;
  }
  ConstitutiveModel::VariableType typeIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return ConstitutiveModel::TYPE_SYM_TENSOR;
        break;
      case 1:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 2:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 3:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 4:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 5:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      default:
        return ConstitutiveModel::TYPE_NONE;
        break;
    }
  }
  unsigned int indexIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return 0;
        break;
      case 1:
        return SYM_TENSOR::MEMSIZE;
        break;
      case 2:
        return SYM_TENSOR::MEMSIZE+1;
        break;
      case 3:
        return SYM_TENSOR::MEMSIZE+2;
        break;
      case 4:
        return SYM_TENSOR::MEMSIZE+3;
        break;
      case 5:
        return SYM_TENSOR::MEMSIZE+4;
        break;
      default:
        return SYM_TENSOR::MEMSIZE+5;
        break;
    }
  }
  std::string labelIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return "plastic strain";
        break;
      case 1:
        return "equivalent plastic strain";
        break;
      case 2:
        return "entropy";
        break;
      case 3:
        return "elastically stored energy";
        break;
      case 4:
        return "thermally stored energy";
        break;
      case 5:
        return "plastically stored energy";
        break;
      default:
        return "";
        break;
    }
  }

 protected:

  // compute the plastic update
  z_real_t plasticUpdate(const MaterialProperties& material,
                         const ConstitutiveModel::ParameterSet& extPar,
                         const SYM_TENSOR& eps,SYM_TENSOR& sig,z_real_t Th0,z_real_t Th1,
                         z_real_t& dN,const SYM_TENSOR& epsPl0,SYM_TENSOR& epsPl,
                         const MatLibArray& intV0,MatLibArray& intV,z_real_t dTime,
                         SYM_TENSOR4& M,SYM_TENSOR& dSig,z_real_t& C,
                         bool update,bool computeTangent) {

    static const z_real_t ONE_THIRD = 1.0/3.0;
    static const z_real_t TWO_THIRD = 2.0/3.0;
    static const SYM_TENSOR I = SYM_TENSOR::identity();

    z_real_t N;
    SYM_TENSOR epsEl,sigDev,Sig,Mp,backStress,backStress0;

    // compute reference temperatures (after linearization)
    z_real_t TRef = material.getRealProperty("REFERENCE_TEMPERATURE");
    z_real_t T0 = TRef;
    z_real_t T1 = TRef+Th1-Th0;

    // extract equivalent plastic strain
    z_real_t ePl0 = intV0[0];
    z_real_t ePl  = intV[0];

    // extract internal parameters
    unsigned int nIntPar = intV.size()-2;
    const MatLibArray intPar0(intV0,nIntPar,2);
    MatLibArray intPar(intV,nIntPar,2);

    // get current kinematic hardening modulus
    z_real_t T = TRef+Th1;
    z_real_t Hk,dHk;
    try {
      Function& fct = material.getFunctionProperty("KINEMATIC_HARDENING_MODULUS_EVOLUTION");
      Hk = fct.value(T,dHk);
    }
    catch (NoSuchPropertyException) {
      Hk = material.getRealProperty("KINEMATIC_HARDENING_MODULUS");
      dHk = 0.0;
    }
    if (Hk < 0.0) throw std::runtime_error("invalid property: kinematic hardening modulus");

    // compute elastic predictor
    z_real_t norm0=0.0,coef=0.0;
    if (update || computeTangent) {

      epsEl = eps-epsPl0;
      this->storedEnergy(material,extPar,epsEl,Th1,sig,N,
                         M,dSig,C,true,false);

      // compute stress deviator
      z_real_t p = trace(sig);
      sigDev = sig-(ONE_THIRD*p)*I;

      // compute initial backstress
      backStress = (TWO_THIRD*Hk)*contravariant(epsPl0);

      // compute radial return direction
      Sig = sigDev-backStress;
      norm0 = innerProd2(Sig,Sig);
      if (norm0 >= 1.0e-16) coef = std::sqrt(1.5/norm0);
      Mp = coef*Sig;
    }

    // update
    viscoPlasticity->initialize = true;
    viscoPlasticity->finalize = false;
    z_real_t dEPl=0.0;
    if (update) {
      // perform update (radial return)
      radialReturn(material,extPar,*viscoPlasticity,
                   intPar0,intPar,Sig,ePl0,ePl,Mp,
                   Th0,Th1,T0,T1,dTime);

      // update internal variables
      intV[0] = ePl;

      // update plastic strain
      dEPl = ePl-ePl0;
      epsPl = epsPl0+dEPl*covariant(Mp);

      viscoPlasticity->finalize = true;
    }

    // elastic deformation
    epsEl = eps-epsPl;

    // elastic free energy
    z_real_t We = this->storedEnergy(material,extPar,epsEl,Th1,
                                     sig,N,M,dSig,C,
                                     update || computeTangent,
                                     computeTangent);
    if (update) intV[2] = We;

    // thermal capacity
    if (this->capacity) {
      z_real_t NT,CT;
      z_real_t WT = this->capacity->freeEnergy(material,extPar,Th1,NT,
                                               CT,update,computeTangent);
      We += WT;
      if (update) {
        intV[3] = WT;
        N += NT;
      }
      if (computeTangent) C += CT;
    }

    // plastic free energy increment + dissipated energy
    z_real_t dummy,Np,dNp,Hp,dSigPl,Cp;
    z_real_t Wp = viscoPlasticity->irreversibleEnergy(material,extPar,
                                                      intPar0,intPar,ePl0,ePl,Th0,Th1,
                                                      T0,T1,dummy,Np,dNp,Hp,dSigPl,Cp,
                                                      dTime,update,computeTangent);
    // compute backstress and contribution to free energy
    z_real_t val = ONE_THIRD*innerProd(contravariant(epsPl),epsPl);
    z_real_t WpKin = Hk*val;
    intPar[0] += WpKin;

    // update
    if (update) {
      z_real_t NpKin = dHk*val;
      dN = N+dNp+intV0[1];
      intV[1] = -N-Np-NpKin;
    }

    // tangents
    if (computeTangent) {

      // capacity
      C += Cp;

      dEPl = ePl-ePl0;
      if (dEPl > 0.0) {

        // get current shear modulus
        z_real_t mu;
        try {
          z_real_t E,nu;
          try { // get Young's modulus
            Function& fctE = material.getFunctionProperty("YOUNG_MODULUS_EVOLUTION");
            E = fctE.value(T);
          }
          catch (NoSuchPropertyException) {
            E = material.getRealProperty("YOUNG_MODULUS");
          }
          if (E < 0.0) throw std::runtime_error("invalid property: Young's modulus");

          try { // get Poisson's coefficient
            Function& fctN = material.getFunctionProperty("POISSON_COEFFICIENT_EVOLUTION");
            nu = fctN.value(T);
          }
          catch (NoSuchPropertyException) {
            nu = material.getRealProperty("POISSON_COEFFICIENT");
          }
          if (nu < -1.0 || nu > 0.5) throw std::runtime_error("invalid property: Poisson's coefficient");
          mu = 0.5*E/(1.+nu);
        }
        catch (NoSuchPropertyException) {
          mu = material.getRealProperty("SHEAR_MODULUS");
        }
        z_real_t mu2=2*mu,mu3=3*mu;

        // (visco)plastic correction
        static const SYM_TENSOR4 II = SYM_TENSOR4::contravariantIdentity();
        static const SYM_TENSOR4 KK = SYM_TENSOR4::baseK();
        z_real_t coef1 = mu2*dEPl*coef;
        z_real_t coef2 = mu2/(mu3+Hp+Hk);
        z_real_t coef3 = 4*ONE_THIRD*mu*(1.5*coef2-coef1);
        M -= ((coef1*mu2)*(II-KK)+coef3*outerProd(Mp,Mp));

        SYM_TENSOR dSigDev = dSig-(ONE_THIRD*trace(dSig))*I;
        z_real_t dEPldT = dSigPl-innerProd2(dSigDev-(TWO_THIRD*dHk)*contravariant(epsPl0),Mp);
        dSig += coef2*dEPldT*Mp;

        C -= dEPldT*dEPldT/(mu3+Hp+Hk);
      }
    }

    return We+Wp+WpKin-ONE_THIRD*Hk*innerProd(contravariant(epsPl0),epsPl0)
          -intV0[2]-intV0[3]+intV0[1]*(Th1-Th0);
  }

 public:

  // radial return algorithm
  static unsigned int radialReturn(const MaterialProperties& material,
                                   const ConstitutiveModel::ParameterSet& extPar,
                                   ThermoViscoPlasticitySimple& viscoPlasticity,
                                   const MatLibArray& intPar0,MatLibArray& intPar,
                                   const SYM_TENSOR& sigDev,z_real_t ePl0,z_real_t& ePl,
                                   const SYM_TENSOR& Mp,z_real_t Th0,z_real_t Th1,
                                   z_real_t T0,z_real_t T1,z_real_t dTime) {

    static const unsigned int ITMAX = 30;
    static const z_real_t MULT = 0.9;
    static const z_real_t PREC = 1.0e-12;
    static const z_real_t TOLE = 1.0e-07;
    static const z_real_t THRSHLD = 0.1*std::numeric_limits<z_real_t>::max();

    // get algorithmic parameter
    unsigned int maxIt;
    if (material.checkProperty("RR_MAX_ITER_PARAMETER"))
      maxIt = material.getIntegerProperty("RR_MAX_ITER_PARAMETER");
    else
      maxIt = ITMAX;

    // compute test function
    z_real_t sigEq = innerProd2(sigDev,Mp);
    ePl = ePl0;
    z_real_t sigPl,Np,dNp,Hp,dSigPl,Cp;
    viscoPlasticity.irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                       Th0,Th1,T0,T1,sigPl,Np,dNp,Hp,dSigPl,Cp,
                                       dTime,true,true);
    z_real_t fct0 = sigEq-sigPl;
    if (fct0 <= 0.0) return 0;

    // get current shear modulus
    z_real_t TRef = material.getRealProperty("REFERENCE_TEMPERATURE");
    z_real_t T = TRef+Th1;
    z_real_t mu;
    try {
      z_real_t E,nu;
      try { // get Young's modulus
        Function& fctE = material.getFunctionProperty("YOUNG_MODULUS_EVOLUTION");
        E = fctE.value(T);
      }
      catch (NoSuchPropertyException) {
        E = material.getRealProperty("YOUNG_MODULUS");
      }
      if (E < 0.0) throw std::runtime_error("invalid property: Young's modulus");

      try { // get Poisson's coefficient
        Function& fctN = material.getFunctionProperty("POISSON_COEFFICIENT_EVOLUTION");
        nu = fctN.value(T);
      }
      catch (NoSuchPropertyException) {
        nu = material.getRealProperty("POISSON_COEFFICIENT");
      }
      if (nu < -1.0 || nu > 0.5) throw std::runtime_error("invalid property: Poisson's coefficient");
      mu = 0.5*E/(1.+nu);
    }
    catch (NoSuchPropertyException) {
      mu = material.getRealProperty("SHEAR_MODULUS");
    }
    z_real_t mu3=3*mu;

    // get current kinematic hardening modulus
    z_real_t Hk;
    try {
      Function& fctH = material.getFunctionProperty("KINEMATIC_HARDENING_MODULUS_EVOLUTION");
      Hk = fctH.value(T);
    }
    catch (NoSuchPropertyException) {
      Hk = material.getRealProperty("KINEMATIC_HARDENING_MODULUS");
    }
    if (Hk < 0.0) throw std::runtime_error("invalid property: kinematic hardening modulus");

    // apply plastic corrector
    z_real_t mu3h=mu3+Hk;
    z_real_t dEPl = 0.0;
    z_real_t ePl00 = ePl0;
    z_real_t fct = fct0;
    z_real_t fct00 = fct0;
    z_real_t test = TOLE*(fct+TOLE);
    unsigned int iter=0;
    for (; iter < maxIt; iter++) {
      z_real_t coef = 1.0/(mu3h+Hp);
      if (std::fabs(Hp) < THRSHLD && !std::isnan(coef))
        dEPl = fct*coef;
      else
        dEPl = fct/mu3h;
      if (std::fabs(dEPl) < PREC) break;
      if ((ePl+dEPl) < (ePl00+PREC)) { /* use secant method */
        z_real_t mult = fct/(fct00-fct);
        if (mult < -MULT) mult=-MULT;
        dEPl = mult*(ePl-ePl00);
      }
      if (std::fabs(dEPl) < PREC) break;
      sigEq -= dEPl*mu3h;
      ePl += dEPl;
      viscoPlasticity.irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                         Th0,Th1,T0,T1,sigPl,Np,dNp,Hp,dSigPl,Cp,
                                         dTime,true,true);
      fct = sigEq-sigPl;
      if (std::fabs(fct) < test) break;
      if (fct > 0.0 && dEPl < 0.0) {
        fct00 = fct;
        ePl00 = ePl;
      }
    }
    // check convergence
    if (iter == maxIt) {
      throw UpdateFailedException("no convergence in radial return");
    }

    return iter;
  }
};


/**
 * Implementations of the model.
 */

/**
 * J2 thermo-plasticity with linear mixed hardening.
 */
class LinearMixedJ2ThermoPlasticity3D : public J2ThermoPlasticityMixed<TensorAlgebra3D> {

 public:

  // constructor
  LinearMixedJ2ThermoPlasticity3D()
  : ThermoElasticity<TensorAlgebra3D>(new IsotropicThermoElasticPotential<TensorAlgebra3D>(),
                                      new StdLinThermalCapacity(),
                                      new IsotropicThermoElasticDilatancy<TensorAlgebra3D>()),
    J2ThermoPlasticityMixed<TensorAlgebra3D>(
                new StdThermoViscoPlasticitySimple(new ThermalLinearIsotropicHardeningModel(),
                                                   new ThermalPowerLawRateDependencyModel())) {}

  // copy constructor
  LinearMixedJ2ThermoPlasticity3D(const LinearMixedJ2ThermoPlasticity3D& src)
  : ThermoElasticity<TensorAlgebra3D>(src), ThermoElastoPlasticity<TensorAlgebra3D>(src),
    J2ThermoPlasticityMixed<TensorAlgebra3D>(src) {}

  // destructor
  virtual ~LinearMixedJ2ThermoPlasticity3D() {}
};
class LinearMixedJ2ThermoPlasticity2D : public J2ThermoPlasticityMixed<TensorAlgebra2D> {

 public:

  // constructor
  LinearMixedJ2ThermoPlasticity2D()
  : ThermoElasticity<TensorAlgebra2D>(new IsotropicThermoElasticPotential<TensorAlgebra2D>(),
                                      new StdLinThermalCapacity(),
                                      new IsotropicThermoElasticDilatancy<TensorAlgebra2D>()),
    J2ThermoPlasticityMixed<TensorAlgebra2D>(
                new StdThermoViscoPlasticitySimple(new ThermalLinearIsotropicHardeningModel(),
                                                   new ThermalPowerLawRateDependencyModel())) {}

  // copy constructor
  LinearMixedJ2ThermoPlasticity2D(const LinearMixedJ2ThermoPlasticity2D& src)
  : ThermoElasticity<TensorAlgebra2D>(src), ThermoElastoPlasticity<TensorAlgebra2D>(src),
    J2ThermoPlasticityMixed<TensorAlgebra2D>(src) {}

  // destructor
  virtual ~LinearMixedJ2ThermoPlasticity2D() {}
};
class LinearMixedJ2ThermoPlasticity1D : public J2ThermoPlasticityMixed<TensorAlgebra1D> {

 public:

  // constructor
  LinearMixedJ2ThermoPlasticity1D()
  : ThermoElasticity<TensorAlgebra1D>(new IsotropicThermoElasticPotential<TensorAlgebra1D>(),
                                      new StdLinThermalCapacity(),
                                      new IsotropicThermoElasticDilatancy<TensorAlgebra1D>()),
    J2ThermoPlasticityMixed<TensorAlgebra1D>(
                new StdThermoViscoPlasticitySimple(new ThermalLinearIsotropicHardeningModel(),
                                                   new ThermalPowerLawRateDependencyModel())) {}

  // copy constructor
  LinearMixedJ2ThermoPlasticity1D(const LinearMixedJ2ThermoPlasticity1D& src)
  : ThermoElasticity<TensorAlgebra1D>(src), ThermoElastoPlasticity<TensorAlgebra1D>(src),
    J2ThermoPlasticityMixed<TensorAlgebra1D>(src) {}

  // destructor
  virtual ~LinearMixedJ2ThermoPlasticity1D() {}
};

/**
 * The associated model builder
 */
class LinearMixedJ2ThermoPlasticityBuilder : public ModelBuilder {

 private:

  // constructor
  LinearMixedJ2ThermoPlasticityBuilder();

  // the instance
  static LinearMixedJ2ThermoPlasticityBuilder const* BUILDER;

 public:

  // destructor
  virtual ~LinearMixedJ2ThermoPlasticityBuilder() {}

  // build model
  ConstitutiveModel* build(unsigned int) const;
};

/**
 * J2 thermo-plasticity with linear kinematic hardening + non-linear isotropic hardening.
 */
class NonLinearMixedJ2ThermoPlasticity3D : public J2ThermoPlasticityMixed<TensorAlgebra3D> {

 public:

  // constructor
  NonLinearMixedJ2ThermoPlasticity3D()
  : ThermoElasticity<TensorAlgebra3D>(new IsotropicThermoElasticPotential<TensorAlgebra3D>(),
                                      new StdLinThermalCapacity(),
                                      new IsotropicThermoElasticDilatancy<TensorAlgebra3D>()),
    J2ThermoPlasticityMixed<TensorAlgebra3D>(
                new StdThermoViscoPlasticitySimple(new ThermalNonLinearIsotropicHardeningModel(),
                                                   new ThermalPowerLawRateDependencyModel())) {}

  // copy constructor
  NonLinearMixedJ2ThermoPlasticity3D(const NonLinearMixedJ2ThermoPlasticity3D& src)
  : ThermoElasticity<TensorAlgebra3D>(src), ThermoElastoPlasticity<TensorAlgebra3D>(src),
    J2ThermoPlasticityMixed<TensorAlgebra3D>(src) {}

  // destructor
  virtual ~NonLinearMixedJ2ThermoPlasticity3D() {}
};
class NonLinearMixedJ2ThermoPlasticity2D : public J2ThermoPlasticityMixed<TensorAlgebra2D> {

 public:

  // constructor
  NonLinearMixedJ2ThermoPlasticity2D()
  : ThermoElasticity<TensorAlgebra2D>(new IsotropicThermoElasticPotential<TensorAlgebra2D>(),
                                      new StdLinThermalCapacity(),
                                      new IsotropicThermoElasticDilatancy<TensorAlgebra2D>()),
    J2ThermoPlasticityMixed<TensorAlgebra2D>(
                new StdThermoViscoPlasticitySimple(new ThermalNonLinearIsotropicHardeningModel(),
                                                   new ThermalPowerLawRateDependencyModel())) {}

  // copy constructor
  NonLinearMixedJ2ThermoPlasticity2D(const NonLinearMixedJ2ThermoPlasticity2D& src)
  : ThermoElasticity<TensorAlgebra2D>(src), ThermoElastoPlasticity<TensorAlgebra2D>(src),
    J2ThermoPlasticityMixed<TensorAlgebra2D>(src) {}

  // destructor
  virtual ~NonLinearMixedJ2ThermoPlasticity2D() {}
};
class NonLinearMixedJ2ThermoPlasticity1D : public J2ThermoPlasticityMixed<TensorAlgebra1D> {

 public:

  // constructor
  NonLinearMixedJ2ThermoPlasticity1D()
  : ThermoElasticity<TensorAlgebra1D>(new IsotropicThermoElasticPotential<TensorAlgebra1D>(),
                                      new StdLinThermalCapacity(),
                                      new IsotropicThermoElasticDilatancy<TensorAlgebra1D>()),
    J2ThermoPlasticityMixed<TensorAlgebra1D>(
                new StdThermoViscoPlasticitySimple(new ThermalNonLinearIsotropicHardeningModel(),
                                                   new ThermalPowerLawRateDependencyModel())) {}

  // copy constructor
  NonLinearMixedJ2ThermoPlasticity1D(const NonLinearMixedJ2ThermoPlasticity1D& src)
  : ThermoElasticity<TensorAlgebra1D>(src), ThermoElastoPlasticity<TensorAlgebra1D>(src),
    J2ThermoPlasticityMixed<TensorAlgebra1D>(src) {}

  // destructor
  virtual ~NonLinearMixedJ2ThermoPlasticity1D() {}
};

/**
 * The associated model builder
 */
class NonLinearMixedJ2ThermoPlasticityBuilder : public ModelBuilder {

 private:

  // constructor
  NonLinearMixedJ2ThermoPlasticityBuilder();

  // the instance
  static NonLinearMixedJ2ThermoPlasticityBuilder const* BUILDER;

 public:

  // destructor
  virtual ~NonLinearMixedJ2ThermoPlasticityBuilder() {}

  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
