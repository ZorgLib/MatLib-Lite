/*
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2023, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MATERIAL_MODEL_H
#define ZORGLIB_MATL_MATERIAL_MODEL_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include "matl/ConstitutiveModel.h"
#include "matl/MaterialProperties.h"
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Define ParameterSet type outside of classes, for convenience
 */
typedef ConstitutiveModel::ParameterSet ParameterSet;

/**
 * Class constituting a complete material model:
 *   constitutive model + material properties
 */
class MaterialModel {

 public:
 
  // alias to ConstitutiveModel
  typedef ConstitutiveModel Model;

 private:

  // constitutive model
  ConstitutiveModel* modl;

  // material properties
  MaterialProperties* prop;

 public:

  // constructors
  MaterialModel(ConstitutiveModel&,const std::string&);
  MaterialModel(ConstitutiveModel&,MaterialProperties&);

  // copy constructor
  MaterialModel(const MaterialModel&);

  // destructor
  ~MaterialModel();

  // assignment operator
  MaterialModel& operator=(const MaterialModel&);

  // get constitutive model
  ConstitutiveModel& model() const {return *modl;}

  // set constitutive model
  void setModel(ConstitutiveModel& m) {modl = &m;}

  // get material properties
  MaterialProperties& properties() const {return *prop;}

  // set material properties
  void setProperties(MaterialProperties& p);

  // initialize material model
  void initialize(std::ostream*);
  void initialize(const char* = 0);

  // rotate material properties
  void rotateProperties(const Rotation&);
  
  // update material properties
  void updateProperties(const ParameterSet&);
  
  // how many external variables ?
  unsigned int nExtVar() const;
  
  // how many internal variables ?
  unsigned int nIntVar() const;

  // check if the material behaviour is linear ?
  bool isLinear() const;
  
  // check if the material is "standard" ?
  bool isVariational() const;

  // initialize the state(s) of the material
  void initState(MaterialState&);
  void initStates(MaterialState&,MaterialState&);

  // update the state of the material (with the ability to compute tangents)
  void updateState(const ParameterSet&,const MaterialState&,MaterialState&,
                   z_real_t,MatLibMatrix&,bool);
  
  // compute material tangents (without updating)
  void computeTangent(const ParameterSet&,const MaterialState&,MaterialState&,
                      z_real_t,MatLibMatrix&);
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
