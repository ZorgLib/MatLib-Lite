/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_HYPER_ELASTICITY_H
#define ZORGLIB_MATL_MECA_HYPER_ELASTICITY_H

// config
#include <matlib_macros.h>

// std C library
#include <cmath>
// local
#include <matl/ConstitutiveModel.h>
#include <matl/meca/eos/EOS.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for hyperelasticity models.
 */
template <class ALG>
class HyperElasticity : virtual public StandardMaterial {

 public:

  // define new types
  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::Tensor     TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;
  typedef typename ALG::Tensor4    TENSOR4;

  // nested classes
  class Potential;
  class Dilatancy;
  
 protected:
  
  // associated potential
  Potential *potential;
  
  // associated equation-of-state
  EOS *eos;

  // associated dilatancy
  Dilatancy *dilatancy;

  // instance counter
  unsigned int *count;
  
  // empty constructor
  HyperElasticity(Potential* p = 0,EOS* e = 0,Dilatancy* d = 0) {
    count = new unsigned int(1);
    potential = p;
    eos       = e;
    dilatancy = d;
  }
  
 public:
    
  // constructors
  HyperElasticity(Potential& p) {
    count = new unsigned int(1);
    potential = &p;
    eos = 0;
    dilatancy = 0;
  }
  HyperElasticity(Potential& p,EOS& e) {
    count = new unsigned int(1);
    potential = &p;
    eos = &e;
    dilatancy = 0;
  }
  HyperElasticity(Potential& p,Dilatancy& d) {
    count = new unsigned int(1);
    potential = &p;
    eos = 0;
    dilatancy = &d;
  }
  HyperElasticity(Potential& p,EOS& e,Dilatancy& d) {
    count = new unsigned int(1);
    potential = &p;
    eos = &e;
    dilatancy = &d;
  }
  
  // copy constructor
  HyperElasticity(const HyperElasticity& src) {
    count = src.count;
    (*count)++;
    potential = src.potential;
    eos = src.eos;
    dilatancy = src.dilatancy;
  }
  
  // destructor
  virtual ~HyperElasticity() {
    if (--(*count) > 0) return;
    delete count;
    if (potential) delete potential;
    if (eos) delete eos;
    if (dilatancy) delete dilatancy;
  }

  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    if (os) (*os) << "\nHyperelastic material:" << std::endl;

    // density
    try {
      z_real_t rho = material.getRealProperty("MASS_DENSITY");
      if (rho <= 0.0) {
        if (os) (*os) << "ERROR: mass density must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: mass density");
      }
      if (os) (*os) << "\n\tmass density = " << rho << std::endl;
    }
    catch (NoSuchPropertyException) {
      if (os) (*os) << "\n\tmass density is not defined" << std::endl;
    }

    // check potential
    if (potential) potential->checkProperties(material,os);

    // eos
    if (eos) eos->checkProperties(material,os);

    // check dilatancy
    if (dilatancy) dilatancy->checkProperties(material,os);
  }
  
  // apply rotation to material properties
  void rotateProperties(MaterialProperties& material,const Rotation& R) {
    if (potential) potential->rotateProperties(material,R);
    if (dilatancy) dilatancy->rotateProperties(material,R);
  }
  
  // update properties in function of external parameters
  void updateProperties(MaterialProperties& mater,
                        const ConstitutiveModel::ParameterSet& extPar) {
    if (potential) potential->updateProperties(mater,extPar);
    if (eos) eos->updateProperties(mater,extPar);
    if (dilatancy) dilatancy->updateProperties(mater,extPar);
  }
  
  // how many external variables ?
  unsigned int nExtVar() const {return TENSOR::MEMSIZE;}
  
  // self-documenting utilities
  unsigned int nExtVarBundled() const {return 1;}
  ConstitutiveModel::VariableType typeExtVar(unsigned int i) const {
    switch (i) {
      case 0:
        return ConstitutiveModel::TYPE_TENSOR;
        break;
      default:
        return ConstitutiveModel::TYPE_NONE;
        break;
    }
  }
  unsigned int indexExtVar(unsigned int i) const {
    switch (i) {
      case 0:
        return 0;
        break;
      default:
        return TENSOR::MEMSIZE;
        break;
    }
  }
  std::string labelExtVar(unsigned int i) const {
    switch (i) {
      case 0:
        return "deformation gradient";
        break;
      default:
        return "";
        break;
    }
  }
  std::string labelExtForce(unsigned int i) const {
    switch (i) {
      case 0:
        return "Piola stress";
        break;
      default:
        return "";
        break;
    }
  }

  // how many internal variables ?
  unsigned int nIntVar() const {return 1;}
  
  // self-documenting utilities
  unsigned int nIntVarBundled() const {return 1;}
  unsigned int getIntVar(const std::string& str) const {
    if (str == "ENRG")
      return 0;
    else
      return 1;
  }
  ConstitutiveModel::VariableType typeIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      default:
        return ConstitutiveModel::TYPE_NONE;
        break;
    }
  }
  unsigned int indexIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return 0;
        break;
      default:
        return 1;
        break;
    }
  }
  std::string labelIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return "elastically stored energy";
        break;
      default:
        return "";
        break;
    }
  }
  
  // initialize the state of the material
  void initState(const MaterialProperties& material,MaterialState& state) {
    ConstitutiveModel::initState(material,state);
    state.grad = TENSOR::identity();
    state.flux = 0.e0;
    state.internal = 0.e0;
  }
  
  // compute the incremental potential
  z_real_t incrementalPotential(const MaterialProperties& material,
                                const ConstitutiveModel::ParameterSet& extPar,
                                const MaterialState& state0,MaterialState& state,
                                z_real_t dTime,MatLibMatrix& T,
                                bool update,bool tangent) {
    
    // get tensors
    TENSOR F(state.grad);
    TENSOR P(state.flux);
    TENSOR4 K(T);

    // right Cauchy-Green tensor
    SYM_TENSOR C;
    ALG::RightCauchyGreen(F,C);
    
    // compute stored energy
    SYM_TENSOR S;
    SYM_TENSOR4 M;
    z_real_t W = storedEnergy(material,extPar,C,S,M,update || tangent,tangent);
    state.internal[0] = W;
    
    // compute Piola tensor and Lagrangian tangents
    if (update) ALG::PK2ToPK1(S,F,P);
    if (tangent) ALG::MaterialToLagrangian(M,S,F,K);

    return W-state0.internal[0];
  }

 protected:

  // compute stored energy, accounting for EOS
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& C,SYM_TENSOR& S,SYM_TENSOR4& M,
                        bool first,bool second) {
    z_real_t W = 0.e0;
    
    static const z_real_t ONE_THIRD = 1./3.;
    static const z_real_t TWO_THIRD = 2./3.;
    
    // if there is an e.o.s.
    if (potential && eos) {
      
      // compute distortion strain
      z_real_t J;
      SYM_TENSOR Cinv,Cbar;
      if (first || second)
        Cinv = C.inverse(J);
      else
        J = determinant(C);
      if (J < 1.0e-16)
	      throw UpdateFailedException("zero jacobian (det[C])");
      J = std::sqrt(J);
      z_real_t coef = std::pow(J,-TWO_THIRD);
      Cbar = coef*C;
      
      // deviatoric part
      SYM_TENSOR Sbar;
      SYM_TENSOR4 Mbar;
      W = potential->storedEnergy(material,extPar,Cbar,Sbar,Mbar,
                                  first,second);
      
      // volumic part
      z_real_t p,K;
      W += eos->storedEnergy(material,extPar,J,p,K,first || second,second);

      // add contributions
      z_real_t press=0.e0,trS=0.e0;
      if (first || second) {
        
        // Lagrangian pressure
        press = J*p;
        
        // compute stress (Lagrangian) trace
        trS = ONE_THIRD*innerProd2(Sbar,C);
        
        // compute stresses (PK2)
        S = coef*Sbar+(press-coef*trS)*Cinv;
      }

      if (second) {
        z_real_t coef1 = TWO_THIRD*coef;
        z_real_t coef2 = coef*coef;
        
        SYM_TENSOR tmp = ONE_THIRD*innerProd2(Mbar,C);
        z_real_t CMC = ONE_THIRD*innerProd2(C,tmp);
        
        z_real_t val1 = coef*trS-press;
        z_real_t val2 = press+K*J*J;
        M = coef2*(Mbar-outerProd(Cinv,tmp)-outerProd(tmp,Cinv))
           -coef1*(outerProd(Cinv,Sbar)+outerProd(Sbar,Cinv))
           +(coef2*CMC+coef1*trS+val2)*outerProd(Cinv,Cinv);
        M.addIJKL(val1,Cinv);
      }
    }
    // no e.o.s.
    else if (potential) {
      W = potential->storedEnergy(material,extPar,C,S,M,first,second);
    }
    // e.o.s. only (does not really make sense at this level)
    else if (eos) {
      // TO DO
    }
    else {
      if (first) S = 0.0;
      if (second) M = 0.0;
    }
    
    // dilatancy term (part of this may also be included in the e.o.s.)
    if (dilatancy) {
      SYM_TENSOR ST;
      SYM_TENSOR4 MT;
      W += dilatancy->couplingEnergy(material,extPar,C,ST,MT,first,second);
      if (first) S += ST;
      if (second) M += MT;
    }
    
    return W;
  }
};  


/**
 * Base class for hyperelastic potentials.
 */
template <class ALG>
class HyperElasticity<ALG>::Potential {

 protected:
  
  // constructor
  Potential() {}

 public:

  // destructor
  virtual ~Potential() {}

  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // apply rotation to material properties
  virtual void rotateProperties(MaterialProperties&,const Rotation&) {}
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&) {}
  
  // compute stored energy
  virtual z_real_t storedEnergy(const MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&,
                                const SYM_TENSOR&,SYM_TENSOR&,
                                SYM_TENSOR4&,bool,bool) = 0;
};


/**
 * Base class for (isotropic) hyperelastic potentials,
 * which are function of principal stretches.
 */
template <class ALG>
class SpectralHEPotential : virtual public HyperElasticity<ALG>::Potential {

 public:
  
  // define new types
  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;
  
 protected:
  
  // constructor
  SpectralHEPotential() {}
  
 public:
  
  // destructor
  virtual ~SpectralHEPotential() {}
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& C,SYM_TENSOR& S,
                        SYM_TENSOR4& M,bool first,bool second) {
    
    // compute principal stretches
    z_real_t lambda[3],t[3],h[3][3];
    SYM_TENSOR N[3];
    C.eigenSplit(lambda,N);
    
    // compute stored energy
    z_real_t W = storedEnergy(material,extPar,lambda,t,h,
                              first || second,second);
    
    // stresses
    if (first) {
      S = 0.0;
      for (unsigned int k=0; k < 3; k++) S += (2*t[k])*N[k];
    }
    
    // tangents
    if (second) {
      M = 0.0;
      for (unsigned int k=0; k < 3; k++)
        for (unsigned int l=0; l < 3; l++) {
          M += (4*h[k][l])*outerProd(N[k],N[l]);
          if (k == l) continue;
          z_real_t dl = lambda[l]-lambda[k];
          z_real_t coef;
          if (std::fabs(dl) > 1.e-16)
            coef = 2*(t[l]-t[k])/dl;
          else
            coef = 2*(h[l][l]-h[k][l]);
          M.addIJKL(coef,N[k],N[l]);
        }
    }

    return W;
  }
  
  // compute stored energy from principal stretches
  virtual z_real_t storedEnergy(const MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&,
                                const double[],double[],double[][3],bool,bool) = 0;
};


/**
 * Base class for dilatancy potentials.
 */
template <class ALG>
class HyperElasticity<ALG>::Dilatancy {
  
 protected:
  
  // constructor
  Dilatancy() {}
  
 public:
  
  // destructor
  virtual ~Dilatancy() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // apply rotation to material properties
  virtual void rotateProperties(MaterialProperties&,const Rotation&) {}
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&) {}
  
  // definition of the coupling energy
  virtual z_real_t couplingEnergy(const MaterialProperties&,
                                  const ConstitutiveModel::ParameterSet&,
                                  const SYM_TENSOR&,SYM_TENSOR&,
                                  SYM_TENSOR4&,bool,bool) = 0;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
