/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_HYPER_NEOHOOKEAN_POTENTIAL_H
#define ZORGLIB_MATL_MECA_HYPER_NEOHOOKEAN_POTENTIAL_H

// config
#include <matlib_macros.h>

// std C++ library
#include <stdexcept>
// local
#include <math/TensorAlgebra.h>
#include <matl/ModelDictionary.h>
#include <matl/meca/eos/StandardEOS.h>
#include <matl/meca/hyper/HyperElasticity.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing hyperelastic neohookean potentials.
 */
template <class ALG>
class NeohookeanPotential : virtual public SpectralHEPotential<ALG> {
  
 public:

  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;

 public:

  // constructor
  NeohookeanPotential() {}
  
  // copy constructor
  NeohookeanPotential(const NeohookeanPotential&) {}

  // destructor
  virtual ~NeohookeanPotential() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
     if (os) (*os) << "\n\t***Neo-Hookean potential***" << std::endl;
     
    static const z_real_t ONE_THIRD = 1.0/3.0;
    static const z_real_t TWO_THIRD = 2.0/3.0;

    z_real_t E,K,lambda,mu,nu;
    try {
      // get Young's modulus
      E = material.getRealProperty("YOUNG_MODULUS");
      if (E < 0.0) {
        if (os) (*os) << "ERROR: Young's modulus must be positive." << std::endl;
        throw std::runtime_error("invalid property: Young's modulus");
      }

      // get Poisson's coefficient
      nu = material.getRealProperty("POISSON_COEFFICIENT");
      if (nu < -1.0 || nu > 0.5) {
        if (os) (*os) << "ERROR: Poisson's coefficient must be in [-1.0,0.5]." << std::endl;
        throw std::runtime_error("invalid property: Poisson's coefficient");
      }

      // compute other properties
      mu = 0.5*E/(1.0+nu);
      K = ONE_THIRD*E/(1.0-2*nu);
      lambda = K-TWO_THIRD*mu;

      material.setProperty("BULK_MODULUS",K);
      material.setProperty("SHEAR_MODULUS",mu);
      material.setProperty("1ST_LAME_CONSTANT",lambda);
      material.setProperty("2ND_LAME_CONSTANT",mu);
    }
    catch (NoSuchPropertyException) {
      // get second Lame constant (a.k.a. shear modulus)
      try {
        mu = material.getRealProperty("2ND_LAME_CONSTANT");
        if (mu < 0.0) {
          if (os) (*os) << "ERROR: Lame constants must be positive." << std::endl;
          throw std::runtime_error("invalid property: second Lame constant");
        }
      }
      catch (NoSuchPropertyException) {
        try {
          mu = material.getRealProperty("SHEAR_MODULUS");
          if (mu < 0.0) {
            if (os) (*os) << "ERROR: shear modulus must be positive." << std::endl;
            throw std::runtime_error("invalid property: shear modulus");
          }
          material.setProperty("2ND_LAME_CONSTANT",mu);
        }
        catch (NoSuchPropertyException e) {
          if (os) (*os) << "ERROR: second Lame constant is not defined." << std::endl;
          throw e;
        }
      }

      // get first Lame constant
      try {
        lambda = material.getRealProperty("1ST_LAME_CONSTANT");
        if (lambda < 0.0e0) {
          if (os) (*os) << "ERROR: Lame constants must be positive." << std::endl;
          throw std::runtime_error("invalid property: first Lame constant");
        }
        K = lambda+TWO_THIRD*mu;
        material.setProperty("BULK_MODULUS",K);
      }
      catch (NoSuchPropertyException) {
        try {
          K = material.getRealProperty("BULK_MODULUS");
          if (K < 0.0) {
            if (os) (*os) << "ERROR: bulk modulus must be positive." << std::endl;
            throw std::runtime_error("invalid property: bulk modulus");
          }
        }
        catch (NoSuchPropertyException) {
          if (os) (*os) << "WARNING: bulk modulus set to zero." << std::endl;
          K = 0.0e0;
          material.setProperty("BULK_MODULUS",K);
        }
        lambda = K-TWO_THIRD*mu;
        material.setProperty("1ST_LAME_CONSTANT",lambda);
      }

      // compute other properties
      nu = (3*K-2*mu)/(6*K+2*mu);
      E = 2*mu*(1.+nu);

      material.setProperty("YOUNG_MODULUS",E);
      material.setProperty("POISSON_COEFFICIENT",nu);
    }
     
    if (os) {
      (*os) << "\tYoung's modulus       = " << E;
      (*os) << "\n\tPoisson's coefficient = " << nu;
      (*os) << "\n\tbulk modulus          = " << K;
      (*os) << "\n\t1st Lame constant     = " << lambda;
      (*os) << "\n\t2nd Lame constant     = " << mu << std::endl;
    }
    
    // compute dilatational elastic wave speed
    try {
      z_real_t rho = material.getRealProperty("MASS_DENSITY");
      z_real_t c = std::sqrt((lambda+2*mu)/rho);
      material.setProperty("CELERITY",c);
      if (os) (*os) << "\n\tcelerity              = " << c << std::endl;
    }
    catch (NoSuchPropertyException) {
      if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
    }
  }
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& C,SYM_TENSOR& S,
                        SYM_TENSOR4& M,bool first,bool second) {
    
    // shear modulus
    z_real_t mu = material.getRealProperty("2ND_LAME_CONSTANT");
    
    // potential
    z_real_t trC = trace(C);
    z_real_t W = 0.5*mu*(trC-3);
    
    // stress tensor
    static const SYM_TENSOR I = SYM_TENSOR::identity();
    if (first) S = mu*I;
    
    // consistent tangent
    if (second) M = 0.0;
    
    return W;
  }
  
  // compute stored energy from principal stretches
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const z_real_t eps[],z_real_t sig[],
                        z_real_t M[][3],bool first,bool second) {
    
    // 1st invariant
    z_real_t trC  = eps[0]+eps[1]+eps[2];
    
    // shear modulus
    z_real_t mu = material.getRealProperty("2ND_LAME_CONSTANT");
    
    // potential
    z_real_t W = 0.5*mu*(trC-3);
    
    // principal stresses
    if (first) {
      for (unsigned int k=0; k < 3; k++) sig[k] = 0.5*mu;
    }

    // second derivatives
    if (second) {
      for (unsigned int k=0; k < 3; k++)
        for (unsigned int l=0; l < 3; l++)
          M[k][l] = 0.0;
    }
    
    return W;
  }
};


/**
 * Implementations of the model.
 */
class Neohookean3D : public HyperElasticity<TensorAlgebra3D> {
  
 public:
  
  // constructor
  Neohookean3D()
  : HyperElasticity<TensorAlgebra3D>(new NeohookeanPotential<TensorAlgebra3D>(),
                                     new StandardEOS()) {}
  
  // copy constructor
  Neohookean3D(const Neohookean3D& src) 
  : HyperElasticity<TensorAlgebra3D>(src) {}
  
  // destructor
  virtual ~Neohookean3D() {}
};
class Neohookean2D : public HyperElasticity<TensorAlgebra2D> {
  
 public:
  
  // constructor
  Neohookean2D()
  : HyperElasticity<TensorAlgebra2D>(new NeohookeanPotential<TensorAlgebra2D>(),
                                     new StandardEOS()) {}
  
  // copy constructor
  Neohookean2D(const Neohookean2D& src) 
  : HyperElasticity<TensorAlgebra2D>(src) {}
  
  // destructor
  virtual ~Neohookean2D() {}
};
class Neohookean1D : public HyperElasticity<TensorAlgebra1D> {
  
 public:
  
  // constructor
  Neohookean1D()
  : HyperElasticity<TensorAlgebra1D>(new NeohookeanPotential<TensorAlgebra1D>(),
                                     new StandardEOS()) {}
  
  // copy constructor
  Neohookean1D(const Neohookean1D& src) 
  : HyperElasticity<TensorAlgebra1D>(src) {}
  
  // destructor
  virtual ~Neohookean1D() {}
};

/**
 * The associated model builder
 */
class NeohookeanBuilder : public ModelBuilder {

 private:
  
  // constructor
  NeohookeanBuilder();

  // the instance
  static NeohookeanBuilder const* BUILDER;

 public:
  
  // destructor
  virtual ~NeohookeanBuilder() {}

  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
