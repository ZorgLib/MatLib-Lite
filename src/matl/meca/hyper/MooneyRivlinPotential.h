/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_HYPER_MOONEY_RIVLIN_POTENTIAL_H
#define ZORGLIB_MATL_MECA_HYPER_MOONEY_RIVLIN_POTENTIAL_H

// config
#include <matlib_macros.h>

// local
#include <math/TensorAlgebra.h>
#include <matl/ModelDictionary.h>
#include <matl/meca/eos/StandardEOS.h>
#include <matl/meca/hyper/HyperElasticity.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing hyperelastic Mooney-Rivlin potentials.
 */
template <class ALG>
class MooneyRivlinPotential : virtual public SpectralHEPotential<ALG> {
  
 public:

  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;

 public:

  // constructor
  MooneyRivlinPotential() {}
  
  // copy constructor
  MooneyRivlinPotential(const MooneyRivlinPotential&) {}

  // destructor
  virtual ~MooneyRivlinPotential() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    if (os) (*os) << "\n\t***Mooney-Rivlin potential***" << std::endl;
    
    z_real_t C1,C2;
    // get C1 modulus
    C1 = material.getRealProperty("C10_MODULUS");
    if (C1 < 0.0) {
      if (os) (*os) << "ERROR: C10 modulus must be positive." << std::endl;
      throw std::runtime_error("invalid property: C10 modulus");
    }

    // get C2 modulus
    C2 = material.getRealProperty("C01_MODULUS");
    if (C2 < 0.0) {
      if (os) (*os) << "ERROR: C01 modulus must be positive." << std::endl;
      throw std::runtime_error("invalid property: C01 modulus");
    }
     
    if (os) {
      (*os) <<   "\tC10 modulus       = " << C1;
      (*os) << "\n\tC01 modulus       = " << C2 << std::endl;
    }
    
    // compute dilatational elastic wave speed
    if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
  }
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& C,SYM_TENSOR& S,
                        SYM_TENSOR4& M,bool first,bool second) {
    
    // constants
    z_real_t C1 = material.getRealProperty("C10_MODULUS");
    z_real_t C2 = material.getRealProperty("C01_MODULUS");
    
    // invariants
    z_real_t I1 = trace(C);
    z_real_t I2 = 0.5*(I1*I1-innerProd2(C,C));

    // potential
    z_real_t W = C1*(I1-3.0)+C2*(I2-3.0);
    
    // stress tensor
    static const SYM_TENSOR I = SYM_TENSOR::identity();
    if (first) S = 2*((C1+C2*I1)*I-C2*C);
    
    // consistent tangent
    if (second) M = 4*C2*(outerProd(I,I)-SYM_TENSOR4::contravariantIdentity());
    
    return W;
  }
  
  // compute stored energy from principal stretches
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const z_real_t eps[],z_real_t sig[],
                        z_real_t M[][3],bool first,bool second) {
 
    // constants
    z_real_t C1 = material.getRealProperty("C10_MODULUS");
    z_real_t C2 = material.getRealProperty("C01_MODULUS");
    
    // invariants
    z_real_t I1 = eps[0]+eps[1]+eps[2];
    z_real_t I2 = eps[0]*eps[1]+eps[0]*eps[2]+eps[1]*eps[2];

    // potential
    z_real_t W = C1*(I1-3.0)+C2*(I2-3.0);
    
    // stress tensor
    if (first) {
      sig[0] = C1+C2*(eps[1]+eps[2]);
      sig[1] = C1+C2*(eps[0]+eps[2]);
      sig[2] = C1+C2*(eps[0]+eps[1]);
    }
    
    // consistent tangent
    if (second) {
      M[0][0] = 0.0e0; M[0][1] = C2;    M[0][2] = C2;
      M[1][0] = C2;    M[1][1] = 0.0e0; M[1][2] = C2;
      M[2][0] = C2;    M[2][1] = C2;    M[2][2] = 0.0e0;
    }
    
    return W;
  }
};


/**
 * Implementations of the model.
 */
class MooneyRivlin3D : public HyperElasticity<TensorAlgebra3D> {
  
 public:
  
  // constructor
  MooneyRivlin3D()
  : HyperElasticity<TensorAlgebra3D>(new MooneyRivlinPotential<TensorAlgebra3D>(),
                                     new StandardEOS()) {}
  
  // copy constructor
  MooneyRivlin3D(const MooneyRivlin3D& src) 
  : HyperElasticity<TensorAlgebra3D>(src) {}
  
  // destructor
  virtual ~MooneyRivlin3D() {}
};
class MooneyRivlin2D : public HyperElasticity<TensorAlgebra2D> {
  
 public:
  
  // constructor
  MooneyRivlin2D()
  : HyperElasticity<TensorAlgebra2D>(new MooneyRivlinPotential<TensorAlgebra2D>(),
                                     new StandardEOS()) {}
  
  // copy constructor
  MooneyRivlin2D(const MooneyRivlin2D& src) 
  : HyperElasticity<TensorAlgebra2D>(src) {}
  
  // destructor
  virtual ~MooneyRivlin2D() {}
};
class MooneyRivlin1D : public HyperElasticity<TensorAlgebra1D> {
  
 public:
  
  // constructor
  MooneyRivlin1D()
  : HyperElasticity<TensorAlgebra1D>(new MooneyRivlinPotential<TensorAlgebra1D>(),
                                     new StandardEOS()) {}
  
  // copy constructor
  MooneyRivlin1D(const MooneyRivlin1D& src) 
  : HyperElasticity<TensorAlgebra1D>(src) {}
  
  // destructor
  virtual ~MooneyRivlin1D() {}
};

/**
 * The associated model builder
 */
class MooneyRivlinBuilder : public ModelBuilder {

 private:
  
  // constructor
  MooneyRivlinBuilder();

  // the instance
  static MooneyRivlinBuilder const* BUILDER;

 public:
  
  // destructor
  virtual ~MooneyRivlinBuilder() {}

  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
