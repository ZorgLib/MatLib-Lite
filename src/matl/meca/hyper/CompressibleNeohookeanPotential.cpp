/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "CompressibleNeohookeanPotential.h"

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

/*
 * Methods for class NeohookeanBuilder.
 */

// the instance
CompressibleNeohookeanBuilder const* CompressibleNeohookeanBuilder::BUILDER = new CompressibleNeohookeanBuilder();

// constructor
CompressibleNeohookeanBuilder::CompressibleNeohookeanBuilder() {
  ModelDictionary::add("COMPRESSIBLE_NEOHOOKEAN",*this);
}

// build model
ConstitutiveModel* CompressibleNeohookeanBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
      return new CompressibleNeohookean3D();
      break;
    case 2:
      return new CompressibleNeohookean2D();
      break;
    case 1:
      return new CompressibleNeohookean1D();
      break;
    default:
      return 0;
      break;
  }
}
