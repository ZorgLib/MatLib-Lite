/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_HYPER_COMPRESSIBLE_NEOHOOKEAN_POTENTIAL_H
#define ZORGLIB_MATL_MECA_HYPER_COMPRESSIBLE_NEOHOOKEAN_POTENTIAL_H

// config
#include <matlib_macros.h>

// std C++ library
#include <stdexcept>
// local
#include <math/TensorAlgebra.h>
#include <matl/ModelDictionary.h>
#include <matl/meca/hyper/HyperElasticity.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing hyperelastic compressible neohookean potentials.
 */
template <class ALG>
class CompressibleNeohookeanPotential : virtual public SpectralHEPotential<ALG> {
  
 public:

  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;

 public:

  // constructor
  CompressibleNeohookeanPotential() {}
  
  // copy constructor
  CompressibleNeohookeanPotential(const CompressibleNeohookeanPotential&) {}

  // destructor
  virtual ~CompressibleNeohookeanPotential() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
     if (os) (*os) << "\n\t***Compressible Neo-Hookean potential***" << std::endl;
     
    static const z_real_t ONE_THIRD = 1.0/3.0;
    static const z_real_t TWO_THIRD = 2.0/3.0;

    z_real_t E,K,lambda,mu,nu;
    try {
      // get Young's modulus
      E = material.getRealProperty("YOUNG_MODULUS");
      if (E < 0.0) {
        if (os) (*os) << "ERROR: Young's modulus must be positive." << std::endl;
        throw std::runtime_error("invalid property: Young's modulus");
      }

      // get Poisson's coefficient
      nu = material.getRealProperty("POISSON_COEFFICIENT");
      if (nu < -1.0 || nu > 0.5) {
        if (os) (*os) << "ERROR: Poisson's coefficient must be in [-1.0,0.5]." << std::endl;
        throw std::runtime_error("invalid property: Poisson's coefficient");
      }

      // compute other properties
      mu = 0.5*E/(1.0+nu);
      K = ONE_THIRD*E/(1.0-2*nu);
      lambda = K-TWO_THIRD*mu;

      material.setProperty("BULK_MODULUS",K);
      material.setProperty("SHEAR_MODULUS",mu);
      material.setProperty("1ST_LAME_CONSTANT",lambda);
      material.setProperty("2ND_LAME_CONSTANT",mu);
    }
    catch (NoSuchPropertyException) {
      // get second Lame constant (a.k.a. shear modulus)
      try {
        mu = material.getRealProperty("2ND_LAME_CONSTANT");
        if (mu < 0.0) {
          if (os) (*os) << "ERROR: Lame constants must be positive." << std::endl;
          throw std::runtime_error("invalid property: second Lame constant");
        }
      }
      catch (NoSuchPropertyException) {
        try {
          mu = material.getRealProperty("SHEAR_MODULUS");
          if (mu < 0.0) {
            if (os) (*os) << "ERROR: shear modulus must be positive." << std::endl;
            throw std::runtime_error("invalid property: shear modulus");
          }
          material.setProperty("2ND_LAME_CONSTANT",mu);
        }
        catch (NoSuchPropertyException e) {
          if (os) (*os) << "ERROR: second Lame constant is not defined." << std::endl;
          throw e;
        }
      }

      // get first Lame constant
      try {
        lambda = material.getRealProperty("1ST_LAME_CONSTANT");
        if (lambda < 0.0e0) {
          if (os) (*os) << "ERROR: Lame constants must be positive." << std::endl;
          throw std::runtime_error("invalid property: first Lame constant");
        }
        K = lambda+TWO_THIRD*mu;
        material.setProperty("BULK_MODULUS",K);
      }
      catch (NoSuchPropertyException) {
        try {
          K = material.getRealProperty("BULK_MODULUS");
          if (K < 0.0) {
            if (os) (*os) << "ERROR: bulk modulus must be positive." << std::endl;
            throw std::runtime_error("invalid property: bulk modulus");
          }
        }
        catch (NoSuchPropertyException) {
          if (os) (*os) << "WARNING: bulk modulus set to zero." << std::endl;
          K = 0.0e0;
          material.setProperty("BULK_MODULUS",K);
        }
        lambda = K-TWO_THIRD*mu;
        material.setProperty("1ST_LAME_CONSTANT",lambda);
      }

      // compute other properties
      nu = (3*K-2*mu)/(6*K+2*mu);
      E = 2*mu*(1.+nu);

      material.setProperty("YOUNG_MODULUS",E);
      material.setProperty("POISSON_COEFFICIENT",nu);
    }
     
    if (os) {
      (*os) << "\tYoung's modulus       = " << E;
      (*os) << "\n\tPoisson's coefficient = " << nu;
      (*os) << "\n\tbulk modulus          = " << K;
      (*os) << "\n\t1st Lame constant     = " << lambda;
      (*os) << "\n\t2nd Lame constant     = " << mu << std::endl;
    }
    
    // compute dilatational elastic wave speed
    try {
      z_real_t rho = material.getRealProperty("MASS_DENSITY");
      z_real_t c = std::sqrt((lambda+2*mu)/rho);
      material.setProperty("CELERITY",c);
      if (os) (*os) << "\n\tcelerity              = " << c << std::endl;
    }
    catch (NoSuchPropertyException) {
      if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
    }
  }
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& C,SYM_TENSOR& S,
                        SYM_TENSOR4& M,bool first,bool second) {
    
    // compute determinant and inverse
    z_real_t detC;
    SYM_TENSOR Cinv;
    if (first || second)
      Cinv = C.inverse(detC);
    else
      detC = determinant(C);
    
    // Lame constants
    z_real_t lambda = material.getRealProperty("1ST_LAME_CONSTANT");
    z_real_t mu = material.getRealProperty("2ND_LAME_CONSTANT");
    
    // potential
    z_real_t trC = trace(C);
    z_real_t J = std::sqrt(detC);
    z_real_t logJ = 0.5*std::log(detC);
    z_real_t W = lambda*(J-1.0-logJ) - mu*logJ + 0.5*mu*(trC-3);
    
    // stress tensor
    z_real_t muBar = mu-lambda*(J-1.0);
    static const SYM_TENSOR I = SYM_TENSOR::identity();
    if (first) S = mu*I-muBar*Cinv;
    
    // consistent tangent
    if (second) {
      M = (lambda*J)*outerProd(Cinv,Cinv);
      M.addIJKL(muBar,Cinv);
    }
    
    return W;
  }
  
  // compute stored energy from principal stretches
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const z_real_t eps[],z_real_t sig[],
                        z_real_t M[][3],bool first,bool second) {
    
    // invariants
    z_real_t trC  = eps[0]+eps[1]+eps[2];
    z_real_t detC = eps[0]*eps[1]*eps[2];
    
    // Lame constants
    z_real_t lambda = material.getRealProperty("1ST_LAME_CONSTANT");
    z_real_t mu = material.getRealProperty("2ND_LAME_CONSTANT");
    
    // potential
    z_real_t J = std::sqrt(detC);
    z_real_t logJ = 0.5*std::log(detC);
    z_real_t W = lambda*(J-1.0-logJ) - mu*logJ + 0.5*mu*(trC-3);
    
    // principal stresses
    if (first) {
      z_real_t coef = lambda*(J-1.0)-mu;
      for (unsigned int k=0; k < 3; k++) sig[k] = 0.5*(coef/eps[k]+mu);
    }

    // second derivatives
    if (second) {
      z_real_t coef1 = lambda*J;
      z_real_t coef2 = 0.5*(coef1-lambda-mu);
      for (unsigned int k=0; k < 3; k++)
        for (unsigned int l=0; l < 3; l++) {
          z_real_t val = 1.0/(eps[k]*eps[l]);
          M[k][l] = 0.25*coef1*val;
          if (k == l) M[k][l] -= coef2*val;
        }
    }
    
    return W;
  }
};


/**
 * Implementations of the model.
 */
class CompressibleNeohookean3D : public HyperElasticity<TensorAlgebra3D> {
  
 public:
  
  // constructor
  CompressibleNeohookean3D()
  : HyperElasticity<TensorAlgebra3D>(new CompressibleNeohookeanPotential<TensorAlgebra3D>()) {}
  
  // copy constructor
  CompressibleNeohookean3D(const CompressibleNeohookean3D& src)
  : HyperElasticity<TensorAlgebra3D>(src) {}
  
  // destructor
  virtual ~CompressibleNeohookean3D() {}
};
class CompressibleNeohookean2D : public HyperElasticity<TensorAlgebra2D> {
  
 public:
  
  // constructor
  CompressibleNeohookean2D()
  : HyperElasticity<TensorAlgebra2D>(new CompressibleNeohookeanPotential<TensorAlgebra2D>()) {}
  
  // copy constructor
  CompressibleNeohookean2D(const CompressibleNeohookean2D& src)
  : HyperElasticity<TensorAlgebra2D>(src) {}
  
  // destructor
  virtual ~CompressibleNeohookean2D() {}
};
class CompressibleNeohookean1D : public HyperElasticity<TensorAlgebra1D> {
  
 public:
  
  // constructor
  CompressibleNeohookean1D()
  : HyperElasticity<TensorAlgebra1D>(new CompressibleNeohookeanPotential<TensorAlgebra1D>()) {}
  
  // copy constructor
  CompressibleNeohookean1D(const CompressibleNeohookean1D& src)
  : HyperElasticity<TensorAlgebra1D>(src) {}
  
  // destructor
  virtual ~CompressibleNeohookean1D() {}
};

/**
 * The associated model builder
 */
class CompressibleNeohookeanBuilder : public ModelBuilder {

 private:
  
  // constructor
  CompressibleNeohookeanBuilder();

  // the instance
  static CompressibleNeohookeanBuilder const* BUILDER;

 public:
  
  // destructor
  virtual ~CompressibleNeohookeanBuilder() {}

  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
