/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "HenckyEOS.h"

// std C library
#include <cmath>
// std C++ library
#include <stdexcept>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


// check consistency of material properties
void HenckyEOS::checkProperties(MaterialProperties& mater,std::ostream* os) {

  if (os) (*os) << "\n\t***Hencky-type equation-of-state***\n";

  // get bulk modulus
  z_real_t K = mater.getRealProperty("BULK_MODULUS");
  if (K < 0.0) {
    std::string msg("ERROR: bulk modulus must be positive.");
    if (os) (*os) << msg << std::endl;
    throw std::runtime_error(msg);
  }

  if (os) (*os) << "\tbulk modulus = " << K << std::endl;   
}

// compute stored energy
z_real_t HenckyEOS::storedEnergy(const MaterialProperties& mater,
                                 const ConstitutiveModel::ParameterSet& extPar,
                                 z_real_t J,z_real_t& p,z_real_t& K,
                                 bool first,bool second) {
  // bulk modulus
  z_real_t B = mater.getRealProperty("BULK_MODULUS");
  
  // potential
  z_real_t defVol = std::log(J);
  z_real_t press = B*defVol;
  z_real_t W = B*(J*defVol-J+1.0);
  
  // pressure
  if (first) p = press/J;
  
  // bulk modulus
  if (second) K = (B-press)/(J*J);
  
  return W;
}
