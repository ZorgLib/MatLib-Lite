/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_EOS_EQUATION_OF_STATE_H
#define ZORGLIB_MATL_MECA_EOS_EQUATION_OF_STATE_H

// config
#include <matlib_macros.h>

// local
#include <matl/ConstitutiveModel.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Virtual base class for equation-of-states.
 */
class EOS {

 protected:
  
  // constructor
  EOS() {}

 public:
  
  // destructor
  virtual ~EOS() {}

  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&) {}
  
  // compute stored energy
  virtual z_real_t storedEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                z_real_t,z_real_t&,z_real_t&,bool,bool) = 0;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
