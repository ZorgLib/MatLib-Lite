/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "ElasticBar.h"

// std C library
#include <cmath>
// std C++ library
#include <stdexcept>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

/*
 * Methods for class ElasticBar
 */

// destructor
ElasticBar::~ElasticBar() {
  if (--(*count) > 0) return;
  delete count;
  if (potential) delete potential;
  if (dilatancy) delete dilatancy;
}

// update properties in function of external parameters
void ElasticBar::updateProperties(MaterialProperties& mater,
                                  const ConstitutiveModel::ParameterSet& extPar) {
  if (potential) potential->updateProperties(mater,extPar);
  if (dilatancy) dilatancy->updateProperties(mater,extPar);
}

// check consistency of material properties
void ElasticBar::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\nElastic material (pure 1D bar):" << std::endl;
  
  // density
  try {
    z_real_t rho = material.getRealProperty("MASS_DENSITY");
    if (rho <= 0.0) {
      if (os) (*os) << "ERROR: mass density must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: mass density");
    }
    if (os) (*os) << "\n\tmass density = " << rho << std::endl;
  }
  catch (NoSuchPropertyException) {
    if (os) (*os) << "\n\tmass density is not defined" << std::endl;
  }
  
  // bar cross section
  try {
    z_real_t A = material.getRealProperty("SECTION");
    if (A <= 0.0) {
      if (os) (*os) << "ERROR: bar cross-section must be positive." << std::endl;
      throw std::runtime_error("invalid property: bar cross-section");
    }
    if (os) (*os) << "\n\tbar cross-section = " << A << std::endl;
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "\n\tbar cross-section set to 1" << std::endl;
    material.setProperty("SECTION",1.0);
  }

  // elastic potential
  potential->checkProperties(material,os);
  
  // dilatancy model
  if (dilatancy) dilatancy->checkProperties(material,os);
}

// check if the material behaviour is linear ?
bool ElasticBar::isLinear() const {return potential->isLinear();}

// compute the incremental potential
z_real_t ElasticBar::incrementalPotential(const MaterialProperties& material,
                                          const ConstitutiveModel::ParameterSet& extPar,
                                          const MaterialState& state0,MaterialState& state,
                                          z_real_t dTime,MatLibMatrix& M,
                                          bool update,bool tangent) {
  // get cross-section
  z_real_t A = material.getRealProperty("SECTION");

  // get strain
  z_real_t eps = state.grad[0];
  
  // compute stored energy
  z_real_t sig,K;
  z_real_t W = storedEnergy(material,extPar,eps,sig,K,update,tangent);
  if (update) {
    state.flux[0] = A*sig;
    state.internal[0] = sig;
    state.internal[1] = W;
  }
  if (tangent) M[0][0] = A*K;

  return A*W-state0.internal[0];
}

// compute stored energy
z_real_t ElasticBar::storedEnergy(const MaterialProperties& material,
                                  const ConstitutiveModel::ParameterSet& extPar,
                                  z_real_t eps,z_real_t& sig,z_real_t& M,
                                  bool first,bool second) {
  
  // elastic energy
  z_real_t W = 0.0;
  if (potential)
    W = potential->storedEnergy(material,extPar,eps,sig,M,first,second);
  else {
    if (first) sig = 0.0;
    if (second) M = 0.0;
  }
  
  // dilatancy term
  if (dilatancy) {
    z_real_t sigT,MT;
    W += dilatancy->couplingEnergy(material,extPar,eps,sigT,MT,first,second);
    if (first) sig += sigT;
    if (second) M += MT;
  }
  
  return W;
}

// compute the dual/conjugate energy
z_real_t ElasticBar::dualEnergy(const MaterialProperties& material,
                                const ConstitutiveModel::ParameterSet& extPar,
                                z_real_t sig,z_real_t& eps,z_real_t& M,
                                bool first,bool second) {
  
  // find eps = arg sup [eps.sig - W(eps)]
  z_real_t s,MM;
  z_real_t W = this->storedEnergy(material,extPar,eps,s,MM,first,first || second);
  if (first) {
    static const unsigned int ITMAX = 20;
    static const z_real_t PRECISION = 1.e-12;
    static const z_real_t TOLERANCE = 1.e-08;
    unsigned int iter = 0;
    z_real_t norm0;
    while (iter < ITMAX) {
      
      // evaluate residual
      z_real_t R = sig-s;
      z_real_t norm1 = std::fabs(R);
      if (norm1 < PRECISION) break;
      if (iter > 0) {
        if (norm1 < TOLERANCE*norm0) break;
      }
      else
        norm0 = norm1;
      
      // solve
      if (std::fabs(MM) < 1.0e-16)
        throw std::runtime_error("singular stiffness in ElasticBar::dualEnergy()");
      z_real_t dEps = R/MM;
      eps += dEps;
      
      // compute incremental energy
      W = this->storedEnergy(material,extPar,eps,s,MM,true,true);
      iter++;
    }
  }
  
  // tangents
  if (second) {
    
    // invert material tangent
    M = 1.0/MM;
  }
  
  return eps*sig-W;
}


/*
 * Methods for class LinearElasticPotential1D
 */

// check consistency of material properties
void LinearElasticPotential1D::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\n\t***Linear elastic potential (pure 1D bar)***" << std::endl;
  
  // get Young's modulus
  z_real_t E = material.getRealProperty("YOUNG_MODULUS");
  if (E < 0.0) {
    if (os) (*os) << "ERROR: Young's modulus must be positive." << std::endl;
    throw std::runtime_error("invalid property: Young's modulus");
  }
  
  if (os) (*os) << "\tYoung's modulus       = " << E << std::endl;
  
  // compute elastic wave speed
  try {
    z_real_t rho = material.getRealProperty("MASS_DENSITY");
    z_real_t c = std::sqrt(E/rho);
    material.setProperty("CELERITY",c);
    if (os) (*os) << "\n\tcelerity              = " << c << std::endl;
  }
  catch (NoSuchPropertyException) {
    if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
  }
}

// compute stored energy
z_real_t LinearElasticPotential1D::storedEnergy(const MaterialProperties& material,
                                                const ConstitutiveModel::ParameterSet& extPar,
                                                z_real_t eps,z_real_t& sig,z_real_t& M,
                                                bool first,bool second) {
  // get elastic modulus
  z_real_t E = material.getRealProperty("YOUNG_MODULUS");
  
  // potential
  z_real_t W = 0.5*E*eps*eps;
  if (!first && !second) return W;
  
  // stress
  if (first) sig = E*eps;
  
  // tangent
  if (second) M = E;
  
  return W;
}

// compute material stiffness (Hooke) tensor
void LinearElasticPotential1D::computeStiffness(const MaterialProperties& material,
                                                const ConstitutiveModel::ParameterSet& extPar,
                                                z_real_t& M) {
  // get elastic modulus
  M = material.getRealProperty("YOUNG_MODULUS");
}

/*
 * Methods for class LinearElasticBarBuilder.
 */

// the instance
LinearElasticBarBuilder const* LinearElasticBarBuilder::BUILDER
= new LinearElasticBarBuilder();

// constructor
LinearElasticBarBuilder::LinearElasticBarBuilder() {
  ModelDictionary::add("LINEAR_ELASTIC_BAR",*this);
}

// build model
ConstitutiveModel* LinearElasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new LinearElasticBar();
      break;
    default:
      return 0;
      break;
  }
}
