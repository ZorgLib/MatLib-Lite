/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_LINEAR_DILATANT_VISCO_PLASTICITY_H
#define ZORGLIB_MATL_MECA_LINEAR_DILATANT_VISCO_PLASTICITY_H

// config
#include <matlib_macros.h>

// local
#include <matl/meca/linear/ViscoPlasticitySimple.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class for describing a dilatant visco-plasticity model.
 * Also works for Tresca.
 */
class DilatantViscoPlasticity {
  
 public:
  
  // flag indicating if the model needs initialization
  bool initialize;
  
  // flag indicating if the model needs finalization (update internal parameters)
  bool finalize;
  
  // destructor
  virtual ~DilatantViscoPlasticity() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&) {}
  
  // number of internal parameters (should be at least 1 for the stored plastic energy)
  virtual unsigned int nIntPar() const = 0;
  
  // compute irreversible energy and derivatives
  virtual z_real_t irreversibleEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                      const MatLibArray&,MatLibArray&,z_real_t,z_real_t,z_real_t,z_real_t,
                                      z_real_t&,z_real_t&,z_real_t&,z_real_t&,z_real_t&,z_real_t,bool,bool) = 0;

  // compute steepest gradient direction (at origin)
  virtual z_real_t steepestGradient(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                    const MatLibArray&,MatLibArray&,z_real_t,z_real_t,z_real_t,z_real_t,
                                    z_real_t,z_real_t,z_real_t&,z_real_t&,z_real_t) = 0;
};


/**
 * Stored energy potential describing consolidation
 */
class ConsolidationPotential {
  
 public:
  
  // flag indicating if the model needs initialization
  bool initialize;
  
  // flag indicating if the model needs finalization (update internal parameters)
  bool finalize;
  
  // destructor
  virtual ~ConsolidationPotential() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&) {}
  
  // number of internal parameters
  virtual unsigned int nIntPar() const = 0;
  
  // plastically stored energy
  virtual z_real_t storedEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                const MatLibArray&,MatLibArray&,z_real_t,z_real_t,
                                z_real_t,z_real_t&,z_real_t&,bool,bool) = 0;
};


/**
 * Visco-plasticity model with elliptic yield function (with isotropic hardening).
 */
class EllipticViscoPlasticity : public DilatantViscoPlasticity {
  
 protected:
  
  // plastic hardening part (stored and dissipated)
  IsotropicHardeningModel *hardening;
  
  // viscous dissipation part
  ScalarRateDependencyModel *viscous;
  
  // instance counter
  unsigned int *count;
  
 public:

  // constructor
  EllipticViscoPlasticity(IsotropicHardeningModel*,
                          ScalarRateDependencyModel*);
  
  // copy constructor
  EllipticViscoPlasticity(const EllipticViscoPlasticity&);
  
  // destructor
  virtual ~EllipticViscoPlasticity();
  
  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // update properties in function of external parameters
  void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&);
  
  // number of internal parameters
  unsigned int nIntPar() const;
  
  // compute irreversible energy and derivatives
  z_real_t irreversibleEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                              const MatLibArray&,MatLibArray&,z_real_t,z_real_t,z_real_t,z_real_t,
                              z_real_t&,z_real_t&,z_real_t&,z_real_t&,z_real_t&,z_real_t,bool,bool);
  
  // compute steepest gradient direction (at origin)
  z_real_t steepestGradient(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                            const MatLibArray&,MatLibArray&,z_real_t,z_real_t,z_real_t,z_real_t,
                            z_real_t,z_real_t,z_real_t&,z_real_t&,z_real_t);
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
