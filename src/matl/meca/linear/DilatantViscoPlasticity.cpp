/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "DilatantViscoPlasticity.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


// constructor
EllipticViscoPlasticity::EllipticViscoPlasticity(IsotropicHardeningModel* h,
                                                 ScalarRateDependencyModel* v) {
  count = new unsigned int(1);
  hardening = h;
  viscous = v;
}

// copy constructor
EllipticViscoPlasticity::EllipticViscoPlasticity(const EllipticViscoPlasticity& src) {
  count = src.count;
  (*count)++;
  hardening = src.hardening;
  viscous = src.viscous;
}

// destructor
EllipticViscoPlasticity::~EllipticViscoPlasticity() {
  if (--(*count) > 0) return;
  delete count;
  if (hardening) delete hardening;
  if (viscous) delete viscous;
}

// check consistency of material properties
void EllipticViscoPlasticity::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\n\t***Elliptic viscoplasticity model (isotropic hardening)***" << std::endl;

  // yield surface shape parameter
  z_real_t q = material.getRealProperty("PLASTIC_DILATANCY_PARAMETER");
  if (q <= 0.0) {
    if (os) (*os) << "ERROR: plastic dilatancy parameter must be positive" << std::endl;
    throw std::runtime_error("invalid property: plastic dilatancy parameter");
  }
  if (os) (*os) << "\n\tplastic dilatancy parameter = " << q;

  // look for algorithmic parameter
  z_real_t alpha = 0.5;
  try {
    alpha = material.getRealProperty("VP_ALGORITHMIC_PARAMETER");
  }
  catch (NoSuchPropertyException) {
    material.setProperty("VP_ALGORITHMIC_PARAMETER",alpha);
  }
  if (os) (*os) << "\n\talgorithmic parameter = " << alpha << std::endl;
    
  // rate-independent part (stored and dissipated)
  if (hardening) hardening->checkProperties(material,os);
  
  // dissipated viscous (rate-dependent) part
  if (viscous) viscous->checkProperties(material,os);
}

// update properties in function of external parameters
void EllipticViscoPlasticity::updateProperties(MaterialProperties& material,
                                               const ConstitutiveModel::ParameterSet& extPar) {

  // rate-independent part (stored and dissipated)
  if (hardening) hardening->updateProperties(material,extPar);
  
  // dissipated viscous (rate-dependent) part
  if (viscous) viscous->updateProperties(material,extPar);
}

// number of internal parameters
unsigned int EllipticViscoPlasticity::nIntPar() const {
  unsigned int np = 2; // equiv. plastic strain + plastic stored energy
  
  // rate-independent part (stored and dissipated)
  if (hardening) np += hardening->nIntPar();
  
  // dissipated viscous (rate-dependent) part
  if (viscous) np += viscous->nIntPar();
  
  return np;
}

// compute irreversible energy and derivatives
z_real_t EllipticViscoPlasticity::irreversibleEnergy(const MaterialProperties& material,
                                                     const ConstitutiveModel::ParameterSet& extPar,
                                                     const MatLibArray& intPar0,MatLibArray& intPar,
                                                     z_real_t epsPlD0,z_real_t epsPlD1,
                                                     z_real_t epsPlV0,z_real_t epsPlV1,
                                                     z_real_t& sigD,z_real_t& sigV,
                                                     z_real_t& hDD,z_real_t& hDV,z_real_t& hVV,
                                                     z_real_t dTime,bool first,bool second) {
  static const z_real_t PRECISION = 1.0e-16;

  // get initial plastic stored energy
  z_real_t Wp0,Wp;
  Wp0 = intPar0[1];
  
  // get algorithmic parameter
  z_real_t alpha = material.getRealProperty("VP_ALGORITHMIC_PARAMETER");
  
  // get plastic dilatancy parameter
  z_real_t q = material.getRealProperty("PLASTIC_DILATANCY_PARAMETER");
  z_real_t coef = 1.0/(q*q);

  // compute equivalent plastic strain
  z_real_t dEpsPlD = epsPlD1-epsPlD0;
  z_real_t dEpsPlV = epsPlV1-epsPlV0;
  z_real_t dEpsPl = std::sqrt(dEpsPlD*dEpsPlD+coef*dEpsPlV*dEpsPlV);
  z_real_t epsPl0 = intPar0[0];
  z_real_t epsPl1 = epsPl0+dEpsPl;
  z_real_t epsPl = epsPl0+alpha*dEpsPl;
  intPar[0] = epsPl1;
  
  // compute hardening part (stored and dissipated energy)
  unsigned int nIntParHarden = 0;
  z_real_t sig0=0.0,h0=0.0;
  z_real_t Dp=0.0,sig1=0.0,h1=0.0,dh1=0.0;
  if (hardening) {
    nIntParHarden = hardening->nIntPar();
    const MatLibArray intP0Harden(intPar0,nIntParHarden,2);
    MatLibArray intP1Harden(intPar,nIntParHarden,2);
    // compute plastic stored energy
    Wp = hardening->storedEnergy(material,extPar,intP0Harden,intP1Harden,Wp0,
                                 epsPl0,epsPl1,sig0,h0,first || second,second);
    // compute plastic dissipated energy
    sig1 = hardening->yieldStress(material,extPar,intP0Harden,intP1Harden,
                                  epsPl,h1,dh1,first || second,second);
    Dp = sig1*(epsPl1-epsPl0);
  }
  else
    Wp = 0.0;
  intPar[1] = Wp;

  // compute viscous (rate-dependent) dissipated energy
  unsigned int nIntParDiss = 0;
  z_real_t Dv=0.0,sig2=0.0,h2=0.0;
  if (viscous && dTime > 0.0) {
    nIntParDiss = viscous->nIntPar();
    const MatLibArray intP0Diss(intPar0,nIntParDiss,1+nIntParHarden);
    MatLibArray intP1Diss(intPar,nIntParDiss,1+nIntParHarden);
    z_real_t epsPlDot = (epsPl1-epsPl0)/dTime;
    z_real_t siga,sigb,haa,hbb,hab;
    Dv = dTime*viscous->dissipatedEnergy(material,extPar,intP0Diss,intP1Diss,
                                         epsPl,epsPlDot,siga,sigb,haa,hbb,hab,
                                         first || second,second);
    z_real_t fact = alpha*dTime;
    if (first || second) sig2 = fact*siga + sigb;
    if (second) h2 =  alpha*fact*haa + 2*alpha*hab + hbb/dTime;
  }

  // assemble components
  z_real_t coefD,coefV,sig;
  if (first || second) {
    if (dEpsPl > PRECISION) {
      coefD = dEpsPlD/dEpsPl;
      coefV = coef*dEpsPlV/dEpsPl;
    }
    else {
      coefD = 1.0;
      coefV = 1.0/q;
    }
    sig = sig0 + sig1+alpha*h1*(epsPl1-epsPl0) + sig2;
  }
  if (first) {
    sigD = coefD*sig;
    sigV = coefV*sig;
  }
  if (second) {
    z_real_t h = h0 + alpha*(2*h1+alpha*dh1*(epsPl1-epsPl0)) + h2;
    hDD = coefD*coefD*h;
    hDV = coefD*coefV*h;
    hVV = coefV*coefV*h;
    if (dEpsPl > PRECISION) {
      z_real_t val = sig/dEpsPl;
      hDD += (1.-coefD*coefD)*val;
      hDV -= coefD*coefV*val;
      hVV += (coef-coefV*coefV)*val;
    }
  }

  return Wp-Wp0+Dp+Dv;
}

// compute steepest gradient direction (at origin)
z_real_t EllipticViscoPlasticity::steepestGradient(const MaterialProperties& material,
                                                   const ConstitutiveModel::ParameterSet& extPar,
                                                   const MatLibArray& intPar0,MatLibArray& intPar,
                                                   z_real_t epsPlD0,z_real_t epsPlD1,
                                                   z_real_t epsPlV0,z_real_t epsPlV1,
                                                   z_real_t sigEq,z_real_t sigVol,
                                                   z_real_t& dEpsD,z_real_t& dEpsV,z_real_t dTime) {
  static const z_real_t PRECISION = 1.0e-16;
  
  // get plastic dilatancy parameter
  z_real_t q = material.getRealProperty("PLASTIC_DILATANCY_PARAMETER");
  
  // steepest gradient direction
  z_real_t norm = std::sqrt(sigEq*sigEq+q*q*sigVol*sigVol);
  if (norm < PRECISION) {
    dEpsD = 1.0;
    dEpsV = 0.0;
    return 0.0;
  }
  z_real_t coef = 1.0/norm;
  dEpsD = coef*sigEq;
  dEpsV = coef*q*q*sigVol;
  
  // get algorithmic parameter
  z_real_t alpha = material.getRealProperty("VP_ALGORITHMIC_PARAMETER");
  
  // compute equivalent plastic strain
  z_real_t dEpsPlD = epsPlD1-epsPlD0;
  z_real_t dEpsPlV = epsPlV1-epsPlV0;
  z_real_t coef1 = 1.0/(q*q);
  z_real_t dEpsPl = std::sqrt(dEpsPlD*dEpsPlD+coef1*dEpsPlV*dEpsPlV);
  z_real_t epsPl0 = intPar0[0];
  z_real_t epsPl1 = epsPl0+dEpsPl;
  z_real_t epsPl = epsPl0+alpha*dEpsPl;

  // steepest slope
  z_real_t sig0=0.0,sig1=0.0,h1=0.0;
  unsigned int nIntParHarden=0;
  if (hardening) {
    nIntParHarden = hardening->nIntPar();
    const MatLibArray intP0Harden(intPar0,nIntParHarden,2);
    MatLibArray intP1Harden(intPar,nIntParHarden,2);
    // compute plastic stored energy
    z_real_t Wp0 = intPar0[1],dummy;
    hardening->storedEnergy(material,extPar,intP0Harden,intP1Harden,
                            Wp0,epsPl0,epsPl1,sig0,dummy,true,false);
    // compute plastic dissipated energy
    sig1 = hardening->yieldStress(material,extPar,intP0Harden,intP1Harden,
                                  epsPl,h1,dummy,true,false);
  }
  z_real_t sig2=0.0;
  if (viscous && dTime > 0.0) {
    unsigned int nIntParDiss = viscous->nIntPar();
    const MatLibArray intP0Diss(intPar0,nIntParDiss,1+nIntParHarden);
    MatLibArray intP1Diss(intPar,nIntParDiss,1+nIntParHarden);
    z_real_t epsPlDot = (epsPl1-epsPl0)/dTime;
    z_real_t siga,sigb,dummy;
    viscous->dissipatedEnergy(material,extPar,intP0Diss,intP1Diss,
                              epsPl,epsPlDot,siga,sigb,dummy,dummy,
                              dummy,true,false);
    z_real_t coef = alpha*dTime;
    sig2 = coef*siga + sigb;
  }
  
  return sig0 + sig1+alpha*h1*dEpsPl + sig2;
}
