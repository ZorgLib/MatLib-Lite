/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_LINEAR_ISOTROPIC_ELASTIC_POTENTIAL_H
#define ZORGLIB_MATL_MECA_LINEAR_ISOTROPIC_ELASTIC_POTENTIAL_H

// config
#include <matlib_macros.h>

// std C++ library
#include <stdexcept>
// local
#include <math/TensorAlgebra.h>
#include <matl/ModelDictionary.h>
#include <matl/meca/linear/Elasticity.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing elastic isotropic potentials.
 */
template <class ALG>
class IsotropicElasticPotential : virtual public Elasticity<ALG>::Potential {
  
 public:
  
  // define new types
  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;
    
  // constructor
  IsotropicElasticPotential() {}
  
  // copy constructor
  IsotropicElasticPotential(const IsotropicElasticPotential&) {}
  
  // destructor
  virtual ~IsotropicElasticPotential() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    if (os) (*os) << "\n\t***Isotropic elastic potential***" << std::endl;

    static const z_real_t ONE_THIRD = 1.0/3.0;
    static const z_real_t TWO_THIRD = 2.0/3.0;

    z_real_t E,K,lambda,mu,nu;
    try {
      // get Young's modulus
      E = material.getRealProperty("YOUNG_MODULUS");
      if (E < 0.0) {
        if (os) (*os) << "ERROR: Young's modulus must be positive." << std::endl;
        throw std::runtime_error("invalid property: Young's modulus");
      }

      // get Poisson's coefficient
      nu = material.getRealProperty("POISSON_COEFFICIENT");
      if (nu < -1.0 || nu > 0.5) {
        if (os) (*os) << "ERROR: Poisson's coefficient must be in [-1.0,0.5]." << std::endl;
        throw std::runtime_error("invalid property: Poisson's coefficient");
      }

      // compute other properties
      mu = 0.5*E/(1.+nu);
      K = ONE_THIRD*E/(1.0-2*nu);
      lambda = K-TWO_THIRD*mu;

      material.setProperty("BULK_MODULUS",K);
      material.setProperty("SHEAR_MODULUS",mu);
      material.setProperty("1ST_LAME_CONSTANT",lambda);
      material.setProperty("2ND_LAME_CONSTANT",mu);
    }
    catch (NoSuchPropertyException) {
      // get second Lame constant (a.k.a. shear modulus)
      try {
        mu = material.getRealProperty("2ND_LAME_CONSTANT");
        if (mu < 0.0) {
          if (os) (*os) << "ERROR: second Lame constant must be positive." << std::endl;
          throw std::runtime_error("invalid property: second Lame constant");
        }
      }
      catch (NoSuchPropertyException) {
        try {
          mu = material.getRealProperty("SHEAR_MODULUS");
          if (mu < 0.0) {
            if (os) (*os) << "ERROR: shear modulus must be positive." << std::endl;
            throw std::runtime_error("invalid property: shear modulus");
          }
          material.setProperty("2ND_LAME_CONSTANT",mu);
        }
        catch (NoSuchPropertyException e) {
          if (os) (*os) << "ERROR: second Lame constant is not defined." << std::endl;
          throw e;
        }
      }

      // get first Lame constant
      try {
        lambda = material.getRealProperty("1ST_LAME_CONSTANT");
        K = lambda+TWO_THIRD*mu;
        if (K < 0.0) {
          if (os) (*os) << "ERROR: bulk modulus must be positive." << std::endl;
          throw std::runtime_error("invalid property: first Lame constant");
        }
        material.setProperty("BULK_MODULUS",K);
      }
      catch (NoSuchPropertyException) {
        try {
          K = material.getRealProperty("BULK_MODULUS");
          if (K < 0.0) {
            if (os) (*os) << "ERROR: bulk modulus must be positive." << std::endl;
            throw std::runtime_error("invalid property: bulk modulus");
          }
        }
        catch (NoSuchPropertyException) {
          if (os) (*os) << "WARNING: bulk modulus set to zero." << std::endl;
          K = 0.0;
          material.setProperty("BULK_MODULUS",K);
        }
        lambda = K-TWO_THIRD*mu;
        material.setProperty("1ST_LAME_CONSTANT",lambda);
      }

      // compute other properties
      nu = (3*K-2*mu)/(6*K+2*mu);
      E = 2*mu*(1.+nu);

      material.setProperty("YOUNG_MODULUS",E);
      material.setProperty("POISSON_COEFFICIENT",nu);
    }

    if (os) {
      (*os) << "\tYoung's modulus       = " << E;
      (*os) << "\n\tPoisson's coefficient = " << nu;
      (*os) << "\n\tbulk modulus          = " << K;
      (*os) << "\n\t1st Lame constant     = " << lambda;
      (*os) << "\n\t2nd Lame constant     = " << mu << std::endl;
    }
    
    // compute dilatational elastic wave speed
    try {
      z_real_t rho = material.getRealProperty("MASS_DENSITY");
      z_real_t c = std::sqrt((lambda+2*mu)/rho);
      material.setProperty("CELERITY",c);
      if (os) (*os) << "\n\tcelerity              = " << c << std::endl;
    }
    catch (NoSuchPropertyException) {
      if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
    }
  }
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& gam,SYM_TENSOR& sig,
                        SYM_TENSOR4& M,bool first,bool second) {

    // get elastic constants
    z_real_t lambda = material.getRealProperty("1ST_LAME_CONSTANT");
    z_real_t mu     = material.getRealProperty("2ND_LAME_CONSTANT");
    
    // transform engineering strains
    SYM_TENSOR eps = contravariant(gam);

    // potential
    z_real_t tr = trace(eps);
    z_real_t W = 0.5*lambda*tr*tr + mu*innerProd2(eps,eps);
    if (!first && !second) return W;
    
    // stress
    z_real_t mu2 = mu+mu;
    if (first) {
      static SYM_TENSOR delta = SYM_TENSOR::identity();
      sig = (lambda*tr)*delta + mu2*eps;
    }
    
    // tangent
    if (second) {
      static const SYM_TENSOR4 I = SYM_TENSOR4::contravariantIdentity();
      static const SYM_TENSOR4 K = SYM_TENSOR4::baseK();
      M = mu2*I+(3*lambda)*K;
    }

    return W;
  }
      
  // compute stored energy (deviatoric part: eps = dev)
  z_real_t storedEnergyDev(const MaterialProperties& material,
                           const ConstitutiveModel::ParameterSet& extPar,
                           const SYM_TENSOR& gam,SYM_TENSOR& sig,
                           SYM_TENSOR4& M,bool first,bool second) {

    // get shear modulus
    z_real_t mu = material.getRealProperty("SHEAR_MODULUS");
        
    // transform engineering strains
    SYM_TENSOR eps = contravariant(gam);
        
    // potential (strains are supposed to be deviatoric)
    z_real_t W = mu*innerProd2(eps,eps);
    if (!first && !second) return W;
        
    // stress
    z_real_t mu2 = mu+mu;
    if (first) sig = mu2*eps;
    
    // tangent
    if (second) {
      static const SYM_TENSOR4 J = SYM_TENSOR4::contravariantJ();
      M = mu2*J;
    }
        
    return W;
  }
      
  // compute stored energy (volumic part: eps = trace)
  z_real_t storedEnergyVol(const MaterialProperties& material,
                           const ConstitutiveModel::ParameterSet& extPar,
                           z_real_t eps,z_real_t& sig,
                           z_real_t M,bool first,bool second) {
        
    // get bulk modulus
    z_real_t K = material.getRealProperty("BULK_MODULUS");
    
    // potential
    z_real_t W = 0.5*K*eps*eps;
    if (!first && !second) return W;

    // stress
    if (first) sig = K*eps;
    
    // tangent
    if (second) M = K;
    
    return W;
  }

  // compute material stiffness (Hooke) tensor
  void computeStiffness(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        SYM_TENSOR4& M) {
    
    // get elastic constants
    z_real_t lambda = material.getRealProperty("1ST_LAME_CONSTANT");
    z_real_t mu2 = 2*material.getRealProperty("2ND_LAME_CONSTANT");
    
    // stiffness
    static const SYM_TENSOR4 I = SYM_TENSOR4::contravariantIdentity();
    static const SYM_TENSOR4 K = SYM_TENSOR4::baseK();
    M = mu2*I+(3*lambda)*K;
  }
};


/**
 * Implementations of the model.
 */
class IsotropicElasticity3D : public Elasticity<TensorAlgebra3D> {
  
 public:
  
  // constructor
  IsotropicElasticity3D()
  : Elasticity<TensorAlgebra3D>(new IsotropicElasticPotential<TensorAlgebra3D>()) {}
  
  // copy constructor
  IsotropicElasticity3D(const IsotropicElasticity3D& src) 
  : Elasticity<TensorAlgebra3D>(src) {}
  
  // destructor
  virtual ~IsotropicElasticity3D() {}
};
class IsotropicElasticity2D : public Elasticity<TensorAlgebra2D> {
  
 public:
  
  // constructor
  IsotropicElasticity2D()
  : Elasticity<TensorAlgebra2D>(new IsotropicElasticPotential<TensorAlgebra2D>()) {}
  
  // copy constructor
  IsotropicElasticity2D(const IsotropicElasticity2D& src) 
  : Elasticity<TensorAlgebra2D>(src) {}
  
  // destructor
  virtual ~IsotropicElasticity2D() {}
};
class IsotropicElasticity1D : public Elasticity<TensorAlgebra1D> {
  
 public:
  
  // constructor
  IsotropicElasticity1D()
  : Elasticity<TensorAlgebra1D>(new IsotropicElasticPotential<TensorAlgebra1D>()) {}
  
  // copy constructor
  IsotropicElasticity1D(const IsotropicElasticity1D& src) 
  : Elasticity<TensorAlgebra1D>(src) {}
  
  // destructor
  virtual ~IsotropicElasticity1D() {}
};

/**
 * The associated model builder
 */
class IsotropicElasticityBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  IsotropicElasticityBuilder();
  
  // the instance
  static IsotropicElasticityBuilder const* BUILDER;
  
 public:
    
  // destructor
  virtual ~IsotropicElasticityBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
