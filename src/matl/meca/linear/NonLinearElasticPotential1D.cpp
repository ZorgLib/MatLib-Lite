/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "NonLinearElasticPotential1D.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

/*
 * Methods for class NonLinearElasticPotential1D
 */

// check consistency of material properties
void NonLinearElasticPotential1D::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\n\t***Non-linear elastic potential (pure 1D bar)***" << std::endl;
  
  // get Young's modulus
  z_real_t E = material.getRealProperty("YOUNG_MODULUS");
  if (E <= 0.0) {
    if (os) (*os) << "ERROR: Young's modulus must be strictly positive." << std::endl;
    throw std::runtime_error("invalid property: Young's modulus");
  }
  
  // get non-linear coefficient
  z_real_t alpha = material.getRealProperty("NON_LINEAR_COEFFICIENT");
  if (alpha < 0.0) {
    if (os) (*os) << "ERROR: non-linear coefficient must be positive." << std::endl;
    throw std::runtime_error("invalid property: non-linear coefficient");
  }

  if (os) {
    (*os) << "\tYoung's modulus        = " << E;
    (*os) << "\n\tnon-linear coefficient = " << alpha << std::endl;
  }
  
  // compute elastic wave speed
  try {
    z_real_t rho = material.getRealProperty("MASS_DENSITY");
    z_real_t c = std::sqrt(E/rho);
    material.setProperty("CELERITY",c);
    if (os) (*os) << "\n\tcelerity               = " << c << std::endl;
  }
  catch (NoSuchPropertyException) {
    if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
  }
}

// compute stored energy
z_real_t NonLinearElasticPotential1D::storedEnergy(const MaterialProperties& material,
                                                   const ConstitutiveModel::ParameterSet& extPar,
                                                   z_real_t eps,z_real_t& sig,z_real_t& M,
                                                   bool first,bool second) {
  // get material parameters
  z_real_t E = material.getRealProperty("YOUNG_MODULUS");
  z_real_t alpha = material.getRealProperty("NON_LINEAR_COEFFICIENT");

  // potential
  z_real_t eps2 = eps*eps;
  z_real_t W = 0.5*E*(1.0+0.5*alpha*eps2)*eps2;
  if (!first && !second) return W;
  
  // stress
  if (first) sig = E*(1.0+alpha*eps2)*eps;
  
  // tangent
  if (second) M = E*(1.0+3*alpha*eps2);
  
  return W;
}

/*
 * Methods for class NonLinearElasticBarBuilder.
 */

// the instance
NonLinearElasticBarBuilder const* NonLinearElasticBarBuilder::BUILDER
= new NonLinearElasticBarBuilder();

// constructor
NonLinearElasticBarBuilder::NonLinearElasticBarBuilder() {
  ModelDictionary::add("NON_LINEAR_ELASTIC_BAR",*this);
}

// build model
ConstitutiveModel* NonLinearElasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new NonLinearElasticBar();
      break;
    default:
      return 0;
      break;
  }
}
