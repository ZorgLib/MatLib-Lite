/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_LINEAR_ELASTIC_PLASTIC_BAR_H
#define ZORGLIB_MATL_MECA_LINEAR_ELASTIC_PLASTIC_BAR_H

// config
#include <matlib_macros.h>

// local
#include <matl/meca/linear/ElasticBar.h>
#include <matl/meca/linear/HardeningModels.h>
#include <matl/meca/linear/RateDependencyModels.h>
#include <matl/meca/linear/ViscoPlasticitySimple.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for pure 1D elasto-plasticity models.
 */
class ElastoPlasticBar : virtual public ElasticBar {
  
 protected:
  
  // associated visco-plasticity model
  ViscoPlasticitySimple *viscoPlasticity;

  // empty constructor
  ElastoPlasticBar(ViscoPlasticitySimple* vp = 0) {
    viscoPlasticity = vp;
  }
  
 public:
  
  // constructors
  ElastoPlasticBar(Potential& p,ViscoPlasticitySimple& vp)
  : ElasticBar(p) {viscoPlasticity = &vp;}
  ElastoPlasticBar(Potential& p,Dilatancy& d,ViscoPlasticitySimple& vp)
  : ElasticBar(p,d) {viscoPlasticity = &vp;}

  // copy constructor
  ElastoPlasticBar(const ElastoPlasticBar& src)
  : ElasticBar(src) {viscoPlasticity = src.viscoPlasticity;}

  // destructor
  virtual ~ElastoPlasticBar() {
    if (*(this->count) > 1) return;
    if (viscoPlasticity) delete viscoPlasticity;
  }

  // check consistency of properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // update properties in function of external parameters
  void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&);

  // how many internal variables ?
  unsigned int nIntVar() const {
    return 4+viscoPlasticity->nIntPar();
  }
  
  // self-documenting utilities
  unsigned int nIntVarBundled() const {return 5;}
  unsigned int getIntVar(const std::string& str) const {
    if (str == "STRS")
      return 0;
    else if (str == "PSTN")
      return 1;
    else if (str == "EPLS")
      return 2;
    else if (str == "ENRG")
      return 3;
    else if (str == "PNRG")
      return 4;
    else
      return 5;
  }
  ConstitutiveModel::VariableType typeIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 1:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 2:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 3:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 4:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      default:
        return ConstitutiveModel::TYPE_NONE;
        break;
    }
  }
  unsigned int indexIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return 0;
        break;
      case 1:
        return 1;
        break;
      case 2:
        return 2;
        break;
      case 3:
        return 3;
        break;
      case 4:
        return 4;
        break;
      default:
        return 5;
        break;
    }
  }
  std::string labelIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return "stress";
        break;
      case 1:
        return "plastic strain";
        break;
      case 2:
        return "equivalent plastic strain";
        break;
      case 3:
        return "elastically stored energy";
        break;
      case 4:
        return "plastically stored energy";
        break;
      default:
        return "";
        break;
    }
  }

  // check if the material behaviour is linear ?
  bool isLinear() const {return false;}
  
  // compute the incremental potential
  z_real_t incrementalPotential(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                const MaterialState&,MaterialState&,z_real_t,
                                MatLibMatrix&,bool,bool);

 protected:
  
  // compute the plastic update
  virtual z_real_t plasticUpdate(const MaterialProperties&,
                                 const ConstitutiveModel::ParameterSet&,
                                 z_real_t,z_real_t&,z_real_t,z_real_t&,
                                 const MatLibArray&,MatLibArray&,z_real_t,
                                 z_real_t&,bool,bool);
  
  // radial return algorithm
  virtual unsigned int radialReturn(const MaterialProperties&,
                                    const ConstitutiveModel::ParameterSet&,
                                    const MatLibArray&,MatLibArray&,
                                    z_real_t,z_real_t,z_real_t,z_real_t&,z_real_t);
};

/**
 * Class for pure 1D elasto-plasticity models with mixed hardening (isotropic + linear kinematic).
 */
class ElastoPlasticBarMixed : virtual public ElastoPlasticBar {
  
 protected:
  
  // empty constructor
  ElastoPlasticBarMixed() {}
  
 public:
  
  // constructors
  ElastoPlasticBarMixed(Potential& p,ViscoPlasticitySimple& vp)
  : ElasticBar(p), ElastoPlasticBar(&vp) {}
  ElastoPlasticBarMixed(Potential& p,Dilatancy& d,ViscoPlasticitySimple& vp)
  : ElasticBar(p,d), ElastoPlasticBar(&vp) {}
  
  // copy constructor
  ElastoPlasticBarMixed(const ElastoPlasticBarMixed& src)
  : ElasticBar(src), ElastoPlasticBar(src) {}
  
  // destructor
  virtual ~ElastoPlasticBarMixed() {}
  
  // check consistency of properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
 protected:
  
  // compute the plastic update
  virtual z_real_t plasticUpdate(const MaterialProperties&,
                                 const ConstitutiveModel::ParameterSet&,
                                 z_real_t,z_real_t&,z_real_t,z_real_t&,
                                 const MatLibArray&,MatLibArray&,z_real_t,
                                 z_real_t&,bool,bool);
  
  // radial return algorithm
  virtual unsigned int radialReturn(const MaterialProperties&,
                                    const ConstitutiveModel::ParameterSet&,
                                    const MatLibArray&,MatLibArray&,
                                    z_real_t,z_real_t,z_real_t,z_real_t&,z_real_t);
};


/*
 * Implementations of the model.
 */

/**
 * Elasto-plastic bar with linear isotropic hardening.
 */
class LinearIsotropicElastoPlasticBar : public ElastoPlasticBar {
  
 public:
  
  // constructor
  LinearIsotropicElastoPlasticBar()
  : ElasticBar(new LinearElasticPotential1D()),
    ElastoPlasticBar(new StdViscoPlasticitySimple(new LinearIsotropicHardeningModel(),
                                                  new PowerLawRateDependencyModel())) {}
  
  // copy constructor
  LinearIsotropicElastoPlasticBar(const LinearIsotropicElastoPlasticBar& src)
  : ElasticBar(src), ElastoPlasticBar(src) {}
  
  // destructor
  virtual ~LinearIsotropicElastoPlasticBar() {}
};

/**
 * The associated model builder
 */
class LinearIsotropicElastoPlasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  LinearIsotropicElastoPlasticBarBuilder();
  
  // the instance
  static LinearIsotropicElastoPlasticBarBuilder const* BUILDER;
  
public:
  
  // destructor
  virtual ~LinearIsotropicElastoPlasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};


/**
 * Elasto-plastic bar with nonlinear isotropic hardening.
 */
class NonLinearIsotropicElastoPlasticBar : public ElastoPlasticBar {
  
 public:
  
  // constructor
  NonLinearIsotropicElastoPlasticBar()
  : ElasticBar(new LinearElasticPotential1D()),
    ElastoPlasticBar(new StdViscoPlasticitySimple(new NonLinearIsotropicHardeningModel(),
                                                  new PowerLawRateDependencyModel())) {}
  
  // copy constructor
  NonLinearIsotropicElastoPlasticBar(const NonLinearIsotropicElastoPlasticBar& src)
  : ElasticBar(src), ElastoPlasticBar(src) {}
  
  // destructor
  virtual ~NonLinearIsotropicElastoPlasticBar() {}
};

/**
 * The associated model builder
 */
class NonLinearIsotropicElastoPlasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  NonLinearIsotropicElastoPlasticBarBuilder();
  
  // the instance
  static NonLinearIsotropicElastoPlasticBarBuilder const* BUILDER;
  
 public:
  
  // destructor
  virtual ~NonLinearIsotropicElastoPlasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};

/**
 * Elasto-plastic bar with mixed (linear isotropic + kinematic) hardening.
 */
class LinearMixedElastoPlasticBar : public ElastoPlasticBarMixed {
  
 public:
  
  // constructor
  LinearMixedElastoPlasticBar()
  : ElasticBar(new LinearElasticPotential1D()),
    ElastoPlasticBar(new StdViscoPlasticitySimple(new LinearIsotropicHardeningModel(),
                                                  new PowerLawRateDependencyModel())) {}
  
  // copy constructor
  LinearMixedElastoPlasticBar(const LinearMixedElastoPlasticBar& src)
  : ElasticBar(src), ElastoPlasticBar(src) {}
  
  // destructor
  virtual ~LinearMixedElastoPlasticBar() {}
};

/**
 * The associated model builder
 */
class LinearMixedElastoPlasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  LinearMixedElastoPlasticBarBuilder();
  
  // the instance
  static LinearMixedElastoPlasticBarBuilder const* BUILDER;
  
 public:
  
  // destructor
  virtual ~LinearMixedElastoPlasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};


/**
 * Elasto-plastic bar with mixed (nonlinear isotropic + linear kinematic) hardening.
 */
class NonLinearMixedElastoPlasticBar : public ElastoPlasticBarMixed {
  
 public:
  
  // constructor
  NonLinearMixedElastoPlasticBar()
  : ElasticBar(new LinearElasticPotential1D()),
    ElastoPlasticBar(new StdViscoPlasticitySimple(new NonLinearIsotropicHardeningModel(),
                                                  new PowerLawRateDependencyModel())) {}
  
  // copy constructor
  NonLinearMixedElastoPlasticBar(const NonLinearMixedElastoPlasticBar& src)
  : ElasticBar(src), ElastoPlasticBar(src) {}
  
  // destructor
  virtual ~NonLinearMixedElastoPlasticBar() {}
};

/**
 * The associated model builder
 */
class NonLinearMixedElastoPlasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  NonLinearMixedElastoPlasticBarBuilder();
  
  // the instance
  static NonLinearMixedElastoPlasticBarBuilder const* BUILDER;
  
 public:
  
  // destructor
  virtual ~NonLinearMixedElastoPlasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
