/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_POWER_LAW_ELASTIC_POTENTIAL_1D_H
#define ZORGLIB_MATL_MECA_POWER_LAW_ELASTIC_POTENTIAL_1D_H

// config
#include <matlib_macros.h>

// local
#include <matl/meca/linear/ElasticBar.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing 1D non-linear (power-law) elastic potential.
 */
class PowerLawElasticPotential1D : virtual public ElasticBar::Potential {
  
 public:
  
  // constructor
  PowerLawElasticPotential1D() {}
  
  // copy constructor
  PowerLawElasticPotential1D(const PowerLawElasticPotential1D&) {}
  
  // destructor
  virtual ~PowerLawElasticPotential1D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties&,
                        const ConstitutiveModel::ParameterSet&,
                        z_real_t,z_real_t&,z_real_t&,bool,bool);
};


/**
 * Implementations of the model.
 */
class PowerLawElasticBar : public ElasticBar {
  
 public:
  
  // constructor
  PowerLawElasticBar()
  : ElasticBar(new PowerLawElasticPotential1D()) {}
  
  // copy constructor
  PowerLawElasticBar(const PowerLawElasticBar& src)
  : ElasticBar(src) {}
  
  // destructor
  virtual ~PowerLawElasticBar() {}
};

/**
 * The associated model builder
 */
class PowerLawElasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  PowerLawElasticBarBuilder();
  
  // the instance
  static PowerLawElasticBarBuilder const* BUILDER;
  
public:
  
  // destructor
  virtual ~PowerLawElasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
