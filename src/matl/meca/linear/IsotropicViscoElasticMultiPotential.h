/*
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2023, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_LINEAR_ISOTROPIC_VISCO_ELASTIC_MULTI_POTENTIAL_H
#define ZORGLIB_MATL_MECA_LINEAR_ISOTROPIC_VISCO_ELASTIC_MULTI_POTENTIAL_H

// std C library
#include <cstdio>
// local
#include <matl/meca/linear/IsotropicViscousPotential.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing isotropic visco-elasticity models:
 * elastic part.
 */
template <class ALG>
class IsotropicElasticMultiPotential : virtual public Elasticity<ALG>::Potential {
  
 protected:
  
  // rank of viscoelastic module
  unsigned int rank;

  // isochoric?
  bool isochoric;
  
 public:
    
  // define new types
  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;
  
  // constructor
  IsotropicElasticMultiPotential(unsigned int r,bool i) {
    rank = r;
    isochoric = i;
  }
  
  // copy constructor
  IsotropicElasticMultiPotential(const IsotropicElasticMultiPotential& src) {
    rank = src.rank;
    isochoric = src.isochoric;
  }
  
  // destructor
  virtual ~IsotropicElasticMultiPotential() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    if (os) {
      (*os) << "\n\t***Isotropic viscoelastic branch #";
      (*os) << rank << " (elastic part)***" << std::endl;
    }
      
    static const z_real_t ONE_THIRD = 1.0/3.0;
    static const z_real_t TWO_THIRD = 2.0/3.0;

    char str[64];
    z_real_t E,K=0.0,lambda=0.0,mu,nu;
    try {
      // get Young's modulus
      std::snprintf(str,64,"YOUNG_MODULUS_%u",rank);
      E = material.getRealProperty(str);
      if (E < 0.0) {
        if (os) (*os) << "ERROR: Young's modulus must be positive." << std::endl;
        throw std::runtime_error(std::string("invalid property: ") + str);
      }

      // get Poisson's coefficient
      std::snprintf(str,64,"POISSON_COEFFICIENT_%u",rank);
      nu = material.getRealProperty(str);
      if (nu < -1.0 || nu > 0.5) {
        if (os) (*os) << "ERROR: Poisson's coefficient must be in [-1.0,0.5]." << std::endl;
        throw std::runtime_error(std::string("invalid property: ") + str);
      }

      // compute other properties
      mu = 0.5*E/(1.0+nu);
      K = ONE_THIRD*E/(1.0-2*nu);
      lambda = K-TWO_THIRD*mu;

      std::snprintf(str,64,"BULK_MODULUS_%u",rank);
      material.setProperty(str,K);
      std::snprintf(str,64,"SHEAR_MODULUS_%u",rank);
      material.setProperty(str,mu);
      std::snprintf(str,64,"1ST_LAME_CONSTANT_%u",rank);
      material.setProperty(str,lambda);
      std::snprintf(str,64,"2ND_LAME_CONSTANT_%u",rank);
      material.setProperty(str,mu);
    }
    catch (NoSuchPropertyException) {
      // get second Lame constant (a.k.a. shear modulus)
      try {
        std::snprintf(str,64,"2ND_LAME_CONSTANT_%u",rank);
        mu = material.getRealProperty(str);
        if (mu < 0.0) {
          if (os) (*os) << "ERROR: Lame constants must be positive." << std::endl;
          throw std::runtime_error(std::string("invalid property: ") + str);
        }
      }
      catch (NoSuchPropertyException) {
        try {
          std::snprintf(str,64,"SHEAR_MODULUS_%u",rank);
          mu = material.getRealProperty(str);
          if (mu < 0.0) {
            if (os) (*os) << "ERROR: shear modulus must be positive." << std::endl;
            throw std::runtime_error(std::string("invalid property: ") + str);
          }
          std::snprintf(str,64,"2ND_LAME_CONSTANT_%u",rank);
          material.setProperty(str,mu);
        }
        catch (NoSuchPropertyException e) {
          if (os) (*os) << "ERROR: second Lame constant is not defined." << std::endl;
          throw e;
        }
      }

      // get first Lame constant
      if (!isochoric) {
        try {
          std::snprintf(str,64,"1ST_LAME_CONSTANT_%u",rank);
          lambda = material.getRealProperty(str);
          if (lambda < 0.0) {
            if (os) (*os) << "ERROR: Lame constants must be positive." << std::endl;
            throw std::runtime_error(std::string("invalid property: ") + str);
          }
          K = lambda+TWO_THIRD*mu;
          std::snprintf(str,64,"BULK_MODULUS_%u",rank);
          material.setProperty(str,K);
        }
        catch (NoSuchPropertyException) {
          try {
            std::snprintf(str,64,"BULK_MODULUS_%u",rank);
            K = material.getRealProperty(str);
            if (K < 0.0) {
              if (os) (*os) << "ERROR: bulk modulus must be positive." << std::endl;
              throw std::runtime_error(std::string("invalid property: ") + str);
            }
          }
          catch (NoSuchPropertyException) {
            if (os) (*os) << "WARNING: bulk modulus set to zero." << std::endl;
            K = 0.0;
            material.setProperty(str,K);
          }
          lambda = K-TWO_THIRD*mu;
          std::snprintf(str,64,"1ST_LAME_CONSTANT_%u",rank);
          material.setProperty(str,lambda);
        }
        
        // compute other properties
        nu = (3*K-2*mu)/(6*K+2*mu);
        E = 2*mu*(1.+nu);
      }
      else {
        nu = 0.5;
        E = 3*mu;
      }

      std::snprintf(str,64,"YOUNG_MODULUS_%u",rank);
      material.setProperty(str,E);
      std::snprintf(str,64,"POISSON_COEFFICIENT_%u",rank);
      material.setProperty(str,nu);
    }
      
    if (os) {
      if (isochoric) {
        (*os) << "\tincompressibility enforced";
        (*os) << "\n\tshear modulus = " << mu << std::endl;
      }
      else {
        (*os) << "\tYoung's modulus       = "   << E;
        (*os) << "\n\tPoisson's coefficient = " << nu;
        (*os) << "\n\tbulk modulus          = " << K;
        (*os) << "\n\t1st Lame constant     = " << lambda;
        (*os) << "\n\t2nd Lame constant     = " << mu << std::endl;
      }
    }
  }
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        const SYM_TENSOR& gam,SYM_TENSOR& sig,
                        SYM_TENSOR4& M,bool first,bool second) {
    
    // get elastic modulus
    char str[64];
    std::snprintf(str,64,"SHEAR_MODULUS_%u",rank);
    z_real_t mu = material.getRealProperty(str);
    z_real_t mu2 = mu+mu;
    
    // transform engineering strains
    SYM_TENSOR eps = contravariant(gam);
    
    // compute deviatoric part of energy
    z_real_t W = mu*innerProd2(eps,eps);
    if (first) sig = mu2*eps;
    
    if (!isochoric) {
      // get 1st Lame constant
      std::snprintf(str,64,"1ST_LAME_CONSTANT_%u",rank);
      z_real_t lambda = material.getRealProperty(str);
      
      // compute volumic part of energy
      z_real_t tr = trace(eps);
      W += 0.5*lambda*tr*tr;
      if (first) {
        static SYM_TENSOR delta = SYM_TENSOR::identity();
        sig += (lambda*tr)*delta;
      }
      if (second) {
        static const SYM_TENSOR4 I = SYM_TENSOR4::contravariantIdentity();
        static const SYM_TENSOR4 K = SYM_TENSOR4::baseK();
        M = mu2*I + (3*lambda)*K;
      }
    }
    else {
      if (second) {
        static const SYM_TENSOR4 J = SYM_TENSOR4::contravariantJ ();
        M = mu2*J;
      }
    }
    
    return W;
  }
  
  // compute material stiffness (Hooke) tensor
  void computeStiffness(const MaterialProperties& material,
                        const ConstitutiveModel::ParameterSet& extPar,
                        SYM_TENSOR4& M) {
    
    // get elastic constants
    char str[64];
    std::snprintf(str,64,"SHEAR_MODULUS_%u",rank);
    z_real_t mu2 = 2*material.getRealProperty(str);
    
    if (!isochoric) {
      // get 1st Lame constant
      std::snprintf(str,64,"1ST_LAME_CONSTANT_%u",rank);
      z_real_t lambda = material.getRealProperty(str);
    
      // stiffness
      static const SYM_TENSOR4 I = SYM_TENSOR4::contravariantIdentity();
      static const SYM_TENSOR4 K = SYM_TENSOR4::baseK();
      M = mu2*I + (3*lambda)*K;
    }
    else {
      static const SYM_TENSOR4 J = SYM_TENSOR4::contravariantJ();
      M = mu2*J;
    }
  }
};


/**
 * Class describing isotropic visco-elasticity models:
 * viscous part.
 */
template <class ALG>
class IsotropicViscousMultiPotential
: virtual public ViscoElasticity<ALG>::ViscousPotential {
  
 protected:
  
  // rank of viscoelastic module
  unsigned int rank;

  // isochoric?
  bool isochoric;

 public:
    
  // define new types
  typedef typename ALG::SymTensor  SYM_TENSOR;
  typedef typename ALG::SymTensor4 SYM_TENSOR4;
  
  // constructor
  IsotropicViscousMultiPotential(unsigned int r,bool i) {
    rank = r;
    isochoric = i;
  }
  
  // copy constructor
  IsotropicViscousMultiPotential(const IsotropicViscousMultiPotential& src) {
    rank = src.rank;
    isochoric = src.isochoric;
  }
  
  // destructor
  virtual ~IsotropicViscousMultiPotential() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    if (os) {
      (*os) << "\n\t***Isotropic viscoelastic branch #";
      (*os) << rank << " (viscous part)***" << std::endl;
    }
      
    static const z_real_t ONE_THIRD = 1.0/3.0;
    static const z_real_t TWO_THIRD = 2.0/3.0;

    char str[64];
    z_real_t E,K=0.0,lambda=0.0,mu,nu;
    try {
      // get viscous Young's modulus
      std::snprintf(str,64,"VISCOUS_YOUNG_MODULUS_%u",rank);
      E = material.getRealProperty(str);
      if (E < 0.0) {
        if (os) (*os) << "ERROR: viscous Young's modulus must be positive." << std::endl;
        throw std::runtime_error(std::string("invalid property: ") + str);
      }
      
      // get viscous Poisson's coefficient
      std::snprintf(str,64,"VISCOUS_POISSON_COEFFICIENT_%u",rank);
      nu = material.getRealProperty(str);
      if (nu < -1.0 || nu > 0.5) {
        if (os) (*os) << "ERROR: viscous Poisson's coefficient must be in [-1.0,0.5]." << std::endl;
        throw std::runtime_error(std::string("invalid property: ") + str);
      }
      
      // compute other properties
      mu = 0.5*E/(1.0+nu);
      K = ONE_THIRD*E/(1.0-2*nu);
      lambda = K-TWO_THIRD*mu;
      
      std::snprintf(str,64,"VISCOUS_BULK_MODULUS_%u",rank);
      material.setProperty(str,K);
      std::snprintf(str,64,"VISCOUS_SHEAR_MODULUS_%u",rank);
      material.setProperty(str,mu);
      std::snprintf(str,64,"VISCOUS_1ST_LAME_CONSTANT_%u",rank);
      material.setProperty(str,lambda);
      std::snprintf(str,64,"VISCOUS_2ND_LAME_CONSTANT_%u",rank);
      material.setProperty(str,mu);
    }
    catch (NoSuchPropertyException) {
      // get viscous second Lame constant (a.k.a. viscous shear modulus)
      try {
        std::snprintf(str,64,"VISCOUS_2ND_LAME_CONSTANT_%u",rank);
        mu = material.getRealProperty(str);
        if (mu < 0.0) {
          if (os) (*os) << "ERROR: viscous Lame constants must be positive." << std::endl;
          throw std::runtime_error(std::string("invalid property: ") + str);
        }
      }
      catch (NoSuchPropertyException) {
        try {
          std::snprintf(str,64,"VISCOUS_SHEAR_MODULUS_%u",rank);
          mu = material.getRealProperty(str);
          if (mu < 0.0) {
            if (os) (*os) << "ERROR: viscous shear modulus must be positive." << std::endl;
            throw std::runtime_error(std::string("invalid property: ") + str);
          }
          std::snprintf(str,64,"VISCOUS_2ND_LAME_CONSTANT_%u",rank);
          material.setProperty(str,mu);
        }
        catch (NoSuchPropertyException e) {
          if (os) (*os) << "ERROR: viscous second Lame constant is not defined." << std::endl;
          throw e;
        }
      }
      
      // get viscous first Lame constant
      if (!isochoric) {
        try {
          std::snprintf(str,64,"VISCOUS_1ST_LAME_CONSTANT_%u",rank);
          lambda = material.getRealProperty(str);
          if (lambda < 0.0) {
            if (os) (*os) << "ERROR: viscous Lame constants must be positive." << std::endl;
            throw std::runtime_error(std::string("invalid property: ") + str);
          }
          K = lambda+TWO_THIRD*mu;
          std::snprintf(str,64,"VISCOUS_BULK_MODULUS_%u",rank);
          material.setProperty(str,K);
        }
        catch (NoSuchPropertyException) {
          try {
            std::snprintf(str,64,"VISCOUS_BULK_MODULUS_%u",rank);
            K = material.getRealProperty(str);
            if (K < 0.0) {
              if (os) (*os) << "ERROR: viscous bulk modulus must be positive." << std::endl;
              throw std::runtime_error(std::string("invalid property: ") + str);
            }
          }
          catch (NoSuchPropertyException) {
            if (os) (*os) << "WARNING: viscous bulk modulus set to zero." << std::endl;
            K = 0.0;
            material.setProperty(str,K);
          }
          lambda = K-TWO_THIRD*mu;
          std::snprintf(str,64,"VISCOUS_1ST_LAME_CONSTANT_%u",rank);
          material.setProperty(str,lambda);
        }
        
        // compute other properties
        nu = (3*K-2*mu)/(6*K+2*mu);
        E = 2*mu*(1.0+nu);
      }
      else {
        nu = 0.5;
        E = 3*mu;
      }
      
      std::snprintf(str,64,"VISCOUS_YOUNG_MODULUS_%u",rank);
      material.setProperty(str,E);
      std::snprintf(str,64,"VISCOUS_POISSON_COEFFICIENT_%u",rank);
      material.setProperty(str,nu);
    }
    
    if (os) {
      if (isochoric) {
        (*os) << "\tincompressibility enforced";
        (*os) << "\n\tviscous shear modulus = " << mu << std::endl;
      }
      else {
        (*os) << "\tviscous Young's modulus       = "   << E;
        (*os) << "\n\tviscous Poisson's coefficient = " << nu;
        (*os) << "\n\tviscous bulk modulus          = " << K;
        (*os) << "\n\tviscous 1st Lame constant     = " << lambda;
        (*os) << "\n\tviscous 2nd Lame constant     = " << mu << std::endl;
      }
    }
  }

  // compute dissipated energy
  z_real_t dissipatedEnergy(const MaterialProperties& material,
                            const ConstitutiveModel::ParameterSet& extPar,
                            const SYM_TENSOR& gam,const SYM_TENSOR& gamDot,
                            SYM_TENSOR& sig1,SYM_TENSOR& sig2,
                            SYM_TENSOR4& M11,SYM_TENSOR4& M22,
                            SYM_TENSOR4& M12,z_real_t dTime,
                            bool first,bool second) {
    
    // get viscous modulus
    char str[64];
    std::snprintf(str,64,"VISCOUS_SHEAR_MODULUS_%u",rank);
    z_real_t mu = material.getRealProperty(str);
    z_real_t mu2 = mu+mu;
      
    // transform engineering strains
    SYM_TENSOR epsDot = contravariant(gamDot);
      
    // compute deviatoric part of energy
    z_real_t Wv = mu*innerProd2(epsDot,epsDot);
    if (first) {
      sig1 = 0.0;
      sig2 = mu2*epsDot;
    }
    
    if (!isochoric) {
      // get viscous 1st Lame constant
      std::snprintf(str,64,"VISCOUS_1ST_LAME_CONSTANT_%u",rank);
      z_real_t lambda = material.getRealProperty(str);
      
      // compute volumic part of energy
      z_real_t tr = trace(epsDot);
      Wv += 0.5*lambda*tr*tr; 
      if (first) {
        static SYM_TENSOR delta = SYM_TENSOR::identity();
        sig2 += (lambda*tr)*delta;
      }
      if (second) {
        static const SYM_TENSOR4 I = SYM_TENSOR4::contravariantIdentity();
        static const SYM_TENSOR4 K = SYM_TENSOR4::baseK();
        M11 = 0.0;
        M12 = 0.0;
        M22 = mu2*I + (3*lambda)*K;
      }
    }
    else {
      if (second) {
        static const SYM_TENSOR4 J = SYM_TENSOR4::contravariantJ();
        M11 = 0.0;
        M12 = 0.0;
        M22 = mu2*J;
      }
    }

    return Wv;
  }
};


/**
 * Class for standard (linear) isotropic viscoelastic model (Maxwell branch).
 */
template <class ALG>
class IsotropicMaxwellViscoElasticity 
: virtual public StdMaxwellViscoElasticity<ALG> {
  
 public:
    
  // constructor
  IsotropicMaxwellViscoElasticity(unsigned int r,bool i = true)
  : StdMaxwellViscoElasticity<ALG>(*(new IsotropicElasticMultiPotential<ALG>(r,i)),
                                   *(new IsotropicViscousMultiPotential<ALG>(r,i)),
                                   i) {}
  
  // copy constructor
  IsotropicMaxwellViscoElasticity(const IsotropicMaxwellViscoElasticity& src)
  : StdMaxwellViscoElasticity<ALG>(src) {}
};


/**
 * Implementations of the model : Maxwell.
 */
class IsotropicMaxwellViscoElasticity3D : public ViscoElasticity<TensorAlgebra3D> {
  
 public:
  
  // constructor
  IsotropicMaxwellViscoElasticity3D()
  : Elasticity<TensorAlgebra3D>(new IsotropicElasticPotential<TensorAlgebra3D>()) {}
  
  // copy constructor
  IsotropicMaxwellViscoElasticity3D(const IsotropicMaxwellViscoElasticity3D& src) 
  : Elasticity<TensorAlgebra3D>(src), ViscoElasticity<TensorAlgebra3D>(src) {}
  
  // destructor
  virtual ~IsotropicMaxwellViscoElasticity3D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0){

    // initialize maxwell branches
    try {
      unsigned int nBranches = material.getIntegerProperty("NUMBER_OF_MAXWELL_BRANCHES");
      if (nBranches < nMaxwellBranches())
        throw std::runtime_error("ERROR: inconsistency in number of Maxwell branches");
      for (unsigned int i=nMaxwellBranches(); i < nBranches; i++)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra3D>(i+1)));
    }
    catch (NoSuchPropertyException) {
      if (nMaxwellBranches() == 0)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra3D>(1)));
    }

    // check properties
    ViscoElasticity<TensorAlgebra3D>::checkProperties(material,os);
  }
};
class IsotropicMaxwellViscoElasticity2D : public ViscoElasticity<TensorAlgebra2D> {
  
 public:
  
  // constructor
  IsotropicMaxwellViscoElasticity2D()
  : Elasticity<TensorAlgebra2D>(new IsotropicElasticPotential<TensorAlgebra2D>()) {}
  
  // copy constructor
  IsotropicMaxwellViscoElasticity2D(const IsotropicMaxwellViscoElasticity2D& src) 
  : Elasticity<TensorAlgebra2D>(src), ViscoElasticity<TensorAlgebra2D>(src) {}
  
  // destructor
  virtual ~IsotropicMaxwellViscoElasticity2D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    
    // initialize maxwell branches
    try {
      unsigned int nBranches = material.getIntegerProperty("NUMBER_OF_MAXWELL_BRANCHES");
      if (nBranches < nMaxwellBranches())
        throw std::runtime_error("ERROR: inconsistency in number of Maxwell branches");
      for (unsigned int i=nMaxwellBranches(); i < nBranches; i++)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra2D>(i+1)));
    }
    catch (NoSuchPropertyException) {
      addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra2D>(1)));
    }
    
    // check properties
    ViscoElasticity<TensorAlgebra2D>::checkProperties(material,os);
  }
};
class IsotropicMaxwellViscoElasticity1D : public ViscoElasticity<TensorAlgebra1D> {
  
 public:
  
  // constructor
  IsotropicMaxwellViscoElasticity1D()
  : Elasticity<TensorAlgebra1D>(new IsotropicElasticPotential<TensorAlgebra1D>()) {}
  
  // copy constructor
  IsotropicMaxwellViscoElasticity1D(const IsotropicMaxwellViscoElasticity1D& src) 
  : Elasticity<TensorAlgebra1D>(src), ViscoElasticity<TensorAlgebra1D>(src) {}
  
  // destructor
  virtual ~IsotropicMaxwellViscoElasticity1D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    
    // initialize maxwell branches
    try {
      unsigned int nBranches = material.getIntegerProperty("NUMBER_OF_MAXWELL_BRANCHES");
      if (nBranches < nMaxwellBranches())
        throw std::runtime_error("ERROR: inconsistency in number of Maxwell branches");
      for (unsigned int i=nMaxwellBranches(); i < nBranches; i++)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra1D>(i+1)));
    }
    catch (NoSuchPropertyException) {
      addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra1D>(1)));
    }
    
    // check properties
    ViscoElasticity<TensorAlgebra1D>::checkProperties(material,os);
  }
};

/**
 * The associated model builder
 */
class IsotropicMaxwellViscoElasticityBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  IsotropicMaxwellViscoElasticityBuilder();
  
  // the instance
  static IsotropicMaxwellViscoElasticityBuilder const* BUILDER;
  
 public:
    
  // destructor
  virtual ~IsotropicMaxwellViscoElasticityBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};


/**
 * Implementations of the model : general viscoelasticity model (Kelvin+Maxwell).
 */
class IsotropicViscoElasticity3D : public ViscoElasticity<TensorAlgebra3D> {
  
 public:
  
  // constructor
  IsotropicViscoElasticity3D()
  : Elasticity<TensorAlgebra3D>(new IsotropicElasticPotential<TensorAlgebra3D>()),
    ViscoElasticity<TensorAlgebra3D>(new IsotropicViscousPotential<TensorAlgebra3D>()) {}
  
  // copy constructor
  IsotropicViscoElasticity3D(const IsotropicViscoElasticity3D& src) 
  : Elasticity<TensorAlgebra3D>(src), ViscoElasticity<TensorAlgebra3D>(src) {}
  
  // destructor
  virtual ~IsotropicViscoElasticity3D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    
    // initialize maxwell branches
    try {
      unsigned int nBranches = material.getIntegerProperty("NUMBER_OF_MAXWELL_BRANCHES");
      if (nBranches < nMaxwellBranches())
        throw std::runtime_error("ERROR: inconsistency in number of Maxwell branches");
      for (unsigned int i=nMaxwellBranches(); i < nBranches; i++)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra3D>(i+1)));
    }
    catch (NoSuchPropertyException) {
      addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra3D>(1)));
    }
    
    // check properties
    ViscoElasticity<TensorAlgebra3D>::checkProperties(material,os);
  }
};
class IsotropicViscoElasticity2D : public ViscoElasticity<TensorAlgebra2D> {
  
 public:
  
  // constructor
  IsotropicViscoElasticity2D()
  : Elasticity<TensorAlgebra2D>(new IsotropicElasticPotential<TensorAlgebra2D>()),
    ViscoElasticity<TensorAlgebra2D>(new IsotropicViscousPotential<TensorAlgebra2D>()) {}
  
  // copy constructor
  IsotropicViscoElasticity2D(const IsotropicViscoElasticity2D& src) 
  : Elasticity<TensorAlgebra2D>(src), ViscoElasticity<TensorAlgebra2D>(src) {}
  
  // destructor
  virtual ~IsotropicViscoElasticity2D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    
    // initialize maxwell branches
    try {
      unsigned int nBranches = material.getIntegerProperty("NUMBER_OF_MAXWELL_BRANCHES");
      if (nBranches < nMaxwellBranches())
        throw std::runtime_error("ERROR: inconsistency in number of Maxwell branches");
      for (unsigned int i=nMaxwellBranches(); i < nBranches; i++)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra2D>(i+1)));
    }
    catch (NoSuchPropertyException) {
      addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra2D>(1)));
    }
    
    // check properties
    ViscoElasticity<TensorAlgebra2D>::checkProperties(material,os);
  }
};
class IsotropicViscoElasticity1D : public ViscoElasticity<TensorAlgebra1D> {
  
 public:
  
  // constructor
  IsotropicViscoElasticity1D()
  : Elasticity<TensorAlgebra1D>(new IsotropicElasticPotential<TensorAlgebra1D>()),
    ViscoElasticity<TensorAlgebra1D>(new IsotropicViscousPotential<TensorAlgebra1D>()) {}
  
  // copy constructor
  IsotropicViscoElasticity1D(const IsotropicViscoElasticity1D& src) 
  : Elasticity<TensorAlgebra1D>(src), ViscoElasticity<TensorAlgebra1D>(src) {}
  
  // destructor
  virtual ~IsotropicViscoElasticity1D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties& material,std::ostream* os = 0) {
    
    // initialize maxwell branches
    try {
      unsigned int nBranches = material.getIntegerProperty("NUMBER_OF_MAXWELL_BRANCHES");
      if (nBranches < nMaxwellBranches())
        throw std::runtime_error("ERROR: inconsistency in number of Maxwell branches");
      for (unsigned int i=nMaxwellBranches(); i < nBranches; i++)
        addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra1D>(i+1)));
    }
    catch (NoSuchPropertyException) {
      addMaxwellBranch(*(new IsotropicMaxwellViscoElasticity<TensorAlgebra1D>(1)));
    }
    
    // check properties
    ViscoElasticity<TensorAlgebra1D>::checkProperties(material,os);
  }
};

/**
 * The associated model builder
 */
class IsotropicViscoElasticityBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  IsotropicViscoElasticityBuilder();
  
  // the instance
  static IsotropicViscoElasticityBuilder const* BUILDER;
  
 public:
    
  // destructor
  virtual ~IsotropicViscoElasticityBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};


#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
