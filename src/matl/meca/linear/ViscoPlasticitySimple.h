/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_LINEAR_VISCO_PLASTICITY_SIMPLE_H
#define ZORGLIB_MATL_MECA_LINEAR_VISCO_PLASTICITY_SIMPLE_H

// config
#include <matlib_macros.h>

// local
#include <matl/ConstitutiveModel.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class for describing a J2 visco-plasticity model with isotropic hardening.
 * Also works for Tresca.
 */
class ViscoPlasticitySimple {
  
 public:
  
  // flag indicating if the model needs initialization
  bool initialize;
  
  // flag indicating if the model needs finalization (update internal parameters)
  bool finalize;
  
  // destructor
  virtual ~ViscoPlasticitySimple() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&) {}
  
  // number of internal parameters (should be at least 1 for the stored plastic energy)
  virtual unsigned int nIntPar() const = 0;
  
  // compute irreversible energy and derivatives
  virtual z_real_t irreversibleEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                      const MatLibArray&,MatLibArray&,z_real_t,z_real_t,
                                      z_real_t&,z_real_t&,z_real_t,bool,bool) = 0;
};


/**
 * Isotropic hardening model.
 */
class IsotropicHardeningModel {
  
 public:
  
  // flag indicating if the model needs initialization
  bool initialize;
  
  // flag indicating if the model needs finalization (update internal parameters)
  bool finalize;
  
  // destructor
  virtual ~IsotropicHardeningModel() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&) {}
  
  // number of internal parameters
  virtual unsigned int nIntPar() const = 0;

  // plastically stored energy
  virtual z_real_t storedEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                const MatLibArray&,MatLibArray&,z_real_t,z_real_t,
                                z_real_t,z_real_t&,z_real_t&,bool,bool) = 0;
  
  // yield stress
  virtual z_real_t yieldStress(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                               const MatLibArray&,MatLibArray&,z_real_t,
                               z_real_t&,z_real_t&,bool,bool) = 0;
};


/**
 * Scalar rate-dependency model.
 */
class ScalarRateDependencyModel {
  
 public:
  
  // flag indicating if the model needs initialization
  bool initialize;
  
  // flag indicating if the model needs finalization (update internal parameters)
  bool finalize;
  
  // destructor
  virtual ~ScalarRateDependencyModel() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&) {}
  
  // number of internal parameters
  virtual unsigned int nIntPar() const = 0;
  
  // dissipated energy
  virtual z_real_t dissipatedEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                    const MatLibArray&,MatLibArray&,
                                    z_real_t,z_real_t,z_real_t&,z_real_t&,z_real_t&,z_real_t&,
                                    z_real_t&,bool,bool) = 0;
};


/**
 * Standard visco-plasticity model (with isotropic hardening).
 */
class StdViscoPlasticitySimple : public ViscoPlasticitySimple {
  
 protected:
  
  // plastic hardening part (stored and dissipated)
  IsotropicHardeningModel *hardening;
  
  // viscous dissipation part
  ScalarRateDependencyModel *viscous;
  
  // instance counter
  unsigned int *count;
  
 public:

  // constructor
  StdViscoPlasticitySimple(IsotropicHardeningModel*,
                           ScalarRateDependencyModel*);
  
  // copy constructor
  StdViscoPlasticitySimple(const StdViscoPlasticitySimple&);
  
  // destructor
  virtual ~StdViscoPlasticitySimple();
  
  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // update properties in function of external parameters
  void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&);
  
  // number of internal parameters
  unsigned int nIntPar() const;
  
  // compute irreversible energy and derivatives
  z_real_t irreversibleEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                              const MatLibArray&,MatLibArray&,z_real_t,z_real_t,
                              z_real_t&,z_real_t&,z_real_t,bool,bool);
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
