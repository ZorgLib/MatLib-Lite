/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "ElastoPlasticBar.h"

// std C library
#include <cmath>
// std C++ library
#include <limits>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

/*
 * Methods for class ElastoPlasticBar
 */

// check consistency of properties
void ElastoPlasticBar::checkProperties(MaterialProperties& material,std::ostream *os) {
  if (os) (*os) << "\nElasto-plasticity model with isotropic hardening (pure 1D bar):" << std::endl;

  // density
  try {
    z_real_t rho = material.getRealProperty("MASS_DENSITY");
    if (rho <= 0.0) {
      if (os) (*os) << "ERROR: mass density must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: mass density");
    }
    if (os) (*os) << "\n\tmass density = " << rho << std::endl;
  }
  catch (NoSuchPropertyException) {
    if (os) (*os) << "\n\tmass density is not defined" << std::endl;
  }

  // bar cross section
  try {
    z_real_t A = material.getRealProperty("SECTION");
    if (A <= 0.0) {
      if (os) (*os) << "ERROR: bar cross-section must be positive." << std::endl;
      throw std::runtime_error("invalid property: bar cross-section");
    }
    if (os) (*os) << "\n\tbar cross-section = " << A << std::endl;
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "\n\tbar cross-section set to 1" << std::endl;
    material.setProperty("SECTION",1.0);
  }

  // elastic potential
  this->potential->checkProperties(material,os);

  // dilatancy model
  if (this->dilatancy) this->dilatancy->checkProperties(material,os);

  // viscoplastic model
  viscoPlasticity->checkProperties(material,os);
}

// update properties in function of external parameters
void ElastoPlasticBar::updateProperties(MaterialProperties& mater,const ConstitutiveModel::ParameterSet& extPar) {
  ElasticBar::updateProperties(mater,extPar);
  viscoPlasticity->updateProperties(mater,extPar);
}

// compute the incremental potential
z_real_t ElastoPlasticBar::incrementalPotential(const MaterialProperties& material,
                                                const ConstitutiveModel::ParameterSet& extPar,
                                                const MaterialState& state0,MaterialState& state,
                                                z_real_t dTime,MatLibMatrix& M,
                                                bool update,bool tangent) {
  // get cross-section
  z_real_t A = material.getRealProperty("SECTION");

  // get strain
  z_real_t eps = state.grad[0];

  // get plastic strain
  z_real_t epsP0 = state0.internal[1];

  // get incremental potential
  unsigned int sz = nIntVar()-2;
  const MatLibArray intVar0(state0.internal,sz,2);
  MatLibArray intVar(state.internal,sz,2);
  z_real_t sig,epsP,K;
  z_real_t W = plasticUpdate(material,extPar,eps,sig,epsP0,epsP,
                             intVar0,intVar,dTime,K,update,tangent);
  if (update) {
    state.flux[0] = A*sig;
    state.internal[0] = sig;
    state.internal[1] = epsP;
  }
  if (tangent) M[0][0] = A*K;

  return W;
}

// compute the plastic update
z_real_t ElastoPlasticBar::plasticUpdate(const MaterialProperties& material,
                                         const ConstitutiveModel::ParameterSet& extPar,
                                         z_real_t eps,z_real_t& sig,
                                         z_real_t epsPl0,z_real_t& epsPl,
                                         const MatLibArray& intV0,MatLibArray& intV,
                                         z_real_t dTime,z_real_t& M,
                                         bool update,bool computeTangent) {
  // get equivalent plastic strain
  z_real_t ePl0 = intV0[0];
  z_real_t ePl  = intV[0];

  // extract internal parameters
  unsigned int nIntPar = intV.size()-2;
  const MatLibArray intPar0(intV0,nIntPar,2);
  MatLibArray intPar(intV,nIntPar,2);

  // update
  viscoPlasticity->initialize = true;
  viscoPlasticity->finalize = false;
  z_real_t dEPl=0.0;
  if (update) {

    // perform update (radial return)
    radialReturn(material,extPar,intPar0,intPar,
                 eps,epsPl0,ePl0,ePl,dTime);

    // update internal variables
    intV[0] = ePl;

    // update plastic strain
    dEPl = ePl-ePl0;
    epsPl = epsPl0+std::copysign(dEPl,eps-epsPl0);

    viscoPlasticity->finalize = true;
  }

  // elastic free energy
  z_real_t epsEl = eps-epsPl;
  z_real_t We = this->storedEnergy(material,extPar,epsEl,sig,M,
                                   update || computeTangent,
                                   computeTangent);
  if (update) intV[1] = We;

  // plastic free energy increment + dissipated energy
  z_real_t dummy,Hp;
  z_real_t Wp = viscoPlasticity->irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                                    dummy,Hp,dTime,false,computeTangent);

  // tangents
  dEPl = ePl-ePl0;
  if (computeTangent && dEPl > 0.0) {
    // (visco)plastic correction
    M -= M*M/(M+Hp);
  }

  return We+Wp-intV0[1];
}

// radial return algorithm
unsigned int ElastoPlasticBar::radialReturn(const MaterialProperties& material,
                                            const ConstitutiveModel::ParameterSet& extPar,
                                            const MatLibArray& intPar0,MatLibArray& intPar,
                                            z_real_t eps,z_real_t epsPl,
                                            z_real_t ePl0,z_real_t& ePl,z_real_t dTime) {
  static const unsigned int ITMAX = 30;
  static const z_real_t MULT = 0.9;
  static const z_real_t PREC = 1.0e-12;
  static const z_real_t TOLE = 1.0e-07;
  static const z_real_t THRSHLD = 0.1*std::numeric_limits<z_real_t>::max();

  // get algorithmic parameter
  unsigned int maxIt;
  if (material.checkProperty("RR_MAX_ITER_PARAMETER"))
    maxIt = material.getIntegerProperty("RR_MAX_ITER_PARAMETER");
  else
    maxIt = ITMAX;

  // compute elastic predictor
  z_real_t epsEl = eps-epsPl;
  z_real_t sig,M;
  this->storedEnergy(material,extPar,epsEl,sig,M,true,true);

  // compute test function
  ePl = ePl0;
  z_real_t sigPl,Hp;
  viscoPlasticity->irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                      sigPl,Hp,dTime,true,true);
  z_real_t fct0 = std::fabs(sig)-sigPl;
  if (fct0 <= 0.0) return 0;

  // apply plastic corrector
  z_real_t dEPl = 0.0;
  z_real_t ePl00 = ePl0;
  z_real_t fct = fct0;
  z_real_t fct00 = fct0;
  z_real_t test = TOLE*(fct+TOLE);
  unsigned int iter=0;
  for (; iter < maxIt; iter++) {
    z_real_t coef = 1.0/(M+Hp);
    if (std::fabs(Hp) < THRSHLD && !std::isnan(coef))
      dEPl = fct*coef;
    else
      dEPl = fct/M;
    if (std::fabs(dEPl) < PREC) break;
    if ((ePl+dEPl) < (ePl00+PREC)) { /* use secant method */
      z_real_t mult = fct/(fct00-fct);
      if (mult < -MULT) mult=-MULT;
      dEPl = mult*(ePl-ePl00);
    }
    if (std::fabs(dEPl) < PREC) break;
    epsEl -= std::copysign(1.0,sig)*dEPl;
    ePl += dEPl;
    this->storedEnergy(material,extPar,epsEl,sig,M,true,true);
    viscoPlasticity->irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                        sigPl,Hp,dTime,true,true);
    fct = std::fabs(sig)-sigPl;
    if (std::fabs(fct) < test) break;
    if (fct > 0.0 && dEPl < 0.0) {
      fct00 = fct;
      ePl00 = ePl;
    }
  }
  // check convergence
  if (iter == maxIt) {
    throw UpdateFailedException("no convergence in radial return");
  }

  return iter;
}

/*
 * Methods for ElastoPlasticBarMixed
 */

// check consistency of properties
void ElastoPlasticBarMixed::checkProperties(MaterialProperties& material,std::ostream *os) {
  if (os) (*os) << "\nElasto-plasticity model with mixed (isotropic+linear kinematic) hardening (pure 1D bar):" << std::endl;

  // density
  try {
    z_real_t rho = material.getRealProperty("MASS_DENSITY");
    if (rho <= 0.0) {
      if (os) (*os) << "ERROR: mass density must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: mass density");
    }
    if (os) (*os) << "\n\tmass density = " << rho << std::endl;
  }
  catch (NoSuchPropertyException) {
    if (os) (*os) << "\n\tmass density is not defined" << std::endl;
  }

  // bar cross section
  try {
    z_real_t A = material.getRealProperty("SECTION");
    if (A <= 0.0) {
      if (os) (*os) << "ERROR: bar cross-section must be positive." << std::endl;
      throw std::runtime_error("invalid property: bar cross-section");
    }
    if (os) (*os) << "\n\tbar cross-section = " << A << std::endl;
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "\n\tbar cross-section set to 1" << std::endl;
    material.setProperty("SECTION",1.0);
  }

  // elastic potential
  this->potential->checkProperties(material,os);

  // dilatancy model
  if (this->dilatancy) this->dilatancy->checkProperties(material,os);

  // kinematic hardening modulus
  z_real_t Hk;
  try {
    Hk = material.getRealProperty("KINEMATIC_HARDENING_MODULUS");
    if (Hk < 0.0) {
      if (os) (*os) << "ERROR: kinematic hardening modulus must be positive." << std::endl;
      throw std::runtime_error("invalid property: kinematic hardening modulus");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: kinematic hardening modulus is not defined." << std::endl;
    throw e;
  }

  // viscoplastic model
  viscoPlasticity->checkProperties(material,os);
}

// compute the plastic update
z_real_t ElastoPlasticBarMixed::plasticUpdate(const MaterialProperties& material,
                                              const ConstitutiveModel::ParameterSet& extPar,
                                              z_real_t eps,z_real_t& sig,
                                              z_real_t epsPl0,z_real_t& epsPl,
                                              const MatLibArray& intV0,MatLibArray& intV,
                                              z_real_t dTime,z_real_t& M,
                                              bool update,bool computeTangent) {
  // get equivalent plastic strain
  z_real_t ePl0 = intV0[0];
  z_real_t ePl  = intV[0];

  // extract internal parameters
  unsigned int nIntPar = intV.size()-2;
  const MatLibArray intPar0(intV0,nIntPar,2);
  MatLibArray intPar(intV,nIntPar,2);

  // update
  viscoPlasticity->initialize = true;
  viscoPlasticity->finalize = false;
  z_real_t dEPl=0.0;
  if (update) {

    // perform update (radial return)
    radialReturn(material,extPar,intPar0,intPar,
                 eps,epsPl0,ePl0,ePl,dTime);

    // update internal variables
    intV[0] = ePl;

    // update plastic strain
    dEPl = ePl-ePl0;
    epsPl = epsPl0+std::copysign(dEPl,eps-epsPl0);

    viscoPlasticity->finalize = true;
  }

  // elastic free energy
  z_real_t epsEl = eps-epsPl;
  z_real_t We = this->storedEnergy(material,extPar,epsEl,sig,M,
                                   update || computeTangent,
                                   computeTangent);
  if (update) intV[1] = We;

  // plastic free energy increment + dissipated energy
  z_real_t dummy,Hp;
  z_real_t Wp = viscoPlasticity->irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                                    dummy,Hp,dTime,false,computeTangent);
  // get kinematic hardening modulus
  z_real_t Hk = material.getRealProperty("KINEMATIC_HARDENING_MODULUS");
  // compute contribution to free energy
  z_real_t WpKin = 0.5*Hk*epsPl*epsPl;
  intPar[0] += WpKin;

  // tangents
  dEPl = ePl-ePl0;
  if (computeTangent && dEPl > 0.0) {
    // (visco)plastic correction
    M -= M*M/(M+Hp+Hk);
  }

  return We-intV0[1]+WpKin-0.5*Hk*epsPl0*epsPl0+Wp;
}

// radial return algorithm
unsigned int ElastoPlasticBarMixed::radialReturn(const MaterialProperties& material,
                                                 const ConstitutiveModel::ParameterSet& extPar,
                                                 const MatLibArray& intPar0,MatLibArray& intPar,
                                                 z_real_t eps,z_real_t epsPl,
                                                 z_real_t ePl0,z_real_t& ePl,z_real_t dTime) {
  static const unsigned int ITMAX = 30;
  static const z_real_t MULT = 0.9;
  static const z_real_t PREC = 1.0e-12;
  static const z_real_t TOLE = 1.0e-07;
  static const z_real_t THRSHLD = 0.1*std::numeric_limits<z_real_t>::max();

  // get algorithmic parameter
  unsigned int maxIt;
  if (material.checkProperty("RR_MAX_ITER_PARAMETER"))
    maxIt = material.getIntegerProperty("RR_MAX_ITER_PARAMETER");
  else
    maxIt = ITMAX;

  // compute elastic predictor
  z_real_t epsEl = eps-epsPl;
  z_real_t sig,M;
  this->storedEnergy(material,extPar,epsEl,sig,M,true,true);

  // get kinematic hardening modulus
  z_real_t Hk = material.getRealProperty("KINEMATIC_HARDENING_MODULUS");

  // compute test function
  ePl = ePl0;
  z_real_t sigPl,Hp;
  viscoPlasticity->irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                      sigPl,Hp,dTime,true,true);
  z_real_t fct0 = std::fabs(sig-Hk*epsPl)-sigPl;
  if (fct0 <= 0.0) return 0;

  // apply plastic corrector
  z_real_t dEPl = 0.0;
  z_real_t ePl00 = ePl0;
  z_real_t fct = fct0;
  z_real_t fct00 = fct0;
  z_real_t test = TOLE*(fct+TOLE);
  unsigned int iter=0;
  for (; iter < maxIt; iter++) {
    z_real_t coef = 1.0/(M+Hk+Hp);
    if (std::fabs(Hp) < THRSHLD && !std::isnan(coef))
      dEPl = fct*coef;
    else
      dEPl = fct/(M+Hk);
    if (std::fabs(dEPl) < PREC) break;
    if ((ePl+dEPl) < (ePl00+PREC)) { /* use secant method */
      z_real_t mult = fct/(fct00-fct);
      if (mult < -MULT) mult=-MULT;
      dEPl = mult*(ePl-ePl00);
    }
    if (std::fabs(dEPl) < PREC) break;
    epsEl -= std::copysign(1.0,sig)*dEPl;
    this->storedEnergy(material,extPar,epsEl,sig,M,true,true);
    epsPl += std::copysign(1.0,sig)*dEPl;
    ePl += dEPl;
    viscoPlasticity->irreversibleEnergy(material,extPar,intPar0,intPar,ePl0,ePl,
                                        sigPl,Hp,dTime,true,true);
    fct = std::fabs(sig-Hk*epsPl)-sigPl;
    if (std::fabs(fct) < test) break;
    if (fct > 0.0 && dEPl < 0.0) {
      fct00 = fct;
      ePl00 = ePl;
    }
  }
  // check convergence
  if (iter == maxIt) {
    throw UpdateFailedException("no convergence in radial return");
  }

  return iter;
}

/*
 * Methods for class LinearIsotropicElastoPlasticBarBuilder.
 */

// the instance
LinearIsotropicElastoPlasticBarBuilder const* LinearIsotropicElastoPlasticBarBuilder::BUILDER
= new LinearIsotropicElastoPlasticBarBuilder();

// constructor
LinearIsotropicElastoPlasticBarBuilder::LinearIsotropicElastoPlasticBarBuilder() {
  ModelDictionary::add("LINEAR_ISOTROPIC_ELASTO_PLASTIC_BAR",*this);
}

// build model
ConstitutiveModel* LinearIsotropicElastoPlasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new LinearIsotropicElastoPlasticBar();
      break;
    default:
      return 0;
      break;
  }
}

/*
 * Methods for class NonLinearIsotropicElastoPlasticBarBuilder.
 */

// the instance
NonLinearIsotropicElastoPlasticBarBuilder const* NonLinearIsotropicElastoPlasticBarBuilder::BUILDER
= new NonLinearIsotropicElastoPlasticBarBuilder();

// constructor
NonLinearIsotropicElastoPlasticBarBuilder::NonLinearIsotropicElastoPlasticBarBuilder() {
  ModelDictionary::add("NONLINEAR_ISOTROPIC_ELASTO_PLASTIC_BAR",*this);
}

// build model
ConstitutiveModel* NonLinearIsotropicElastoPlasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new NonLinearIsotropicElastoPlasticBar();
      break;
    default:
      return 0;
      break;
  }
}

/*
 * Methods for class LinearMixedElastoPlasticBarBuilder.
 */

// the instance
LinearMixedElastoPlasticBarBuilder const* LinearMixedElastoPlasticBarBuilder::BUILDER
= new LinearMixedElastoPlasticBarBuilder();

// constructor
LinearMixedElastoPlasticBarBuilder::LinearMixedElastoPlasticBarBuilder() {
  ModelDictionary::add("LINEAR_MIXED_ELASTO_PLASTIC_BAR",*this);
}

// build model
ConstitutiveModel* LinearMixedElastoPlasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new LinearMixedElastoPlasticBar();
      break;
    default:
      return 0;
      break;
  }
}

/*
 * Methods for class NonLinearMixedElasticBarBuilder.
 */

// the instance
NonLinearMixedElastoPlasticBarBuilder const* NonLinearMixedElastoPlasticBarBuilder::BUILDER
= new NonLinearMixedElastoPlasticBarBuilder();

// constructor
NonLinearMixedElastoPlasticBarBuilder::NonLinearMixedElastoPlasticBarBuilder() {
  ModelDictionary::add("NONLINEAR_MIXED_ELASTO_PLASTIC_BAR",*this);
}

// build model
ConstitutiveModel* NonLinearMixedElastoPlasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new NonLinearMixedElastoPlasticBar();
      break;
    default:
      return 0;
      break;
  }
}
