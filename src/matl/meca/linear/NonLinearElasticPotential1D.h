/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_NONLINEAR_ELASTIC_POTENTIAL_1D_H
#define ZORGLIB_MATL_MECA_NONLINEAR_ELASTIC_POTENTIAL_1D_H

// config
#include <matlib_macros.h>

// local
#include <matl/meca/linear/ElasticBar.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing 1D non-linear elastic potential.
 */
class NonLinearElasticPotential1D : virtual public ElasticBar::Potential {
  
 public:
  
  // constructor
  NonLinearElasticPotential1D() {}
  
  // copy constructor
  NonLinearElasticPotential1D(const NonLinearElasticPotential1D&) {}
  
  // destructor
  virtual ~NonLinearElasticPotential1D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // check if the material behaviour is linear ?
  bool isLinear() const {return false;}

  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties&,
                        const ConstitutiveModel::ParameterSet&,
                        z_real_t,z_real_t&,z_real_t&,bool,bool);
};


/**
 * Implementations of the model.
 */
class NonLinearElasticBar : public ElasticBar {
  
 public:
  
  // constructor
  NonLinearElasticBar()
  : ElasticBar(new NonLinearElasticPotential1D()) {}
  
  // copy constructor
  NonLinearElasticBar(const NonLinearElasticBar& src)
  : ElasticBar(src) {}
  
  // destructor
  virtual ~NonLinearElasticBar() {}
};

/**
 * The associated model builder
 */
class NonLinearElasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  NonLinearElasticBarBuilder();
  
  // the instance
  static NonLinearElasticBarBuilder const* BUILDER;
  
public:
  
  // destructor
  virtual ~NonLinearElasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
