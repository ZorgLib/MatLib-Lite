/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "RateDependencyModels.h"

// std C library
#include <cmath>
#if defined(_WIN32) || defined(_WIN64)
z_real_t asinh(z_real_t x) {
  return std::log(x+std::sqrt(x*x+1.0));
}
#endif

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for class PowerLawRateDependencyModel.
 */

// check consistency of material properties
void PowerLawRateDependencyModel::checkProperties(MaterialProperties& material,
                                                  std::ostream* os) {
  if (os) (*os) << "\n\t***Power-law rate dependency model***" << std::endl;
  
  z_real_t sig0;
  try {
    sig0 = material.getRealProperty("REFERENCE_STRESS");
  }
  catch (NoSuchPropertyException) {
    sig0 = 0.0;
    material.setProperty("REFERENCE_STRESS",0.0);
  }
  if (sig0 < 0.0) {
    if (os) (*os) << "ERROR: reference stress must be positive or zero." << std::endl;
    throw std::runtime_error("invalid property: reference stress");
  }
  else if (sig0 == 0.0) {
    if (os) (*os) << "\tnot defined (REFERENCE_STRESS = 0.0)" << std::endl;
    return;
  }
  
  z_real_t epsDot0,m;
  try {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    if (epsDot0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain rate must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain rate");
    }
  }
  catch (NoSuchPropertyException e) {
    epsDot0 = 1.0;
    material.setProperty("REFERENCE_STRAIN_RATE",epsDot0);
  }
  
  try {
    m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
    if (m <= 1.0) {
      if (os) (*os) << "ERROR: rate dependency exponent must be > 1." << std::endl;
      throw std::runtime_error("invalid property: rate dependency exponent");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: rate dependency exponent is not defined." << std::endl;
    throw e;
  }
  
  // print-out properties
  if (os) {
    (*os) << "\treference stress               = " << sig0    << "\n";
    (*os) << "\treference strain rate          = " << epsDot0 << "\n";
    (*os) << "\trate dependency exponent       = " <<   m     << std::endl;
  }
}

// dissipated energy
z_real_t PowerLawRateDependencyModel::dissipatedEnergy(const MaterialProperties& material,
                                                       const ConstitutiveModel::ParameterSet& extPar,
                                                       const MatLibArray& intPar0,MatLibArray& intPar1,
                                                       z_real_t eps,z_real_t epsDot,z_real_t& sig1,z_real_t& sig2,
                                                       z_real_t& h11,z_real_t& h22,z_real_t& h12,
                                                       bool first,bool second) {
  // get material parameters
  z_real_t sig0 = material.getRealProperty("REFERENCE_STRESS");
  if (sig0 == 0.0 || epsDot <= 0.0) {
    sig1 = sig2 = 0.0;
    h11 = h22 = h12 = 0.0;
    return 0.0;
  }
  z_real_t epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
  z_real_t m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
  
  // compute dissipation pseudo-potential and derivatives
  z_real_t phi;
  z_real_t expo = 1.0/m;
  z_real_t val = epsDot/epsDot0;
  if (first) {
    sig1 = 0.0;
    sig2 = sig0*std::pow(val,expo);
    phi = epsDot/(expo+1)*sig2;
  }
  else {
    z_real_t expo1 = expo+1;
    phi = sig0*epsDot0/(expo1)*std::pow(val,expo1);
  }
  
  // compute second derivatives
  if (second) {
    h11 = h12 = 0.0;
    h22 = expo*sig0/epsDot0*std::pow(val,expo-1);
  }
  
  return phi;
}


/*
 * Methods for class ASinhRateDependencyModel.
 */

// check consistency of material properties
void ASinhRateDependencyModel::checkProperties(MaterialProperties& material,
                                               std::ostream* os) {
  if (os) (*os) << "\n\t***ASinh (thermal activation) rate dependency model***" << std::endl;
  
  z_real_t sig0;
  try {
    sig0 = material.getRealProperty("REFERENCE_STRESS");
  }
  catch (NoSuchPropertyException) {
    sig0 = 0.0;
    material.setProperty("REFERENCE_STRESS",0.0);
  }
  if (sig0 < 0.0) {
    if (os) (*os) << "ERROR: reference stress must be positive or zero." << std::endl;
    throw std::runtime_error("invalid property: reference stress");
  }
  else if (sig0 == 0.0) {
    if (os) (*os) << "\tnot defined (REFERENCE_STRESS = 0.0)" << std::endl;
    return;
  }
  
  z_real_t epsDot0;
  try {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    if (epsDot0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain rate must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain rate");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: reference strain rate is not defined." << std::endl;
    throw e;
  }

  // print-out properties
  if (os) {
    (*os) << "\treference stress               = " << sig0    << "\n";
    (*os) << "\treference strain rate          = " << epsDot0 << std::endl;
  }
}

// dissipated energy
z_real_t ASinhRateDependencyModel::dissipatedEnergy(const MaterialProperties& material,
                                                    const ConstitutiveModel::ParameterSet& extPar,
                                                    const MatLibArray& intPar0,MatLibArray& intPar1,
                                                    z_real_t eps,z_real_t epsDot,z_real_t& sig1,z_real_t& sig2,
                                                    z_real_t& h11,z_real_t& h22,z_real_t& h12,
                                                    bool first,bool second) {
  // get material parameters
  z_real_t sig0 = material.getRealProperty("REFERENCE_STRESS");
  if (sig0 == 0.0 || epsDot <= 0.0) {
    sig1 = sig2 = 0.0;
    h11 = h22 = h12 = 0.0;
    return 0.0;
  }
  z_real_t epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
  
  // compute dissipation pseudo-potential and derivatives
  z_real_t phi;
  z_real_t val = epsDot/epsDot0;
  z_real_t val1 = std::sqrt(val*val+1.0);
  if (first) {
    sig1 = 0.0;
    sig2 = sig0*asinh(val);
    phi = epsDot0*(val*sig2-sig0*val1+sig0);
  }
  else {
    phi = sig0*epsDot0*(val*asinh(val)-val1+1.0);
  }
  
  // compute second derivatives
  if (second) {
    h11 = h12 = 0.0;
    h22 = sig0/(epsDot0*val1);
  }
  
  return phi;
}


/*
 * Methods for class NortonHoffRateDependencyModel.
 */

// check consistency of material properties
void NortonHoffRateDependencyModel::checkProperties(MaterialProperties& material,
                                                    std::ostream* os) {
  if (os) (*os) << "\n\t***Norton-Hoff rate dependency model***" << std::endl;
  
  z_real_t sig0;
  try {
    sig0 = material.getRealProperty("REFERENCE_YIELD_STRESS");
  }
  catch (NoSuchPropertyException) {
    sig0 = 0.0;
    material.setProperty("REFERENCE_YIELD_STRESS",0.0);
  }
  if (sig0 < 0.0) {
    if (os) (*os) << "ERROR: reference yield stress must be positive or zero." << std::endl;
    throw std::runtime_error("invalid property: reference stress");
  }
  else if (sig0 == 0.0) {
    if (os) (*os) << "\tnot defined (REFERENCE_YIELD_STRESS = 0.0)" << std::endl;
    return;
  }
  
  z_real_t epsDot0,m;
  try {
    epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
    if (epsDot0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain rate must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain rate");
    }
  }
  catch (NoSuchPropertyException e) {
    epsDot0 = 1.0;
    material.setProperty("REFERENCE_STRAIN_RATE",epsDot0);
  }
  
  try {
    m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
    if (m <= 1.0) {
      if (os) (*os) << "ERROR: rate dependency exponent must be > 1." << std::endl;
      throw std::runtime_error("invalid property: rate dependency exponent");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: rate dependency exponent is not defined." << std::endl;
    throw e;
  }
  
  z_real_t eps0,n;
  try {
    eps0 = material.getRealProperty("REFERENCE_STRAIN_NH");
    if (eps0 <= 0.0) {
      if (os) (*os) << "ERROR: reference strain must be strictly positive." << std::endl;
      throw std::runtime_error("invalid property: reference strain");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: reference strain is not defined." << std::endl;
    throw e;
  }
  
  try {
    n = material.getRealProperty("HARDENING_EXPONENT_NH");
    if (n <= 1.0) {
      if (os) (*os) << "ERROR: hardening exponent must be > 1." << std::endl;
      throw std::runtime_error("invalid property: hardening exponent");
    }
  }
  catch (NoSuchPropertyException e) {
    if (os) (*os) << "ERROR: hardening exponent is not defined." << std::endl;
    throw e;
  }
  // print-out properties
  if (os) {
    (*os) << "\treference yield stress         = " << sig0    << "\n";
    (*os) << "\treference strain rate          = " << epsDot0 << "\n";
    (*os) << "\trate dependency exponent       = " <<   m     << "\n";
    (*os) << "\treference strain               = " << eps0    << "\n";
    (*os) << "\thardening exponent             = " <<   n     << std::endl;
  }
}

// dissipated energy
z_real_t NortonHoffRateDependencyModel::dissipatedEnergy(const MaterialProperties& material,
                                                         const ConstitutiveModel::ParameterSet& extPar,
                                                         const MatLibArray& intPar0,MatLibArray& intPar1,
                                                         z_real_t eps,z_real_t epsDot,z_real_t& sig1,z_real_t& sig2,
                                                         z_real_t& h11,z_real_t& h22,z_real_t& h12,
                                                         bool first,bool second) {
  // get material parameters
  z_real_t sig0 = material.getRealProperty("REFERENCE_YIELD_STRESS");
  if (sig0 == 0.0) {
    sig1 = 0.0;
    sig2 = 0.0;
    h11 = h22 = h12 = 0.0;
    return 0.0;
  }
  z_real_t epsDot0 = material.getRealProperty("REFERENCE_STRAIN_RATE");
  z_real_t m = material.getRealProperty("RATE_DEPENDENCY_EXPONENT");
  z_real_t eps0 = material.getRealProperty("REFERENCE_STRAIN_NH");
  z_real_t n = material.getRealProperty("HARDENING_EXPONENT_NH");
  
  // compute dissipation pseudo-potential and derivatives
  z_real_t phi;
  z_real_t expo1 = 1.0/n;
  z_real_t expo2 = 1.0/m;
  z_real_t val1,val2,val11,val21;
  if (eps >= eps0) {
    val1 = eps/eps0;
    val11 = std::pow(val1,expo1);
  }
  else {
    val1 = 1.0;
    val11 = 1.0;
  }
  if (epsDot >= epsDot0) {
    val2 = epsDot/epsDot0;
    val21 = std::pow(val2,expo2);
  }
  else {
    val2 = 1.0;
    val21 = 1.0;
  }
                                                    
  // pseudo-potential
  if (epsDot >= epsDot0)
    phi = sig0*val11*(epsDot*val21+epsDot0*expo2)/(expo2+1.);
  else
    phi = sig0*val11*epsDot;

  // first derivative
  if (first) {
    if (eps >= eps0)
      sig1 = phi*expo1/eps;
    else
      sig1 = 0.0;
    sig2 = sig0*val11*val21;
  }
  
  // compute second derivatives
  if (second) {
    if (eps >= eps0) {
      h11 = phi*expo1*(expo1-1.)/(eps*eps);
      h12 = sig0*val11*val21*expo1/eps;
    }
    else {
      h11 = 0.0;
      h12 = 0.0;
    }
    if (epsDot >= epsDot0)
      h22 = sig0*val11*expo2/epsDot0*std::pow(val2,expo2-1.);
    else
      h22 = 0.0;
  }
  
  return phi;
}
