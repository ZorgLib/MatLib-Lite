/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "HardeningModels.h"

// std C library
#include <cmath>
// std C++ library
#include <limits>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for class LinearIsotropicHardeningModel.
 */

// check consistency of material properties
void LinearIsotropicHardeningModel::checkProperties(MaterialProperties& material,
                                                    std::ostream* os) {
  if (os) (*os) << "\n\t***Linear isotropic hardening model***" << std::endl;
  
  /*
   * stored part
   */
  z_real_t sig0,H0;
  try {
    // initial yield stress
    sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_STORED");
    
    // hardening modulus
    try {
      H0 = material.getRealProperty("HARDENING_MODULUS_STORED");
    }
    catch (NoSuchPropertyException) {
      H0 = 0.0;
      material.setProperty("HARDENING_MODULUS_STORED",H0);
    }
  }
  catch (NoSuchPropertyException) {
    // initial yield stress
    try {
      sig0 = material.getRealProperty("INITIAL_YIELD_STRESS");
    }
    catch (NoSuchPropertyException) {
      try {
        sig0 = material.getRealProperty("YIELD_IN_TENSION");
      }
      catch (NoSuchPropertyException e) {
        if (os) (*os) << "ERROR: initial yield stress is not defined." << std::endl;
        throw e;
      }
    }
    material.setProperty("INITIAL_YIELD_STRESS_STORED",sig0);
    
    // hardening modulus
    try {
      H0 = material.getRealProperty("HARDENING_MODULUS");
    }
    catch (NoSuchPropertyException) {
      H0 = 0.0;
      material.setProperty("HARDENING_MODULUS",H0);
    }
    material.setProperty("HARDENING_MODULUS_STORED",H0);
  }

  /*
   * dissipated part
   */
  z_real_t sig1,H1;
  // initial yield stress
  try {
    sig1 = material.getRealProperty("INITIAL_YIELD_STRESS_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    sig1 = 0.0;
    material.setProperty("INITIAL_YIELD_STRESS_DISSIPATED",sig1);
  }
  
  // hardening modulus
  try {
    H1 = material.getRealProperty("HARDENING_MODULUS_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    H1 = 0.0;
    material.setProperty("HARDENING_MODULUS_DISSIPATED",H1);
  }
    
  /*
   * check values
   */
  z_real_t sig = sig0+sig1;
  if (sig < 0.0) {
    if (os) (*os) << "ERROR: initial yield stress must be positive." << std::endl;
    throw std::runtime_error("invalid property: initial yield stress");
  }
  z_real_t H = H0+H1;
  try {
    z_real_t mu = material.getRealProperty("SHEAR_MODULUS");
    if (H < -3*mu) {
      if (os) (*os) << "ERROR: hardening modulus must be larger than -3 times the shear modulus." << std::endl;
      throw std::runtime_error("invalid property: hardening modulus");
    }
  }
  catch (NoSuchPropertyException) {
    z_real_t E = material.getRealProperty("YOUNG_MODULUS");
    if (H < -E) {
      if (os) (*os) << "ERROR: hardening modulus must be larger than -Young modulus." << std::endl;
      throw std::runtime_error("invalid property: hardening modulus");
    }
  }
  
  /*
   * print-out
   */
  if (os) {
    (*os) << "\tinitial yield stress (stored part)     = " << sig0   << "\n";
    (*os) << "\thardening modulus (stored part)        = " <<   H0   << "\n";
    (*os) << "\tinitial yield stress (dissipated part) = " << sig1   << "\n";
    (*os) << "\thardening modulus (dissipated part)    = " <<   H1   << std::endl;
  }
}

// plastically stored energy
z_real_t LinearIsotropicHardeningModel::storedEnergy(const MaterialProperties& material,
                                                     const ConstitutiveModel::ParameterSet& extPar,
                                                     const MatLibArray& intPar0,MatLibArray& intPar1,
                                                     z_real_t Wp0,z_real_t epsPl0,z_real_t epsPl1,
                                                     z_real_t& sig,z_real_t& h,bool first,bool second) {
  // initial yield stress
  z_real_t sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_STORED");
  
  // hardening modulus
  h = material.getRealProperty("HARDENING_MODULUS_STORED");
  
  // compute plastic potential
  z_real_t Wp;
  if (first) {
    z_real_t dSig = h*epsPl1;
    sig = sig0+dSig;
    Wp = (sig0+0.5*dSig)*epsPl1;
  }
  else
    Wp = (sig0+0.5*h*epsPl1)*epsPl1;
  
  return Wp;  
}

// yield stress
z_real_t LinearIsotropicHardeningModel::yieldStress(const MaterialProperties& material,
                                                    const ConstitutiveModel::ParameterSet& extPar,
                                                    const MatLibArray&intPar0,MatLibArray& intPar1,
                                                    z_real_t epsPl,z_real_t& h,z_real_t& dh,
                                                    bool first,bool second) {
  // initial yield stress
  z_real_t sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_DISSIPATED");
  
  // hardening modulus
  h = material.getRealProperty("HARDENING_MODULUS_DISSIPATED");
  
  // compute yield stress
  z_real_t sig = sig0+h*epsPl;
  
  // second derivative
  if (second) dh = 0.0;
  
  return sig;
}


/*
 * Methods for class NonLinearIsotropicHardeningModel.
 */

// check consistency of material properties
void NonLinearIsotropicHardeningModel::checkProperties(MaterialProperties& material,
                                                       std::ostream* os) {
  if (os) (*os) << "\n\t***Nonlinear isotropic hardening model***" << std::endl;
  
  /*
   * stored part - power law
   */
  z_real_t sig0,b0,n0=1.0;
  // initial yield stress
  try {
    sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_STORED");
  }
  catch (NoSuchPropertyException) {
    try {
      sig0 = material.getRealProperty("INITIAL_YIELD_STRESS");
    }
    catch (NoSuchPropertyException) {
      try {
        sig0 = material.getRealProperty("YIELD_IN_TENSION");
      }
      catch (NoSuchPropertyException e) {
        if (os) (*os) << "ERROR: initial yield stress is not defined." << std::endl;
        throw e;
      }
    }
    material.setProperty("INITIAL_YIELD_STRESS_STORED",sig0);
  }
  // hardening coefficient
  try {
    b0 = material.getRealProperty("HARDENING_COEFFICIENT_STORED");
  }
  catch (NoSuchPropertyException) {
    try {
      b0 = material.getRealProperty("HARDENING_COEFFICIENT");
    }
    catch (NoSuchPropertyException) {
      b0 = 0.0;
    }
    material.setProperty("HARDENING_COEFFICIENT_STORED",b0);
  }
  // hardening exponent
  if (b0 != 0.0) {
    try {
      n0 = material.getRealProperty("HARDENING_EXPONENT_STORED");
    }
    catch (NoSuchPropertyException) {
      try {
        n0 = material.getRealProperty("HARDENING_EXPONENT");
      }
      catch (NoSuchPropertyException) {
        n0 = 1.0;
      }
      material.setProperty("HARDENING_EXPONENT_STORED",n0);
    }
    if (n0 < 1.0) {
      if (os) (*os) << "ERROR: hardening exponent (stored part) must be >= 1." << std::endl;
      throw std::runtime_error("invalid property: hardening exponent (stored)");
    }
  }
  
  /*
   * stored part - saturation law
   */
  z_real_t dSig0,d0=0.0;
  // saturation yield stress
  try {
	  dSig0 = material.getRealProperty("SATURATION_YIELD_STRESS_STORED");
  }
  catch (NoSuchPropertyException) {
    try {
      dSig0 = material.getRealProperty("SATURATION_YIELD_STRESS");
    }
    catch (NoSuchPropertyException) {
      dSig0 = 0.0;
    }
    material.setProperty("SATURATION_YIELD_STRESS_STORED",dSig0);
  }
  // saturation coefficient
  if (dSig0 != 0.0) {
    try {
      try {
        d0 = material.getRealProperty("HARDENING_SATURATION_COEFFICIENT_STORED");
      }
      catch (NoSuchPropertyException) {
        d0 = material.getRealProperty("HARDENING_SATURATION_COEFFICIENT");
        material.setProperty("HARDENING_SATURATION_COEFFICIENT_STORED",d0);
      }
      if (d0 <= 0.0) {
        if (os) (*os) << "ERROR: hardening saturation coefficient (stored part) must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: hardening saturation coefficient (stored)");
      }
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: hardening saturation coefficient (stored part) is not defined." << std::endl;
      throw e;
    }
  }
  
  /*
   * dissipated part - power law
   */
  z_real_t sig1,b1,n1=1.0;
  // initial yield stress
  try {
    sig1 = material.getRealProperty("INITIAL_YIELD_STRESS_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    sig1 = 0.0;
    material.setProperty("INITIAL_YIELD_STRESS_DISSIPATED",sig1);
  }
  // hardening coefficient
  try {
    b1 = material.getRealProperty("HARDENING_COEFFICIENT_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    b1 = 0.0;
    material.setProperty("HARDENING_COEFFICIENT_DISSIPATED",b1);
  }
  // hardening exponent
  if (b1 != 0.0) {
    try {
      n1 = material.getRealProperty("HARDENING_EXPONENT_DISSIPATED");
    }
    catch (NoSuchPropertyException) {
      n1 = 1.0;
      material.setProperty("HARDENING_EXPONENT_DISSIPATED",n1);
    }
    if (n1 < 1.0) {
      if (os) (*os) << "ERROR: hardening exponent (dissipated part) must be >= 1." << std::endl;
      throw std::runtime_error("invalid property: hardening exponent (dissipated)");
    }
  }
  
  /*
   * dissipated part - saturation law
   */
  z_real_t dSig1,d1=0.0;
  // saturation yield stress
  try {
    dSig1 = material.getRealProperty("SATURATION_YIELD_STRESS_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    dSig1 = 0.0;
    material.setProperty("SATURATION_YIELD_STRESS_DISSIPATED",dSig1);
  }
  // saturation coefficient
  if (dSig1 != 0.0) {
    try {
      d1 = material.getRealProperty("HARDENING_SATURATION_COEFFICIENT_DISSIPATED");
      if (d1 <= 0.0) {
        if (os) (*os) << "ERROR: hardening saturation coefficient (dissipated part) must be strictly positive." << std::endl;
        throw std::runtime_error("invalid property: hardening saturation coefficient (dissipated)");
      }
    }
    catch (NoSuchPropertyException e) {
      if (os) (*os) << "ERROR: hardening saturation coefficient (dissipated part) is not defined." << std::endl;
      throw e;
    }
  }
  
  /*
   * check values
   */
  z_real_t sig = sig0+sig1;
  if (sig < 0.0) {
    if (os) (*os) << "ERROR: initial yield stress must be positive." << std::endl;
    throw std::runtime_error("invalid property: initial yield stress");
  }
  z_real_t dSig = dSig0+dSig1;
  if (dSig < -sig) {
    if (os) (*os) << "ERROR: saturation yield stress cannot lead to negative yield stress." << std::endl;
    throw std::runtime_error("invalid property: saturation yield stress");
  }
  
  /*
   * print-out
   */
  if (os) {
    (*os) << "\tinitial yield stress (stored part)                 = " <<  sig0 << "\n";
    (*os) << "\thardening coefficient (stored part)                = " <<    b0 << "\n";
    (*os) << "\thardening exponent (stored part)                   = " <<    n0 << "\n";
    (*os) << "\tsaturation yield stress (stored part)              = " << dSig0 << "\n";
    (*os) << "\thardening saturation coefficient (stored part)     = " <<    d0 << "\n";
    (*os) << "\tinitial yield stress (dissipated part)             = " <<  sig1 << "\n";
    (*os) << "\thardening coefficient (dissipated part)            = " <<    b1 << "\n";
    (*os) << "\thardening exponent (dissipated part)               = " <<    n1 << "\n";
    (*os) << "\tsaturation yield stress (dissipated part)          = " << dSig1 << "\n";
    (*os) << "\thardening saturation coefficient (dissipated part) = " <<    d1 << std::endl;
  }
}

// plastically stored energy
z_real_t NonLinearIsotropicHardeningModel::storedEnergy(const MaterialProperties& material,
                                                        const ConstitutiveModel::ParameterSet& extPar,
                                                        const MatLibArray& intPar0,MatLibArray& intPar1,
                                                        z_real_t Wp0,z_real_t epsPl0,z_real_t epsPl1,
                                                        z_real_t& sig,z_real_t& h,bool first,bool second) {
  static const z_real_t PRECISION = 1.0e-16;

  // initialize
  z_real_t Wp = 0.0;
  sig = 0.0;
  h = 0.0;

  // power-law part (Swift)
  z_real_t sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_STORED");
  z_real_t b = material.getRealProperty("HARDENING_COEFFICIENT_STORED");
  if (b != 0.0) {
    z_real_t n = material.getRealProperty("HARDENING_EXPONENT_STORED");
    z_real_t val = 1.0+b*epsPl1;
    if (val < PRECISION) val = 0.0;
    z_real_t expo = 1.0/n;
    if (first) {
      z_real_t val1 = std::pow(val,expo);
      sig = sig0*val1;
      Wp = sig0*(val*val1-1.0)/(b*(expo+1));
    }
    else {
      z_real_t val1 = std::pow(val,expo+1)-1.0;
      Wp = sig0*val1/(b*(expo+1));
    }
    if (second || val > PRECISION) {
      h = expo*b*sig0*std::pow(val,expo-1);
    }
  }
  else {
    if (first) sig = sig0;
    Wp = sig0*epsPl1;
  }
  
  // saturation part (Voce)
  z_real_t dSig = material.getRealProperty("SATURATION_YIELD_STRESS_STORED");
  if (dSig != 0.0) {
    z_real_t d = material.getRealProperty("HARDENING_SATURATION_COEFFICIENT_STORED");
    z_real_t val = std::exp(-d*epsPl1);
    if (first) sig += dSig*(1.0-val);
    if (second) h += dSig*d*val;
    Wp += dSig*(epsPl1+(val-1.0)/d);
  }
  
  return Wp;
}

// yield stress
z_real_t NonLinearIsotropicHardeningModel::yieldStress(const MaterialProperties& material,
                                                       const ConstitutiveModel::ParameterSet& extPar,
                                                       const MatLibArray&intPar0,MatLibArray& intPar1,
                                                       z_real_t epsPl,z_real_t& h,z_real_t& dh,
                                                       bool first,bool second) {
  static const z_real_t PRECISION = 1.0e-16;

  // initialize
  z_real_t sig = 0.0;
  h = 0.0;
  dh = 0.0;

  // power-law part (Swift)
  z_real_t sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_DISSIPATED");
  z_real_t b = material.getRealProperty("HARDENING_COEFFICIENT_DISSIPATED");
  if (b != 0.0) {
    z_real_t n = material.getRealProperty("HARDENING_EXPONENT_DISSIPATED");
    z_real_t val = 1.0+b*epsPl;
    if (val < PRECISION) val = 0.0;
    z_real_t expo = 1.0/n;
    sig = sig0*std::pow(val,expo);
    if (first && val > PRECISION) {
      h = expo*b*sig/val;
    }
    if (second && val > PRECISION) {
      dh = expo*(expo-1.0)*b*b*sig/(val*val);
    }
  }
  else {
    sig = sig0;
  }
  
  // saturation part (Voce)
  z_real_t dSig = material.getRealProperty("SATURATION_YIELD_STRESS_DISSIPATED");
  if (dSig != 0.0) {
    z_real_t d = material.getRealProperty("HARDENING_SATURATION_COEFFICIENT_DISSIPATED");
    z_real_t val = std::exp(-d*epsPl);
    sig += dSig*(1.0-val);
    if (first) h += dSig*d*val;
    if (second) dh -= dSig*d*d*val;
  }
  
  return sig;
}


/*
 * Methods for class LudwikIsotropicHardeningModel.
 */

// check consistency of material properties
void LudwikIsotropicHardeningModel::checkProperties(MaterialProperties& material,
                                                    std::ostream* os) {
  if (os) (*os) << "\n\t***Ludwik isotropic hardening model***" << std::endl;
  
  /*
   * stored part
   */
  z_real_t sig0,b0,n0=1.0;
  // initial yield stress
  try {
    sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_STORED");
  }
  catch (NoSuchPropertyException) {
    try {
      sig0 = material.getRealProperty("INITIAL_YIELD_STRESS");
    }
    catch (NoSuchPropertyException) {
      try {
        sig0 = material.getRealProperty("YIELD_IN_TENSION");
      }
      catch (NoSuchPropertyException e) {
        if (os) (*os) << "ERROR: initial yield stress is not defined." << std::endl;
        throw e;
      }
    }
    material.setProperty("INITIAL_YIELD_STRESS_STORED",sig0);
  }
  // hardening coefficient
  try {
    b0 = material.getRealProperty("HARDENING_COEFFICIENT_STORED");
  }
  catch (NoSuchPropertyException) {
    try {
      b0 = material.getRealProperty("HARDENING_COEFFICIENT");
    }
    catch (NoSuchPropertyException) {
      b0 = 0.0;
    }
    material.setProperty("HARDENING_COEFFICIENT_STORED",b0);
  }
  // hardening exponent
  if (b0 != 0.0) {
    try {
      n0 = material.getRealProperty("HARDENING_EXPONENT_STORED");
    }
    catch (NoSuchPropertyException) {
      try {
        n0 = material.getRealProperty("HARDENING_EXPONENT");
      }
      catch (NoSuchPropertyException) {
        n0 = 1.0;
      }
      material.setProperty("HARDENING_EXPONENT_STORED",n0);
    }
    if (n0 < 1.0) {
      if (os) (*os) << "ERROR: hardening exponent (stored part) must be >= 1." << std::endl;
      throw std::runtime_error("invalid property: hardening exponent (stored)");
    }
  }
  
  /*
   * dissipated part
   */
  z_real_t sig1,b1,n1=1.0;
  // initial yield stress
  try {
    sig1 = material.getRealProperty("INITIAL_YIELD_STRESS_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    sig1 = 0.0;
    material.setProperty("INITIAL_YIELD_STRESS_DISSIPATED",sig1);
  }
  // hardening coefficient
  try {
    b1 = material.getRealProperty("HARDENING_COEFFICIENT_DISSIPATED");
  }
  catch (NoSuchPropertyException) {
    b1 = 0.0;
    material.setProperty("HARDENING_COEFFICIENT_DISSIPATED",b1);
  }
  // hardening exponent
  if (b1 != 0.0) {
    try {
      n1 = material.getRealProperty("HARDENING_EXPONENT_DISSIPATED");
    }
    catch (NoSuchPropertyException) {
      n1 = 1.0;
      material.setProperty("HARDENING_EXPONENT_DISSIPATED",n1);
    }
    if (n1 < 1.0) {
      if (os) (*os) << "ERROR: hardening exponent (dissipated part) must be >= 1." << std::endl;
      throw std::runtime_error("invalid property: hardening exponent (dissipated)");
    }
  }
  
  /*
   * check values
   */
  z_real_t sig = sig0+sig1;
  if (sig < 0.0) {
    if (os) (*os) << "ERROR: initial yield stress must be positive." << std::endl;
    throw std::runtime_error("invalid property: initial yield stress");
  }
  
  /*
   * print-out
   */
  if (os) {
    (*os) << "\tinitial yield stress (stored part)                 = " <<  sig0 << "\n";
    (*os) << "\thardening coefficient (stored part)                = " <<    b0 << "\n";
    (*os) << "\thardening exponent (stored part)                   = " <<    n0 << "\n";
    (*os) << "\tinitial yield stress (dissipated part)             = " <<  sig1 << "\n";
    (*os) << "\thardening coefficient (dissipated part)            = " <<    b1 << "\n";
    (*os) << "\thardening exponent (dissipated part)               = " <<    n1 << std::endl;
  }
}

// plastically stored energy
z_real_t LudwikIsotropicHardeningModel::storedEnergy(const MaterialProperties& material,
                                                     const ConstitutiveModel::ParameterSet& extPar,
                                                     const MatLibArray& intPar0,MatLibArray& intPar1,
                                                     z_real_t Wp0,z_real_t epsPl0,z_real_t epsPl1,
                                                     z_real_t& sig,z_real_t& h,bool first,bool second) {
  static const z_real_t PRECISION = 1.0e-16;
  
  // initialize
  z_real_t Wp = 0.0;
  sig = 0.0;
  h = 0.0;
  
  // power-law (Ludwik)
  z_real_t sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_STORED");
  z_real_t b = material.getRealProperty("HARDENING_COEFFICIENT_STORED");
  if (b != 0.0 && epsPl1 > PRECISION) {
    z_real_t n = material.getRealProperty("HARDENING_EXPONENT_STORED");
    z_real_t expo = 1.0/n;
    if (first) {
      z_real_t val = std::pow(epsPl1,expo);
      sig = sig0+b*val;
      Wp = (sig0+b*val/(expo+1))*epsPl1;
    }
    else {
      z_real_t val = std::pow(epsPl1,expo+1);
      Wp = sig0*epsPl1+b*val/(expo+1);
    }
    if (second) {
      h = expo*b*std::pow(epsPl1,expo-1);
    }
  }
  else {
    Wp = sig0*epsPl1;
    if (first) sig = sig0;
    if (second && b != 0.0)
      h = std::numeric_limits<z_real_t>::max();
  }
  
  return Wp;
}

// yield stress
z_real_t LudwikIsotropicHardeningModel::yieldStress(const MaterialProperties& material,
                                                    const ConstitutiveModel::ParameterSet& extPar,
                                                    const MatLibArray&intPar0,MatLibArray& intPar1,
                                                    z_real_t epsPl,z_real_t& h,z_real_t& dh,
                                                    bool first,bool second) {
  static const z_real_t PRECISION = 1.0e-16;
  
  // initialize
  z_real_t sig = 0.0;
  h = 0.0;
  dh = 0.0;
  
  // power-law (Ludwik)
  z_real_t sig0 = material.getRealProperty("INITIAL_YIELD_STRESS_DISSIPATED");
  z_real_t b = material.getRealProperty("HARDENING_COEFFICIENT_DISSIPATED");
  if (b != 0.0 && epsPl > PRECISION) {
    z_real_t n = material.getRealProperty("HARDENING_EXPONENT_DISSIPATED");
    z_real_t expo = 1.0/n;
    z_real_t val = b*std::pow(epsPl,expo);
    sig = sig0+val;
    if (first) h = expo*val/epsPl;
    if (second) dh = expo*(expo-1.0)*val/(epsPl*epsPl);
  }
  else {
    sig = sig0;
    if (first && b != 0.0)
      h = std::numeric_limits<z_real_t>::max();
  }
  
  return sig;
}
