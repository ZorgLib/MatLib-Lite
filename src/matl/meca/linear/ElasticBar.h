/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATL_MECA_LINEAR_ELASTIC_BAR_H
#define ZORGLIB_MATL_MECA_LINEAR_ELASTIC_BAR_H

// config
#include <matlib_macros.h>

// local
#include <matl/ConstitutiveModel.h>
#include <matl/ModelDictionary.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for pure 1D elasticity models.
 */
class ElasticBar : virtual public StandardMaterial {
  
 public:

  // nested classes
  class Potential;
  class Dilatancy;

 protected:
  
  // associated potential
  Potential *potential;
  
  // associated dilatancy
  Dilatancy *dilatancy;
  
  // instance counter
  unsigned int *count;
  
  // empty constructor
  ElasticBar(Potential* p = 0,Dilatancy* d = 0) {
    count = new unsigned int(1);
    potential = p;
    dilatancy = d;
  }

 public:
  
  // constructors
  ElasticBar(Potential& p) {
    count = new unsigned int(1);
    potential = &p;
    dilatancy = 0;
  }
  ElasticBar(Potential& p,Dilatancy& d) {
    count = new unsigned int(1);
    potential = &p;
    dilatancy = &d;
  }

  // copy constructor
  ElasticBar(const ElasticBar& src) {
    count = src.count;
    (*count)++;
    potential = src.potential;
    dilatancy = src.dilatancy;
  }
  
  // destructor
  virtual ~ElasticBar();

  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // apply rotation to material properties
  void rotateProperties(MaterialProperties&,const Rotation&) {}
  
  // update properties in function of external parameters
  void updateProperties(MaterialProperties&,const ConstitutiveModel::ParameterSet&);
  
  // how many external variables ?
  unsigned int nExtVar() const {return 1;}
  
  // self-documenting utilities
  unsigned int nExtVarBundled() const {return 1;}
  ConstitutiveModel::VariableType typeExtVar(unsigned int i) const {
    switch (i) {
      case 0:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      default:
        return ConstitutiveModel::TYPE_NONE;
        break;
    }
  }
  unsigned int indexExtVar(unsigned int i) const {
    switch (i) {
      case 0:
        return 0;
        break;
      default:
        return 1;
        break;
    }
  }
  std::string labelExtVar(unsigned int i) const {
    switch (i) {
      case 0:
        return "strain";
        break;
      default:
        return "";
        break;
    }
  }
  std::string labelExtForce(unsigned int i) const {
    switch (i) {
      case 0:
        return "force";
        break;
      default:
        return "";
        break;
    }
  }
  
  // how many internal variables ?
  unsigned int nIntVar() const {return 2;}
  
  // self-documenting utilities
  unsigned int nIntVarBundled() const {return 2;}
  unsigned int getIntVar(const std::string& str) const {
    if (str == "STRS")
      return 0;
    else if (str == "ENRG")
      return 1;
    else
      return 2;
  }
  ConstitutiveModel::VariableType typeIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      case 1:
        return ConstitutiveModel::TYPE_SCALAR;
        break;
      default:
        return ConstitutiveModel::TYPE_NONE;
        break;
    }
  }
  unsigned int indexIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return 0;
        break;
      case 1:
        return 1;
        break;
      default:
        return 2;
        break;
    }
  }
  std::string labelIntVar(unsigned int i) const {
    switch (i) {
      case 0:
        return "stress";
        break;
      case 1:
        return "elastically stored energy";
        break;
      default:
        return "";
        break;
    }
  }
  
  // check if the material behaviour is linear ?
  bool isLinear() const;
  
  // initialize the state of the material
  void initState(const MaterialProperties& material,MaterialState& state) {
    ConstitutiveModel::initState(material,state);
    state.grad = 0.0;
    state.flux = 0.0;
    state.internal = 0.0;
  }
  
  // compute the incremental potential
  z_real_t incrementalPotential(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                                const MaterialState&,MaterialState&,z_real_t,
                                MatLibMatrix&,bool,bool);
  
 protected:
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                        z_real_t,z_real_t&,z_real_t&,bool,bool);
  
  // compute the dual/conjugate energy
  z_real_t dualEnergy(const MaterialProperties&,const ConstitutiveModel::ParameterSet&,
                      z_real_t,z_real_t&,z_real_t&,bool,bool);
};


/**
 * Base class for pure 1D elastic potentials.
 */
class ElasticBar::Potential {
  
 protected:
  
  // constructor
  Potential() {}
  
 public:
  
  // destructor
  virtual ~Potential() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&) {}
  
  // check if the material behaviour is linear ?
  virtual bool isLinear() const {return true;}

  // compute stored energy
  virtual z_real_t storedEnergy(const MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&,
                                z_real_t,z_real_t&,z_real_t&,bool,bool) = 0;
};

/**
 * Base class for pure 1D elastic dilatancy models.
 */
class ElasticBar::Dilatancy {
  
 protected:
  
  // constructor
  Dilatancy() {}
  
 public:
  
  // destructor
  virtual ~Dilatancy() {}
  
  // check consistency of material properties
  virtual void checkProperties(MaterialProperties&,std::ostream* = 0) = 0;
  
  // update properties in function of external parameters
  virtual void updateProperties(MaterialProperties&,
                                const ConstitutiveModel::ParameterSet&) {}
  
  // compute coupling energy
  virtual z_real_t couplingEnergy(const MaterialProperties&,
                                  const ConstitutiveModel::ParameterSet&,
                                  z_real_t,z_real_t&,z_real_t&,bool,bool) = 0;
};


/**
 * Class describing 1D linear elastic potential.
 */
class LinearElasticPotential1D : virtual public ElasticBar::Potential {
  
 public:
  
  // constructor
  LinearElasticPotential1D() {}
  
  // copy constructor
  LinearElasticPotential1D(const LinearElasticPotential1D&) {}
  
  // destructor
  virtual ~LinearElasticPotential1D() {}
  
  // check consistency of material properties
  void checkProperties(MaterialProperties&,std::ostream* = 0);
  
  // compute stored energy
  z_real_t storedEnergy(const MaterialProperties&,
                        const ConstitutiveModel::ParameterSet&,
                        z_real_t,z_real_t&,z_real_t&,bool,bool);
  
  // compute material stiffness (Hooke) tensor
  void computeStiffness(const MaterialProperties&,
                        const ConstitutiveModel::ParameterSet&,
                        z_real_t&);
};


/**
 * Implementations of the model.
 */
class LinearElasticBar : public ElasticBar {
  
 public:
  
  // constructor
  LinearElasticBar()
  : ElasticBar(new LinearElasticPotential1D()) {}
  
  // copy constructor
  LinearElasticBar(const LinearElasticBar& src)
  : ElasticBar(src) {}
  
  // destructor
  virtual ~LinearElasticBar() {}
};

/**
 * The associated model builder
 */
class LinearElasticBarBuilder : public ModelBuilder {
  
 private:
  
  // constructor
  LinearElasticBarBuilder();
  
  // the instance
  static LinearElasticBarBuilder const* BUILDER;
  
 public:
  
  // destructor
  virtual ~LinearElasticBarBuilder() {}
  
  // build model
  ConstitutiveModel* build(unsigned int) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
