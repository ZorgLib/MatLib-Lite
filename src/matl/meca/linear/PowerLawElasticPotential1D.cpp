/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "PowerLawElasticPotential1D.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

/*
 * Methods for class PowerLawElasticPotential1D
 */

// check consistency of material properties
void PowerLawElasticPotential1D::checkProperties(MaterialProperties& material,std::ostream* os) {
  if (os) (*os) << "\n\t***Power-law elastic potential (pure 1D bar)***" << std::endl;
  
  // get Young's modulus
  z_real_t E = material.getRealProperty("YOUNG_MODULUS");
  if (E <= 0.0) {
    if (os) (*os) << "ERROR: Young's modulus must be strictly positive." << std::endl;
    throw std::runtime_error("invalid property: Young's modulus");
  }
  
  // get reference strain
  z_real_t eps0 = material.getRealProperty("REFERENCE_STRAIN");
  if (eps0 <= 0.0) {
    if (os) (*os) << "ERROR: reference strain must be strictly positive." << std::endl;
    throw std::runtime_error("invalid property: reference strain");
  }
  
  // get exponent
  z_real_t n = material.getRealProperty("ELASTIC_EXPONENT");
  if (n < 1.0) {
    if (os) (*os) << "ERROR: elastic exponent must be larger than 1." << std::endl;
    throw std::runtime_error("invalid property: elastic exponent");
  }

  if (os) {
    (*os) << "\tYoung's modulus       = " << E;
    (*os) << "\n\treference strain      = " << eps0;
    (*os) << "\n\telastic exponent      = " << n << std::endl;
  }
  
  // compute elastic wave speed
  try {
    z_real_t rho = material.getRealProperty("MASS_DENSITY");
    z_real_t c = std::sqrt(E/rho);
    material.setProperty("CELERITY",c);
    if (os) (*os) << "\n\tcelerity              = " << c << std::endl;
  }
  catch (NoSuchPropertyException) {
    if (os) (*os) << "\n\tcelerity is not defined" << std::endl;
  }
}

// compute stored energy
z_real_t PowerLawElasticPotential1D::storedEnergy(const MaterialProperties& material,
                                                   const ConstitutiveModel::ParameterSet& extPar,
                                                   z_real_t eps,z_real_t& sig,z_real_t& M,
                                                   bool first,bool second) {
  // get material parameters
  z_real_t E = material.getRealProperty("YOUNG_MODULUS");
  z_real_t eps0 = material.getRealProperty("REFERENCE_STRAIN");
  z_real_t n = material.getRealProperty("ELASTIC_EXPONENT");

  // potential
  z_real_t expo = 1.0/n;
  z_real_t val0 = std::fabs(eps)/eps0+1.0;
  z_real_t W = n*E*eps0*(eps0/(expo+1.0)*(std::pow(val0,expo+1.0)-1.0)-std::fabs(eps));
  if (!first && !second) return W;
  
  // stress
  z_real_t val1 = std::pow(val0,expo-1.0);
  if (first) sig = std::copysign(n*E*eps0*(val0*val1-1.0),eps);
  
  // tangent
  if (second) M = E*val1;
  
  return W;
}

/*
 * Methods for class PowerLawElasticBarBuilder.
 */

// the instance
PowerLawElasticBarBuilder const* PowerLawElasticBarBuilder::BUILDER
= new PowerLawElasticBarBuilder();

// constructor
PowerLawElasticBarBuilder::PowerLawElasticBarBuilder() {
  ModelDictionary::add("POWER_LAW_ELASTIC_BAR",*this);
}

// build model
ConstitutiveModel* PowerLawElasticBarBuilder::build(unsigned int d) const {
  switch(d) {
    case 3:
    case 2:
    case 1:
      return new PowerLawElasticBar();
      break;
    default:
      return 0;
      break;
  }
}
