/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "ShortArray.h"

// std C library
#include <cmath>
#include <cstring>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for ShortArrayBase
 */

// print to string object
std::string ShortArrayBase::toString() const {
  O_STRING_STREAM os;
  os << "[";
  for (unsigned int i=0; i < size(); i++)
    os << " " << (*this)[i];
  os << "]";
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}


/*
 * Methods for ShortArray
 */

// constructor
ShortArray::ShortArray(unsigned int s) {
  sz = s;
  if (s > 0)
    data = new z_real_t[s];
  else
    data = 0;
  v = data;
}

// subarray constructor
ShortArray::ShortArray(const ShortArray& src,unsigned int s,unsigned int idx0) {
  sz = s;
  if (sz+idx0 > src.size())
    throw std::range_error("ShortArray subarray constructor");
  if (s > 0)
    v = src.v+idx0;
  else
    v = 0;
  data = 0;
}
ShortArray::ShortArray(const ShortArrayBase& src,unsigned int s,unsigned int idx0) {
  sz = s;
  if (sz+idx0 > src.size())
    throw std::range_error("ShortArray subarray constructor");
  if (s > 0) {
    data = new z_real_t[s];
    z_real_t* p = data;
    unsigned int j = idx0;
    for (unsigned int i=0; i < sz; i++, j++, p++) (*p) = src[j];
  }
  else
    data = 0;
  v = data;
}
ShortArray::ShortArray(const std::valarray<z_real_t>& src,unsigned int s,unsigned int idx0) {
  sz = s;
  if (sz+idx0 > src.size())
    throw std::range_error("ShortArray subarray constructor");
  if (s > 0) {
    data = new z_real_t[s];
    z_real_t* p = data;
    unsigned int j = idx0;
    for (unsigned int i=0; i < sz; i++, j++, p++) (*p) = src[j];
  }
  else
    data = 0;
  v = data;
}

// wrapper constructor (for the experienced user)
ShortArray::ShortArray(z_real_t* a,unsigned int s) {
  sz = s;
  v = a;
  data = 0;
}

// copy constructor
ShortArray::ShortArray(const ShortArray& src) {
  sz = src.sz;
  if (sz > 0) {
    data = new z_real_t[sz];
    std::memcpy(data,src.v,src.sz*sizeof(z_real_t));
  }
  else {
    data = 0;
  }
  v = data;
}
ShortArray::ShortArray(const ShortArrayBase& src) {
  sz = src.size();
  if (sz > 0) {
    data = new z_real_t[sz];
    z_real_t* p = data;
    for (unsigned int i=0; i < sz; i++, p++) (*p) = src[i];
  }
  else {
    data = 0;
  }
  v = data;
}
ShortArray::ShortArray(const std::valarray<z_real_t>& src) {
  sz = src.size();
  if (sz > 0) {
    data = new z_real_t[sz];
    z_real_t* p = data;
    for (unsigned int i=0; i < sz; i++, p++) (*p) = src[i];
  }
  else {
    data = 0;
  }
  v = data;
}

// assignment operator
ShortArray& ShortArray::operator=(const ShortArray& src) {
  resize(src.size());
  if (size()) std::memcpy(v,src.v,src.size()*sizeof(z_real_t));
  return *this;
}
ShortArray& ShortArray::operator=(const ShortArrayBase& src) {
  resize(src.size());
  z_real_t* p = v;
  for (unsigned int i=0; i < src.size(); i++, p++) (*p) = src[i];
  return *this;
}
ShortArray& ShortArray::operator=(const std::valarray<z_real_t>& src) {
  resize(src.size());
  z_real_t* p = v;
  for (unsigned int i=0; i < src.size(); i++, p++) (*p) = src[i];
  return *this;
}
ShortArray& ShortArray::operator=(z_real_t val) {
  z_real_t* p = v;
  for (unsigned int i=0; i < size(); i++, p++) (*p) = val;
  return *this;
}

// unary operators
ShortArray& ShortArray::operator+=(const ShortArray& a) {
  if (size() != a.size()) throw std::range_error("ShortArray +=");
  z_real_t* pv = v;
  z_real_t* pa = a.v;
  for (unsigned int i=0; i < a.size(); i++, pv++, pa++) (*pv) += (*pa);
  return *this;
}
ShortArray& ShortArray::operator+=(const ShortArrayBase& a) {
  if (size() != a.size()) throw std::range_error("ShortArray +=");
  z_real_t* p = v;
  for (unsigned int i=0; i < a.size(); i++, p++) (*p) += a[i];
  return *this;
}
ShortArray& ShortArray::operator+=(const std::valarray<z_real_t>& a) {
  if (size() != a.size()) throw std::range_error("ShortArray +=");
  z_real_t* p = v;
  for (unsigned int i=0; i < a.size(); i++, p++) (*p) += a[i];
  return *this;
}
ShortArray& ShortArray::operator-=(const ShortArray& a) {
  if (size() != a.size()) throw std::range_error("ShortArray -=");
  z_real_t* pv = v;
  z_real_t* pa = a.v;
  for (unsigned int i=0; i < a.size(); i++, pv++, pa++) (*pv) -= (*pa);
  return *this;
}
ShortArray& ShortArray::operator-=(const ShortArrayBase& a) {
  if (size() != a.size()) throw std::range_error("ShortArray -=");
  z_real_t* p = v;
  for (unsigned int i=0; i < a.size(); i++, p++) (*p) -= a[i];
  return *this;
}
ShortArray& ShortArray::operator-=(const std::valarray<z_real_t>& a) {
  if (size() != a.size()) throw std::range_error("ShortArray -=");
  z_real_t* p = v;
  for (unsigned int i=0; i < a.size(); i++, p++) (*p) -= a[i];
  return *this;
}
ShortArray& ShortArray::operator*=(z_real_t val) {
  z_real_t* p = v;
  for (unsigned int i=0; i < size(); i++, p++) (*p) *= val;
  return *this;
}
ShortArray& ShortArray::operator/=(z_real_t val) {
  z_real_t valInv = 1.0/val;
  z_real_t* p = v;
  for (unsigned int i=0; i < size(); i++, p++) (*p) *= valInv;
  return *this;
}

// resize
void ShortArray::resize(unsigned int s) {
  if (size() == s) return;
  if (data) delete [] data;
  sz = s;
  if (s > 0) 
    data = new z_real_t[s];
  else
    data = 0;
  v = data;
}

// wrap another array
void ShortArray::wrap(ShortArray& src,unsigned int s,unsigned int idx0) {
  if (data) delete [] data;
  sz = s;
  v = src.v+idx0;
  data = 0;
}

// wrap C array (for the experienced user)
void ShortArray::wrap(z_real_t* a,unsigned int s) {
  if (data) delete [] data;
  sz = s;
  v = a;
  data = 0;
}

// get raw C array (for the experienced user)
z_real_t* ShortArray::raw() {
  return v;
}

// access operators
z_real_t ShortArray::operator()(unsigned int i) const {
  if (i < size())
    return v[i];
  else
    throw std::out_of_range("ShortArray");
}
z_real_t& ShortArray::operator()(unsigned int i) {
  if (i < size())
    return v[i];
  else
    throw std::out_of_range("ShortArray");
}

// define inner product
z_real_t MATLIB_NAMESPACE innerProd(const ShortArrayBase& a,const ShortArrayBase& b) {
  if (a.size() != b.size()) throw std::range_error("innerProd");
  z_real_t prod = 0.0;
  for (unsigned int i=0; i < a.size(); i++) prod += a[i]*b[i];
  return prod;
}

// compute norm of an array
z_real_t MATLIB_NAMESPACE normL1(const ShortArrayBase& a) {
  z_real_t norm = 0.0;
  for (unsigned int i=0; i < a.size(); i++) norm += std::fabs(a[i]);
  return norm;
}
z_real_t MATLIB_NAMESPACE normL2(const ShortArrayBase& a) {
  z_real_t norm = 0.0;
  for (unsigned int i=0; i < a.size(); i++) norm += a[i]*a[i];
  return std::sqrt(norm);
}

// compute max absolute value
z_real_t MATLIB_NAMESPACE normLInf(const ShortArrayBase& a) {
  z_real_t norm = 0.0;
  for (unsigned int i=0; i < a.size(); i++) {
    z_real_t test = std::fabs(a[i]);
    if (test > norm) norm = test;
  }
  return norm;
}
