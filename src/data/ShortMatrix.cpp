/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "ShortMatrix.h"

// std C library
#include <cmath>
#include <cstring>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for ShortMatrixBase.
 */

// print to string object
std::string ShortMatrixBase::toString() const {
  O_STRING_STREAM os;
  os << std::endl;
  for (unsigned int i=0; i < this->nRows(); i++) {
    os << "|";
    for (unsigned int j=0; j < this->nCols(); j++) os << " " << (*this)(i,j);
    os << "|" << std::endl;
  }
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}


/*
 * Methods for ShortMatrix.
 */

// constructor
ShortMatrix::ShortMatrix(unsigned int m,unsigned int n) {
  nr = m; nc = n;
  if (m > 0 && n > 0) {
    unsigned int memSize = m*n;
    data = new z_real_t[memSize];
    r = new z_real_t*[m];
    z_real_t* p = data;
    for (unsigned int i=0; i < m; i++, p+=n) r[i] = p;
  }
  else {
    data = 0;
    r = 0;
  }
}

// submatrix constructor
ShortMatrix::ShortMatrix(const ShortMatrix& src,unsigned int m,unsigned int n,
                         unsigned int idx0,unsigned int jdx0) {
  nr = m; nc = n;
  if (m > 0 && n > 0) {
    r = new z_real_t*[m];
    for (unsigned int i=0; i < m; i++) r[i] = src.r[idx0+i]+jdx0;
  }
  else
    r = 0;
  data = 0;
}
ShortMatrix::ShortMatrix(const ShortMatrixBase& src,unsigned int m,unsigned int n,
                         unsigned int idx0,unsigned int jdx0) {
  nr = m; nc = n;
  if (m > 0 && n > 0) {
    r = new z_real_t*[m];
    unsigned int memSize = m*n;
    data = new z_real_t[memSize];
    z_real_t* p = data;
    unsigned int ii=idx0;
    for (unsigned int i=0; i < nr; i++, ii++) {
      r[i] = p;
      unsigned int jj=jdx0;
      for (unsigned int j=0; j < nc; j++, jj++, p++) (*p) = src(ii,jj);
    }
  }
  else {
    data = 0;
    r = 0;
  }
}
  
// wrapper constructor (for the experienced user)
ShortMatrix::ShortMatrix(z_real_t* a,unsigned int m,unsigned int n) {
  nr = m;
  nc = n;
  if (m > 0 && n > 0) {
    r = new z_real_t*[m];
    z_real_t* p = a;
    for (unsigned int i=0; i < nr; i++, p+=nc)
      r[i] = p;
  }
  data = 0;
}

// copy constructor
ShortMatrix::ShortMatrix(const ShortMatrix& src) {
  nr = src.nr; nc = src.nc;
  if (nr > 0 && nc > 0) {
    r = new z_real_t*[nr];
    unsigned int memSize = nr*nc;
    data = new z_real_t[memSize];
    if (src.data) std::memcpy(data,src.data,memSize*sizeof(z_real_t));
    z_real_t* p = data;
    for (unsigned int i=0; i < nr; i++, p+=nc) {
      r[i] = p;
      if ((!src.data) && src.r[i])
        std::memcpy(p,src.r[i],nc*sizeof(z_real_t));
    }
  }
  else {
    data = 0;
    r = 0;
  }
}
ShortMatrix::ShortMatrix(const ShortMatrixBase& src) {
  nr = src.nRows(); nc = src.nCols();
  if (nr > 0 && nc > 0) {
    r = new z_real_t*[nr];
    unsigned int memSize = nr*nc;
    data = new z_real_t[memSize];
    z_real_t* p = data;
    for (unsigned int i=0; i < nr; i++) {
      r[i] = p;
      for (unsigned int j=0; j < nc; j++, p++) (*p) = src(i,j);
    }
  }
  else {
    data = 0;
    r = 0;
  }
}

// assignment operator
ShortMatrix& ShortMatrix::operator=(const ShortMatrix& src) {
  resize(src.nRows(),src.nCols());
  for (unsigned int i=0; i < src.nRows(); i++)
    std::memcpy(r[i],src.r[i],src.nCols()*sizeof(z_real_t));
  return *this;
}
ShortMatrix& ShortMatrix::operator=(const ShortMatrixBase& src) {
  resize(src.nRows(),src.nCols());
  for (unsigned int i=0; i < src.nRows(); i++) {
    z_real_t* p = r[i];
    for (unsigned int j=0; j < src.nCols(); j++, p++) (*p) = src(i,j);
  }
  return *this;
}
ShortMatrix& ShortMatrix::operator=(z_real_t val) {
  for (unsigned int i=0; i < nRows(); i++) {
    z_real_t* p = r[i];
    for (unsigned int j=0; j < nCols(); j++, p++) (*p) = val;
  }
  return *this;
}

// unary operators
ShortMatrix& ShortMatrix::operator+=(const ShortMatrix& src) {
  if (nRows() != src.nRows() || nCols() != src.nCols()) 
    throw std::range_error("ShortMatrix");
  for (unsigned int i=0; i < src.nRows(); i++) {
    z_real_t* p = r[i];
    z_real_t* q = src.r[i];
    for (unsigned int j=0; j < src.nCols(); j++, p++, q++) (*p) += (*q);
  }
  return *this;
}
ShortMatrix& ShortMatrix::operator+=(const ShortMatrixBase& src) {
  if (nRows() != src.nRows() || nCols() != src.nCols())
    throw std::range_error("ShortMatrix");
  for (unsigned int i=0; i < src.nRows(); i++) {
    z_real_t* p = r[i];
    for (unsigned int j=0; j < src.nCols(); j++, p++) (*p) += src(i,j);
  }
  return *this;
}
ShortMatrix& ShortMatrix::operator-=(const ShortMatrix& src) {
  if (nRows() != src.nRows() || nCols() != src.nCols()) 
    throw std::range_error("ShortMatrix");
  for (unsigned int i=0; i < src.nRows(); i++) {
    z_real_t* p = r[i];
    z_real_t* q = src.r[i];
    for (unsigned int j=0; j < src.nCols(); j++, p++, q++) (*p) -= (*q);
  }
  return *this;
}
ShortMatrix& ShortMatrix::operator-=(const ShortMatrixBase& src) {
  if (nRows() != src.nRows() || nCols() != src.nCols())
    throw std::range_error("ShortMatrix");
  for (unsigned int i=0; i < src.nRows(); i++) {
    z_real_t* p = r[i];
    for (unsigned int j=0; j < src.nCols(); j++, p++) (*p) -= src(i,j);
  }
  return *this;
}
ShortMatrix& ShortMatrix::operator*=(z_real_t val) {
  for (unsigned int i=0; i < nRows(); i++) {
    z_real_t* p = r[i];
    for (unsigned int j=0; j < nCols(); j++, p++) (*p) *= val;
  }
  return *this;
}
ShortMatrix& ShortMatrix::operator/=(z_real_t val) {
  z_real_t valInv = 1.0/val;
  for (unsigned int i=0; i < nRows(); i++) {
    z_real_t* p = r[i];
    for (unsigned int j=0; j < nCols(); j++, p++) (*p) *= valInv;
  }
  return *this;
}

// resize
void ShortMatrix::resize(unsigned int m,unsigned int n) {
  if (nRows() == m && nCols() == n) return;
  if (data) delete [] data;
  if (r) delete [] r;
  nr = m;
  nc = n;
  unsigned int memSize = nr*nc;
  if (memSize > 0) {
    data = new z_real_t[memSize];
    r = new z_real_t*[m];
    z_real_t* p = data;
    for (unsigned int i=0; i < m; i++, p+=n) r[i] = p;
  } else {
    data = 0;
    r = 0;
  }
}

// wrap another matrix
void ShortMatrix::wrap(const ShortMatrix& src,unsigned int m,unsigned int n,unsigned int idx0, unsigned int jdx0) {
  if (data) delete [] data;
  if (r) delete [] r;
  nr = m;
  nc = n;
  if (m > 0 && n > 0) {
    r = new z_real_t*[m];
    for (unsigned int i=0; i < m; i++) r[i] = src.r[idx0+i]+jdx0;
  }
  else
    r = 0;
  data = 0;
}

// wrap C array (for the experienced user)
void ShortMatrix::wrap(z_real_t* a,unsigned int m,unsigned int n) {
  if (data) delete [] data;
  if (r) delete [] r;
  nr = m;
  nc = n;
  if (m > 0 && n > 0) {
    r = new z_real_t*[m];
    z_real_t* p = a;
    for (unsigned int i=0; i < nr; i++, p+=nc)
      r[i] = p;
  }
  data = 0;
}

// get raw C array (for the experienced user)
z_real_t* ShortMatrix::raw() {
  return r[0];
}

// access operators
z_real_t ShortMatrix::operator()(unsigned int i,unsigned int j) const {
  if (i < nRows() && j < nCols())
    return r[i][j];
  else
    throw std::out_of_range("ShortMatrix");
}
z_real_t& ShortMatrix::operator()(unsigned int i,unsigned int j) {
  if (i < nRows() && j < nCols())
    return r[i][j];
  else
    throw std::out_of_range("ShortMatrix");
}

// build matrix by vector outer product
void ShortMatrix::outerProd(const ShortArrayBase& a,const ShortArrayBase& b,
                            ShortMatrix& M) {
  M.resize(a.size(),b.size());
  for (unsigned int i=0; i < a.size(); i++)
    for (unsigned int j=0; j < b.size(); j++) M.r[i][j] = a[i]*b[j];
}

// simple matrix*array product
ShortArray MATLIB_NAMESPACE operator*(const ShortMatrixBase& A,const ShortArrayBase& b) {
  if (A.nCols() != b.size()) throw std::range_error("matrix*array product");
  ShortArray c(A.nRows());
  for (unsigned int i=0; i < A.nRows(); i++) {
    z_real_t val = 0.0;
    for (unsigned int j=0; j < A.nCols(); j++) val += A[ShortMatrixBase::Indices(i,j)]*b[j];
    c[i] = val;
  }
  return c;
}

// simple matrix*matrix product
ShortMatrix MATLIB_NAMESPACE operator*(const ShortMatrixBase& A,const ShortMatrixBase& B) {
  if (A.nCols() != B.nRows()) throw std::range_error("matrix*matrix product");
  ShortMatrix C(A.nRows(),B.nCols());
  for (unsigned int i=0; i < A.nRows(); i++)
    for (unsigned int j=0; j < B.nCols(); j++) {
      z_real_t val = 0.0;
      for (unsigned int k=0; k < A.nCols(); k++)
        val += A[ShortMatrixBase::Indices(i,k)]*B[ShortMatrixBase::Indices(k,j)];
      C[ShortMatrixBase::Indices(i,j)] = val;
    }
  return C;
}

// define inner product
z_real_t MATLIB_NAMESPACE innerProd(const ShortMatrixBase& A,const ShortMatrixBase& B) {
  if ((A.nRows() != B.nRows()) || (A.nCols() != B.nCols())) throw std::range_error("innerProd");
  z_real_t prod = 0.e0;
  for (unsigned int i=0; i < A.nRows(); i++)
    for (unsigned int j=0; j < B.nCols(); j++)
      prod += A[ShortMatrixBase::Indices(i,j)]*B[ShortMatrixBase::Indices(i,j)];
  return prod;
}

// compute norm of an array
z_real_t MATLIB_NAMESPACE normL1(const ShortMatrixBase& A) {
  z_real_t norm = 0.e0;
  for (unsigned int i=0; i < A.nRows(); i++)
    for (unsigned int j=0; j < A.nCols(); j++)
      norm += std::fabs(A(i,j));
  return norm;
}
z_real_t MATLIB_NAMESPACE normL2(const ShortMatrixBase& A) {
  z_real_t norm = 0.e0;
  for (unsigned int i=0; i < A.nRows(); i++)
    for (unsigned int j=0; j < A.nCols(); j++)
      norm += A(i,j)*A(i,j);
  return std::sqrt(norm);
}

// compute max absolute value
z_real_t MATLIB_NAMESPACE normLInf(const ShortMatrixBase& A) {
  z_real_t norm = 0.e0;
  for (unsigned int i=0; i < A.nRows(); i++)
    for (unsigned int j=0; j < A.nCols(); j++) {
      z_real_t test = std::fabs(A(i,j));
      if (test > norm) norm = test;
    }
  return norm;
}
