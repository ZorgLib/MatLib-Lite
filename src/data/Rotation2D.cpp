/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "Rotation2D.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for class Rotation2D.
 */

// constructor (from angle)
Rotation2D::Rotation2D(z_real_t th) {
  theta = th;
}

// constructor (from rotation matrix)
Rotation2D::Rotation2D(const RotationMatrix& R) {
  theta = std::atan2(R[1][0],R[0][0]);
}

// copy constructor
Rotation2D::Rotation2D(const Rotation2D& src) {
  theta = src.theta;
}

// assignment operator
Rotation2D& Rotation2D::operator=(const Rotation2D& src) {
  theta = src.theta;
  return *this;
}

// export to matrix
void Rotation2D::toMatrix(RotationMatrix& R) const {
  R.resize(2);
  z_real_t cs = std::cos(theta);
  z_real_t sn = std::sin(theta);
  R[0][0] = cs; R[0][1] = -sn;
  R[1][0] = sn; R[1][1] =  cs;
  return;
}
  
// export to tensor (Voigt notation)
void Rotation2D::toTensor(RotationTensor& R) const {
  R.resize(5);
  z_real_t cs = std::cos(theta);
  z_real_t sn = std::sin(theta);
  R[0] = cs; R[1] = -sn;
  R[2] = sn; R[3] =  cs;
  R[4] = 1.e0;
  return;
}
