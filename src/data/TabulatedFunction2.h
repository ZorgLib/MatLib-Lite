/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_TABULATED_FUNCTION2_H
#define ZORGLIB_DATA_TABULATED_FUNCTION2_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/Function2.h>
#include <data/TabulatedFunction.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Tabulated functions (with C1-continuity).
 */
class TabulatedFunction2 : virtual public TabulatedFunction,
                           virtual public Function2 {

 protected:
  
  // list of slopes
  z_real_t *dy;

  // slope initialization flag
  bool updateSlopes;

  // compute slopes
  void computeSlopes();

 public:

  // default constructor
  TabulatedFunction2(const std::string&,const std::string& = "no name",unsigned int = 0);

  // constructor
  TabulatedFunction2(const std::string&,const std::string&,unsigned int,z_real_t*,z_real_t*);

  // copy constructor
  TabulatedFunction2(const TabulatedFunction2&);

  // destructor
  virtual ~TabulatedFunction2();

  // duplicate object
  virtual TabulatedFunction2* clone() const {return new TabulatedFunction2(*this);}

  // resize
  void resize(unsigned int);
  
  // set value
  void setPoint(unsigned int,z_real_t,z_real_t);
  
  // get value
  z_real_t value(z_real_t);
  
  // get derivative
  z_real_t slope(z_real_t);
  
  // get curvature
  z_real_t curvature(z_real_t);
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&);
  
  // get value and derivatives
  z_real_t value(z_real_t,z_real_t&,z_real_t&);
  
  // print-out
  std::string toString() const {return TabulatedFunction::toString();}
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
