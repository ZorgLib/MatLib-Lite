/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_RAMP_FUNCTION_H
#define ZORGLIB_DATA_RAMP_FUNCTION_H

// config
#include <matlib_macros.h>

// std C library
#include <cstring>

// local
#ifndef WITH_MATLIB_H
#include <data/Function2.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Ramp functions.
 */
class RampFunction : virtual public Function2 {
  
 protected:
  
  // slope
  z_real_t rate;
  
 public:
  
  // default constructor
  RampFunction(const std::string& arg,const std::string& s = "no name",z_real_t sl = 1.0)
  : Function(arg,s) {rate = sl;}
  
  // copy constructor
  RampFunction(const RampFunction& src)
  : Function(src) {rate = src.rate;}
  
  // destructor
  virtual ~RampFunction() {}
  
  // duplicate object
  virtual RampFunction* clone() const {return new RampFunction(*this);}
  
  // get value
  z_real_t value(z_real_t t) {return rate*t;}
  
  // get derivative
  z_real_t slope(z_real_t) {return rate;}
  
  // get curvature
  z_real_t curvature(z_real_t) {return 0.0;}
  
  // get value and derivative
  z_real_t value(z_real_t t,z_real_t& sl) {
    sl = rate;
    return rate*t;
  }
  
  // get value and derivatives
  z_real_t value(z_real_t t,z_real_t& sl,z_real_t& curv) {
    sl = rate; curv = 0.0;
    return rate*t;
  }
  
  // print-out
  std::string toString() const {
    O_STRING_STREAM os;
    os << "Ramp function: " << getName() << " with slope " << rate << std::endl;
#ifdef HAVE_SSTREAM
    return os.str();
#else
    return std::string(os.str(),os.pcount());
#endif
  }
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
