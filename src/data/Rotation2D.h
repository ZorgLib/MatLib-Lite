/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2019, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_ROTATION_2D_H
#define ZORGLIB_DATA_ROTATION_2D_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/Rotation.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class for 2D rotations.
 */
class Rotation2D : virtual public Rotation {
  
 private:
  
  // rotation angle
  z_real_t theta;
  
 public:
  
  // constructor (from angle)
  Rotation2D(z_real_t = 0.0);
  
  // constructor (from rotation matrix)
  Rotation2D(const RotationMatrix&);
  
  // copy constructor
  Rotation2D(const Rotation2D&);
  
  // destructor
  virtual ~Rotation2D() {}
  
  // assignment operator
  Rotation2D& operator=(const Rotation2D&);

  // export to matrix
  void toMatrix(RotationMatrix&) const;
  
  // export to tensor
  void toTensor(RotationTensor&) const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
