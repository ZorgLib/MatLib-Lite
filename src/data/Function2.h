/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_FUNCTION2_H
#define ZORGLIB_DATA_FUNCTION2_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/Function.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for functions for which a curvature can be computed.
 * It normally implies C1 continuity.
 */
class Function2 : virtual public Function {

 protected:

  // default constructor
  Function2() {}

 public:
  
  // destructor
  virtual ~Function2() {}
  
  // duplicate object
  virtual Function2* clone() const = 0;
  
  // get value
  virtual z_real_t value(z_real_t) = 0;
  
  // get derivative
  virtual z_real_t slope(z_real_t) = 0;
  
  // get curvature
  virtual z_real_t curvature(z_real_t) = 0;
  
  // get value and derivative
  virtual z_real_t value(z_real_t,z_real_t&) = 0;
  
  // get value and derivatives
  virtual z_real_t value(z_real_t,z_real_t&,z_real_t&) = 0;
};
  
#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif
  
#endif  
