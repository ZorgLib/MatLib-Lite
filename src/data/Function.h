/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_FUNCTION_H
#define ZORGLIB_DATA_FUNCTION_H

// config
#include <matlib_macros.h>

// std C++ library
#include <string>
// local
#ifndef WITH_MATLIB_H
#include <data/Cloneable.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for functions.
 */
class Function : virtual public Cloneable {
  
 protected:
  
  // name/type of function argument
  std::string arg_name;

  // name of function
  std::string name;

  // default constructor
  Function() {}
  
  // constructor
  Function(const std::string& a,const std::string& s = "no name")
  : arg_name(a), name(s) {}
  
  // copy constructor
  Function(const Function& src)
  : arg_name(src.arg_name), name(src.name) {}
  
 public:

  // destructor
  virtual ~Function() {}
  
  // duplicate object
  virtual Function* clone() const = 0;

  // get the function's argument name
  std::string getArgName() const {return arg_name;}

  // get the function's name
  std::string getName() const {return name;}

  // get value
  virtual z_real_t value(z_real_t) = 0;
  
  // get derivative
  virtual z_real_t slope(z_real_t) = 0;
  
  // get value and derivative
  virtual z_real_t value(z_real_t,z_real_t&) = 0;
  
  // print-out
  virtual std::string toString() const = 0;
};


/**
 * Overload the output stream operator
 */
inline std::ostream& operator<<(std::ostream& os,const Function& obj) {
  os << obj.toString(); return os;
}

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
