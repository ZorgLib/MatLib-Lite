/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "Property.h"

// local
#include <data/TabulatedFunction.h>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


// convert to string
std::string BooleanProperty::toString() const {
  O_STRING_STREAM os;
  if (val)
    os << "True (boolean)";
  else
    os << "False (boolean)";
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}

// convert to string
std::string IntegerProperty::toString() const {
  O_STRING_STREAM os;
  os << val << " (integer)";
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}

// convert to string
std::string RealProperty::toString() const {
  O_STRING_STREAM os;
  os << val << " (real)";
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}

// convert to string
std::string StringProperty::toString() const {
  O_STRING_STREAM os;
  os << "\"" << val << "\" (string)";
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}

// convert to string
std::string FunctionProperty::toString() const {
  O_STRING_STREAM os;
  TabulatedFunction* tab = dynamic_cast<TabulatedFunction*>(fct);
  if (tab) {
    os << "[";
    if (tab->nPoints() > 0) {
      std::pair<z_real_t,z_real_t> val = tab->getPoint(0);
      os << val.first << "," << val.second;
      for (unsigned int n=1; n < tab->nPoints(); n++) {
        std::pair<z_real_t,z_real_t> val = tab->getPoint(n);
        os << ";" << val.first << "," << val.second;
      }
    }
    os << "] (function)";
  }
  else
    os << fct->getName() << " (function)";
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}
