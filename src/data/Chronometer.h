/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_CHRONO_H
#define ZORGLIB_DATA_CHRONO_H

// config
#include <matlib_macros.h>

// std C++ library
#include <string>
// std C library
#include <ctime>
// OpenMP
#ifdef _OPENMP
#include <omp.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class for CPU chronometer.
 */
class Chronometer {

 public:
 
  // time type
  typedef unsigned long long time_t;

 private:

  // indicator
  bool isRunning;

  // reference time
  clock_t refTime;

  // accumulated time
  time_t accTime;

 public:

  // constructor
  Chronometer();

  // copy constructor
  Chronometer(const Chronometer&);

  // destructor
  ~Chronometer() {}

  // start chrono
  void start();

  // stop chrono
  void stop();

  // reset chrono
  void reset();

  // elapsed time
  time_t elapsed();

  // format elapsed time
  static std::string toString(time_t);
};

#ifdef _OPENMP
/**
 * Class for OpenMP-compatible chronometer.
 */
class OMPChronometer {

 public:
  
  // time type
  typedef double time_t;
  
 private:
  
  // indicator
  bool isRunning;
  
  // reference time
  time_t refTime;
  
  // accumulated time
  time_t accTime;
  
 public:
  
  // constructor
  OMPChronometer();
  
  // copy constructor
  OMPChronometer(const OMPChronometer&);
  
  // destructor
  ~OMPChronometer() {}
  
  // start chrono
  void start();
  
  // stop chrono
  void stop();
  
  // reset chrono
  void reset();
  
  // elapsed time
  time_t elapsed();
  
  // format elapsed time
  static std::string toString(time_t);
};
#endif

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
