/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_PROPERTY_H
#define ZORGLIB_DATA_PROPERTY_H

// config
#include <matlib_macros.h>

// std C++ library
#include <iostream>
#include <string>
// local
#ifndef WITH_MATLIB_H
#include <data/Cloneable.h>
#include <data/Dictionary.h>
#include <data/Function.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for multi-type properties.
 */
class Property : virtual public Cloneable {

 public:

  // destructor
  virtual ~Property() {}

  // duplicate object
  virtual Property* clone() const = 0;

  // output as a string
  virtual std::string toString() const = 0;
};

// overload the output stream operator
inline std::ostream& operator<<(std::ostream& os,const Property& x) {
  os << x.toString(); return os;
}


/**
 * Boolean-valued property.
 */
class BooleanProperty : public Property {
  
 private:
  
  bool val;
  
 public:
  
  // default constructor
  BooleanProperty(bool v = false) {val = v;}
  
  // copy constructor
  BooleanProperty(const BooleanProperty& src) {val = src.val;}
  
  // destructor
  virtual ~BooleanProperty() {}
  
  // get value
  bool value() const {return val;}
  
  // set value
  void setValue(bool v) {val = v;}
  
  // duplicate object
  BooleanProperty* clone() const {return new BooleanProperty(*this);}
  
  // output as a string
  std::string toString() const;
};


/**
 * Integer-valued property.
 */
class IntegerProperty : public Property {

 private:

  z_int_t val;

 public:

  // default constructor
  IntegerProperty(z_int_t v = 0) {val = v;}

  // copy constructor
  IntegerProperty(const IntegerProperty& src) {val = src.val;}

  // destructor
  virtual ~IntegerProperty() {}

  // get value
  z_int_t value() const {return val;}

  // set value
  void setValue(z_int_t v) {val = v;}

  // duplicate object
  IntegerProperty* clone() const {return new IntegerProperty(*this);}

  // output as a string
  std::string toString() const;
};


/**
 * Real-valued property.
 */
class RealProperty : public Property {

 private:

  z_real_t val;

 public:

  // default constructor
  RealProperty(z_real_t v = 0.e0) {val = v;}

  // copy constructor
  RealProperty(const RealProperty& src) {val = src.val;}

  // destructor
  virtual ~RealProperty() {}

  // get value
  z_real_t value() const {return val;}

  // set value
  void setValue(z_real_t v) {val = v;}

  // duplicate object
  RealProperty* clone() const {return new RealProperty(*this);}

  // output as a string
  std::string toString() const;
};


/**
 * String-valued (character string) property.
 */
class StringProperty : public Property {
  
 private:
  
  std::string val;
  
 public:
  
  // default constructor
  StringProperty(const char* s) {val = s;}
  
  // copy constructor
  StringProperty(const StringProperty& src) {val = src.val;}
  
  // destructor
  virtual ~StringProperty() {}
  
  // get value
  std::string value() const {return val;}
  
  // set value
  void setValue(const char* s) {val = s;}
  
  // duplicate object
  StringProperty* clone() const {return new StringProperty(*this);}
  
  // output as a string
  std::string toString() const;
};


/**
 * Function-valued property.
 */
class FunctionProperty : public Property {

 private:

  Function *fct;

 public:

  // default constructor
  FunctionProperty(Function& f) {fct = f.clone();}

  // copy constructor
  FunctionProperty(const FunctionProperty& src) {
    fct = (src.fct)->clone();
  }

  // destructor
  virtual ~FunctionProperty() {delete fct;}

  // get value
  Function& function() const {return *fct;}

  // set value
  void setFunction(Function& f) {
    delete fct; fct = f.clone();
  }

  // duplicate object
  FunctionProperty* clone() const {return new FunctionProperty(*this);}

  // output as a string
  std::string toString() const;
};


/**
 * Template-based property.
 */
template <class T>
class StdProperty : public Property {

 private:
  
  // data
  T *data;

 public:

  // default constructor
  StdProperty() {data = new T();}
  
  // constructor
  StdProperty(const T& d) {data = new T(d);}
  
  // copy constructor
  StdProperty(const StdProperty& src) {data = new T(*(src.data));}
  
  // destructor
  virtual ~StdProperty() {delete data;}
  
  // get value
  T& value() const {return *data;}
  
  // duplicate object
  StdProperty* clone() const {return new StdProperty(*this);}
  
  // output as a string
  std::string toString() const {return data->toString();}
};


/**
 * Define new type PropertyTable
 */
typedef Dictionary<Property*>::Type PropertyTable;

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
