/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_CONSTANT_FUNCTION_H
#define ZORGLIB_DATA_CONSTANT_FUNCTION_H

// config
#include <matlib_macros.h>

// std C library
#include <cstring>

// local
#ifndef WITH_MATLIB_H
#include <data/Function2.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Constant functions.
 */
class ConstantFunction : virtual public Function2 {
  
 protected:
  
  // value
  z_real_t val;
  
 public:
    
  // default constructor
  ConstantFunction(const std::string& s = "no name",z_real_t v = 0.0)
  : Function("no argument",s) {val = v;}
  
  // copy constructor
  ConstantFunction(const ConstantFunction& src)
  : Function(src) {val = src.val;}
  
  // destructor
  virtual ~ConstantFunction() {}
  
  // duplicate object
  virtual ConstantFunction* clone() const {return new ConstantFunction(*this);}
  
  // get value
  z_real_t value(z_real_t) {return val;}
  
  // get derivative
  z_real_t slope(z_real_t) {return 0.0;}
  
  // get curvature
  z_real_t curvature(z_real_t) {return 0.0;}

  // get value and derivative
  z_real_t value(z_real_t,z_real_t& sl) {
    sl = 0.0;
    return val;
  }
  
  // get value and derivatives
  z_real_t value(z_real_t,z_real_t& sl,z_real_t& curv) {
    sl = 0.0; curv = 0.0;
    return val;
  }

  // print-out
  std::string toString() const {
    O_STRING_STREAM os;
    os << "Constant function: " << getName() << " with value " << val << std::endl;
#ifdef HAVE_SSTREAM
    return os.str();
#else
    return std::string(os.str(),os.pcount());
#endif
  }
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
