/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_ROTATION_H
#define ZORGLIB_DATA_ROTATION_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#include <data/ShortSqrMatrix.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing a rotation operation.
 */
class Rotation {

 public:
 
  // useful types
  typedef ShortSqrMatrix RotationMatrix;
  typedef ShortArray     RotationTensor;

 protected:

  // constructor
  Rotation() {}
  
 public:
  
  // destructor
  virtual ~Rotation() {}
  
  // export to matrix
  virtual void toMatrix(RotationMatrix&) const = 0;

  // export to tensor
  virtual void toTensor(RotationTensor&) const = 0;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
