/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_SHORT_MATRIX_H
#define ZORGLIB_DATA_SHORT_MATRIX_H

// config
#include <matlib_macros.h>

// std C library
#include <cstdlib>
#include <cstring>
// std C++ library
#include <stdexcept>
#include <utility>
// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for (short) matrices.
 */
class ShortMatrixBase {
  
 public:
  
  // constructor
  ShortMatrixBase() {}
  
  // copy constructor
  ShortMatrixBase(const ShortMatrixBase&) {}
  
  // destructor
  virtual ~ShortMatrixBase() {}
  
  // matrix size
  virtual unsigned int nCols() const = 0;
  virtual unsigned int nRows() const = 0;
  
  // expression operator
  typedef std::pair<unsigned int,unsigned int> Indices;
  virtual z_real_t operator[](const Indices& idx) const {
    return this->operator()(idx.first,idx.second);
  }
  virtual z_real_t operator()(unsigned int,unsigned int) const = 0;

  // print to string object
  std::string toString() const;
};

// define output stream operator
inline
std::ostream& operator<<(std::ostream& os,const ShortMatrixBase& obj) {
  os << obj.toString(); return os;
}


/**
 * Class for small, full matrices.
 */
class ShortMatrix : virtual public ShortMatrixBase {

 protected:
  
  // size
  unsigned int nr,nc;
  
  // data
  z_real_t *data;

  // pointers to rows
  z_real_t* *r;

 public:
  
  // constructor
  ShortMatrix(unsigned int = 0,unsigned int = 1);
  
  // submatrix constructor
  ShortMatrix(const ShortMatrix&,unsigned int,unsigned int,
              unsigned int = 0,unsigned int = 0);
  ShortMatrix(const ShortMatrixBase&,unsigned int,unsigned int,
              unsigned int = 0,unsigned int = 0);
    
  // wrapper constructor (for the experienced user)
  ShortMatrix(z_real_t*,unsigned int,unsigned int);

  // copy constructor
  ShortMatrix(const ShortMatrix&);
  ShortMatrix(const ShortMatrixBase&);
  
  // destructor
  virtual ~ShortMatrix() {
    if (r) delete [] r;
    if (data) delete [] data;
  }
  
  // assignment operator
  ShortMatrix& operator=(const ShortMatrix&);
  ShortMatrix& operator=(const ShortMatrixBase&);
  ShortMatrix& operator=(z_real_t);

  // unary operators
  ShortMatrix& operator+=(const ShortMatrix&);
  ShortMatrix& operator+=(const ShortMatrixBase&);
  ShortMatrix& operator-=(const ShortMatrix&);
  ShortMatrix& operator-=(const ShortMatrixBase&);
  ShortMatrix& operator*=(z_real_t);
  ShortMatrix& operator/=(z_real_t);
  
  // size
  unsigned int nRows() const {return nr;}
  unsigned int nCols() const {return nc;}
  
  // resize
  void resize(unsigned int,unsigned int);
  
  // wrap another matrix
  void wrap(const ShortMatrix&,unsigned int,unsigned int,unsigned int = 0, unsigned int = 0);

  // wrap C array (for the experienced user)
  void wrap(z_real_t*,unsigned int,unsigned int);
  
  // get raw C array (for the experienced user)
  z_real_t* raw();

  // access operators
  ShortArray operator[](unsigned int i) const {return ShortArray(r[i],nc);}
  z_real_t operator[](const Indices& idx) const {return r[idx.first][idx.second];}
  z_real_t& operator[](const Indices& idx) {return r[idx.first][idx.second];}
  z_real_t operator()(unsigned int,unsigned int) const;
  z_real_t& operator()(unsigned int,unsigned int);
  
  // build matrix by vector outer product
  static void outerProd(const ShortArrayBase&,const ShortArrayBase&,ShortMatrix&);
};


/**
 * ShortMatrix sums and differences.
 */
class ShortMatrixSum : public ShortMatrixBase {
  
 private:
  
  // pointers to matrices
  const ShortMatrixBase *A,*B;
  
 public:
    
  // constructor
  ShortMatrixSum(const ShortMatrixBase& a,
                 const ShortMatrixBase& b) {
    if ((a.nCols() != b.nCols()) || (a.nRows() != b.nRows()))
      throw std::invalid_argument("ShortMatrixSum");
    A = &a; B = &b;
  }
  
  // copy constructor
  ShortMatrixSum(const ShortMatrixSum& src) {A = src.A; B = src.B;}
  
  // destructor
  ~ShortMatrixSum() {}
  
  // matrix size
  unsigned int nCols() const {return A->nCols();}
  unsigned int nRows() const {return A->nRows();}
  
  // expression operator
  z_real_t operator()(unsigned int i,unsigned int j) const {
    return (*A)[Indices(i,j)]+(*B)[Indices(i,j)];
  }
};

inline ShortMatrixSum operator+(const ShortMatrixBase& A,
                                const ShortMatrixBase& B) {
  return ShortMatrixSum(A,B);
}


class ShortMatrixDifference : public ShortMatrixBase {
  
 private:
  
  // pointers to matrices
  const ShortMatrixBase *A,*B;
  
 public:
  
  // constructor
  ShortMatrixDifference(const ShortMatrixBase& a,
                        const ShortMatrixBase& b) {
    if ((a.nCols() != b.nCols()) || (a.nRows() != b.nRows()))
      throw std::invalid_argument("ShortMatrixDifference");
    A = &a; B = &b;
  }
  
  // copy constructor
  ShortMatrixDifference(const ShortMatrixDifference& src) {A = src.A; B = src.B;}
  
  // destructor
  ~ShortMatrixDifference() {}
  
  // matrix size
  unsigned int nCols() const {return A->nCols();}
  unsigned int nRows() const {return A->nRows();}
  
  // expression operator
  z_real_t operator()(unsigned int i,unsigned int j) const {
    return (*A)[Indices(i,j)]-(*B)[Indices(i,j)];
  }
};

inline ShortMatrixDifference operator-(const ShortMatrixBase& A,
                                       const ShortMatrixBase& B) {
  return ShortMatrixDifference(A,B);
}


/**
 * Matrix products.
 */
class ShortMatrixScalarProduct : public ShortMatrixBase {
  
 private:
  
  // pointers to matrices
  const ShortMatrixBase *A;
  z_real_t fact;
  
 public:
    
  // constructor
  ShortMatrixScalarProduct(const ShortMatrixBase& a,z_real_t f) {
    A = &a; fact = f;
  }
  
  // copy constructor
  ShortMatrixScalarProduct(const ShortMatrixScalarProduct& src) {
    A = src.A; fact = src.fact;
  }
  
  // destructor
  ~ShortMatrixScalarProduct() {}
  
  // matrix size
  unsigned int nCols() const {return A->nCols();}
  unsigned int nRows() const {return A->nRows();}
  
  // expression operator
  z_real_t operator()(unsigned int i,unsigned int j) const {return fact*(*A)[Indices(i,j)];}
};

inline ShortMatrixScalarProduct operator*(z_real_t f,const ShortMatrixBase& a) {
  return ShortMatrixScalarProduct(a,f);
}
inline ShortMatrixScalarProduct operator*(const ShortMatrixBase& a,z_real_t f) {
  return ShortMatrixScalarProduct(a,f);
}

inline ShortMatrixScalarProduct operator/(const ShortMatrixBase& a,z_real_t f) {
  return ShortMatrixScalarProduct(a,1.0/f);
}

inline ShortMatrixScalarProduct operator-(const ShortMatrixBase& a) {
  return ShortMatrixScalarProduct(a,-1);
}

// define outer product
class ShortArrayOuterProduct : public ShortMatrixBase {
  
 private:
  
  // pointers to arrays
  const ShortArrayBase *a,*b;
  
 public:
  
  // constructor
  ShortArrayOuterProduct(const ShortArrayBase& aa,const ShortArrayBase& bb) {
    a = &aa; b = &bb;
  }
  
  // copy constructor
  ShortArrayOuterProduct(const ShortArrayOuterProduct& src) {a = src.a; b = src.b;}
  
  // destructor
  ~ShortArrayOuterProduct() {}
  
  // matrix size
  unsigned int nCols() const {return b->size();}
  unsigned int nRows() const {return a->size();}
  
  // expression operator
  z_real_t operator()(unsigned int i,unsigned int j) const {return (*a)[i]*(*b)[j];}
};
inline
ShortArrayOuterProduct outerProd(const ShortArrayBase& a,const ShortArrayBase& b) {
  return ShortArrayOuterProduct(a,b);
}

// simple matrix*array product
ShortArray operator*(const ShortMatrixBase&,const ShortArrayBase&);

// simple matrix*matrix product
ShortMatrix operator*(const ShortMatrixBase&,const ShortMatrixBase&);

// define inner product
z_real_t innerProd(const ShortMatrixBase&,const ShortMatrixBase&);

// compute norm of an array
z_real_t normL1(const ShortMatrixBase&);
z_real_t normL2(const ShortMatrixBase&);

// compute max absolute value
z_real_t normLInf(const ShortMatrixBase&);

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif


