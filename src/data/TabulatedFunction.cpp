/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "TabulatedFunction.h"

// std C library
#include <cstring>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for class TabulatedFunction.
 */

// default constructor
TabulatedFunction::TabulatedFunction(const std::string& arg,const std::string& str,unsigned int n)
 : Function(arg,str) {
  mem_init(n);
  lastIdx = 0;
}

// constructor
TabulatedFunction::TabulatedFunction(const std::string& arg,const std::string& str,
                                     unsigned int n,z_real_t *xx,z_real_t *yy)
 : Function(arg,str) {

  mem_copy(n,xx,yy);
  lastIdx = 0;
}

// copy constructor
TabulatedFunction::TabulatedFunction(const TabulatedFunction& src)
 : Function(src) {
  mem_copy(src.nPts,src.x,src.y);
  lastIdx = 0;
}

// destructor
TabulatedFunction::~TabulatedFunction() {
  if (x) delete [] x;
  if (y) delete [] y;
}

// resize
void TabulatedFunction::resize(unsigned int n) {
  if (n == nPts) return;

  if (x) delete [] x;
  if (y) delete [] y;

  mem_init(n);
  lastIdx = 0;
}

// utility functions
void TabulatedFunction::mem_init(unsigned int n) {

  nPts = n;
  if (nPts > 0) {
    // allocate memory
    x = new z_real_t[nPts];
    y = new z_real_t[nPts];
  }
  else {
    x = y = 0;
  }
}

void TabulatedFunction::mem_copy(unsigned int n,z_real_t *xx,z_real_t *yy) {
  
  nPts = n;
  if (nPts > 0) {
    // allocate memory
    x = new z_real_t[nPts];
    y = new z_real_t[nPts];
    std::memcpy(x,xx,nPts*sizeof(z_real_t));
    std::memcpy(y,yy,nPts*sizeof(z_real_t));
  }
  else {
    x = y = 0;
  }
}

// set value
void TabulatedFunction::setPoint(unsigned int i,z_real_t xx,z_real_t yy) {
  if (i < nPts) {
    x[i] = xx;
    y[i] = yy;
  }
}

// get value
z_real_t TabulatedFunction::value(z_real_t u) {

  // quick return
  unsigned int idxMax = nPts-1;
  if (u <= x[0]) return y[0];
  if (u >= x[idxMax]) return y[idxMax];
  
  // find interval
  unsigned int idx=lastIdx;
  if (u > x[idx]) {
    while (idx < idxMax-1) {
      if (u <= x[idx+1]) break;
      idx++;
    }
  }
  else {
    while (idx > 0) {
      idx--;
      if (u > x[idx]) break;
    }
  }
  lastIdx = idx;

  // compute value
  return y[idx]+(u-x[idx])*(y[idx+1]-y[idx])/(x[idx+1]-x[idx]);
}

// get derivative
z_real_t TabulatedFunction::slope(z_real_t u) {
  
  // quick return
  unsigned int idxMax = nPts-1;
  if (u < x[0]) return 0.0;
  if (u > x[idxMax]) return 0.0;
  
  // find interval
  unsigned int idx=lastIdx;
  if (u > x[idx]) {
    while (idx < idxMax-1) {
      if (u <= x[idx+1]) break;
      idx++;
    }
  }
  else {
    while (idx > 0) {
      idx--;
      if (u > x[idx]) break;
    }
  }
  lastIdx = idx;

  // compute slope
  return (y[idx+1]-y[idx])/(x[idx+1]-x[idx]);
}

// get value and derivative
z_real_t TabulatedFunction::value(z_real_t u,z_real_t& df) {
  
  // quick return
  unsigned int idxMax = nPts-1;
  if (u < x[0]) {
    df = 0.0; return y[0];
  }
  if (u > x[idxMax]) {
    df = 0.0; return y[idxMax];
  }
  
  // find interval
  unsigned int idx=lastIdx;
  if (u > x[idx]) {
    while (idx < idxMax-1) {
      if (u <= x[idx+1]) break;
      idx++;
    }
  }
  else {
    while (idx > 0) {
      idx--;
      if (u > x[idx]) break;
    }
  }
  lastIdx = idx;

  // compute slope
  df = (y[idx+1]-y[idx])/(x[idx+1]-x[idx]);

  // compute value
  return y[idx]+(u-x[idx])*df;
}

// print-out
std::string TabulatedFunction::toString() const {
  O_STRING_STREAM os;
  os << "Tabulated function of " << getArgName() << ": ";
  os << getName() << " (" << nPts << " points)" << std::endl;
  for (unsigned int i=0; i < nPts; i++)
    os << "\t\t" << x[i] << "\t" << y[i] << std::endl;
#ifdef HAVE_SSTREAM
  return os.str();
#else
  return std::string(os.str(),os.pcount());
#endif
}
