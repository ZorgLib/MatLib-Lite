/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_TABULATED_FUNCTION_H
#define ZORGLIB_DATA_TABULATED_FUNCTION_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/Function.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Tabulated functions.
 */
class TabulatedFunction : virtual public Function {
  
 protected:
  
  // number of points
  unsigned int nPts;
  
  // pointer to last index
  unsigned int lastIdx;

  // list of points
  z_real_t *x,*y;
  
  // utility functions
  void mem_init(unsigned int);
  void mem_copy(unsigned int,z_real_t*,z_real_t*);

 public:
    
  // default constructor
  TabulatedFunction(const std::string&,const std::string& = "no name",unsigned int = 0);
  
  // constructor
  TabulatedFunction(const std::string&,const std::string&,unsigned int,z_real_t*,z_real_t*);
  
  // copy constructor
  TabulatedFunction(const TabulatedFunction&);
  
  // destructor
  virtual ~TabulatedFunction();
  
  // duplicate object
  virtual TabulatedFunction* clone() const {return new TabulatedFunction(*this);}

  // resize
  void resize(unsigned int);
  
  // get number of points
  unsigned int nPoints() const {return nPts;}
  
  // get point of given index
  std::pair<z_real_t,z_real_t> getPoint(unsigned int i) const {
    return std::pair<z_real_t,z_real_t>(x[i],y[i]);
  }

  // set value
  void setPoint(unsigned int,z_real_t,z_real_t);
  
  // get value
  z_real_t value(z_real_t);
  
  // get derivative
  z_real_t slope(z_real_t);
  
  // get value and derivative
  z_real_t value(z_real_t,z_real_t&);
  
  // print-out
  std::string toString() const;
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
