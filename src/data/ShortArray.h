/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_SHORT_ARRAY_H
#define ZORGLIB_DATA_SHORT_ARRAY_H

// config
#include <matlib_macros.h>

// std C library
#include <cstdlib>
// std C++ library
#include <iostream>
#include <stdexcept>
#include <valarray>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Base class for (short) arrays.
 */
class ShortArrayBase {
  
 public:
  
  // constructor
  ShortArrayBase() {}
  
  // copy constructor
  ShortArrayBase(const ShortArrayBase&) {}
  
  // destructor
  virtual ~ShortArrayBase() {}
  
  // size
  virtual unsigned int size() const = 0;
  
  // expression operator
  virtual z_real_t operator[](unsigned int) const = 0;
  
  // print to string object
  std::string toString() const;
};

// define output stream operator
inline
std::ostream& operator<<(std::ostream& os,const ShortArrayBase& obj) {
  os << obj.toString(); return os;
}


/**
 * Class for short arrays.
 */
class ShortArray : virtual public ShortArrayBase {

 protected:
  
  // size
  unsigned int sz;
  
  // pointer to data
  z_real_t *v;

  // data
  z_real_t *data;

 public:

  // constructor
  ShortArray(unsigned int = 0);

  // subarray constructor
  ShortArray(const ShortArray&,unsigned int,unsigned int = 0);
  ShortArray(const ShortArrayBase&,unsigned int,unsigned int = 0);
  ShortArray(const std::valarray<z_real_t>&,unsigned int,unsigned int = 0);

  // wrapper constructor (for the experienced user)
  ShortArray(z_real_t*,unsigned int);

  // copy constructor
  ShortArray(const ShortArray&);
  ShortArray(const ShortArrayBase&);
  ShortArray(const std::valarray<z_real_t>&);

  // destructor
  virtual ~ShortArray() {
    if (data) delete [] data;
  }

  // assignment operator
  ShortArray& operator=(const ShortArray&);
  ShortArray& operator=(const ShortArrayBase&);
  ShortArray& operator=(const std::valarray<z_real_t>&);
  ShortArray& operator=(z_real_t);
  
  // unary operators
  ShortArray& operator+=(const ShortArray&);
  ShortArray& operator+=(const ShortArrayBase&);
  ShortArray& operator+=(const std::valarray<z_real_t>&);
  ShortArray& operator-=(const ShortArray&);
  ShortArray& operator-=(const ShortArrayBase&);
  ShortArray& operator-=(const std::valarray<z_real_t>&);
  ShortArray& operator*=(z_real_t);
  ShortArray& operator/=(z_real_t);
  
  // export as valarray
  std::valarray<z_real_t> as_valarray() const {
    return std::valarray<z_real_t>(v,sz);
  }

  // size
  unsigned int size() const {return sz;}
  
  // resize
  void resize(unsigned int);

  // wrap another array
  void wrap(ShortArray&,unsigned int,unsigned int = 0);

  // wrap C array (for the experienced user)
  void wrap(z_real_t*,unsigned int);

  // get raw C array (for the experienced user)
  z_real_t* raw();

  // access operators
  z_real_t operator[](unsigned int i) const {return v[i];}
  z_real_t& operator[](unsigned int i) {return v[i];}
  z_real_t operator()(unsigned int) const;
  z_real_t& operator()(unsigned int);
};


/**
 * ShortArray sums and differences.
 */
class ShortArraySum : public ShortArrayBase {
  
 private:
  
  // pointers to arrays
  const ShortArrayBase *a,*b;
  
 public:
    
  // constructor
  ShortArraySum(const ShortArrayBase& aa,
                const ShortArrayBase& bb) {
    if (aa.size() != bb.size()) 
      throw std::invalid_argument("ShortArraySum");
    a = &aa; b = &bb;
  }
  
  // copy constructor
  ShortArraySum(const ShortArraySum& src) {a = src.a; b = src.b;}
  
  // destructor
  ~ShortArraySum() {}
  
  // size
  unsigned int size() const {return (*a).size();}
  
  // expression operator
  z_real_t operator[](unsigned int i) const {return (*a)[i]+(*b)[i];}
};

inline ShortArraySum operator+(const ShortArrayBase& a,
                               const ShortArrayBase& b) {
  return ShortArraySum(b,a);
}


class ShortArrayDifference : public ShortArrayBase {
  
 private:
  
  // pointers to arrays
  const ShortArrayBase *a,*b;
  
 public:
    
  // constructor
  ShortArrayDifference(const ShortArrayBase& aa,
                       const ShortArrayBase& bb) {
    if (aa.size() != bb.size()) 
      throw std::invalid_argument("ShortArrayDifference");
    a = &aa; b = &bb;
  }
  
  // copy constructor
  ShortArrayDifference(const ShortArrayDifference& src) {a = src.a; b = src.b;}
  
  // destructor
  ~ShortArrayDifference() {}
  
  // size
  unsigned int size() const {return (*a).size();}
  
  // expression operator
  z_real_t operator[](unsigned int i) const {return (*a)[i]-(*b)[i];}
};

inline ShortArrayDifference operator-(const ShortArrayBase& a,
                                      const ShortArrayBase& b) {
  return ShortArrayDifference(a,b);
}


/**
 * Array products.
 */
class ShortArrayScalarProduct : public ShortArrayBase {
  
 private:
  
  // pointer to Array
  const ShortArrayBase *a;
  z_real_t fact;
  
 public:
    
  // constructor
  ShortArrayScalarProduct(const ShortArrayBase& aa,z_real_t f) {
    a = &aa; fact = f;
  }
  
  // copy constructor
  ShortArrayScalarProduct(const ShortArrayScalarProduct& src) {
    a = src.a; fact = src.fact;
  }
  
  // destructor
  ~ShortArrayScalarProduct() {}
  
  // size
  unsigned int size() const {return (*a).size();}
  
  // expression operator
  z_real_t operator[](unsigned int i) const {return fact*(*a)[i];}
};

inline ShortArrayScalarProduct operator*(z_real_t f,const ShortArrayBase& a) {
  return ShortArrayScalarProduct(a,f);
}
inline ShortArrayScalarProduct operator*(const ShortArrayBase& a,z_real_t f) {
  return ShortArrayScalarProduct(a,f);
}

inline ShortArrayScalarProduct operator/(const ShortArrayBase& a,z_real_t f) {
  return ShortArrayScalarProduct(a,1.0/f);
}

inline ShortArrayScalarProduct operator-(const ShortArrayBase& a) {
  return ShortArrayScalarProduct(a,-1);
}

// define inner product
z_real_t innerProd(const ShortArrayBase&,const ShortArrayBase&);

// compute norm of an array
z_real_t normL1(const ShortArrayBase&);
z_real_t normL2(const ShortArrayBase&);

// compute max absolute value
z_real_t normLInf(const ShortArrayBase&);

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif

