/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_DATA_SHORT_SQUARE_MATRIX_H
#define ZORGLIB_DATA_SHORT_SQUARE_MATRIX_H

// config
#include <matlib_macros.h>

// STL
#include <stdexcept>
#include <vector>
// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#include <data/ShortMatrix.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Exception thrown when attempting to factorize a singular matrix.
 */
class SingularMatrixException : public std::runtime_error {
  
 public:
  
  // default constructor
  SingularMatrixException(const std::string& msg = "singular matrix")
  : std::runtime_error(msg) {}
  
  // copy constructor
  SingularMatrixException(const SingularMatrixException& src)
  : std::runtime_error(src) {}
};


/**
 * Class for small, full, square matrices.
 */
class ShortSqrMatrix : virtual public ShortMatrix {

 protected:

  // pivot indices
  std::vector<unsigned int> piv;

  // symmetric factorization
  bool sym;

 public:

  // constructor
  ShortSqrMatrix(unsigned int m = 0) : ShortMatrix(m,m) {}
  
  // submatrix constructor
  ShortSqrMatrix(const ShortSqrMatrix& src,unsigned int m,unsigned int idx0 = 0) 
   : ShortMatrix(src,m,m,idx0,idx0) {}
  ShortSqrMatrix(const ShortMatrixBase& src,unsigned int m,unsigned int idx0 = 0)
   : ShortMatrix(src,m,m,idx0,idx0) {}
      
  // wrapper constructor (for the experienced user)
  ShortSqrMatrix(z_real_t* a,unsigned int m)
   : ShortMatrix(a,m,m) {}

  // copy constructor
  ShortSqrMatrix(const ShortSqrMatrix& src) 
   : ShortMatrix(src) {piv = src.piv; sym = src.sym;}
                         
  // destructor
  virtual ~ShortSqrMatrix() {}
                         
  // assignment operator
  ShortSqrMatrix& operator=(const ShortMatrix&);
  ShortSqrMatrix& operator=(const ShortMatrixBase&);
  ShortSqrMatrix& operator=(z_real_t);
  
  // size
  unsigned int size() const {return nRows();}
  
  // resize
  void resize(unsigned int m) {ShortMatrix::resize(m,m);}
  
  // wrap another square matrix
  void wrap(const ShortSqrMatrix& src,unsigned int m,unsigned int idx0 = 0) {
    ShortMatrix::wrap(src,m,m,idx0,idx0);
  }
  
  // wrap another matrix
  void wrap(const ShortMatrix& src,unsigned int m,unsigned int idx0,unsigned int jdx0) {
    ShortMatrix::wrap(src,m,m,idx0,jdx0);
  }

  // wrap C array (for the experienced user)
  void wrap(z_real_t* a,unsigned int m) {ShortMatrix::wrap(a,m,m);}

  // get identity matrix
  static ShortSqrMatrix identity(unsigned int);
  
  // invert (in place)
  void invert();

  // factorize (LU)
  void factorize(bool);

  // back-substitute
  void backsubstitute(ShortArray&,const ShortArrayBase&) const;
  
  // solve linear system
  void solve(ShortArray&,const ShortArrayBase&,bool=false);
  
  // solve a linear system known to be symmetric
  // (will be modified if not definite positive)
  bool symSolve(ShortArray&,const ShortArrayBase&,std::vector<bool>&,
                z_real_t = 0.0,z_real_t = 0.0);
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
