/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_EIGMAT_H
#define ZORGLIB_MATH_EIGMAT_H

// config
#include <matlib_macros.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Prototypes.
 */
int eigenLR3(const z_real_t[],z_real_t[],z_real_t[][3],z_real_t[][3]);
int eigenLR2(const z_real_t[],z_real_t[],z_real_t[][2],z_real_t[][2]);
int eigen3(const z_real_t[],z_real_t[],z_real_t[][3]);
int eigen2(const z_real_t[],z_real_t[],z_real_t[][2]);
int eigen(int,int,z_real_t*,z_real_t*,z_real_t*,z_real_t*,int*,z_real_t*);

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif

