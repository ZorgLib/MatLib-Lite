/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "TensorAlgebra.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for TensorAlgebra2D.
 */
static const int MAP1[2][2] = {{0,1},{1,2}};
static const int MAP2[2][2] = {{0,1},{2,3}};


// operations
void TensorAlgebra2D::RightCauchyGreen(const Tensor2D& F,SymTensor2D& C) {
  // C=F^t.F
  C[0] = F[0]*F[0]+F[2]*F[2];
  C[1] = F[0]*F[1]+F[2]*F[3];
  C[2] = F[1]*F[1]+F[3]*F[3];
  C[3] = F[4]*F[4];
}
void TensorAlgebra2D::RUDecomposition(const Tensor2D& F,Tensor2D& R,SymTensor2D& U) {
  // F=R.U
  z_real_t val1 = F[0]+F[3];
  z_real_t val2 = F[1]-F[2];
  z_real_t val = std::sqrt(val1*val1+val2*val2);
  val1 /= val;
  val2 /= val;
  R[0] =  val1; R[1] = val2;
  R[2] = -val2; R[3] = val1; R[4] = 1.e0;
  
  // U = R^-1.F
  U[0] = val1*F[0]-val2*F[2];
  U[1] = val1*F[1]-val2*F[3];
  U[2] = val2*F[1]+val1*F[3];
  U[3] = F[4];
}
void TensorAlgebra2D::CauchyToPK1(const SymTensor2D& sig,const Tensor2D& F,Tensor2D& P) {
  // P = sig.(J F^-t)
  Tensor2D Fc = F.cofactor();
  P[0] = sig[0]*Fc[0]+sig[1]*Fc[2];
  P[1] = sig[0]*Fc[1]+sig[1]*Fc[3];
  P[2] = sig[1]*Fc[0]+sig[2]*Fc[2];
  P[3] = sig[1]*Fc[1]+sig[2]*Fc[3];
  P[4] = sig[3]*Fc[4];
}
void TensorAlgebra2D::PK1ToCauchy(const Tensor2D& P,const Tensor2D& F,SymTensor2D& sig) {
  // sig = (1/J) P.F^t
  z_real_t J = F.determinant();
  z_real_t Jinv = 1.0/J;
  sig[0] = Jinv*(P[0]*F[0]+P[1]*F[1]);
  sig[1] = Jinv*(P[2]*F[0]+P[3]*F[1]);
  sig[2] = Jinv*(P[2]*F[2]+P[3]*F[3]);
  sig[3] = Jinv*P[4]*F[4];
}
void TensorAlgebra2D::CauchyToPK2(const SymTensor2D& sig,const Tensor2D& F,SymTensor2D& S) {
  // S = J F^-1.sig.F^-t
  z_real_t J;
  Tensor2D Finv = F.inverse(J);
  S = 0.0;
  unsigned int i,j,k,l,ij,ik;
  for (i=0; i < DIMENSION; i++) {
    for (j=0; j <= i; j++) {
      ij = MAP1[i][j];
      for (k=0; k < DIMENSION; k++) {
        ik = MAP2[i][k];
        for (l=0; l < DIMENSION; l++)
          S[ij] += Finv[ik]*sig[MAP1[k][l]]*Finv[MAP2[j][l]];
      }
      S[ij] *= J;
    }
  }
  S[3] = J*Finv[4]*S[3]*Finv[4];
}
void TensorAlgebra2D::PK2ToCauchy(const SymTensor2D& S,const Tensor2D& F,SymTensor2D& sig) {
  // sig = (1/J) F.S.F^t
  z_real_t J = F.determinant();
  z_real_t Jinv = 1.0/J;
  sig = 0.0;
  unsigned int i,j,k,l,ij,ik;
  for (i=0; i < DIMENSION; i++) {
    for (j=0; j <= i; j++) {
      ij = MAP1[i][j];
      for (k=0; k < DIMENSION; k++) {
        ik = MAP2[i][k];
        for (l=0; l < DIMENSION; l++)
          sig[ij] += F[ik]*S[MAP1[k][l]]*F[MAP2[j][l]];
      }
      sig[ij] *= Jinv;
    }
  }
  sig[3] = Jinv*F[4]*S[3]*F[4];
}
void TensorAlgebra2D::PK2ToPK1(const SymTensor2D& S,const Tensor2D& F,Tensor2D& P) {
  // P=F.S
  P[0] = F[0]*S[0]+F[1]*S[1];
  P[1] = F[0]*S[1]+F[1]*S[2];
  P[2] = F[2]*S[0]+F[3]*S[1];
  P[3] = F[2]*S[1]+F[3]*S[2];
  P[4] = F[4]*S[3];
}
void TensorAlgebra2D::PK1ToPK2(const Tensor2D& P,const Tensor2D& F,SymTensor2D& S) {
  // S=F^-1.P
  z_real_t J;
  Tensor2D Finv = F.inverse(J);
  S[0] = Finv[0]*P[0]+Finv[1]*P[2];
  S[1] = Finv[0]*P[1]+Finv[1]*P[3]; // = Finv[2]*P[0]+Finv[3]*P[2]
  S[2] = Finv[2]*P[1]+Finv[3]*P[3];
  S[3] = Finv[4]*P[4];
}
void TensorAlgebra2D::MaterialToLagrangian(const SymTensor4_2D& M,const SymTensor2D& S,
                                           const Tensor2D& F,Tensor4_2D& T) {
  // T_iJkL=F_iI.M_IJKL.F_kK+S_JL 1_ik
  T = 0.0;
  unsigned int i,j,k,l,m,n;
  for (i=0; i < DIMENSION; i++) {
    for (m=0; m < DIMENSION; m++) {
      unsigned int im = MAP2[i][m];
      for (j=0; j < DIMENSION; j++) {
        unsigned int ij = MAP2[i][j];
        unsigned int mj = MAP1[m][j];
        for (k=0; k < DIMENSION; k++)
          for (n=0; n < DIMENSION; n++) {
            unsigned int kn = MAP2[k][n];
            for (l=0; l < DIMENSION; l++)
              T[ij][MAP2[k][l]] += F[im]*M[mj][MAP1[n][l]]*F[kn];
          }
        T[ij][4] += F[im]*M[mj][3]*F[4];
        T[4][ij] += F[4]*M[3][mj]*F[im];
      }
    }
    for (j=0; j < DIMENSION; j++) {
      unsigned int ij = MAP2[i][j];
      for (l=0; l < DIMENSION; l++) T[ij][MAP2[i][l]] += S[MAP1[j][l]];
    }
  }
  T[4][4] = F[4]*M[3][3]*F[4]+S[3];
}
void TensorAlgebra2D::SpatialToLagrangian(const SymTensor4_2D& M,const SymTensor2D& sig,
                                          const Tensor2D& F,Tensor4_2D& T) {
  // compute PK2
  z_real_t J;
  Tensor2D Finv = F.inverse(J);
  SymTensor2D S;
  S = 0.0;
  unsigned int i,j,k,l;
  for (i=0; i < DIMENSION; i++) {
    for (j=0; j <= i; j++) {
      unsigned int ij = MAP1[i][j];
      for (k=0; k < DIMENSION; k++) {
        unsigned int ik = MAP2[i][k];
        for (l=0; l < DIMENSION; l++)
          S[ij] += J*Finv[ik]*sig[MAP1[k][l]]*Finv[MAP2[j][l]];
      }
    }
  }
  S[3] = Finv[4]*sig[3]*Finv[4];

  // T_iJkL=J F^-1_Jj.(M_ijkl+sig_jl 1_ik).F^-1_Ll
  T = 0.e0;
  unsigned int m,n;
  for (i=0; i < DIMENSION; i++) {
    for (m=0; m < DIMENSION; m++) {
      unsigned int im = MAP1[i][m];
      for (j=0; j < DIMENSION; j++) {
        unsigned int ij = MAP2[i][j];
        unsigned int jm = MAP2[j][m];
        for (k=0; k < DIMENSION; k++)
          for (n=0; n < DIMENSION; n++) {
            unsigned int kn = MAP1[k][n];
            for (l=0; l < DIMENSION; l++)
              T[ij][MAP2[k][l]] += J*Finv[jm]*M[im][kn]*Finv[MAP2[l][n]];
          }
        T[ij][4] += J*Finv[jm]*M[im][3]*Finv[4];
        T[4][ij] += J*Finv[4]*M[3][im]*Finv[jm];
      }
    }
    for (j=0; j < DIMENSION; j++) {
      unsigned int ij = MAP2[i][j];
      for (l=0; l < DIMENSION; l++) T[ij][MAP2[i][l]] += S[MAP1[j][l]];
    }
  }
  T[4][4] = J*Finv[4]*M[3][3]*Finv[4]+S[3];
}
