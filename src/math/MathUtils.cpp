/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "MathUtils.h"

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif

// std C library
#include <cmath>

/**
 * Vector functions.
 */

// addition
void MATLIB_NAMESPACE addvec(const z_real_t a[],const z_real_t b[],
                             z_real_t c[],unsigned int n) {
  for (unsigned int i=0; i < n; i++, a++, b++, c++) (*c) = (*a)+(*b);
}

// multiplication by a scalar
void MATLIB_NAMESPACE mulvec(z_real_t a,const z_real_t b[],z_real_t c[],unsigned int n) {
  for (unsigned int i=0; i < n; i++, b++, c++) (*c) = a*(*b);
}

// norm LInf
z_real_t MATLIB_NAMESPACE nrmvec0(const z_real_t a[],unsigned int n) {
  z_real_t norm = 0.0;
  for (unsigned int i=0; i < n; i++, a++) {
    z_real_t test = std::fabs(*a);
    norm = (test > norm) ? test:norm;
  }
  return norm;
}

// norm L1
z_real_t MATLIB_NAMESPACE nrmvec1(const z_real_t a[],unsigned int n) {
  z_real_t norm = 0.0;
  for (unsigned int i=0; i < n; i++, a++) norm += std::fabs(*a);
  return norm;
}

// norm L2
z_real_t MATLIB_NAMESPACE nrmvec2(const z_real_t a[],unsigned int n) {
  z_real_t norm = 0.0;
  for (unsigned int i=0; i < n; i++, a++) norm += (*a)*(*a);
  return std::sqrt(norm);
}


/**
 * 2x2 matrix functions
 */

// matrix multiplication
void MATLIB_NAMESPACE mulmat2(const z_real_t *a,const z_real_t *b,z_real_t *c) {
  c[0] = a[0]*b[0]+a[1]*b[2];
  c[1] = a[0]*b[1]+a[1]*b[3];
  c[2] = a[2]*b[0]+a[3]*b[2];
  c[3] = a[2]*b[1]+a[3]*b[3];
}

// matrix determinant
z_real_t MATLIB_NAMESPACE detmat2(const z_real_t *a) {
  return a[0]*a[3]-a[1]*a[2];
}

// matrix inverse
z_real_t MATLIB_NAMESPACE invmat2(const z_real_t *a,z_real_t *ainv,z_real_t prec) {
  z_real_t det = detmat2(a);
  if (std::fabs(det) < prec) return 0.0;
  z_real_t detinv = 1.0/det;
  ainv[0] =  a[3]*detinv;
  ainv[1] = -a[1]*detinv;
  ainv[2] = -a[2]*detinv;
  ainv[3] =  a[0]*detinv;
  return det;
}

// matrix transposed inverse
z_real_t MATLIB_NAMESPACE tnvmat2(const z_real_t *a,z_real_t *ainv,z_real_t prec) {
  z_real_t det = detmat2(a);
  if (std::fabs(det) < prec) return 0.0;
  z_real_t detinv = 1.0/det;
  ainv[0] =  a[3]*detinv;
  ainv[1] = -a[2]*detinv;
  ainv[2] = -a[1]*detinv;
  ainv[3] =  a[0]*detinv;
  return det;
}

// symmetric matrix determinant
z_real_t MATLIB_NAMESPACE detsym2(const z_real_t *a) {
  return a[0]*a[2]-a[1]*a[1];
}

// symmetric matrix inverse
z_real_t MATLIB_NAMESPACE invsym2(const z_real_t *a,z_real_t *ainv,z_real_t prec) {
  z_real_t det = detsym2(a);
  if (std::fabs(det) < prec) return 0.0;
  z_real_t detinv = 1.0/det;
  ainv[0] =  a[2]*detinv;
  ainv[1] = -a[1]*detinv;
  ainv[2] =  a[0]*detinv;
  return det;
}


/**
 * 3x3 matrix functions
 */

// matrix multiplication
void MATLIB_NAMESPACE mulmat3(const z_real_t *a,const z_real_t *b,z_real_t *c) {
  c[0] = a[0]*b[0]+a[1]*b[3]+a[2]*b[6];
  c[1] = a[0]*b[1]+a[1]*b[4]+a[2]*b[7];
  c[2] = a[0]*b[2]+a[1]*b[5]+a[2]*b[8];
  c[3] = a[3]*b[0]+a[4]*b[3]+a[5]*b[6];
  c[4] = a[3]*b[1]+a[4]*b[4]+a[5]*b[7];
  c[5] = a[3]*b[2]+a[4]*b[5]+a[5]*b[8];
  c[6] = a[6]*b[0]+a[7]*b[3]+a[8]*b[6];
  c[7] = a[6]*b[1]+a[7]*b[4]+a[8]*b[7];
  c[8] = a[6]*b[2]+a[7]*b[5]+a[8]*b[8];
}

// matrix determinant
z_real_t MATLIB_NAMESPACE detmat3(const z_real_t *a) {
  return a[0]*(a[4]*a[8]-a[5]*a[7])
        -a[1]*(a[3]*a[8]-a[5]*a[6])
        +a[2]*(a[3]*a[7]-a[4]*a[6]);
}

// matrix inverse
z_real_t MATLIB_NAMESPACE invmat3(const z_real_t *a,z_real_t *ainv,z_real_t prec) {
  z_real_t det = detmat3(a);
  if (std::fabs(det) < prec) return 0.0;
  z_real_t detinv = 1.0/det;
  ainv[0] =  (a[4]*a[8]-a[5]*a[7])*detinv;
  ainv[1] = -(a[1]*a[8]-a[2]*a[7])*detinv;
  ainv[2] =  (a[1]*a[5]-a[2]*a[4])*detinv;
  ainv[3] = -(a[3]*a[8]-a[5]*a[6])*detinv;
  ainv[4] =  (a[0]*a[8]-a[2]*a[6])*detinv;
  ainv[5] = -(a[0]*a[5]-a[2]*a[3])*detinv;
  ainv[6] =  (a[3]*a[7]-a[4]*a[6])*detinv;
  ainv[7] = -(a[0]*a[7]-a[1]*a[6])*detinv;
  ainv[8] =  (a[0]*a[4]-a[1]*a[3])*detinv;
  return det;
}

// matrix transposed inverse
z_real_t MATLIB_NAMESPACE tnvmat3(const z_real_t *a,z_real_t *ainv,z_real_t prec) {
  z_real_t det = detmat3(a);
  if (std::fabs(det) < prec) return 0.0;
  z_real_t detinv = 1.0/det;
  ainv[0] =  (a[4]*a[8]-a[5]*a[7])*detinv;
  ainv[1] = -(a[3]*a[8]-a[5]*a[6])*detinv;
  ainv[2] =  (a[3]*a[7]-a[4]*a[6])*detinv;
  ainv[3] = -(a[1]*a[8]-a[2]*a[7])*detinv;
  ainv[4] =  (a[0]*a[8]-a[2]*a[6])*detinv;
  ainv[5] = -(a[0]*a[7]-a[1]*a[6])*detinv;
  ainv[6] =  (a[1]*a[5]-a[2]*a[4])*detinv;
  ainv[7] = -(a[0]*a[5]-a[2]*a[3])*detinv;
  ainv[8] =  (a[0]*a[4]-a[1]*a[3])*detinv;
  return det;
}

// symmetric matrix determinant
z_real_t MATLIB_NAMESPACE detsym3(const z_real_t *a) {
  return a[0]*(a[2]*a[5]-a[4]*a[4])
        -a[1]*(a[1]*a[5]-a[3]*a[4])
        +a[3]*(a[1]*a[4]-a[2]*a[3]);
}

// matrix inverse
z_real_t MATLIB_NAMESPACE invsym3(const z_real_t *a,z_real_t *ainv,z_real_t prec) {
  z_real_t det = detsym3(a);
  if (std::fabs(det) < prec) return 0.0;
  z_real_t detinv = 1.0/det;
  ainv[0] =  (a[2]*a[5]-a[4]*a[4])*detinv;
  ainv[1] = -(a[1]*a[5]-a[3]*a[4])*detinv;
  ainv[2] =  (a[0]*a[5]-a[3]*a[3])*detinv;
  ainv[3] =  (a[1]*a[4]-a[2]*a[3])*detinv;
  ainv[4] = -(a[0]*a[4]-a[1]*a[3])*detinv;
  ainv[5] =  (a[0]*a[2]-a[1]*a[1])*detinv;
  return det;
}

#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

// templated functions
template<>
z_real_t determinant<1>(const ShortArray& a) {
  return a[0]*a[1]*a[2];
}
template<>
z_real_t determinant<2>(const ShortArray& a) {
  return (a[0]*a[3]-a[1]*a[2])*a[4];
}
template<>
z_real_t determinant<3>(const ShortArray& a) {
  return a[0]*(a[4]*a[8]-a[5]*a[7])
        -a[1]*(a[3]*a[8]-a[5]*a[6])
        +a[2]*(a[3]*a[7]-a[4]*a[6]);
}

template<>
void eigenSym<1>(const ShortArray& a,z_real_t v[]) {
  v[0] = a[0];
  v[1] = a[1];
  v[2] = a[2];
}
template<>
void eigenSym<2>(const ShortArray& a,z_real_t v[]) {
  z_real_t tr = a[0]+a[2];
  z_real_t det = a[0]*a[2]-a[1]*a[1];
  z_real_t val = std::sqrt(tr*tr-4*det);
  v[0] = 0.5*(tr-val);
  v[1] = 0.5*(tr+val);
  v[2] = a[3];
}
template<>
void eigenSym<3>(const ShortArray& a,z_real_t v[]) {
  static const z_real_t PI = 4*std::atan(1.0);
  z_real_t val = a[1]*a[1]+a[3]*a[3]+a[4];
  // diagonal matrix
  if (val < DEFAULT_MATH_PRECISION) {
    v[0] = a[0];
    v[1] = a[2];
    v[2] = a[5];
  }
  // general symmetric matrix
  else {
    z_real_t q = (a[0]+a[2]+a[5])/3;
    ShortArray b(a);
    b[0] -= q;
    b[2] -= q;
    b[5] -= q;
    z_real_t p = std::sqrt((b[0]*b[0]+b[2]*b[2]+b[5]*b[5]+2*val)/6);
    b /= p;
    z_real_t r = 0.5*detsym3(&b[0]);
    z_real_t phi;
    if (r <= -1.0)
      phi = PI/3;
    else if (r >= 1.0)
      phi = 0.0;
    else
      phi = std::acos(r)/3;
    v[0] = q+2*p*std::cos(phi);
    v[2] = q+2*p*std::cos(phi+2*PI/3);
    v[1] = 3*q-v[0]-v[2];
  }
}

template<>
z_real_t innerProd<1>(const ShortArray& a,const ShortArray& b) {
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}
template<>
z_real_t innerProd<2>(const ShortArray& a,const ShortArray& b) {
  return a[0]*b[0]+a[1]*b[1]
        +a[2]*b[2]+a[3]*b[3]
        +a[4]*b[4];
}
template<>
z_real_t innerProd<3>(const ShortArray& a,const ShortArray& b) {
  return a[0]*b[0]+a[1]*b[1]+a[2]*b[2]
        +a[3]*b[3]+a[4]*b[4]+a[5]*b[5]
        +a[6]*b[6]+a[7]*b[7]+a[8]*b[8];
}

template <>
z_real_t invert<1>(const ShortArray& a,ShortArray& aInv) {
  z_real_t d = a[0]*a[1]*a[2];
  aInv[0] = 1.0/a[0];
  aInv[1] = 1.0/a[1];
  aInv[2] = 1.0/a[2];
  return d;
}
template <>
z_real_t invert<2>(const ShortArray& a,ShortArray& aInv) {
  z_real_t d = (a[0]*a[3]-a[1]*a[2]);
  z_real_t di = 1.0/d;
  aInv[0] =  a[3]*di;
  aInv[1] = -a[1]*di;
  aInv[2] = -a[2]*di;
  aInv[3] =  a[0]*di;
  aInv[4] = 1.0/a[4];
  return d*a[4];
}
template <>
z_real_t invert<3>(const ShortArray& a,ShortArray& aInv) {
  z_real_t d = a[0]*(a[4]*a[8]-a[5]*a[7])
              -a[1]*(a[3]*a[8]-a[5]*a[6])
              +a[2]*(a[3]*a[7]-a[4]*a[6]);
  z_real_t di = 1.0/d;
  aInv[0] =  (a[4]*a[8]-a[5]*a[7])*di;
  aInv[1] = -(a[1]*a[8]-a[2]*a[7])*di;
  aInv[2] =  (a[1]*a[5]-a[2]*a[4])*di;
  aInv[3] = -(a[3]*a[8]-a[5]*a[6])*di;
  aInv[4] =  (a[0]*a[8]-a[2]*a[6])*di;
  aInv[5] = -(a[0]*a[5]-a[2]*a[3])*di;
  aInv[6] =  (a[3]*a[7]-a[4]*a[6])*di;
  aInv[7] = -(a[0]*a[7]-a[1]*a[6])*di;
  aInv[8] =  (a[0]*a[4]-a[1]*a[3])*di;
  return d;
}

template <>
void transpose<1>(const ShortArray& a,ShortArray& aT) {
  aT[0] = a[0]; aT[1] = a[1]; aT[2] = a[2];
}
template <>
void transpose<2>(const ShortArray& a,ShortArray& aT) {
  aT[0] = a[0]; aT[1] = a[2];
  aT[2] = a[1]; aT[3] = a[3];
  aT[4] = a[4];
}
template <>
void transpose<3>(const ShortArray& a,ShortArray& aT) {
  aT[0] = a[0]; aT[1] = a[3]; aT[2] = a[6];
  aT[3] = a[1]; aT[4] = a[4]; aT[5] = a[7];
  aT[6] = a[2]; aT[7] = a[5]; aT[8] = a[8];
}

template<>
ShortArray identSym<1>() {
  ShortArray I(3);
  I[0] = 1.0; I[1] = 1.0; I[2] = 1.0;
  return I;
}
template<>
ShortArray identSym<2>() {
  ShortArray I(4);
  I[0] = 1.0;
  I[1] = 0.0; I[2] = 1.0;
  I[3] = 1.0;
  return I;
}
template<>
ShortArray identSym<3>() {
  ShortArray I(6);
  I[0] = 1.0;
  I[1] = 0.0; I[2] = 1.0;
  I[3] = 0.0; I[4] = 0.0; I[5] = 1.0;
  return I;
}

template<>
ShortArray identity<1>() {
  ShortArray I(3);
  I[0] = 1.0; I[1] = 1.0; I[2] = 1.0;
  return I;
}
template<>
ShortArray identity<2>() {
  ShortArray I(5);
  I[0] = 1.0; I[1] = 0.0;
  I[2] = 0.0; I[3] = 1.0;
  I[4] = 1.0;
  return I;
}
template<>
ShortArray identity<3>() {
  ShortArray I(9);
  I[0] = 1.0; I[1] = 0.0; I[2] = 0.0;
  I[3] = 0.0; I[4] = 1.0; I[5] = 0.0;
  I[6] = 0.0; I[7] = 0.0; I[8] = 1.0;
  return I;
}

template<>
z_real_t trSym<1>(const ShortArray& a) {
  return a[0]+a[1]+a[2];
}
template<>
z_real_t trSym<2>(const ShortArray& a) {
  return a[0]+a[2]+a[3];
}
template<>
z_real_t trSym<3>(const ShortArray& a) {
  return a[0]+a[2]+a[5];
}

template<>
z_real_t trace<1>(const ShortArray& a) {
  return a[0]+a[1]+a[2];
}
template<>
z_real_t trace<2>(const ShortArray& a) {
  return a[0]+a[3]+a[4];
}
template<>
z_real_t trace<3>(const ShortArray& a) {
  return a[0]+a[4]+a[8];
}

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif
