/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_UTILS_H
#define ZORGLIB_MATH_UTILS_H

// config
#include <matlib_macros.h>

// std C library
#include <stddef.h>
//std C++ library
#include <limits>
// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

// default precision for matrix inversion
static const z_real_t DEFAULT_MATH_PRECISION = 1.0e-16;
//static const z_real_t DEFAULT_MATH_PRECISION = std::numeric_limits< z_real_t >::min();

/*
 * Prototypes
 */

// vector functions
void addvec(const z_real_t[],const z_real_t[],z_real_t[],unsigned int);
void mulvec(z_real_t,const z_real_t[],z_real_t[],unsigned int);
z_real_t nrmvec0(const z_real_t[],unsigned int);
z_real_t nrmvec1(const z_real_t[],unsigned int);
z_real_t nrmvec2(const z_real_t[],unsigned int);

// 2x2 matrix functions
void mulmat2(const z_real_t *,const z_real_t *,z_real_t *);
z_real_t detmat2(const z_real_t *);
z_real_t invmat2(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t tnvmat2(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t detsym2(const z_real_t *);
z_real_t invsym2(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);

// 3x3 matrix functions
void mulmat3(const z_real_t *,const z_real_t *,z_real_t *);
z_real_t detmat3(const z_real_t *);
z_real_t invmat3(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t tnvmat3(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);
z_real_t detsym3(const z_real_t *);
z_real_t invsym3(const z_real_t *,z_real_t *,z_real_t = DEFAULT_MATH_PRECISION);

// templated functions
template<unsigned int DIM>
z_real_t determinant(const ShortArray&);
template<unsigned int DIM>
void eigenSym(const ShortArray&,z_real_t[]);
template<unsigned int DIM>
z_real_t innerProd(const ShortArray&,const ShortArray&);
template<unsigned int DIM>
z_real_t invert(const ShortArray&,ShortArray&);
template<unsigned int DIM>
void transpose(const ShortArray&,ShortArray&);

template<unsigned int DIM>
ShortArray identSym();
template<unsigned int DIM>
ShortArray identity();
template<unsigned int DIM>
z_real_t trSym(const ShortArray&);
template<unsigned int DIM>
z_real_t trace(const ShortArray&);

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
