/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "TensorAlgebra.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for TensorAlgebra3D.
 */
static const int MAP1[3][3] = {{0,1,3},{1,2,4},{3,4,5}};
static const int MAP2[3][3] = {{0,1,2},{3,4,5},{6,7,8}};

// operations
void TensorAlgebra3D::RightCauchyGreen(const Tensor3D& F,SymTensor3D& C) {
  // C=F^t.F
  C[0] = F[0]*F[0]+F[3]*F[3]+F[6]*F[6];
  C[1] = F[0]*F[1]+F[3]*F[4]+F[6]*F[7];
  C[2] = F[1]*F[1]+F[4]*F[4]+F[7]*F[7];
  C[3] = F[0]*F[2]+F[3]*F[5]+F[6]*F[8];
  C[4] = F[1]*F[2]+F[4]*F[5]+F[7]*F[8];
  C[5] = F[2]*F[2]+F[5]*F[5]+F[8]*F[8];
}
void TensorAlgebra3D::RUDecomposition(const Tensor3D& F,Tensor3D& R,SymTensor3D& U) {
  // compute C and its invariants
  SymTensor3D C,C2;
  RightCauchyGreen(F,C);
  C2[0] = C[0]*C[0]+C[1]*C[1]+C[3]*C[3];
  C2[1] = C[1]*C[0]+C[2]*C[1]+C[4]*C[3];
  C2[2] = C[1]*C[1]+C[2]*C[2]+C[4]*C[4];
  C2[3] = C[3]*C[0]+C[4]*C[1]+C[5]*C[3];
  C2[4] = C[3]*C[1]+C[4]*C[2]+C[5]*C[4];
  C2[5] = C[3]*C[3]+C[4]*C[4]+C[5]*C[5];
  z_real_t I1 = C.trace();
  z_real_t I2 = 0.5*(I1*I1-C2.trace());
  z_real_t I3 = C.determinant();
  
  // compute principal stretches
  static const z_real_t PI = 4*std::atan(1.0);
  static const z_real_t THIRD = 1.0/3.0;
  z_real_t x1,x2,x3;
  z_real_t b = I2-I1*I1*THIRD;
  z_real_t c = -2.*I1*I1*I1/27.+I1*I2*THIRD-I3;
  if (std::fabs(b) < 1.e-06*I2) {
    if (c >= 0.0)
      x1 = x2 = x3 = -std::pow(c,THIRD);
    else
      x1 = x2 = x3 = std::pow(-c,THIRD);
  }
  else {
    z_real_t d = 2*std::sqrt(-b*THIRD);
    z_real_t e = 3*c/(d*b);
    z_real_t val = 1.0-e*e;
    if (std::fabs(val) < 1.e-06) val = 0.0;
    z_real_t t = std::atan2(std::sqrt(val),e)*THIRD;
    x1 = d*std::cos(t);
    x2 = d*std::cos(t+2*THIRD*PI);
    x3 = d*std::cos(t+4*THIRD*PI);
  }
  z_real_t l1 = std::sqrt(x1+I1*THIRD);
  z_real_t l2 = std::sqrt(x2+I1*THIRD);
  z_real_t l3 = std::sqrt(x3+I1*THIRD);
  
  // compute U and U^-1
  SymTensor3D Uinv;
  z_real_t i1 = l1+l2+l3;
  z_real_t i2 = l1*l2+l1*l3+l2*l3;
  z_real_t i3 = l1*l2*l3;
  z_real_t D = i1*i2-i3;
  U = (1.0/D)*(-C2+(i1*i1-i2)*C+(i1*i3)*SymTensor3D::identity());
  Uinv = (1.0/i3)*(C-i1*U+i2*SymTensor3D::identity());
  
  // compute R
  R[0] = F[0]*Uinv[0]+F[1]*Uinv[1]+F[2]*Uinv[3];
  R[1] = F[0]*Uinv[1]+F[1]*Uinv[2]+F[2]*Uinv[4];
  R[2] = F[0]*Uinv[3]+F[1]*Uinv[4]+F[2]*Uinv[5];
  R[3] = F[3]*Uinv[0]+F[4]*Uinv[1]+F[5]*Uinv[3];
  R[4] = F[3]*Uinv[1]+F[4]*Uinv[2]+F[5]*Uinv[4];
  R[5] = F[3]*Uinv[3]+F[4]*Uinv[4]+F[5]*Uinv[5];
  R[6] = F[6]*Uinv[0]+F[7]*Uinv[1]+F[8]*Uinv[3];
  R[7] = F[6]*Uinv[1]+F[7]*Uinv[2]+F[8]*Uinv[4];
  R[8] = F[6]*Uinv[3]+F[7]*Uinv[4]+F[8]*Uinv[5];
}
void TensorAlgebra3D::CauchyToPK1(const SymTensor3D& sig,const Tensor3D& F,Tensor3D& P) {
  // P = sig.(J F^-t)
  Tensor3D Fc = F.cofactor();
  P[0] = sig[0]*Fc[0]+sig[1]*Fc[3]+sig[3]*Fc[6];
  P[1] = sig[0]*Fc[1]+sig[1]*Fc[4]+sig[3]*Fc[7];
  P[2] = sig[0]*Fc[2]+sig[1]*Fc[5]+sig[3]*Fc[8];
  P[3] = sig[1]*Fc[0]+sig[2]*Fc[3]+sig[4]*Fc[6];
  P[4] = sig[1]*Fc[1]+sig[2]*Fc[4]+sig[4]*Fc[7];
  P[5] = sig[1]*Fc[2]+sig[2]*Fc[5]+sig[4]*Fc[8];
  P[6] = sig[3]*Fc[0]+sig[4]*Fc[3]+sig[5]*Fc[6];
  P[7] = sig[3]*Fc[1]+sig[4]*Fc[4]+sig[5]*Fc[7];
  P[8] = sig[3]*Fc[2]+sig[4]*Fc[5]+sig[5]*Fc[8];
}
void TensorAlgebra3D::PK1ToCauchy(const Tensor3D& P,const Tensor3D& F,SymTensor3D& sig) {
  // sig = (1/J) P.F^t
  z_real_t J = F.determinant();
  z_real_t Jinv = 1.0/J;
  sig[0] = Jinv*(P[0]*F[0]+P[1]*F[1]+P[2]*F[2]);
  sig[1] = Jinv*(P[3]*F[0]+P[4]*F[1]+P[5]*F[2]);
  sig[2] = Jinv*(P[3]*F[3]+P[4]*F[4]+P[5]*F[5]);
  sig[3] = Jinv*(P[6]*F[0]+P[7]*F[1]+P[8]*F[2]);
  sig[4] = Jinv*(P[6]*F[3]+P[7]*F[4]+P[8]*F[5]);
  sig[5] = Jinv*(P[6]*F[6]+P[7]*F[7]+P[8]*F[8]);
}
void TensorAlgebra3D::CauchyToPK2(const SymTensor3D& sig,const Tensor3D& F,SymTensor3D& S) {
  // S = (1/J) F^-1.sig.F^-t
  z_real_t J;
  Tensor3D Finv = F.inverse(J);
  S = 0.0;
  unsigned int i,j,k,l,ij,ik;
  for (i=0; i < DIMENSION; i++) {
    for (j=0; j <= i; j++) {
      ij = MAP1[i][j];
      for (k=0; k < DIMENSION; k++) {
        ik = MAP2[i][k];
        for (l=0; l < DIMENSION; l++)
          S[ij] += Finv[ik]*sig[MAP1[k][l]]*Finv[MAP2[j][l]];
      }
      S[ij] *= J;
    }
  }
}
void TensorAlgebra3D::PK2ToCauchy(const SymTensor3D& S,const Tensor3D& F,SymTensor3D& sig) {
  // sig = (1/J) F.S.F^t
  z_real_t J = F.determinant();
  z_real_t Jinv = 1.0/J;
  sig = 0.0;
  unsigned int i,j,k,l,ij,ik;
  for (i=0; i < DIMENSION; i++) {
    for (j=0; j <= i; j++) {
      ij = MAP1[i][j];
      for (k=0; k < DIMENSION; k++) {
        ik = MAP2[i][k];
        for (l=0; l < DIMENSION; l++)
          sig[ij] += F[ik]*S[MAP1[k][l]]*F[MAP2[j][l]];
      }
      sig[ij] *= Jinv;
    }
  }
}
void TensorAlgebra3D::PK2ToPK1(const SymTensor3D& S,const Tensor3D& F,Tensor3D& P) {
  // P=F.S
  P[0] = F[0]*S[0]+F[1]*S[1]+F[2]*S[3];
  P[1] = F[0]*S[1]+F[1]*S[2]+F[2]*S[4];
  P[2] = F[0]*S[3]+F[1]*S[4]+F[2]*S[5];
  P[3] = F[3]*S[0]+F[4]*S[1]+F[5]*S[3];
  P[4] = F[3]*S[1]+F[4]*S[2]+F[5]*S[4];
  P[5] = F[3]*S[3]+F[4]*S[4]+F[5]*S[5];
  P[6] = F[6]*S[0]+F[7]*S[1]+F[8]*S[3];
  P[7] = F[6]*S[1]+F[7]*S[2]+F[8]*S[4];
  P[8] = F[6]*S[3]+F[7]*S[4]+F[8]*S[5];
}
void TensorAlgebra3D::PK1ToPK2(const Tensor3D& P,const Tensor3D& F,SymTensor3D& S) {
  // S=F^-1.P
  z_real_t J;
  Tensor3D Finv = F.inverse(J);
  S[0] = Finv[0]*P[0]+Finv[1]*P[3]+Finv[2]*P[6];
  S[1] = Finv[3]*P[0]+Finv[4]*P[3]+Finv[5]*P[6];
  S[2] = Finv[3]*P[1]+Finv[4]*P[4]+Finv[5]*P[7];
  S[3] = Finv[6]*P[0]+Finv[7]*P[3]+Finv[8]*P[6];
  S[4] = Finv[6]*P[1]+Finv[7]*P[4]+Finv[8]*P[7];
  S[5] = Finv[6]*P[2]+Finv[7]*P[5]+Finv[8]*P[8];
}
void TensorAlgebra3D::MaterialToLagrangian(const SymTensor4_3D& M,const SymTensor3D& S,
                                           const Tensor3D& F,Tensor4_3D& T) {
  // T_iJkL=F_iI.M_IJKL.F_kK+S_JL 1_ik
  T = 0.0;
  unsigned int i,j,k,l,m,n;
  for (i=0; i < DIMENSION; i++) {
    for (m=0; m < DIMENSION; m++) {
      unsigned int im = MAP2[i][m];
      for (j=0; j < DIMENSION; j++) {
        unsigned int ij = MAP2[i][j];
        unsigned int mj = MAP1[m][j];
        for (k=0; k < DIMENSION; k++)
          for (n=0; n < DIMENSION; n++) {
            unsigned int kn = MAP2[k][n];
            for (l=0; l < DIMENSION; l++)
              T[ij][MAP2[k][l]] += F[im]*M[mj][MAP1[n][l]]*F[kn];
          }
      }
    }
    for (j=0; j < DIMENSION; j++) {
      unsigned int ij = MAP2[i][j];
      for (l=0; l < DIMENSION; l++) T[ij][MAP2[i][l]] += S[MAP1[j][l]];
    }
  }
}
void TensorAlgebra3D::SpatialToLagrangian(const SymTensor4_3D& M,const SymTensor3D& sig,
                                          const Tensor3D& F,Tensor4_3D& T) {
  // compute PK2
  z_real_t J;
  Tensor3D Finv = F.inverse(J);
  SymTensor3D S;
  S = 0.0;
  unsigned int i,j,k,l;
  for (i=0; i < DIMENSION; i++) {
    for (j=0; j <= i; j++) {
      unsigned int ij = MAP1[i][j];
      for (k=0; k < DIMENSION; k++) {
        unsigned int ik = MAP2[i][k];
        for (l=0; l < DIMENSION; l++)
          S[ij] += J*Finv[ik]*sig[MAP1[k][l]]*Finv[MAP2[j][l]];
      }
    }
  }
  
  // T_iJkL=J F^-1_Jj.(M_ijkl+sig_jl 1_ik).F^-1_Ll
  T = 0.0;
  unsigned int m,n;
  for (i=0; i < DIMENSION; i++) {
    for (m=0; m < DIMENSION; m++) {
      unsigned int im = MAP1[i][m];
      for (j=0; j < DIMENSION; j++) {
        unsigned int ij = MAP2[i][j];
        unsigned int jm = MAP2[j][m];
        for (k=0; k < DIMENSION; k++)
          for (n=0; n < DIMENSION; n++) {
            unsigned int kn = MAP1[k][n];
            for (l=0; l < DIMENSION; l++)
              T[ij][MAP2[k][l]] += J*Finv[jm]*M[im][kn]*Finv[MAP2[l][n]];
          }
      }
    }
    for (j=0; j < DIMENSION; j++) {
      unsigned int ij = MAP2[i][j];
      for (l=0; l < DIMENSION; l++) T[ij][MAP2[i][l]] += S[MAP1[j][l]];
    }
  }
}
