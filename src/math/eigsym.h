/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_EIGSYM_H
#define ZORGLIB_MATH_EIGSYM_H

// config
#include <matlib_macros.h>


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/*
 * Prototypes.
 */
int eigsym3(const z_real_t[],z_real_t[],z_real_t[][3]);
int eigsym2(const z_real_t[],z_real_t[],z_real_t[][2]);
int jacobi(int,int,z_real_t*,z_real_t*,z_real_t*,z_real_t*,z_real_t*);

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif

