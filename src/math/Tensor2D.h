/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_TENSOR_2D_H
#define ZORGLIB_MATH_TENSOR_2D_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#include <data/ShortSqrMatrix.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

// forward declaration
#ifndef WITH_MATLIB_MATH_H
class SymTensor2D;
class Vector3D;
#endif

/**
 * Class encapsulating 2D tensor objects.
 * It inherits methods and internal structure from ShortArray.
 */
class Tensor2D : virtual public ShortArray {
  
 public:
  
  static const unsigned int MEMSIZE=5;
  
  // index map
  static const int MAP[3][3];
  
  // default constructor
  Tensor2D() : ShortArray(MEMSIZE) {}
  
  // constructor (also serves as copy constructor)
  Tensor2D(const ShortArray& a,unsigned int idx0 = 0) : ShortArray(a,MEMSIZE,idx0) {}
  
  // constructor
  Tensor2D(const ShortArrayBase& a,unsigned int idx0 = 0)
	: ShortArray(a,MEMSIZE,idx0) {}
  
  // destructor
  virtual ~Tensor2D() {}
  
  // assignment operator
  Tensor2D& operator=(const Tensor2D& src) {
    ShortArray::operator=(src);
    return *this;
  }
  Tensor2D& operator=(const ShortArray& src) {
    ShortArray::operator=(src);
    return *this;
  }
  Tensor2D& operator=(const ShortArrayBase& src) {
    ShortArray::operator=(src);
    return *this;
  }
  Tensor2D& operator=(z_real_t v) {
    ShortArray::operator=(v);
    return *this;
  }
  
  // specific arithmetic operators
  Tensor2D operator*(const Tensor2D&) const;
  Tensor2D operator*(const SymTensor2D&) const;
  Vector3D operator*(const Vector3D&) const;
  
  // symmetrize
  SymTensor2D covariantSym() const;
  SymTensor2D contravariantSym() const;
  
  // compute determinant
  z_real_t determinant() const;
  
  // compute trace
  z_real_t trace() const;

  // compute cofactor
  Tensor2D cofactor() const;

  // compute inverse
  Tensor2D inverse(z_real_t&) const;

  // compute transposed
  Tensor2D transposed() const;
  
  // compute exponential
  Tensor2D exp(Tensor2D[] = 0,Tensor2D[][MEMSIZE] = 0,
               bool = false,bool = false) const;
  
  // compute logarithm
  Tensor2D log(Tensor2D[] = 0,Tensor2D[][MEMSIZE] = 0,
               bool = false,bool = false) const;
  
  // identity tensor
  static Tensor2D identity();
  
  // export as square matrix
  ShortSqrMatrix toMatrix() const;
};

// compute determinant
inline
z_real_t determinant(const Tensor2D& A) {return A.determinant();}

// compute trace
inline
z_real_t trace(const Tensor2D& A) {return A.trace();}

// compute inverse
inline
Tensor2D invert(const Tensor2D& A) {z_real_t d; return A.inverse(d);}

// compute transposed
inline
Tensor2D transpose(const Tensor2D& A) {return A.transposed();}

//compute exponential
inline
Tensor2D exp(const Tensor2D& A,Tensor2D dExpA[] = 0,
             Tensor2D d2ExpA[][Tensor2D::MEMSIZE] = 0,
             bool first = false,bool second = false) {
  return A.exp(dExpA,d2ExpA,first,second);
}

//compute logarithm
inline
Tensor2D log(const Tensor2D& A,Tensor2D dLogA[] = 0,
             Tensor2D d2LogA[][Tensor2D::MEMSIZE] = 0,
             bool first = false,bool second = false) {
  return A.log(dLogA,d2LogA,first,second);
}

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif

