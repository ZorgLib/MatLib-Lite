/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_SYM_TENSOR4_3D_H
#define ZORGLIB_MATH_SYM_TENSOR4_3D_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/ShortSqrMatrix.h>
#endif
#ifndef WITH_MATLIB_MATH_H
#include <math/SymTensor3D.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class encapsulating 3D symmetric 4th-order tensor objects.
 * It inherits methods and internal structure from ShortSqrMatrix.
 */
class SymTensor4_3D : virtual public ShortSqrMatrix {
  
 public:
  
  // default constructor
  SymTensor4_3D() : ShortMatrix(SymTensor3D::MEMSIZE,SymTensor3D::MEMSIZE) {}
  
  // constructor (also serves as copy constructor)
  SymTensor4_3D(const ShortSqrMatrix& M,unsigned int idx0 = 0)
    : ShortMatrix(M,SymTensor3D::MEMSIZE,SymTensor3D::MEMSIZE,idx0,idx0) {}
  
  // constructor
  SymTensor4_3D(const ShortMatrixBase& M,unsigned int idx0 = 0)
    : ShortMatrix(M,SymTensor3D::MEMSIZE,SymTensor3D::MEMSIZE,idx0,idx0) {}

  // destructor
  virtual ~SymTensor4_3D() {}
  
  // assignment operator
  SymTensor4_3D& operator=(const SymTensor4_3D& src) {
    ShortSqrMatrix::operator=(src);
    return *this;
  }
  SymTensor4_3D& operator=(const ShortMatrix& src) {
    ShortSqrMatrix::operator=(src);
    return *this;
  }
  SymTensor4_3D& operator=(const ShortMatrixBase& src) {
    ShortSqrMatrix::operator=(src);
    return *this;
  }
  SymTensor4_3D& operator=(z_real_t v) {
    ShortSqrMatrix::operator=(v);
    return *this;
  }
  
  // access operators
  z_real_t operator()(unsigned int,unsigned int,unsigned int,unsigned int) const;
  z_real_t& operator()(unsigned int,unsigned int,unsigned int,unsigned int);
  
  // specific operation (+= coef*(A_ik*A_jl+A_il*A_jk))
  void addIJKL(z_real_t,const SymTensor3D&);
  
  // specific operation (+= coef*(A_ik*B_jl+A_il*B_jk))
  void addIJKL(z_real_t,const SymTensor3D&,const SymTensor3D&);

  // push-pull operations
  SymTensor4_3D covariantPush(const Tensor3D&) const;
  SymTensor4_3D covariantPull(const Tensor3D&) const;
  SymTensor4_3D contravariantPush(const Tensor3D&) const;
  SymTensor4_3D contravariantPull(const Tensor3D&) const;

  // identity tensor
  static SymTensor4_3D identity();
  static SymTensor4_3D contravariantIdentity();
  static SymTensor4_3D covariantIdentity();
  
  // tensorial bases
  static SymTensor4_3D baseJ();
  static SymTensor4_3D contravariantJ();
  static SymTensor4_3D covariantJ();
  static SymTensor4_3D baseK();
};

// full inner product
SymTensor3D innerProd2(const SymTensor4_3D&,const SymTensor3D&);
SymTensor4_3D innerProd2(const SymTensor4_3D&,const SymTensor4_3D&);

// symmetric part of product between symmetric 4th-order and 2nd-order tensors
SymTensor4_3D symProd(const SymTensor4_3D&,const SymTensor3D&);
SymTensor4_3D symProd(const SymTensor3D&,const SymTensor4_3D&);

// push-pull operations
inline 
SymTensor4_3D covariantPush(const SymTensor4_3D& A,const Tensor3D& B) {
  return A.covariantPush(B);
}
inline 
SymTensor4_3D covariantPull(const SymTensor4_3D& A,const Tensor3D& B) {
  return A.covariantPull(B);
}
inline 
SymTensor4_3D contravariantPush(const SymTensor4_3D& A,const Tensor3D& B) {
  return A.contravariantPush(B);
}
inline 
SymTensor4_3D contravariantPull(const SymTensor4_3D& A,const Tensor3D& B) {
  return A.contravariantPull(B);
}

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif

