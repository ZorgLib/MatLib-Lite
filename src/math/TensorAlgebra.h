/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_TENSOR_ALGEBRA_H
#define ZORGLIB_MATH_TENSOR_ALGEBRA_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#include <data/ShortSqrMatrix.h>
#endif
#ifndef WITH_MATLIB_MATH_H
#include <math/SymTensor3D.h>
#include <math/SymTensor2D.h>
#include <math/SymTensor1D.h>
#include <math/SymTensor4_3D.h>
#include <math/SymTensor4_2D.h>
#include <math/SymTensor4_1D.h>
#include <math/Tensor3D.h>
#include <math/Tensor2D.h>
#include <math/Tensor1D.h>
#include <math/Tensor4_3D.h>
#include <math/Tensor4_2D.h>
#include <math/Tensor4_1D.h>
#include <math/Vector3D.h>
#include <math/Vector2D.h>
#include <math/Vector1D.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class describing standard 3D Euclidean tensor algebra.
 */
struct StdTensorAlgebra3D {
  
  // constants
  static const unsigned int DIMENSION = 3;

  // types
  typedef SymTensor3D SymTensor;
  typedef Vector3D    Vector;
};

/**
 * Class describing standard 2D Euclidean tensor algebra.
 */
struct StdTensorAlgebra2D {
  
  // constants
  static const unsigned int DIMENSION = 2;
  
  // types
  typedef StdSymTensor2D SymTensor;
  typedef Vector2D       Vector;
};

/**
 * Class describing standard 2D Euclidean tensor algebra.
 */
struct StdTensorAlgebra1D {
  
  // constants
  static const unsigned int DIMENSION = 1;
  
  // types
  typedef StdSymTensor1D SymTensor;
  typedef StdSymTensor1D Tensor;
  typedef Vector1D Vector;
};

/**
 * Class describing 3D Euclidean tensor algebra for mechanics.
 */
struct TensorAlgebra3D {
  
  // useful shortcuts
  typedef ShortArray     ARRAY;
  typedef ShortSqrMatrix MATRIX;

  // constants
  static const unsigned int DIMENSION = 3;
  
  // types
  typedef SymTensor3D   SymTensor;
  typedef SymTensor4_3D SymTensor4;
  typedef Tensor3D      Tensor;
  typedef Tensor4_3D    Tensor4;
  typedef Vector3D      Vector;
  typedef Vector3D      LongVector;

  // operations
  static void RightCauchyGreen(const Tensor&,SymTensor&);
  static void RUDecomposition(const Tensor&,Tensor&,SymTensor&);
  static void CauchyToPK1(const SymTensor&,const Tensor&,Tensor&);
  static void PK1ToCauchy(const Tensor&,const Tensor&,SymTensor&);
  static void CauchyToPK2(const SymTensor&,const Tensor&,SymTensor&);
  static void PK2ToCauchy(const SymTensor&,const Tensor&,SymTensor&);
  static void PK2ToPK1(const SymTensor&,const Tensor&,Tensor&);
  static void PK1ToPK2(const Tensor&,const Tensor&,SymTensor&);
  static void MaterialToLagrangian(const SymTensor4&,const SymTensor&,
                                   const Tensor&,Tensor4&);
  static void SpatialToLagrangian(const SymTensor4&,const SymTensor&,
                                  const Tensor&,Tensor4&);
};

/**
 * Class describing 2D Euclidean tensor algebra for mechanics.
 */
struct TensorAlgebra2D {
  
 public:
  
  // useful shortcuts
  typedef ShortArray     ARRAY;
  typedef ShortSqrMatrix MATRIX;
  
  // constants
  static const unsigned int DIMENSION = 2;
  
  // types
  typedef SymTensor2D   SymTensor;
  typedef SymTensor4_2D SymTensor4;
  typedef Tensor2D      Tensor;
  typedef Tensor4_2D    Tensor4;
  typedef Vector2D      Vector;
  typedef Vector3D      LongVector;

  // operations
  static void RightCauchyGreen(const Tensor&,SymTensor&);
  static void RUDecomposition(const Tensor&,Tensor&,SymTensor&);
  static void CauchyToPK1(const SymTensor&,const Tensor&,Tensor&);
  static void PK1ToCauchy(const Tensor&,const Tensor&,SymTensor&);
  static void CauchyToPK2(const SymTensor&,const Tensor&,SymTensor&);
  static void PK2ToCauchy(const SymTensor&,const Tensor&,SymTensor&);
  static void PK2ToPK1(const SymTensor&,const Tensor&,Tensor&);
  static void PK1ToPK2(const Tensor&,const Tensor&,SymTensor&);
  static void MaterialToLagrangian(const SymTensor4&,const SymTensor&,
                                   const Tensor&,Tensor4&);
  static void SpatialToLagrangian(const SymTensor4&,const SymTensor&,
                                  const Tensor&,Tensor4&);
};

/**
 * Class describing 1D Euclidean tensor algebra for mechanics.
 */
struct TensorAlgebra1D {
  
 public:
  
  // useful shortcuts
  typedef ShortArray     ARRAY;
  typedef ShortSqrMatrix MATRIX;
  
  // constants
  static const unsigned int DIMENSION = 1;
  
  // types
  typedef SymTensor1D   SymTensor;
  typedef SymTensor4_1D SymTensor4;
  typedef Tensor1D      Tensor;
  typedef Tensor4_1D    Tensor4;
  typedef Vector1D      Vector;
  typedef Vector3D      LongVector;

  // operations
  static void RightCauchyGreen(const Tensor&,SymTensor&);
  static void RUDecomposition(const Tensor&,Tensor&,SymTensor&);
  static void CauchyToPK1(const SymTensor&,const Tensor&,Tensor&);
  static void PK1ToCauchy(const Tensor&,const Tensor&,SymTensor&);
  static void PK2ToCauchy(const SymTensor&,const Tensor&,SymTensor&);
  static void CauchyToPK2(const SymTensor&,const Tensor&,SymTensor&);
  static void PK2ToPK1(const SymTensor&,const Tensor&,Tensor&);
  static void PK1ToPK2(const Tensor&,const Tensor&,SymTensor&);
  static void MaterialToLagrangian(const SymTensor4&,const SymTensor&,
                                   const Tensor&,Tensor4&);
  static void SpatialToLagrangian(const SymTensor4&,const SymTensor&,
                                  const Tensor&,Tensor4&);
};

// symmetrize
inline
SymTensor1D covariantSym(const Tensor1D& A) {return A.covariant();}
inline
SymTensor1D contravariantSym(const Tensor1D& A) {return A.contravariant();}
inline
SymTensor2D covariantSym(const Tensor2D& A) {return A.covariantSym();}
inline
SymTensor2D contravariantSym(const Tensor2D& A) {return A.contravariantSym();}
inline
SymTensor3D covariantSym(const Tensor3D& A) {return A.covariantSym();}
inline
SymTensor3D contravariantSym(const Tensor3D& A) {return A.contravariantSym();}

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
