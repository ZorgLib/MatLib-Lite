/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_VECTOR_2D_H
#define ZORGLIB_MATH_VECTOR_2D_H

// config
#include <matlib_macros.h>

// local
#ifndef WITH_MATLIB_H
#include <data/ShortArray.h>
#endif


#ifdef MATLIB_USE_NAMESPACE
BEGIN_MATLIB_NAMESPACE
#endif

/**
 * Class encapsulating 2D vector objects.
 * It inherits methods and internal structure from ShortArray.
 */
class Vector2D : virtual public ShortArray {
  
 public:
  
  static const unsigned int MEMSIZE=2;
  
  // default constructor
  Vector2D() : ShortArray(MEMSIZE) {}
  
  // constructor (also serves as copy constructor)
  Vector2D(const ShortArray& a,unsigned int idx0 = 0) : ShortArray(a,MEMSIZE,idx0) {}
  Vector2D(const ShortArrayBase& a,unsigned int idx0 = 0) : ShortArray(a,MEMSIZE,idx0) {}
  
  // destructor
  virtual ~Vector2D() {}
  
  // assignment operator
  Vector2D& operator=(const Vector2D& src) {
    ShortArray::operator=(src);
    return *this;
  }
  Vector2D& operator=(const ShortArrayBase& src) {
    ShortArray::operator=(src);
    return *this;
  }
  Vector2D& operator=(z_real_t v) {
    ShortArray::operator=(v);
    return *this;
  }
  
  // inner product operator
  z_real_t operator*(const Vector2D& other) const {
    return innerProd(*this,other);
  }
};

#ifdef MATLIB_USE_NAMESPACE
END_MATLIB_NAMESPACE
#endif

#endif
