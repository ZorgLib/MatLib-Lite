/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATH_LAPACK_H
#define ZORGLIB_MATH_LAPACK_H

// config
#include <matlib_macros.h>

#ifdef MATLIB_USE_VECLIB

#include <Accelerate/Accelerate.h>
#define LAPACK_INTEGER __CLPK_integer

#ifndef MATLIB_USE_SIMPLE_PRECISION
#define LAPACK_REAL __CLPK_doublereal
#else
#define LAPACK_REAL __CLPK_real
#endif

#else

#define LAPACK_INTEGER int
#ifndef MATLIB_USE_SIMPLE_PRECISION
#define LAPACK_REAL double
#else
#define LAPACK_REAL float
#endif

extern "C" {
  void FORTRAN(dgeev)(char*,char*,int*,double*,int*,double*,double*,double*,
                      int*,double*,int*,double*,int*,int*);
  void FORTRAN(sgeev)(char*,char*,int*,float*,int*,float*,float*,float*,
                      int*,float*,int*,float*,int*,int*);
  void FORTRAN(dgesv)(int*,int*,double*,int*,int*,double*,int*,int*);
  void FORTRAN(sgesv)(int*,int*,float*,int*,int*,float*,int*,int*);
  void FORTRAN(dgesvd)(char*,char*,int*,int*,double*,int*,double*,double*,int*,
                       double*,int*,double*,int*,int*);
  void FORTRAN(sgesvd)(char*,char*,int*,int*,float*,int*,float*,float*,int*,
                       float*,int*,float*,int*,int*);
  void FORTRAN(dgetrf)(int*,int*,double*,int*,int*,int*);
  void FORTRAN(sgetrf)(int*,int*,float*,int*,int*,int*);
  void FORTRAN(dgetri)(int*,double*,int*,int*,double*,int*,int*);
  void FORTRAN(sgetri)(int*,float*,int*,int*,float*,int*,int*);
  void FORTRAN(dspev)(char*,char*,int*,double*,double*,double*,int*,double*,int*);
  void FORTRAN(sspev)(char*,char*,int*,float*,float*,float*,int*,float*,int*);
  void FORTRAN(dsygv)(int*,char*,char*,int*,double*,int*,double*,int*,double*,
                      double*,int*,int*);
  void FORTRAN(ssygv)(int*,char*,char*,int*,float*,int*,float*,int*,float*,
                      float*,int*,int*);
  void FORTRAN(dsysv)(char*,int*,int*,double*,int*,int*,double*,int*,
                      double*,int*,int*);
  void FORTRAN(ssysv)(char*,int*,int*,float*,int*,int*,float*,int*,
                      float*,int*,int*);
}

#endif

#endif
