/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#include "TensorAlgebra.h"

// std C library
#include <cmath>

#ifdef MATLIB_USE_NAMESPACE
USING_MATLIB_NAMESPACE
#endif


/*
 * Methods for TensorAlgebra1D.
 */

// operations
void TensorAlgebra1D::RightCauchyGreen(const Tensor1D& F,SymTensor1D& C) {
  // C=F^t.F
  C[0] = F[0]*F[0];
  C[1] = F[1]*F[1];
  C[2] = F[2]*F[2];
}
void TensorAlgebra1D::RUDecomposition(const Tensor1D& F,Tensor1D& R,SymTensor1D& U) {
  // F=R.U
  R[0] = R[1] = R[2] = 1.e0;
  U[0] = F[0];
  U[1] = F[1];
  U[2] = F[2];
}
void TensorAlgebra1D::CauchyToPK1(const SymTensor1D& sig,const Tensor1D& F,Tensor1D& P) {
  // P = sig.(J F^-t)
  Tensor1D Fc = F.cofactor();
  P[0] = sig[0]*Fc[0];
  P[1] = sig[1]*Fc[1];
  P[2] = sig[2]*Fc[2];
}
void TensorAlgebra1D::PK1ToCauchy(const Tensor1D& P,const Tensor1D& F,SymTensor1D& sig) {
  // sig = (1/J) P.F^t
  z_real_t J = F.determinant();
  z_real_t Jinv = 1.0/J;
  sig[0] = Jinv*P[0]*F[0];
  sig[1] = Jinv*P[1]*F[1];
  sig[2] = Jinv*P[2]*F[2];
}
void TensorAlgebra1D::CauchyToPK2(const SymTensor1D& sig,const Tensor1D& F,SymTensor1D& S) {
  // S = J F^-1.sig.F^-t
  z_real_t J;
  Tensor1D Finv = F.inverse(J);
  S[0] = J*Finv[0]*sig[0]*Finv[0];
  S[1] = J*Finv[1]*sig[1]*Finv[1];
  S[2] = J*Finv[2]*sig[2]*Finv[2];
}
void TensorAlgebra1D::PK2ToCauchy(const SymTensor1D& S,const Tensor1D& F,SymTensor1D& sig) {
  // sig = (1/J) F.S.F^t
  z_real_t J = F.determinant();
  z_real_t Jinv = 1.0/J;
  sig[0] = Jinv*F[0]*S[0]*F[0];
  sig[1] = Jinv*F[1]*S[1]*F[1];
  sig[2] = Jinv*F[2]*S[2]*F[2];
}
void TensorAlgebra1D::PK2ToPK1(const SymTensor1D& S,const Tensor1D& F,Tensor1D& P) {
  // P=F.S
  P[0] = F[0]*S[0];
  P[1] = F[1]*S[1];
  P[2] = F[2]*S[2];
}
void TensorAlgebra1D::PK1ToPK2(const Tensor1D& P,const Tensor1D& F,SymTensor1D& S) {
  // S=F^-1.P
  z_real_t J;
  Tensor1D Finv = F.inverse(J);
  S[0] = Finv[0]*P[0];
  S[1] = Finv[1]*P[1];
  S[2] = Finv[2]*P[2];
}
void TensorAlgebra1D::MaterialToLagrangian(const SymTensor4_1D& M,const SymTensor1D& S,
                                           const Tensor1D& F,Tensor4_1D& T) {
  // T_iJkL=F_iI.M_IJKL.F_kK+S_JL 1_ik
  T[0][0] = F[0]*M[0][0]*F[0]+S[0];
  T[0][1] = F[0]*M[0][1]*F[1];
  T[0][2] = F[0]*M[0][2]*F[2];
  T[1][0] = F[1]*M[1][0]*F[0];
  T[1][1] = F[1]*M[1][1]*F[1]+S[1];
  T[1][2] = F[1]*M[1][2]*F[2];
  T[2][0] = F[2]*M[2][0]*F[0];
  T[2][1] = F[2]*M[2][1]*F[1];
  T[2][2] = F[2]*M[2][2]*F[2]+S[2];
}
void TensorAlgebra1D::SpatialToLagrangian(const SymTensor4_1D& M,const SymTensor1D& sig,
                                          const Tensor1D& F,Tensor4_1D& T) {
  // T_iJkL=J F^-1_Jj.(M_ijkl+sig_jl 1_ik).F^-1_Ll
  z_real_t J;
  Tensor1D Finv = F.inverse(J);
  T[0][0] = J*Finv[0]*(M[0][0]+sig[0])*Finv[0];
  T[0][1] = J*Finv[0]*M[0][1]*Finv[1];
  T[0][2] = J*Finv[0]*M[0][2]*Finv[2];
  T[1][0] = J*Finv[1]*M[1][0]*Finv[0];
  T[1][1] = J*Finv[1]*(M[1][1]+sig[1])*Finv[1];
  T[1][2] = J*Finv[1]*M[1][2]*Finv[2];
  T[2][0] = J*Finv[2]*M[2][0]*Finv[0];
  T[2][1] = J*Finv[2]*M[2][1]*Finv[1];
  T[2][2] = J*Finv[2]*(M[2][2]+sig[2])*Finv[2];
}
