/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2020, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATLIB_LITE_H
#define ZORGLIB_MATLIB_LITE_H

// local
#define WITH_MATLIB_H

// data structures module
#include "Chronometer.h"
#include "Cloneable.h"
#include "Copiable.h"
#include "Function.h"
#include "Function2.h"
#include "ConstantFunction.h"
#include "RampFunction.h"
#include "TabulatedFunction.h"
#include "TabulatedFunction2.h"
#include "Dictionary.h"
#include "Property.h"
#include "ShortArray.h"
#include "ShortMatrix.h"
#include "ShortSqrMatrix.h"
#include "Rotation.h"
#include "Rotation2D.h"
#include "Rotation3D.h"

// from math module
#include "MathUtils.h"

// main module: constitutive models
#include "MaterialProperties.h"
#include "MaterialState.h"
#include "ConstitutiveModel.h"
#include "MaterialModel.h"
#include "ModelDictionary.h"

#endif

