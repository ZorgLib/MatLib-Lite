/*
 *  $Id$
 *
 *  This file is part of ZorgLib, a computational simulation framework
 *  for thermomechanics of solids and structures (systems in general).
 *
 *  Copyright (c) 2001-2018, L. Stainier.
 *  See file LICENSE.txt for license information.
 *  Please report all bugs and problems to <Laurent.Stainier@ec-nantes.fr>.
 */
#ifndef ZORGLIB_MATLIB_MATH_H
#define ZORGLIB_MATLIB_MATH_H

#ifndef WITH_MATLIB_H
#include "MatLib.h"
#endif

// local
#define WITH_MATLIB_MATH_H

// math module
#include "Vector1D.h"
#include "Vector2D.h"
#include "Vector3D.h"
#include "SymTensor1D.h"
#include "SymTensor2D.h"
#include "SymTensor3D.h"
#include "Tensor1D.h"
#include "Tensor2D.h"
#include "Tensor3D.h"
#include "SymTensor4_1D.h"
#include "SymTensor4_2D.h"
#include "SymTensor4_3D.h"
#include "Tensor4_1D.h"
#include "Tensor4_2D.h"
#include "Tensor4_3D.h"
#include "TensorAlgebra.h"

#endif
