#!/usr/bin/env python3
#
# Python script to test consistency of constitutive models
#
import sys

import matlib as matl

# read input file name
if (len(sys.argv) > 1 and sys.argv[1] != ''):
	try:
		input = open(sys.argv[1],'r')
	except IOError:
		print('ERROR: could not open file', sys.argv[1])
		sys.exit(-1)
else:
	input = sys.stdin

# read space dimension
dim = int(input.readline())

# read material model
matl.ModelDictionary.list()
key = input.readline().strip(' \n')
try:
	model = matl.ModelDictionary.build(key,dim)
except matl.NoSuchModelException as e:
	print("ERROR: invalid model keyword (%s)" % e.mesg())
	sys.exit(-1)
if model.isVariational():
    stdModel = model.toStandardMaterial()

# read material properties
properties = matl.MaterialProperties()
key = input.readline().strip(' \n')
try:
	properties.readFrom(key)
except matl.SyntaxError as e:
	print('ERROR:', e.mesg())
	sys.exit(-1)
try:
	model.checkProperties(properties)
except matl.NoSuchPropertyException as e:
	print("ERROR: missing property (%s)" % e.mesg())
	sys.exit(-1)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()

nExtVar = model.nExtVar()
state0.grad.resize(nExtVar)
state0.flux.resize(nExtVar)
state1.grad.resize(nExtVar)
state1.flux.resize(nExtVar)

nIntVar = model.nIntVar()
state0.internal.resize(nIntVar)
state1.internal.resize(nIntVar)

model.initState(properties,state0)
print(flush=True)

# read time step size
words = input.readline().split(' ')
tstep = float(words[0])
nExtPar = int(words[1])
external = {}
for i in range(nExtPar):
	words = input.readline().split(' ')
	external[words[0].strip(' \n')] = float(words[1])

# read new gradient
words = input.readline().split(' ')
for i in range(nExtVar):
	state1.grad.set(i,float(words[i]))

# compute new state
M = matl.MatLibMatrix(nExtVar)
if model.isVariational():
	W = stdModel.incrementalPotential(properties,external,state0,state1,
	                                  tstep,M,True,True)
	print("\nIncremental energy = %11.5e" % W)
else:
	model.updateState(properties,external,state0,state1,tstep,M,True)

print('\nStress tensor =\n\t', state1.flux)

print('\nTangent matrix =\n',end='')
for i in range(nExtVar):
	print('\t',end='')
	for j in range(nExtVar):
		print('%12.5e' % M(i,j),end='')
	print()

if nIntVar > 0:
	print('\nInternal variables =\n\t', state1.internal)
else:
	print('No internal variables')

# compute stress and tangents by finite difference
pertu = 1.e-8
coef = 0.5/pertu
sigp = matl.MatLibArray(nExtVar)
sigm = matl.MatLibArray(nExtVar)
sigd = matl.MatLibArray(nExtVar)
Md = matl.MatLibMatrix(nExtVar)
print('\nComputing: ',end='')
for k in range(nExtVar):
	print(k,end='')
	
	val = state1.grad(k)
	state1.grad.set(k,val+pertu)
	if model.isVariational():
		Wp = stdModel.incrementalPotential(properties,external,state0,state1,
		                                   tstep,M,True,False)
	else:
		model.updateState(properties,external,state0,state1,tstep,M,False)
	sigp.copy(state1.flux)
	print('.',end='')
	
	state1.grad.set(k,val-pertu)
	if model.isVariational():
		Wm = stdModel.incrementalPotential(properties,external,state0,state1,
		                                   tstep,M,True,False)
	else:
		model.updateState(properties,external,state0,state1,tstep,M,False)
	sigm.copy(state1.flux)
	print('.',end='')
	
	if model.isVariational(): sigd.set(k,coef*(Wp-Wm))
	
	for l in range(nExtVar): Md.set(l,k,coef*(sigp(l)-sigm(l)))
	
	state1.grad.set(k,val)
print('\n')

if model.isVariational():
	print('\nStress tensor (numerical) =\n\t', sigd)

print('\nTangent matrix (numerical) =\n',end='')
for i in range(nExtVar):
	print('\t',end='')
	for j in range(nExtVar):
		print('%12.5e' % Md(i,j),end='')
	print()
