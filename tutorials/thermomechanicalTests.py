#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 17 20:18:34 2020

@author: stainier
"""

import math
import matlib as matl

import matplotlib.pyplot as plt
import numpy as np

def UniaxialStress(material,external,state0,state1,tstep):
    N = material.model().nExtVar()
    M = matl.MatLibMatrix(N)

    eps = matl.MatLibArray(state1.grad,N-1,1)
    tau = matl.MatLibArray(state1.flux,N-1,1)
    Mred = matl.MatLibMatrix(M,N-1,1)

    # solve transverse equilibrium
    ITMAX=25
    PRECISION=1.0e-16
    TOLERANCE=1.0e-9
    it = 0
    norm0 = 0.0
    while (it < ITMAX):
        material.updateState(external,state0,state1,tstep,M,True)

        # check norm
        norm = matl.normL2(tau)
        print('\tIter %d - Norm= %13.6e' % (it,norm))
        if (norm0 == 0.0): norm0=norm
        if (norm < TOLERANCE*norm0 or norm < PRECISION): break

        # compute correction
        dEps = matl.MatLibArray(N-1)
        try:
            Mred.solve(dEps,tau,True)
        except matl.SingularMatrixException:
            print("ERROR: singular matrix")
            return

        for i in range(N-1):
            val = eps(i)-dEps(i)
            eps.set(i,val)
            
        it = it+1
    return

def UniaxialStressWithExchange(material,external,state0,state1,tstep):
    N = material.model().nExtVar()
    M = matl.MatLibMatrix(N)

    eps = matl.MatLibArray(state1.grad,N-1,1)
    tau = matl.MatLibArray(state1.flux,N-1,1)
    Mred = matl.MatLibMatrix(M,N-1,1)

    # solve transverse equilibrium
    ITMAX=25
    PRECISION=1.0e-16
    TOLERANCE=1.0e-9
    it = 0
    norm0 = 0.0
    while (it < ITMAX):
        material.updateState(external,state0,state1,tstep,M,True)

        # add exchange term
        TExt = material.properties().getRealProperty("REFERENCE_TEMPERATURE")
        h = material.properties().getRealProperty("EXCHANGE_COEFFICIENT")
        val = tstep*h*state1.grad(N-1)/TExt
        tmp = state1.flux(N-1)
        state1.flux.set(N-1,tmp-val)
        tmp = M(N-1,N-1)
        M.set(N-1,N-1,tmp-tstep*h/TExt)

        # check norm
        norm = matl.normL2(tau)
        print('\tIter %d - Norm= %13.6e' % (it,norm))
        if (norm0 == 0.0): norm0=norm
        if (norm < TOLERANCE*norm0 or norm < PRECISION): break

        # compute correction
        dEps = matl.MatLibArray(N-1)
        try:
            Mred.solve(dEps,tau,True)
        except matl.SingularMatrixException:
            print("ERROR: singular matrix")
            return

        for i in range(N-1):
            val = eps(i)-dEps(i)
            eps.set(i,val)
            
        it = it+1
    return

# SIMPLE THERMO_ELASTIC TEST

# material model
model = matl.ModelDictionary.build("ISOTROPIC_THERMO_DEPENDENT_ELASTICITY")
properties = matl.MaterialProperties()
properties.readFrom("../test/materials/mild-steel.mat")
material = matl.MaterialModel(model,properties)
material.initialize()
print("\n",flush=True)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}
external["TEMPERATURE"] = 293.0

# result arrays
nPts1 = 10
temp1 = np.zeros(nPts1)
sigm1 = np.zeros(nPts1)

# load cycle
state1.grad.set(0,0.001)
for n in range(nPts1):
    external["TEMPERATURE"] += 10.0
    temp1[n] = external["TEMPERATURE"]
    print("Step #%d - Strain = %13.6e - Temperature=%4.1f"%(n+1,state1.grad(0),external["TEMPERATURE"]))
    UniaxialStress(material,external,state0,state1,1.0)
    print("\tStress = %13.6e"%state1.flux(0))
    sigm1[n] = state1.flux(0)
    state0.copy(state1)

# plot results
# plt.figure(1)
# plt.title("Temperature-dependent elasticity")
# plt.xlabel("Temperature (K)")
# plt.ylabel("Stress [MPa]")
# plt.plot(temp1,1.e-6*sigm1)
# plt.show()

# ADIABATIC TENSILE TEST (thermo-elasticity)

# material model
model = matl.ModelDictionary.build("ISOTROPIC_THERMO_ELASTICITY")
properties = matl.MaterialProperties()
properties.readFrom("../test/materials/mild-steel.mat")
material = matl.MaterialModel(model,properties)
material.initialize()
print("\n",flush=True)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}

# load cycle
nPts2 = 10
eps_t = np.zeros(nPts2+1)
sig_t = np.zeros(nPts2+1)
temp_t = np.zeros(nPts2+1)
for n in range(nPts2):
    eps = state0.grad(0)
    state1.grad.set(0,eps+0.0001)
    print("Step #%d - Strain = %13.6e"%(n+1,state1.grad(0)))
    UniaxialStress(material,external,state0,state1,1.0)
    print("\tStress = %13.6e - Temperature variation=%6.3f"%(state1.flux(0),state1.grad(6)))
    eps_t[n+1] = state1.grad(0)
    sig_t[n+1] = state1.flux(0)
    temp_t[n+1] = state1.grad(6)
    state0.copy(state1)

# plot results
# fig,ax1 = plt.subplots(num=2)
# fig.suptitle("Thermo-elasticity")
# ax1.set_xlabel("Strain")
# ax1.set_ylabel("Stress [MPa]",color='red')
# ax1.tick_params(axis ='y',labelcolor = 'red')
# ax1.plot(eps_t,1.e-6*sig_t,color='red')
# ax2 = ax1.twinx()
# ax2.set_ylabel("Temperature diff. [K]",color='blue')
# ax2.tick_params(axis ='y',labelcolor = 'blue')
# ax2.plot(eps_t,temp_t,color='blue')
# plt.show()

# ADIABATIC CYCLIC TEST (thermo-elasticity)

# material model
# model = matl.ModelDictionary.build("ISOTROPIC_THERMO_ELASTICITY")
# properties = matl.MaterialProperties()
# properties.readFrom("../test/materials/mild-steel.mat")
# material = matl.MaterialModel(model,properties)
# material.initialize()
print("\n",flush=True)

# material states
# state0 = matl.MaterialState()
# state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}

# load cycle
nPts3 = 100
dTime = 1.0
times = np.linspace(0.,nPts3*dTime,nPts3+1)
eps_t = np.zeros_like(times)
sig_t = np.zeros_like(times)
temp_t = np.zeros_like(times)
for n in range(nPts3):
    eps = 0.005*math.sin(times[n+1]*math.pi/20)
    state1.grad.set(0,eps)
    print("Step #%d - Strain = %13.6e"%(n+1,state1.grad(0)))
    UniaxialStress(material,external,state0,state1,dTime)
    print("\tStress = %13.6e - Temperature variation=%6.3f"%(state1.flux(0),state1.grad(6)))
    eps_t[n+1] = state1.grad(0)
    sig_t[n+1] = state1.flux(0)
    temp_t[n+1] = state1.grad(6)
    state0.copy(state1)

# plot results
# fig,ax1 = plt.subplots(num=3)
# fig.suptitle("Thermo-elasticity (cyclic)")
# ax1.set_xlabel("Time")
# ax1.set_ylabel("Stress [MPa]",color='red')
# ax1.tick_params(axis ='y',labelcolor = 'red')
# ax1.plot(times,1.e-6*sig_t,color='red')
# ax2 = ax1.twinx()
# ax2.set_ylabel("Temperature diff. [K]",color='blue')
# ax2.tick_params(axis ='y',labelcolor = 'blue')
# ax2.plot(times,temp_t,color='blue')
# plt.show()

# NON-ADIABATIC CYCLIC TEST (thermo-elasticity)

# material model
# model = matl.ModelDictionary.build("ISOTROPIC_THERMO_ELASTICITY")
# properties = matl.MaterialProperties()
# properties.readFrom("../test/materials/mild-steel.mat")
# material = matl.MaterialModel(model,properties)
# material.initialize()
material.properties().setProperty("EXCHANGE_COEFFICIENT",500000.)
print("\n",flush=True)

# material states
# state0 = matl.MaterialState()
# state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}

# load cycle
nPts4 = 100
dTime = 1.0
times = np.linspace(0.,nPts4*dTime,nPts4+1)
eps_t = np.zeros_like(times)
sig_t_x = np.zeros_like(times)
temp_t_x = np.zeros_like(times)
for n in range(nPts4):
    eps = 0.005*math.sin(times[n+1]*math.pi/20)
    state1.grad.set(0,eps)
    print("Step #%d - Strain = %13.6e"%(n+1,state1.grad(0)))
    UniaxialStressWithExchange(material,external,state0,state1,dTime)
    print("\tStress = %13.6e - Temperature variation=%6.3f"%(state1.flux(0),state1.grad(6)))
    eps_t[n+1] = state1.grad(0)
    sig_t_x[n+1] = state1.flux(0)
    temp_t_x[n+1] = state1.grad(6)
    state0.copy(state1)

# plot results
# fig,ax1 = plt.subplots(num=4)
# fig.suptitle("Thermo-elasticity (cyclic, w/ exchange)")
# ax1.set_xlabel("Time")
# ax1.set_ylabel("Stress [MPa]",color='red')
# ax1.tick_params(axis ='y',labelcolor = 'red')
# ax1.plot(times,1.e-6*sig_t_x,color='red')
# ax1.plot(times,1.e-6*sig_t,color='red',linestyle='dotted')
# ax2 = ax1.twinx()
# ax2.set_ylabel("Temperature diff. [K]",color='blue')
# ax2.tick_params(axis ='y',labelcolor = 'blue')
# ax2.plot(times,temp_t_x,color='blue')
# ax2.plot(times,temp_t,color='blue',linestyle='dotted')
# plt.show()

# ADIABATIC CYCLIC TEST (viscoelasticity)

# material model
model = matl.ModelDictionary.build("ISOTROPIC_KELVIN_THERMO_VISCO_ELASTICITY")
properties = matl.MaterialProperties()
properties.readFrom("../test/materials/viscoelastic.mat")
material = matl.MaterialModel(model,properties)
material.initialize()
print("\n",flush=True)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}

# load cycle
nPts5 = 500
dTime = 0.1
times = np.linspace(0.,nPts5*dTime,nPts5+1)
eps_t = np.zeros_like(times)
sig_t = np.zeros_like(times)
temp_t = np.zeros_like(times)
for n in range(nPts5):
    eps = 0.005*math.sin(times[n+1]*math.pi/2)
    state1.grad.set(0,eps)
    print("Step #%d - Strain = %13.6e"%(n+1,state1.grad(0)))
    UniaxialStress(material,external,state0,state1,dTime)
    print("\tStress = %13.6e - Temperature variation=%6.3f"%(state1.flux(0),state1.grad(6)))
    eps_t[n+1] = state1.grad(0)
    sig_t[n+1] = state1.flux(0)
    temp_t[n+1] =state1.grad(6)
    state0.copy(state1)

# plot results
# fig,(ax1,ax2) = plt.subplots(num=5,ncols=2)
# fig.suptitle("Thermo-visco-elasticity (cyclic)")
# fig.tight_layout(w_pad=2)
# ax1.set_xlabel("Time")
# ax1.set_ylabel("Stress [MPa]",color='red')
# ax1.tick_params(axis ='y',labelcolor = 'red')
# ax1.plot(times,1.e-6*sig_t,color='red')
# ax1b = ax1.twinx()
# ax1b.set_ylabel("Temperature diff. [K]",color='blue')
# ax1b.tick_params(axis ='y',labelcolor = 'blue')
# ax1b.plot(times,temp_t,color='blue')
# ax2.set_xlabel("Strain")
# ax2.set_ylabel("Stress [MPa]")
# ax2.plot(eps_t,1.e-6*sig_t)
# plt.show()

# NON-ADIABATIC CYCLIC TEST (viscoelasticity)

# material model
model = matl.ModelDictionary.build("ISOTROPIC_KELVIN_THERMO_VISCO_ELASTICITY")
properties = matl.MaterialProperties()
properties.readFrom("../test/materials/viscoelastic.mat")
properties.setProperty("EXCHANGE_COEFFICIENT",1.e5)
material = matl.MaterialModel(model,properties)
material.initialize()
print("\n",flush=True)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}

# load cycle
nPts6 = 500
dTime = 0.1
times = np.linspace(0.,nPts6*dTime,nPts6+1)
eps_t = np.zeros_like(times)
sig_t_x = np.zeros_like(times)
temp_t_x = np.zeros_like(times)
for n in range(nPts6):
    eps = 0.005*math.sin(times[n+1]*math.pi/2)
    state1.grad.set(0,eps)
    print("Step #%d - Strain = %13.6e"%(n+1,state1.grad(0)))
    UniaxialStressWithExchange(material,external,state0,state1,dTime)
    print("\tStress = %13.6e - Temperature variation=%6.3f"%(state1.flux(0),state1.grad(6)))
    eps_t[n+1] = state1.grad(0)
    sig_t_x[n+1] = state1.flux(0)
    temp_t_x[n+1] =state1.grad(6)
    state0.copy(state1)

# plot results
# fig,(ax1,ax2) = plt.subplots(num=6,ncols=2)
# fig.suptitle("Thermo-visco-elasticity (cyclic w/exchange)")
# fig.tight_layout(w_pad=2)
# ax1.set_xlabel("Time")
# ax1.set_ylabel("Stress [MPa]",color='red')
# ax1.tick_params(axis ='y',labelcolor = 'red')
# ax1.plot(times,1.e-6*sig_t_x,color='red')
# ax1.plot(times,1.e-6*sig_t,color='red',linestyle='dotted')
# ax1b = ax1.twinx()
# ax1b.set_ylabel("Temperature diff. [K]",color='blue')
# ax1b.tick_params(axis ='y',labelcolor = 'blue')
# ax1b.plot(times,temp_t_x,color='blue')
# ax1b.plot(times,temp_t,color='blue',linestyle='dotted')
# ax2.set_xlabel("Strain")
# ax2.set_ylabel("Stress [MPa]")
# ax2.plot(eps_t,1.e-6*sig_t_x,color='blue')
# ax2.plot(eps_t,1.e-6*sig_t,color='grey',linestyle='dotted')
# plt.show()

# ADIABATIC TENSILE TEST (plasticity)

# material model
model = matl.ModelDictionary.build("LINEAR_ISOTROPIC_J2_THERMO_PLASTICITY")
properties = matl.MaterialProperties()
properties.readFrom("../test/materials/mild-steel.mat")
properties.setProperty("INITIAL_YIELD_STRESS_DISSIPATED",200.0e6)
properties.setProperty("INITIAL_YIELD_STRESS_STORED",50.0e6)
properties.setProperty("HARDENING_MODULUS_DISSIPATED",1000.0e6)
properties.setProperty("HARDENING_MODULUS_STORED",0.0e6)
material = matl.MaterialModel(model,properties)
material.initialize()
print("\n",flush=True)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()
material.initState(state0)
material.initState(state1)

# external parameters
external = {}

# load cycle
nPts7 = 500
dTime = 1.0
times = np.linspace(0.,nPts7*dTime,nPts7+1)
eps_t = np.zeros_like(times)
sig_t = np.zeros_like(times)
temp_t = np.zeros_like(times)
epl_t = np.zeros_like(times)
epsp_t = np.zeros_like(times)
beta_t = np.zeros_like(times)
for n in range(nPts7):
    eps = 0.005*math.sin(times[n+1]*math.pi/25)
    state1.grad.set(0,eps)
    print("Step #%d - Strain = %13.6e"%(n+1,state1.grad(0)))
    UniaxialStress(material,external,state0,state1,dTime)
    print("\tStress = %13.6e - Temperature variation=%6.3f"%(state1.flux(0),state1.grad(6)))
    eps_t[n+1] = eps
    sig_t[n+1] = state1.flux(0)
    temp_t[n+1] = state1.grad(6)
    epl_t[n+1] = state1.internal(6)
    epsp_t[n+1] = state1.internal(0)
    beta_t[n+1] = state1.internal(11)
    state0.copy(state1)

# plot results
fig,(ax1,ax2) = plt.subplots(num=7,ncols=2)
fig.suptitle("Thermo-plasticity (cyclic)")
fig.tight_layout(w_pad=2)
ax1.set_xlabel("Time")
ax1.set_ylabel("Stress [MPa]",color='red')
ax1.tick_params(axis ='y',labelcolor = 'red')
ax1.plot(times,1.e-6*sig_t,color='red')
ax1b = ax1.twinx()
ax1b.set_ylabel("Temperature diff. [K]",color='blue')
ax1b.tick_params(axis ='y',labelcolor = 'blue')
ax1b.plot(times,temp_t,color='blue')
ax2.set_xlabel("Strain")
ax2.set_ylabel("Stress [MPa]")
ax2.plot(eps_t,1.e-6*sig_t)
plt.show()

plt.figure(8)
plt.xlabel("Time")
plt.ylabel("beta")
plt.plot(times,beta_t)
plt.show()