#!/usr/bin/env python3
#
# Python script to run virtual tensile tests
#
import sys

import matlib as matl

# define useful functions ---------------------------------------------------
def kirchhoff(tau,P,F,d): # compute Kirchhoff stress from PK1
    """Computes Kirchhoff stress from PK1"""
    if (d == 3):
        tau.set(0, P(0)*F(0)+P(1)*F(1)+P(2)*F(2) )
        tau.set(1, P(0)*F(3)+P(1)*F(4)+P(2)*F(5) )
        tau.set(2, P(0)*F(6)+P(1)*F(7)+P(2)*F(8) )
        tau.set(3, P(3)*F(0)+P(4)*F(1)+P(5)*F(2) )
        tau.set(4, P(3)*F(3)+P(4)*F(4)+P(5)*F(5) )
        tau.set(5, P(3)*F(6)+P(4)*F(7)+P(5)*F(8) )
        tau.set(6, P(6)*F(0)+P(7)*F(1)+P(8)*F(2) )
        tau.set(7, P(6)*F(3)+P(7)*F(4)+P(8)*F(5) )
        tau.set(8, P(6)*F(6)+P(7)*F(7)+P(8)*F(8) )
    elif (d == 2):
        tau.set(0, P(0)*F(0)+P(1)*F(1) )
        tau.set(1, P(0)*F(2)+P(1)*F(3) )
        tau.set(2, P(2)*F(0)+P(3)*F(1) )
        tau.set(3, P(2)*F(2)+P(3)*F(3) )
        tau.set(4, P(4)*F(4) )
    elif (d == 1):
        tau.set(0, P(0)*F(0) )
        tau.set(1, P(1)*F(1) )
        tau.set(2, P(2)*F(2) )
    return

def determinant(F,d): # compute determinant
    """Computes determinant of a full tensor"""
    if (d == 3):
        det = F(0)*(F(4)*F(8)-F(5)*F(7)) \
             -F(1)*(F(3)*F(8)-F(5)*F(6)) \
             +F(2)*(F(3)*F(7)-F(4)*F(6))
    elif (d == 2):
        det = (F(0)*F(3)-F(1)*F(2))*F(4)
    elif (d == 1):
        det = F(0)*F(1)*F(2)
    return det

def cauchy(sig,P,F,d): # compute Cauchy stress from PK1
    """Computes Cauchy stress from PK1"""
    tau = matl.MatLibArray(d*d+3-d)
    kirchhoff(tau,P,F,d)
    coef = 1./determinant(F,d)
    for i in range(d*d+3-d):
        sig.set(i, coef*tau(i) )
    return
# ---------------------------------------------------------------------------

# read input file name
if (len(sys.argv) > 1 and sys.argv[1] != ''):
    try:
        input = open(sys.argv[1],'r')
    except IOError:
        print('ERROR: could not open file', sys.argv[1])
        sys.exit(-1)
else:
    input = sys.stdin

# build output file name
if (len(sys.argv) > 1 and sys.argv[1] != ''):
    file = sys.argv[1].rsplit('.',1)[0] + '.plt'
    try:
        output = open(file,'w')
    except IOError:
        print('ERROR: could not open file', file)
        sys.exit(-1)
else:
    output = sys.stdout

# start
print("=== UNIAXIAL TENSION/COMPRESSION TEST ===")

# read space dimension
dim = int(input.readline())

# read material model
key = input.readline().strip(' \n')
try:
    model = matl.ModelDictionary.build(key,dim)
except matl.NoSuchModelException as e:
    print("ERROR: invalid model keyword (%s)" % e.mesg())
    sys.exit(-1)
if (model.isVariational()):
    stdModel = model.toStandardMaterial()

# read material properties
properties = matl.MaterialProperties()
key = input.readline().strip(' \n')
try:
    properties.readFrom(key)
except matl.SyntaxError as e:
    print('ERROR:', e.mesg())
    sys.exit(-1)
try:
    model.checkProperties(properties)
except matl.NoSuchPropertyException as e:
    print("ERROR: missing property (%s)" % e.mesg())
    sys.exit(-1)
print(flush=True)

# material states
state0 = matl.MaterialState()
state1 = matl.MaterialState()

nExtVar = model.nExtVar()
state0.grad.resize(nExtVar)
state0.flux.resize(nExtVar)
state1.grad.resize(nExtVar)
state1.flux.resize(nExtVar)

nIntVar = model.nIntVar()
state0.internal.resize(nIntVar)
state1.internal.resize(nIntVar)

model.initState(properties,state0)
model.initState(properties,state1)

sig = matl.MatLibArray(dim*dim+3-dim)
M = matl.MatLibMatrix(nExtVar)

# external parameters
nExtPar = int(input.readline())
external = {}
for i in range(nExtPar):
    words = input.readline().split()
    external[words[0].strip(' \n')] = float(words[1])

# read time-stepping information
nseg = int(input.readline())
tseg = [0.0]
tstp = []
eseg = []
for i in range(nseg):
    words = input.readline().split()
    tseg.append(float(words[0]))
    tstp.append(float(words[1]))
    eseg.append(float(words[2]))

# write initial state
nExtVarBundled = model.nExtVarBundled()
nIntVarBundled = model.nIntVarBundled()
output.write('%d %f %13.6e %13.6e' % (0,0.,state0.grad(0),state0.flux(0)))
if (model.typeExtVar(0) == matl.ConstitutiveModel.TYPE_TENSOR):
    cauchy(sig,state0.flux,state0.grad,dim)
    output.write(' %13.6e' % sig(0))
if (nExtVarBundled > 1 and model.typeExtVar(nExtVarBundled-1) == matl.ConstitutiveModel.TYPE_SCALAR):
    output.write(' %13.6e' % state0.grad(nExtVar-1))
if (model.getIntVar('EPLS') < nIntVarBundled):
    output.write(' %13.6e' % state0.internal(model.indexIntVar(model.getIntVar('EPLS'))))
if (model.getIntVar('DAMG') < nIntVarBundled):
    output.write(' %13.6e' % state0.internal(model.indexIntVar(model.getIntVar('DAMG'))))
if (model.getIntVar('TEMP') < nIntVarBundled):
    output.write(' %13.6e' % state0.internal(model.indexIntVar(model.getIntVar('TEMP'))))
output.write('\n')

# time loop
istep = 0
for n in range(nseg):
    tstep = tstp[n]
    time1 = tseg[n]
    while (time1 < tseg[n+1]):
        
        # compute new time
        time0 = time1
        time1 = time0+tstep
        if (time1 > tseg[n+1]):
            time1 = tseg[n+1]
            tstep = time1-time0
        if (tstep < 1.e-8*(tseg[n+1]-tseg[n])): break
        istep = istep+1
        print('Step %d - Time=%f - Step=%f' % (istep,time1,tstep))

        # compute new axial strain
        state0.copy(state1)
        eps11 = state1.grad(0)+eseg[n]*tstep
        state1.grad.set(0,eps11)

        eps = matl.MatLibArray(state1.grad,nExtVar-1,1)
        tau = matl.MatLibArray(state1.flux,nExtVar-1,1)
        Mred = matl.MatLibMatrix(M,nExtVar-1,1)

        # solve transverse equilibrium
        ITMAX=25
        PRECISION=1.e-16
        TOLERANCE=1.e-8
        it = 0
        norm0 = 0.0
        while (it < ITMAX):
            if model.isVariational():
                W = stdModel.incrementalPotential(properties,external,
                                                  state0,state1,tstep,
                                                  M,True,True)
            else:
                model.updateState(properties,external,state0,state1,
                                  tstep,M,True)

            # check norm
            norm = matl.normL2(tau)
            print('\tIter %d - Norm= %13.6e' % (it,norm))
            if (norm0 == 0.0): norm0=norm
            if (norm < TOLERANCE*norm0 or norm < PRECISION): break

            # compute correction
            dEps = matl.MatLibArray(nExtVar-1)
            try:
                Mred.solve(dEps,tau,model.isVariational())
            except matl.SingularMatrixException:
                print("ERROR: singular matrix")
                sys.exit(-1)

            for i in range(nExtVar-1):
                val = eps(i)-dEps(i)
                eps.set(i,val)
            
            it = it+1

        # write solution
        output.write('%d %f %13.6e %13.6e' % (istep,time1,eps11,state1.flux(0)))
        if (model.typeExtVar(0) == matl.ConstitutiveModel.TYPE_TENSOR):
            cauchy(sig,state1.flux,state1.grad,dim)
            output.write(' %13.6e' % sig(0))
        if (nExtVarBundled > 1 and model.typeExtVar(nExtVarBundled-1) == matl.ConstitutiveModel.TYPE_SCALAR):
            output.write(' %13.6e' % state1.grad(nExtVar-1))
        if (model.getIntVar('EPLS') < nIntVarBundled):
            output.write(' %13.6e' % state1.internal(model.indexIntVar(model.getIntVar('EPLS'))))
        if (model.getIntVar('DAMG') < nIntVarBundled):
            output.write(' %13.6e' % state1.internal(model.indexIntVar(model.getIntVar('DAMG'))))
        if (model.getIntVar('TEMP') < nIntVarBundled):
            output.write(' %13.6e' % state1.internal(model.indexIntVar(model.getIntVar('TEMP'))))
        output.write('\n')

print(flush=True)
output.flush()